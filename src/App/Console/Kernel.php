<?php

namespace Olmo\Core\App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        // \Olmo\Core\Generator\Commands\ContainerApiGenerator::class,
        \Olmo\Core\Generator\Commands\SeedFormFields::class,
        \Olmo\Core\Generator\Commands\FormGenerator::class,
        \Olmo\Core\Generator\Commands\ContainerApiGenerator::class,
        \Olmo\Core\Generator\Commands\ModelGenerator::class,
        \Olmo\Core\Generator\Commands\SeederGenerator::class,
        \Olmo\Core\Generator\Commands\RouteGenerator::class,
        \Olmo\Core\Generator\Commands\MigrationGenerator::class,
        \Olmo\Core\Generator\Commands\ControllerGenerator::class,
        \Olmo\Core\Generator\Commands\RepositoryGenerator::class,
        \Olmo\Core\Generator\Commands\OlmoManufactory::class,
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
