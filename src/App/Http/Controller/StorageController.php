<?php

namespace Olmo\Core\App\Http\Controller;

use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\File as HttpFile;

use Illuminate\Support\Facades\Cache;

use Intervention\Image\Facades\Image;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersImage;
use Olmo\Core\App\Helpers\HelpersVideo;
use Olmo\Core\App\Helpers\HelpersGif;

use Olmo\Core\App\Helpers\HelpersFilemanager;

trait StorageController
{

    public static function getFolderContent($request)
    {
        $result = [];
        $folderPath = $request->folder;

        $path = storage_path('app/public/media');
        $cacheKey = md5($path.$folderPath);

        $files = preg_grep('/^([^.])/', scandir($path.$folderPath));
        $i = 0;

        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }        

        foreach ($files as $filename) {
            $res = self::scanFolderContent($filename, $path, $i);
            $result[]  = $res['file'];
            $i += 1;
        }

        Cache::tags(['filemanager'])->put($cacheKey, $result, now()->addMinutes(10));

        return $result;
    }

    public static function scanFolderContent($filename, $path, $i)
    {
        $res = [];
        $info = pathinfo($filename);

        if (array_key_exists('extension', $info)) {
            $res['id']           = uniqid();
            $meta                = self::getMeta('file', $filename, $path, $info);
            $res['name']         = $meta['name'];
            $res['physicname']   = $filename;
            $res['type']         = 'file';
            $res['extension']    = $info['extension'];
            $res['description']  = $meta;
        } else {
            $res['id']           = uniqid();
            $meta                = self::getMeta('folder', $filename, $path, $info);
            $res['name']         = $filename;
            $res['physicname']   = '';
            $res['type']         = 'folder';
            $res['extension']    = '';
            $other               = $path . '/' . $info['basename'];
            $res['description']  = $meta;
            $i += 1;
        }

        return ['file' => $res, 'index' => $i];
    }

    public static function storageClearCache()
    {
        Cache::tags(['filemanager'])->flush();
    }

    public static function storageClearCacheFolder($folderPath)
    {
        $path = storage_path('app/public/media');
        $cacheKey = md5($path.$folderPath);
        return Cache::forget($cacheKey);
    }

    public static function clearCacheFilemanagerMethod()
    {
        /**
         * Empty and generate cache
         */
        return self::storageClearCache();
        // self::serializeAllFile();
    }

    public static function serializeAllFile()
    {
        $res = [];
        $path = storage_path('app/public/media');
        $cacheKey = md5($path);

        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }

        $res =  self::scanAllDir($path, 0);

        Cache::tags(['filemanager'])->put($cacheKey, $res, now()->addMinutes(10));

        return $res;
    }

    public static function serializeAllFolders()
    {
        $path = storage_path('app/public/media');
        $cacheKey = md5('folders');

        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }

        $res =  self::scanFolder($path, 0); 

        Cache::tags(['filemanager'])->put($cacheKey, $res, now()->addMinutes(10));

        return $res;
    }

    public static function scanFolder($path, $i)
    {
        $result = [];

        $files  = preg_grep('/^([^.])/', scandir($path));

        foreach ($files as $filename) {
            $res = self::getFolderInfo($filename, $path, $i);
            if(isset($res['file'])){
                $result[] = $res['file'];
            }
            $i += 1;
        }

        return $result;
    }     

    public static function getFolderInfo($filename, $path, $i)
    {
        $res = [];
        $info = pathinfo($filename);

        if (!array_key_exists('extension', $info)) {
            $res['id'] = uniqid();
            $res['name'] = $filename;
            $res['physicname'] = '';
            $res['type'] = 'folder';
            $res['extension'] = '';
            $other = $path . '/' . $info['basename'];
            $i += 1;
            $res['other'] = self::scanFolder($other, $i);   

            return ['file' => $res, 'index' => $i];
        }

    }    

    public static function scanAllDir($path, $i)
    {
        $result = [];

        $files  = preg_grep('/^([^.])/', scandir($path));

        foreach ($files as $filename) {
            $res = self::getFileInfo($filename, $path, $i);
            $result[]  = $res['file'];
            $i += 1;
        }

        return $result;
    }   

    public static function getFileInfo($filename, $path, $i)
    {
        $res = [];
        $info = pathinfo($filename);

        if (array_key_exists('extension', $info)) {
            $res['id']           = uniqid();
            $meta                = self::getMeta('file', $filename, $path, $info);
            $res['name']         = $meta['name'];
            $res['physicname']   = $filename;
            $res['type']         = 'file';
            $res['extension']    = $info['extension'];
            $res['description']  = $meta;
        } else {
            $res['id']           = uniqid();
            $meta                = self::getMeta('folder', $filename, $path, $info);
            $res['name']         = $filename;
            $res['physicname']   = '';
            $res['type']         = 'folder';
            $res['extension']    = '';
            $other               = $path . '/' . $info['basename'];
            $res['description']  = $meta;
            $i += 1;
            $res['other']        = self::scanAllDir($other, $i);
        }

        return ['file' => $res, 'index' => $i];
    }

    public static function getMeta($type, $filename, $path, $info)
    {
        $res = [];

        if ($type == 'folder') {
            $res['caption']  = '';
            $res['alt']      = '';
            $res['name']     = $filename;
            $res['idfile']   = self::generatePath($path . '/' . $info['basename']);;
        } else {

            $cacheKey = md5($filename);

            if(Cache::has($cacheKey)){
                $file = Cache::get($cacheKey);
            } else {
                $file = Db::table('olmo_storage')->where('filename', 'like', '%' . $filename . '%')->first();
                Cache::tags(['filemanager'])->put($cacheKey, $file, now()->addMinutes(10));            
            }

            $truename        = pathinfo(@$file->truename);
            $res['name']     = $truename['filename'];
            $res['idfile']   = @$file->id;
            $res['path']     = @$file->truename;
            $res['caption']  = (@$file->caption != '')  ?   @$file->caption : '';
            $res['alt']      = (@$file->alt     != '')  ?   @$file->alt : '';
            $res['public']   = (@$file->public  != '')  ?   @$file->public : '';
        }

        return $res;
    }

    public static function generatePath($path)
    {
        $string = substr($path, 0, strpos($path, 'media/'));
        $replace = str_replace($string, '', $path);
        $finalpath = str_replace('media/', '', $replace);
        return $finalpath;
    }

    public function fileEditMethod(Request $request)
    {
        $code = 200;
        $idfile = $request->idfile;
        $new_name = $request->filename;
        $folderPath = $request->folder;
        $resUpdate = false;
        $checkTrueName = 0;
        $assets_url = 'public/assets/';

        $file = Db::table('olmo_storage')->where('id', $idfile)->first();
        $old_path = $file->truename;
        $pathinfo = pathinfo($old_path);
        $current_filename = $pathinfo['filename'];

        $assets_file = Db::table('olmo_storage_versions')->where('id_original', $idfile)->get();

        $data = [
            'alt' => $request->alt,
            'caption' => $request->caption,
            'public' => $request->public
        ];

        if ($new_name != '') {
            $new_filename = HelpersData::normalizestring($new_name, false);
            $truename = str_replace($current_filename, $new_filename, $old_path);
            $data['truename'] = $truename;
            $checkTrueName = DB::table('olmo_storage')->where('truename', $truename)->where('id', '!=', $idfile)->get()->count();
        }
        $notModifiedFiles = [];

        if ($checkTrueName > 0) {
            $code = 403;
        } else {

            $resUpdate = Db::table('olmo_storage')->where('id', $idfile)->update($data);
            foreach ($assets_file as $asset) {

                $new_asset_name = str_replace($current_filename, $new_filename, $asset->dirname);
                $new_asset_name_webp = str_replace($current_filename, $new_filename, $asset->compression_path);

                if ($assets_url . $asset->dirname != $assets_url . $new_asset_name) {
                    Storage::move($assets_url . $asset->dirname, $assets_url . $new_asset_name);
                    Storage::move($assets_url . $asset->compression_path, $assets_url . $new_asset_name_webp);
                    Db::table('olmo_storage_versions')->where('id', $asset->id)->update(['dirname' => $new_asset_name, 'compression_path' => $new_asset_name_webp]);
                }
            }
        }

        /**
         * Empty and generate cache
         */
        $cacheKey = md5($file->filename);
        $cache = Cache::forget($cacheKey);

        self::storageClearCacheFolder($folderPath);

        $msg = [
            'update'   => $resUpdate,
            'infofile' => $pathinfo,
            'ObjError' => $notModifiedFiles,
            'truename' => $truename,
            'cache' => $cache,
        ];

        return response($msg, $code);
    }

    public static function folderCreateMethod($request)
    {

        $folder = @$request->folderName;
        $bread = $request->folder;

        $path = 'public/media/' . $folder;

        $response = Storage::disk('local')->makeDirectory($path, 0755, true, true);

        /**
         * Empty cache folder
         */
        self::storageClearCacheFolder($bread);

        if ($response) {
            $response = true;
        } else {
            $response = false;
        }

        $arr['data'] = $path;
        $arr['isSuccess'] = $response;
        $arr['errorMessage'] = '';

        return $arr;
    }

    public static function moveFolderMethod($request)
    {
        $folderbread = $request->input('folder');
        $sourceFolder = $request->input('folderName');
        $destinationFolder = $request->input('folderPath');
        $destinationFolder = ($destinationFolder == '') ? '' : $destinationFolder . '/';
        $exploded_path = explode('/', $sourceFolder);
        $folder_name = ($exploded_path[count($exploded_path) - 1] == '') ? '' : $exploded_path[count($exploded_path) - 1] . '/';

        $files = Db::table('olmo_storage')->where('truename', 'LIKE', '%' . $sourceFolder . '/' . '%')->get();

        foreach ($files as $file) {
            $cacheKey = md5($file->filename);
            Cache::forget($cacheKey);            

            $folderStructure = $destinationFolder . $folder_name . pathinfo($file->truename)['basename'];
            Db::table('olmo_storage')->where('id', $file->id)->update(['truename' => $folderStructure]);

            $filesversion = Db::table('olmo_storage_versions')->where('id_original', $file->id)->get();

            foreach ($filesversion as $fileversion) {
                $folderStructureVersion = $destinationFolder . $folder_name . pathinfo($fileversion->dirname)['basename'];
                $folderStructureVersionCompression = $destinationFolder . $folder_name . pathinfo($fileversion->compression_path)['basename'];

                Db::table('olmo_storage_versions')
                    ->where('id', $fileversion->id)
                    ->update(['dirname' => $folderStructureVersion, 'compression_path' => $folderStructureVersionCompression]);
            }
        }

        $folder = $folderbread == "" ? '/' . $sourceFolder . '/' : $folderbread . '/' . $sourceFolder . '/';
        $dest = $folder_name != "" ? $destinationFolder . $folder_name : $destinationFolder;

        $media = Storage::disk('local')->move('public/media' . $folder, 'public/media/' . $dest);
        $assets = Storage::move('public/assets' . $folder, 'public/assets/' . $dest);

        /**
         * Empty and generate cache
         */           
        self::storageClearCache();

        return [
            'files' => [$files],
            'move' => [
                'move-media' => $media,
                'move-assets' => $assets
            ],
            'pathmedia' => [
                'origin' => 'public/media' . $folder,
                'destination' => 'public/media/' . $destinationFolder . $folder_name,
            ],
            'pathassets' => [
                'origin' => 'public/assets' . $folder,
                'destination' => 'public/assets/' . $destinationFolder . $folder_name,
            ],
        ];
    }

    public function folderEdit(Request $request)
    {
        $sc               = 200;
        $folderPath       = $request->folderPath;
        $folderName       = $request->folderName;
        $paths            = explode('/', $folderPath);
        $lastDir          = $paths[count($paths) - 1];
        $newpath = '';
        for ($i = 0; $i < count($paths) - 1; $i++) {
            $newpath .= $paths[$i] . '/';
        }
        $newpath .= $folderName;
        $folderPathFull = storage_path('app/public/media/' . $folderPath);
        $newpathFull = storage_path('app/public/media/' . $newpath);

        /**
         * Empty and generate cache
         */
 

        if (file_exists($folderPathFull)) {
            rename($folderPathFull, $newpathFull);
            $files = Db::table('olmo_storage')->get();
            foreach ($files as $file) {
                $truename    = $file->truename;
                $idfile      = $file->id;
                $newtruename = HelpersData::str_replace_first($folderPath, $newpath, $truename);
                Db::table('olmo_storage')->where('id', $idfile)->update(['truename' => $newtruename]);
            }
            return response("Nome cartella cambiato", 200);
        } else {
            return response("Il percorso " . storage_path('app/public/media/' . $folderPath) . " non esiste", 403);
        }
    }

    public function moveFilesMethod(Request $request)
    {
        $folderPath = $request->input('folder');
        $movefiles = $request->movefiles;
        $compressed_version = '';
        $compressed_name = '';
        $compressed_name_new_path = '';
        $versions = [];
        $tmp = [];
        $destinationfolder = ($request->destinationfolder != null) ? $request->destinationfolder . '/' : '';
        $fileMoved = [];

        $pathStorage = storage_path('app/public/media');

        $arrayPush = [];
        $arrayPushUpdate = [];

        foreach ($movefiles as $file) {
            $query = Db::table('olmo_storage')->where('id', $file)->first();
            $originalname = $query->truename; //fisico
            $physicalName  = $query->filename; //encoded

            $path = pathinfo($originalname);
            $newpath = $destinationfolder . $path['basename'];
            $physicnewpath = $destinationfolder . $physicalName;
            $current_physic_path = $path['dirname'] . '/' . $physicalName;

            $physicnewpathFull = 'public/media/' . $physicnewpath;
            $current_physic_path_full = 'public/media/' . $current_physic_path;

            if (Storage::disk('local')->move($current_physic_path_full, $physicnewpathFull)) {
                //if(true){
                Db::table('olmo_storage')->where('id', $file)->update(['truename' => $newpath]);

                $fileMoved[] = $file;
                $message = "File moved. Id: ";

                $versions = Db::table('olmo_storage_versions')->where('id_original', $file)->get();

                foreach ($versions as $version) {

                    $asset_name = $version->dirname;
                    $asset_name_new_path = $destinationfolder . pathinfo($version->dirname)['basename'];
                    $compressed_version = self::getCompressedVersion($version);
                    $compressed_name = $compressed_version;
                    $compressed_name_new_path =  $destinationfolder  . pathinfo($compressed_version)['basename'];

                    Storage::move('public/assets/' . $asset_name, 'public/assets/' . $asset_name_new_path);
                    Storage::move('public/assets/' . $compressed_name, 'public/assets/' . $compressed_name_new_path);

                    DB::table('olmo_storage_versions')
                        ->where('id', $version->id)
                        ->update([
                            'dirname' => $asset_name_new_path,
                            'compression_path' => $compressed_name_new_path
                        ]);
                }
            } else {
                $message = "The file is not in the file system";
            }
        }

        $message = $message . implode(',', $fileMoved);

        /**
         * Empty and generate cache
         */
        //TODO: this could be improved
        self::storageClearCache();

        return response($message, 200);
    }

    public static function getCompressedVersion($version)
    {
        $compressed = [
            'mp4' => 'webm', 'gif' => 'gif',
            'png' => 'webp', 'jpg' => 'webp', 'jpeg' => 'webp'
        ];

        $pathinfo = pathinfo($version->dirname);
        $extension = $pathinfo['extension'];

        return str_replace($extension, $compressed[$extension], $version->dirname) ?? $version->dirname;
    }

    /**
     * Working on the assets folder
     * the full list of folder change name at the moment, need to fix
     */
    public function folderEditMethod(Request $request)
    {
        $folderName = $request->folderName; // nuiovo nome cartella
        $folderPath = $request->folderPath; // path attuale da media/
        $folderPathCache = $request->folder;

        $paths = explode('/', $folderPath);
        $newpath = '';
        for ($i = 0; $i < count($paths) - 1; $i++) {
            $newpath .= $paths[$i] . '/';
        }
        $newpath .= $folderName;

        $folderPathFull = storage_path('app/public/media/' . $folderPath);
        $newpathFull = storage_path('app/public/media/' . $newpath);
        
        /**
         * Empty and generate cache
         */         
        self::storageClearCacheFolder($folderPathCache);

        if (file_exists($folderPathFull)) {

            rename($folderPathFull, $newpathFull); // convertire in laravel?

            $files = Db::table('olmo_storage')->where('truename', 'LIKE', '%' . $folderPath . '/' . '%')->get();

            foreach ($files as $file) {

                $truename = $file->truename;
                $idfile = $file->id;
                $newtruename = HelpersData::str_replace_first($folderPath, $newpath, $truename);

                Db::table('olmo_storage')->where('id', $idfile)->update(['truename' => $newtruename]);
                $filesversion = Db::table('olmo_storage_versions')->where('id_original', $idfile)->get();


                foreach ($filesversion as $fileversion) {

                    $versiondirname = $fileversion->dirname;
                    $versioncompressionpath = $fileversion->compression_path;

                    $newdirname = HelpersData::str_replace_first($folderPath, $newpath, $versiondirname);
                    $newcompressionpath = HelpersData::str_replace_first($folderPath, $newpath, $versioncompressionpath);

                    Db::table('olmo_storage_versions')
                        ->where('id', $fileversion->id)
                        ->update(['dirname' => $newdirname, 'compression_path' => $newcompressionpath]);
                }
            }

            Storage::move('public/assets/' . $folderPath, 'public/assets/' . $newpath);
            // valutabile l'inserimento della cancellazione della folder, ma in base all numero di file presenti

            return response("Vecchio percorso: $folderPath \nNuovo Percorso: $newpath", 200);
        } else {
            return response("Il percorso " . storage_path('app/public/media/' . $folderPath) . " non esiste", 403);
        }
    }

    public function deleteFileMethod(Request $request)
    {
        $ids = $request->ids;
        $folder = $request->folder;
        $result = [];
        foreach ($ids as $id) {
            $query = Db::table('olmo_storage')->where('id', $id)->first();

            if ($query) { // false if ids contains folders
                $pathname = $query->truename;
                $physicName = $query->filename;
                $filename = explode("/", $pathname);
                $path = str_replace($filename[count($filename) - 1], $physicName, $pathname);

                $physicFilePath = storage_path('app/public/media/' . $path);

                if (file_exists($physicFilePath)) {
                    if (realpath($physicFilePath)) {
                        if (is_writable($physicFilePath)) {
                            unlink($physicFilePath);
                            Db::table('olmo_storage')->where('id', $id)->delete();
                            $result = self::deleteAllVersionByOriginalId($id);
                        } else {
                            return response('error here' . $id, 400);
                        }
                    } else {
                        return response('error here' . $id, 400);
                    }
                    $code = 200;
                    $message = ["File deleted", $result];
                } else {
                    return response('error here' . $id, 400);
                }
            } else {
                $media_path = storage_path('app/public/media/' . $id);
                $media = self::deleteFoldersAndFile($media_path, $id);
                $code = 200;
                $message = $media;
            }
        }

        /**
         * Empty cache folder
         */
        self::storageClearCacheFolder($folder);    

        return response($message, $code);
    }

    public static function deleteSingleVersion($version)
    {
        $compressed = [
            'mp4' => 'webm',
            'gif' => 'gif',
            'png' => 'webp',
            'jpg' => 'webp',
            'jpeg' => 'webp'
        ];
        $pathinfo = pathinfo($version->dirname);
        $extension = $pathinfo['extension'];
        $path = 'public/assets/' . $version->dirname;
        $path_compressed = str_replace($extension, $compressed[$extension], $path);
        if (Storage::exists($path) && Storage::exists($path_compressed)) {
            $deleteFileOriginalExtension = Storage::delete($path);
            $deleteFileWebpExtension = Storage::delete($path_compressed);

            if ($deleteFileOriginalExtension && $deleteFileWebpExtension) {
                DB::table('olmo_storage_versions')->where('id', $version->id)->delete();
            }
        }  

        return [$path, $path_compressed];
    }

    public static function deleteAllVersionByOriginalId($id)
    {
        $versions = DB::table('olmo_storage_versions')->where('id_original', $id)->get();
        $obj = [];
        if (count($versions) > 0) {
            foreach ($versions as $version) {
                $obj[] = self::deleteSingleVersion($version);
            }
        }

        return $obj;
    }

    /**
     * Recursive function
     * @param dir - the relative path in storage of the directory
     * @param directoryName - the directory name must be deleted
     */
    public static function deleteFoldersAndFile($dir, $directoryName)
    {
        $result = array();
        $it = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);
        foreach ($it as $key => $child) { // $key contiene l'intero path, child solo il nome del file/directory
            if ($child->isDot()) {
                continue;
            }
            $name = $child->getBasename();

            if ($child->isDir()) {

                $subdirectory = $dir . '/' . $name;
                $result[$name] = self::deleteFoldersAndFile($subdirectory, $name);
            } else {

                $file = Db::table('olmo_storage')->where('filename', $name)->first();
                Db::table('olmo_storage')->where('filename', $name)->delete();
                Db::table('olmo_storage_versions')->where('id_original', $file->id)->delete();

                $result[] =  'Folder deleted';
            }
        }

        File::deleteDirectory($dir);
        Storage::deleteDirectory('public/assets/' . $directoryName);         

        return $result;
    }


    public static function displayMedia($image, $dirname, $truename, $media = 'media', $fileAssets = null)
    {
        $imageFilename = $media == 'assets' ? $fileAssets : $image->filename;

        if ($dirname == '') {
            $finalpath = $imageFilename;
        } else {
            $finalpath = $dirname . '/' . $imageFilename;
        }
        $type = pathinfo($truename, PATHINFO_EXTENSION);
        $data = storage_path('app/public/' . $media . '/' . $finalpath);
        $content = file_get_contents($data);
        $expires = 14 * 60 * 60 * 24;
        if ($type == 'pdf') {
            $explodefile = explode('/', $image->truename);
            $filename = $explodefile[count($explodefile) - 1];
            return response()->download($data, $filename);
            // alternativa per visualizzazione inline:
            // header("Content-type:application/pdf");
            // header("Content-Disposition:inline;filename=".$image->truename);
            // readfile($data);
        } else {
            if ($type == 'mp4') {
                header("Content-Type: video/" . $type);
                header("Accept-Ranges bytes");
            } else {
                header("Content-Type: image/" . $type);
            }
        }
        header("Content-Length: " . strlen($content));
        header("Cache-Control: public", true);
        header("Pragma: public", true);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
        echo $content;
        exit();
    }

    public static function getFileManagerMethod($request)
    {
        $truename = $request->any;
        $pathinfo = pathinfo($truename);
        $dirname = $pathinfo['dirname'];
        $folder = 'filemanager';
        $image = Db::table('olmo_storage')->where('truename', $truename)->where('model', $folder)->first();

        if ($image) {

            if (strpos($image->truename, 'olmoemail/template/') || strpos($image->truename, 'olmoemail/csv/')) {
                return redirect(404);
            }

            $token = $request->query('olmotoken');
            $user = $request->olmouser;
            if ($token) {

                $query = Db::table('olmo_tokens')->where('modelid', $image->truename)->where('token', $token)->first();

                if (!$query) {
                    return redirect(404);
                }

                Db::table('olmo_tokens')->where('modelid', $image->truename)->where('token', $token)->update(['token' => '', 'updated_at' => date('Y-m-d H:i:s')]);

                return self::displayMedia($image, $dirname, $truename);
            } else if ($user) {

                $query = Db::table('olmo_user')->where('token_hidden_general', $user)->first();

                if (!$query) {
                    return redirect(404);
                }

                return self::displayMedia($image, $dirname, $truename);
            } else {
                if ($image->public == 'false') {
                    return redirect(404);
                }

                $url_components = parse_url(url()->full());
                if (isset($url_components['query'])) {
                    parse_str($url_components['query'], $params);
                    $checkMediaGenerateImage = self::checkMediaGenerateImage($params, $dirname, $truename, $pathinfo);
                    if ($checkMediaGenerateImage) {
                        return $checkMediaGenerateImage;
                    }
                    return self::displayMedia($image, $dirname, $truename);
                }

                return self::displayMedia($image, $dirname, $truename);
            }
        }

        return redirect(404);
    }

    public static function checkMediaGenerateImage($params, $dirname, $truename, $pathinfo)
    {

        $checkFile = HelpersImage::checkIfAssetExist($params, $dirname, $pathinfo);

        /**
         * If $checkFile['fileNameWithParams'] == false means there has been a front request in the params
         * so skeep it immediatly
         */
        if ($checkFile['fileexist']) {
            return self::displayMedia(null, $dirname, $truename, 'assets', $checkFile['fileNameWithParams']);
        } else if (!$checkFile['fileNameWithParams']) {
            return false;
        }

        $filename = $pathinfo['filename'] . '.' . $pathinfo['extension'];
        $generateImage = HelpersImage::generateImage($dirname, $filename, $params, $pathinfo);

        if ($generateImage) {
            return self::displayMedia(null, $dirname, $truename, 'assets', $checkFile['fileNameWithParams']);
        }

        return false;
    }

    public static function generateFile($params, $truename, $database = null, $id_original_image = null, $originalsize = 'false')
    {
        $pathinfo = pathinfo($truename);
        $checkFile = HelpersImage::checkIfAssetExist($params, $pathinfo);

        $generateImage = false;
        if (!$checkFile['fileexist']) {
            $generateImage = HelpersImage::generateImage($truename, $params, $database, $id_original_image, $originalsize);
        }
        return $generateImage;
    }

    // TODO
    public static function generateFileMassive($request)
    {
        $compression = $request->compression;
        $fit = $request->fit;

        $allfiles = Db::table('olmo_storage')->get();
        $counter = 0;

        foreach ($allfiles as $file) {
            $type = $file->type;
            if ($type == "jpg" or $type == "jpeg" or $type == "png") {
                $truename = $file->truename;
                $pathinfo = pathinfo($truename);
                $img = Image::make(env('APP_URL') . '/storage/media/' . $truename);
                $params = [
                    "width" => $img->width(),
                    "height" => $img->height(),
                    "fit" => 'clip',
                    "compression" => '70'
                ];
                $database = true;
                $id_original_image = $file->id;
                $originalsize = 'true';

                $fileNameWithParams = $pathinfo['filename'] . '-' . HelpersImage::generateParams($params) . '.' . $pathinfo['extension'];
                $pathFile = 'public/assets/' . $pathinfo['dirname'] . '/' . $fileNameWithParams;
                $fileexist = Storage::exists($pathFile);

                if (!$fileexist) {
                    $image = HelpersImage::generateImage($truename, $params, $database, $id_original_image, $originalsize);
                    if ($image) {
                        $counter++;
                    }
                }
            }
        }

        return $counter + " image generated";
    }

    public static function getFileMethod($request)
    {

        $truename = $request->any;
        $pathinfo = pathinfo($truename);
        $dirname = $pathinfo['dirname'];


        $folders = explode('/', $truename);
        $truename = $folders[count($folders) - 1];

        $image = Db::table('olmo_storage')->where('truename', $truename)->first();

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  => 'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media',
            'csv'  => 'documents'
        ];

        if ($image) {

            if ($image->public == 'false') {
                return response(403);
                exit();
            }

            $type = pathinfo($truename, PATHINFO_EXTENSION);
            unset($folders[count($folders) - 1]);
            $filepath = implode('/', $folders);
            $data = storage_path('app/public/' . $categories_files[$type] . '/' . $filepath . '/' . $image->filename);
            $content = file_get_contents($data);
            $expires = 14 * 60 * 60 * 24;

            if ($type == 'pdf' || $type == 'csv') {
                return response()->download($data, $image->truename);
                // alternativa per visualizzazione inline:
                // header("Content-type:application/pdf");
                // header("Content-Disposition:inline;filename=".$image->truename);
                // readfile($data);
            } else if ($type == 'mp4') {
                header("Content-Type: video/" . $type);
                header("Accept-Ranges: bytes");
                header("Content-Range: bytes 32768-22823645/22823646");
            } else {
                header("Content-Type: image/" . $type);
            }
            header("Content-Length: " . strlen($content));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header("Accept-Ranges: bytes");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $content;
            exit();
        }

        return response(404);
    }

    public static function getBarcodeMethod($request)
    {
        $code    = $request->code;
        $expires   = 14 * 60 * 60 * 24;
        $generator = new BarcodeGeneratorPNG();
        $data      =  $generator->getBarcode($code, $generator::TYPE_CODE_128);
        header("Content-Type: image/png");
        header("Content-Length: " . strlen($data));
        header("Cache-Control: public", true);
        header("Pragma: public", true);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
        echo $data;
        exit();
    }

    public static function getCsvfile($request)
    {
        $id = $request->id;

        $file = Db::table('olmo_storage')->where('id', $id)->first();

        $filepath = explode('/', $file->truename);
        $filepath[count($filepath) - 1] = $file->filename;
        $filename = implode('/', $filepath);
        $path = storage_path('app/public/media/' . $filename);

        $csv = fopen($path, 'r');
        $array = [];
        while (($line = fgetcsv($csv)) !== FALSE) {
            array_push($array, $line);
        }
        fclose($csv);

        if (strpos($file->truename, 'form-') && strpos($file->truename, '.csv')) {
            $explode = explode('/', $file->truename);
            $formidcsv = str_replace("form-", "", $explode[count($explode) - 1]);
            $formid = str_replace(".csv", "", $formidcsv);
            /**
             * Get csv fields to build the column Header
             */
            $formcsvfields = Db::table('olmo_form')->where('id', $formid)->first();

            if ($formcsvfields->csvfield_csvfield_csv != '') {
                $ids = explode(',', $formcsvfields->csvfield_csvfield_csv);
                $arrayOrderField = [];
                foreach ($ids as $id) {
                    $csvfield = Db::table('olmo_formcsvfield')->where('id', $id)->first();
                    array_push($arrayOrderField, $csvfield->name);
                }
                array_unshift($array, $arrayOrderField);
            }
        }

        return $array;
    }

    public static function getPhpfile($request)
    {
        $id = $request->id;

        $file = Db::table('olmo_storage')->where('id', $id)->first();

        $filepath = explode('/', $file->truename);
        $filepath[count($filepath) - 1] = $file->filename;
        $filename = implode('/', $filepath);
        $path = storage_path('app/public/media/' . $filename);

        $filename = $path;
        // $f = fopen($filename, 'r');
        // $content = null;

        // if($f) {
        //     $contents = fread($f, filesize($filename));
        //     fclose($f);
        //     $content = nl2br($contents);
        // }

        $f = fopen($filename, 'r');

        if ($f) {
            $contents = fread($f, filesize($filename));
            fclose($f);
            $content = nl2br($contents);
        }

        return $content;

    }

    public static function createImageVersionObject($item, $fitOptions, $compressionOptions, $checkbox = false)
    {

        $obj = [];
        if (gettype($item) == 'string') { // in caso di massive version get
            $obj["id"] = $item;
            $obj["checkbox"] = $checkbox;
            $obj["size"] = $item;
            $obj["fit"] = [
                'value' => '',
                'options' => $fitOptions
            ];
            $obj["compression"] = [
                'value' => '',
                'options' => $compressionOptions
            ];
        } else {
            $obj["id"] = $item['id'];
            $obj["checkbox"] = $checkbox;
            $obj["size"] = $item['width'] . 'x' . $item['height'];
            $obj["fit"] = [
                'value' => $item['fit'],
                'options' => $fitOptions
            ];
            $obj["compression"] = [
                'value' => $item['compression'],
                'options' => $compressionOptions
            ];
        }

        return $obj;
    }

    public function getListVersions($data)
    {
        $id = $data->id;
        $file = DB::table('olmo_storage')->where('id', $id)->first();
        $access = ['mp4', 'png', 'jpg', 'jpeg'];

        if (!$file) {
            return response(404);
        }

        $type = $file->type;
        if (!in_array($type, $access)) {
            return response([], 200);
        }

        if ($type == 'mp4') {
            HelpersVideo::checkOriginalVersionsAndCreateIt($file, true);
            $version = DB::table('olmo_storage_versions')->where('id_original', $file->id)->first();
            $versionsArray = [];

            if ($version) {
                $versionsArray[] = [
                    'original' => true,
                    'checkbox' => true,
                    'compression' => ['value' => null],
                    'fit' => ['value' => null],
                    'id' => $version->id,
                    'size' => 'mp4',
                ];
                if ($version->compression_path != '') {
                    $versionsArray[] = [
                        'original' => false,
                        'checkbox' => true,
                        'compression' => ['value' => null],
                        'fit' => ['value' => null],
                        'id' => $version->id,
                        'size' => 'webm',
                    ];
                } else {
                    $versionsArray[] = [
                        'original' => false,
                        'checkbox' => false,
                        'compression' => ['value' => null],
                        'fit' => ['value' => null],
                        'id' => $version->id,
                        'size' => 'webm',
                    ];
                }
            }
        } else {
            HelpersImage::checkOriginalVersionsAndCreateIt($file);

            $versions = DB::table('olmo_storage_versions')->where('id_original', $file->id)->get();
            $versions = json_decode($versions, true);

            $versionsArray = [];
            $versionsSizeArray = [];

            $sizeOptions = explode(',', env('IMAGES_SIZES', '200x200'));
            $fitOptions = explode(',', env('IMAGES_FIT', 'clip'));
            $compressionOptions = explode(',', env('IMAGES_COMPRESSION', 75));
            $countersize = 0;

            foreach ($versions as $item) {
                $obj = self::createImageVersionObject($item, $fitOptions, $compressionOptions, true);

                $obj['original'] = ($item['is_original'] == 'true') ? true : false;

                array_push($versionsArray, $obj);
                array_push($versionsSizeArray, $obj["size"]);
                // }
                if ($item['is_originalsize'] == 'true') {
                    $countersize++;
                }
            }

            if ($countersize == 0) {
                array_unshift($sizeOptions, 'original size');
            }

            $result = array_diff($sizeOptions, $versionsSizeArray);

            foreach ($result as $item) {
                $obj = self::createImageVersionObject($item, $fitOptions, $compressionOptions);
                array_push($versionsArray, $obj);
            }
        }



        return $versionsArray;
    }
    /**
     * @var file_original = the original image
     * @var elements = params with information to create new files
     */
    public static function createVersionFiles($file_original, $elements)
    {
        $id = $file_original->id;
        $truename = $file_original->truename;
        $originalsize = 'false';
        $count = 0;
        //
        foreach ($elements as $image) {

            if ($file_original->type == 'mp4') {
                switch ($image['format']) {
                    case 'mp4':
                        $count += HelpersVideo::checkOriginalVersionsAndCreateIt($file_original, true) ? 1 : 0;
                        break;
                    case 'webm':
                        $count += HelpersVideo::createOriginalWebm($truename) ? 1 : 0;
                        break;
                }
            } else {


                if ($image['width'] == 'original size') {
                    $image_mainversion = Db::table('olmo_storage_versions')->where('dirname', $truename)->first();

                    if (!$image_mainversion) {
                        HelpersImage::checkOriginalVersionsAndCreateIt((object)$file_original);
                        $image_mainversion = Db::table('olmo_storage_versions')->where('dirname', $truename)->first();
                    }

                    $image['width'] = $image_mainversion->width;
                    $image['height'] = $image_mainversion->height;
                    $originalsize = 'true';
                }

                $params = ['fit' => $image['fit'], 'width' => $image['width'], 'height' => $image['height'], 'compression' => $image['compression']];
                $count += self::generateFile($params, $truename, 'writeDB', $id, $originalsize) ? 1 : 0;
            }
        }

        return $count;
    }


    public function singleFileCreation($request)
    {

        $id_original_image = $request->id;
        $param_images = $request->input('elements');
        $count = 0;

        /**
         * Structure array of objects
         *  "elements": [{
         *                "width": "100",
         *                "height": "200",
         *                "fit": "",
         *                "compression": 10
         *              }];
         */
        $file_original = Db::table('olmo_storage')->where('id', $id_original_image)->first();
        $count += self::createVersionFiles($file_original, $param_images);

        $string = "Created $count files";
        return response($string, 200);
    }

    public function generateFileResizingMassive($request)
    {

        $ids = $request->input('ids');
        $paramselements = $request->input('elements');
        $count = 0;
        $images_original = Db::table('olmo_storage')->whereIn('id', $ids)->get();

        foreach ($images_original as $image_original) {
            $count += self::createVersionFiles($image_original, $paramselements);
        }

        $string = "Created $count files";

        return response($string, 200);
    }

    public function generateDefaultFileResizingMassive($request){
        $folder = $request->input('folder');
        $images_original = Db::table('olmo_storage')->where('truename', 'LIKE', "%".$folder."/%")->get();
        $count = [];

        foreach($images_original as $file){
            $fileExtension = pathinfo($file->truename, PATHINFO_EXTENSION);
            $allowedExtensions = ['jpg', 'jpeg', 'png'];
            if (in_array(strtolower($fileExtension), $allowedExtensions)) {
                try{
                    $count[$file->truename] = HelpersImage::checkDefaultVersionsAndCreateThem($file);
                }catch(Exception $e){
                    $count[$file->truename] = 'jumped';
                }
                
            }
        }

        // $string = "Created $count files";
        return response($count, 200);
        
    }

    public static function getFolderContents($dir, $needle = '')
    {
        $result = [];
        $it = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);

        foreach ($it as $key => $child) { // $key contiene l'intero path, child solo il nome del file/directory
            if ($child->isDot()) {
                continue;
            }
            $name = $child->getBasename();

            if ($child->isDir()) {
                $subdirectory = $dir . '/' . $name;
                $subdirectory_content = self::getFolderContents($subdirectory);
                $result = array_merge($result, $subdirectory_content);
            } else {

                $needle = 'media/';
                $db_filename = strstr($key, $needle);
                if (!$db_filename) {
                    $needle = 'assets/';
                    $db_filename = strstr($key, $needle);
                }
                if (!$db_filename) {
                    $needle = 'upload/';
                    $db_filename = strstr($key, $needle);
                }

                $db_filename = str_replace($needle, '', $db_filename);

                $result[] =  $db_filename;
            }
        }
        return $result;
    }

    public function alignStorage()
    {

        $all_media = Db::table('olmo_storage')->where('truename', 'not like', '%olmoemail/attachments%')->get();
        $result = self::recursiveAssetsCreation($all_media);

        return $result;
    }

    public function alignStorageImage()
    {

        $all_media = Db::table('olmo_storage')->whereIn('type', ['png', 'jpg', 'jpeg', 'JPG', 'JPEG', 'PNG'])->where('truename', 'not like', '%olmoemail/attachments%')->get();
        $result = self::recursiveAssetsCreation($all_media);

        return $result;
    }

    public function alignStorageVideo()
    {

        $all_media = Db::table('olmo_storage')->where('type', 'mp4')->get();
        $result = self::recursiveAssetsCreation($all_media);

        return $result;
    }

    public function alignStorageGif()
    {

        $all_media = Db::table('olmo_storage')->where('type', 'gif')->get();
        $result = self::recursiveAssetsCreation($all_media);

        return $result;
    }

    public static function recursiveAssetsCreation($all_media)
    {

        $result = [];

        foreach ($all_media as $item) {

            $check = null;

            if ($item->type == 'mp4') {
                $check = HelpersVideo::checkOriginalVersionsAndCreateIt($item);
            } else if ($item->type == 'gif') {
                $check = HelpersGif::checkOriginalVersionsAndCreateIt($item);
            } else if ($item->type == 'JPG' or $item->type == 'JPEG' or $item->type == 'jpg' or $item->type == 'png' or $item->type == 'jpeg') {
                $check = HelpersImage::checkOriginalVersionsAndCreateIt($item);
            }

            array_push($result, $check);
        }

        return $result;
    }

    public static function recursiveAssetsCreationOLD($dir, $all_media)
    {
        $result = [];
        $it = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);
        foreach ($it as $key => $child) { // $key contiene l'intero path, child solo il nome del file/directory

            if ($child->isDot()) {
                continue;
            }

            $name = $child->getBasename();

            if ($child->isDir()) {
                $subdirectory = $dir . '/' . $name . '/';
                $subdirectory_content = self::recursiveAssetsCreation($subdirectory, $all_media);
                $result = array_merge($result, $subdirectory_content);
            } else {

                $needle = 'media/';
                $db_filename = strstr($key, $needle);
                $db_filename = str_replace($needle, '', $db_filename);

                $encryptedname = pathinfo($db_filename)['basename'];
                $file = Db::table('olmo_storage')->where('filename', $encryptedname)->first();

                if (!$file) {
                    return $encryptedname;
                }

                if ($file->type == 'mp4') {
                    $check = HelpersVideo::checkOriginalVersionsAndCreateIt($file);
                } else if ($file->type == 'gif') {
                    $check = HelpersGif::checkOriginalVersionsAndCreateIt($file);
                } else if ($file->type == 'jpg' or $file->type == 'png' or $file->type == 'jpeg') {
                    $check = HelpersImage::checkOriginalVersionsAndCreateIt($file);
                }

                $result[] =  $check;
            }
        }
        return $result;
    }

    public function checkFilemanagerStatus($request, $appPath = 'app/public/media/')
    {
        // TODO: gli assets
        $database_options = [
            ['name' => 'olmo_storage', 'folder' => 'app/public/media/', 'column_check' => 'filename'],
            ['name' => 'olmo_storage_versions', 'folder' => 'app/public/assets/', 'column_check' => 'dirname']
        ];

        $response['status'] = 'done without exception';

        $media_ids = [];
        $response = [];
        foreach ($database_options as $table) {
            $storage_content_db = []; // contiene tutti i path verso gli oggetti trovati
            $table_name = $table['name'];
            $table_column = $table['column_check'];
            $folder = $table['folder'];
            $all_media = Db::table($table_name)->get();
            $filesAndRowDeleted = [];

            foreach ($all_media as $key => $content) {

                $folders = '';
                if ($table_name == 'olmo_storage') {
                    $folders = pathinfo($content->truename)['dirname'];
                    $media_ids[] = $content->id;
                    $include_in_array = true;
                } else {
                    $include_in_array = in_array($content->id_original, $media_ids); // controllo se l'originale esiste, in caso contrario cancello i suoi assets
                }

                $str = $folders . '/' . $content->$table_column;
                $str = ltrim($str, '.');
                $str = ltrim($str, '/');

                if ($include_in_array) {
                    $storage_content_db[] = $str;
                } else {
                    Db::table($table_name)->where($table_column, $str)->delete();
                    $filesAndRowDeleted['dbrows'][] = $str;
                }
            }

            $storage_content_physical = self::getFolderContents(storage_path($folder));

            foreach ($storage_content_physical as $key => &$path) {
                if (@pathinfo($path)['extension'] != 'webp') { // TODO: cancellare webp da assets
                    if ($path[0] == '/' || $path[0] == '.') {
                        $path = substr($path, 1);
                    }
                } else {
                    unset($storage_content_physical[$key]);
                }
            }
            // i path aggiuntivi del primo array vengono assegnati al nuovo array
            $db_diff = array_diff($storage_content_db, $storage_content_physical);
            $physical_diff = array_diff($storage_content_physical, $storage_content_db);
            $response[]['contents'] = [$storage_content_db, $storage_content_physical];
            $response[]['diff'] = [$db_diff, $physical_diff];
            $response[]['tabinfo'] = [$table];



            foreach ($physical_diff as $filepath) {
                try {
                    // if($table_name == 'olmo_storage'){
                    //     $filepath = Db::table($table_name)->select('filename')->where($table_column, $filepath)->get();
                    // }
                    File::delete(storage_path($folder . $filepath));
                    $filesAndRowDeleted['physicalfiles'][] = $filepath;

                    if ($table_name == 'olmo_storage_versions') {
                        $webp_file = str_replace(pathinfo($filepath)['extension'], 'webp', $filepath);
                        File::delete(storage_path($folder . $webp_file));
                        $filesAndRowDeleted['physicalfiles'][] = $webp_file;
                    }
                } catch (\Exception $e) {
                    $filesAndRowDeleted['exception']['file'] = ['name' => $filepath, 'exception' => $e];
                    $response['status'] = 'done with exception';
                }
            }

            foreach ($db_diff as $filepath) {
                try {
                    $filename = ($table_name == 'olmo_storage') ? pathinfo($filepath)['basename'] : $filepath;
                    Db::table($table_name)->where($table_column, $filename)->delete();
                    $filesAndRowDeleted['dbrows'][] = $filename;
                } catch (\Exception $e) {
                    $filesAndRowDeleted['exception']['db'] = ['name' => $filename, 'exception' => $e];
                    $response['status'] = 'done with exception';
                }
            }

            $response[$table_name] = $filesAndRowDeleted;
        }

        //return $filesAndRowDeleted;

        return $response;
    }

    public function uploadLocalFolder()
    {
        $folder_content = self::getFolderContents(storage_path('app/public/upload/'));

        $cantIterate = [];
        $counter = 0;
        $num = count($folder_content);
        foreach ($folder_content as $key => &$path) {
            if($path[0] == '/'){
                $path = ltrim($path, '/');
            }
            
            $pathinfo = pathinfo($path);
            if($pathinfo['basename'][0] != '.'){
                try {
                    $pathinfo = HelpersData::normalizePathString($path, true);
                    // echo print_r($pathinfo, true);

                    $extension = $pathinfo['extension'];
                    $truename = $pathinfo['basename'];
                    $filename = md5($truename) . '.' . $extension;
                    $public = 'true';  

                    $local_file = 'public/media/' . str_replace($truename, $filename, $pathinfo['relativepath']);
                    $upload_path = 'public/upload/' . $path; // qui lo creo, poi manipolo path
                    // echo print_r(['local'=>$local_file], true);

                    $exists = Storage::disk("local")->exists($local_file);

                    if (!$exists) {
                        // I need here to insert the field into the db to understand if there are problems with the files
                        $storage_id = DB::table('olmo_storage')->insertGetId([
                            'filename'    => $filename,
                            'truename'    => $pathinfo['relativepath'],
                            'model'      => 'filemanager',
                            'public'     => $public ? $public : 'true',
                            'type'       => $extension,
                        ]);

                        $res = Storage::disk("local")->copy($upload_path,  $local_file) ? true:false;

                        $checkTrueNameNumber = DB::table('olmo_storage')->where('truename', $truename)->get()->count();

                        $file = DB::table('olmo_storage')->where('id', $storage_id)->first();

                        $check = false;

                        if ($file->type == 'mp4') {
                            $check = HelpersVideo::checkOriginalVersionsAndCreateIt($file);
                        } else if ($file->type == 'gif') {
                            $check = HelpersGif::checkOriginalVersionsAndCreateIt($file);
                        } else if ($file->type == 'jpg' or $file->type == 'png' or $file->type == 'jpeg') {
                            $check = HelpersImage::checkOriginalVersionsAndCreateIt($file);
                        }

                        if ($checkTrueNameNumber > 0) {
                            $truename   = explode('.', $truename);
                            $newTruename = $truename[0] . '-' . $checkTrueNameNumber . '.' . $truename[1];
                            DB::table('olmo_storage')->where('id', $storage_id)->update(['truename' => $newTruename]);
                            // $public_path = $folders !== '' ? '/media/' . $newTruename : '/media/' . $newTruename;
                            // $truename = $truename[0] . '.' . $truename[1];
                        }
                    }

                    unset($folder_content[$key]);
                    $counter++;
                } catch (Exception $e) {
                    $cantIterate[] = [$path, $e];
                }
            }else{
                $num--; // tolgo dal conto i file nascosti
                unset($folder_content[$key]);
            }
        }

        return ['doneNumber' => $counter, 'files_not_loaded' => $folder_content, 'Problems' => $cantIterate, 'total' => $num];
    }

    public function makeSearchMethod($request)
    {
        $search = $request->search;

        $query = Db::table('olmo_storage')->where('truename', 'like', '%' . $search . '%')->get();

        $res = [];

        foreach($query as $key=>$file){

            $res[$key]['id'] = uniqid();
            $truename = pathinfo(@$file->truename);

            $meta = [];
            $meta['name'] = $truename['filename'];
            $meta['idfile'] = @$file->id;
            $meta['path'] = @$file->truename;
            $meta['caption'] = (@$file->caption != '')  ?   @$file->caption : '';
            $meta['alt'] = (@$file->alt     != '')  ?   @$file->alt : '';
            $meta['public'] = (@$file->public  != '')  ?   @$file->public : '';

            $res[$key]['name'] = $meta['name'];
            $res[$key]['physicname'] = $file->filename;
            $res[$key]['type'] = 'file';
            $res[$key]['extension'] = $file->type;
            $res[$key]['description'] = $meta;

        }

        return $res;

    }
}
