<?php

namespace Olmo\Core\App\Http\Controller;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

use Olmo\Core\App\Helpers\HelpersDBRelation;
use Olmo\Core\App\Helpers\HelpersUser;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersRequiredField;
use Olmo\Core\App\Helpers\HelpersRulesField;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersFilemanager;
use Olmo\Core\App\Helpers\HelpersForm;
use Olmo\Core\App\Helpers\HelpersSlider;
use Olmo\Core\App\Helpers\HelpersProperty;
use Olmo\Core\App\Helpers\HelpersVisualBlock;
use Olmo\Core\App\Helpers\HelpersPermission;
use Olmo\Core\App\Helpers\HelpersInformation;
use Olmo\Core\App\Helpers\HelpersCsv;
use Olmo\Core\App\Helpers\HelpersMenu;
use Olmo\Core\App\Helpers\HelpersProduct;
use Olmo\Core\App\Helpers\HelpersItem;
use Olmo\Core\App\Helpers\HelpersOrder;
use Olmo\Core\App\Helpers\HelpersRoute;
use Olmo\Core\App\Helpers\HelpersDotsPosition;
use Olmo\Core\App\Helpers\HelpersFieldsRepeater;
use Olmo\Core\App\Helpers\HelpersSitemap;
use Olmo\Core\App\Helpers\HelpersAgent;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersSerializer;

use Olmo\Ecommerce\App\Helpers\HelpersEmail;
use Olmo\Ecommerce\Containers\Ecommerce\Order\Controllers\Controller as Order;
use Olmo\Ecommerce\Containers\Ecommerce\Cart\Controllers\Controller as Cart;

use Olmo\Core\Containers\Frontend\Base\Controllers\SitemapController;

class BackController extends Controller
{ 

    public function createNewPost($request, $attribute)
    {   
        // return response($attribute, 200);
        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing($attribute['table']);
        /** Flip values in the keys */
        $columns_flip = array_flip($columns);

        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function (&$v, $k, $alter) {
            if (HelpersData::getType($k) == 'number') {
                $v = 0;
            } else if (HelpersData::getType($k) == 'readonly') {
                $v = Carbon::now();
            } else if (HelpersData::getType($k) == 'checkbox') {
                $v = 'false';
            } else if ($k == 'statuscode_select_general') {
                $v = 200;
            } else {
                $v = $alter;
            }
        }, '');

        unset($columns_flip['id']);

        /**
         * If the request gots object inside means a translation post is requested
         * so the request has a body with 2 keys lang and parent
         * lang is the code of the lang the post will be translated in
         * parent is the id of the original post
         */
        $lang = $request->input('lang') ?? $request->lang;
        $parentId = $request->input('parent');

        if ($parentId) {
            $columns_flip['parentid_hidden_general'] = $parentId;
        }
        if (isset($columns_flip['locale_hidden_general'])) {
            $columns_flip['locale_hidden_general'] = $lang;
        }

        /** Insert the post in the DB */
        $response = Db::table($attribute['table'])->insertGetId($columns_flip);

        if(isset($columns_flip['createdby_read_information'])){
            $userEmail = HelpersUser::getUserEmail();
            Db::table($attribute['table'])->where('id', $response)->update([
                'createdby_read_information' => $userEmail,
                'lastmodby_read_information' => $userEmail
            ]);
        }

        /**
         * If index_select_seo exist make the fields index_select_seo and follow_select_seo
         * filled with respetive values (index and follow)
         */
        if(isset($columns_flip['index_select_seo'])){
            Db::table($attribute['table'])->where('id', $response)->update([
                'index_select_seo' => 'index',
                'follow_select_seo' => 'follow'
            ]);
        }

        /**
         * If there is a quantity field update the table olmo_quantity
         */
        if(isset($columns_flip['qty_spin_general'])){
            Db::table('olmo_quantity')->insertGetId([
                'quantity' => 0,
                'prod_id' => $response,
                'type' => 'item'
            ]);
        }        

        $response = array('id' => $response);

        /**
         * EXTRA JOB FOR SPECIFIC DEFAULT MODEL
         */

        /** If the model is template create the slugs in the templateslug table */
        HelpersLang::createTemplateSlugMultilanguage($attribute['table'], $response);
        HelpersExtra::updatePrefillField($attribute, $response);

        /**
         * EXTRA JOB FOR NAME FIELD
         */
        if ($lang && $parentId) {
            HelpersLang::changeNamePost(false, $response['id'], $attribute['table'], $parentId);
        }


        return response($response, 200);
    }

    public function DbRelation($table, $value, $type = 'single')
    {
        if ($type == 'single') {
            return HelpersDBRelation::singleRelation($table, $value);
        } else if ($type == 'array') {
            return HelpersDBRelation::arrayRelation($table, $value);
        }
    }

    /** 
     * Get the value, in case the field is a select or some other list-field use the list to determinate
     * the value by the id
     * 
     * */
    public static function getValue($key, $value, $values, $type, $table, $id)
    {

        if ($type == 'password') {
            $value = '';
        } else if ($type == 'select' || $type == 'disabled_select' || $type == 'agent') {
            $value = HelpersData::currentValueSelect($values, $value);
        } else if ($type == 'multiselect' || $type == 'disabled_multiselect') {
            $value = HelpersData::currentValueMultiSelect($value, $key);
        } else if ($type == 'filemanager') {
            $value =  HelpersFilemanager::getSinglePathFmanagerById($value, 'media');        
        } else if ($type == 'lang') {
            $value = HelpersLang::getLangsBymodelId($table, $id);
        } else if ($type == 'spinner') {
            $value = HelpersData::spinNumber($key, $value, $values, $type, $table, $id);
        } else if ($type == 'copytranslate') {
            $value = $id;
        } else if ($type == 'menu') {
            $value = HelpersMenu::getMenu($value);
        } else if ($type == 'sitemapimage') {
            $value = HelpersSitemap::getSiteImage($value);
        }

        return $value;
    }

    /** 
     * Get the list of values in case the field is a select or some other list-field 
     * 
     * -------------------------------------------------------------------------------
     * $key -> is the field name and column name
     * $value -> the DB value
     * $type -> the field type based on HelpersData::getType((string)$key, $key);
     * $lang -> the locale from the endpoint url
     * $model -> This is the table where the request comes from
     * $id -> Request endpint url id
     * -------------------------------------------------------------------------------
     * */
    public static function getValues($key, $value, $type, $lang, $model, $id, $modelid = null)
    {

        $label = HelpersData::setLabel($key);
        $table = 'olmo_' . $label;

        $model_name = explode('_', $model)[1];

        if ($type == 'select' || $type == 'multiselect' || $type == 'disabled_select' || $type == 'disabled_multiselect' || $type == 'agent') {            
            $table = strpos($key, '_sliderfromitem_') !== false ? 'olmo_productitem' : $table;
            $value = HelpersDBRelation::listRelation($table, $lang, $model_name, $id, $key, $modelid);
        } else if ($type == 'lang') {
            $value = HelpersLang::getLangsBymodelId($model, $id);
        } else if ($type == 'fields') {            
            $value = HelpersFieldsRepeater::getFields($value);
        } else if ($type == 'slider') {            
            $value = HelpersSlider::getSlides($value);
        } else if ($type == 'property') {            
            $value = HelpersProperty::getPropertyvalue($value);
        } else if ($type == 'productitems') {
            $value = HelpersProduct::getProductvalue($value);
        } else if ($type == 'list') {            
            $value = HelpersItem::getItem($value);
        } else if ($type == 'visual') {
            $value = HelpersVisualBlock::retrieveBlocks($value);
        } else if ($type == 'formfield') {
            $value = HelpersForm::getFields($value);
        } else if ($type == 'csvfield') {
            $value = HelpersCsv::getCsvFields($value);
        } else if ($type == 'menu') {
            $value = HelpersMenu::getNavigationMenuLabel($lang, $value, $id);
        } else if ($type == 'dotsposition') {
            $value = HelpersDotsPosition::getDots($value);
        } else if ($type == 'singleimage') {
            $value = HelpersFilemanager::getSinglePathById($value, 'media');        
        }

        return $value;
    }

    public static function getListPostValue($value, $key, $table, $id)
    {

        $label = HelpersData::setLabel($key);
        $key_table = 'olmo_' . $label;
        $type = HelpersData::getType($key);

        if ($type == 'select') {
            $value = HelpersDBRelation::selectRelation($key_table, $value, $type, $key);
        } else if ($type == 'filemanager') {
            $value =  HelpersFilemanager::getSinglePathFmanagerById($value);
            // @Db::table('olmo_storage')->where('id',$v)->first()->truename;                
        } else if ($type == 'lang') {
            $value = HelpersLang::getLangsBymodelId($table, $id);
        }

        return $value;
    }

    public static function serializePost($data, $attribute, $lang, $id, $modelid = null)
    {

        $defaultlang = HelpersLang::getDefaultLang();
        $required = isset($attribute['requiredbackoffice']) ? $attribute['requiredbackoffice'] : null;

        $items = [];
        $i = 0;

        foreach ($data as $key => $value) {

            $item = [];

            $item['name'] = (string)$key;
            $item['label'] = HelpersData::setLabel((string)$key);
            $item['tab'] = HelpersData::setTab((string)$key);
            
            $type = HelpersData::getType($key, $lang, true, $defaultlang, $required, $attribute);

            $item['type'] = ($defaultlang == $lang) ? $type : HelpersLang::isCrossLanguage((string)$key, $attribute, $type);
            
            $item['required'] = HelpersData::isRequired($required, $key);
            // TODO: here agents mods
            $item['values'] = self::getValues($key, (string)$value, $item['type'], $lang, $attribute['table'], $id, $modelid);
            $item['value'] = self::getValue($key, (string)$value, $item['values'], $item['type'], $attribute['table'], $id);

            if($key == 'agent_agent_general'){
                $item['type'] = 'select';
            }
            $items[$i] = $item;
            $i++;
        }

        return $items;
    }    

    public static function getTemplate($id) 
    {
        $template = Db::table('olmo_template')->where('id', $id)->first();

        return isset($template->name_txt_general) ? $template->name_txt_general : null;
    }

    public static function checkThePreviewData($data)
    {
        // Check if the current post has a preview
        if(isset($data['preview_hidden_general'])){
            $preview = [];
            if($data['preview_hidden_general'] != ''){
                $storage = storage_path('app/'.$data['preview_hidden_general']);
                $preview = json_decode(file_get_contents($storage), true);
                return $preview;
            }
        }
        return $data; 

    }

    public static function checkTheIsPreview($data)
    {
        // Check if the current post has a preview
        if(isset($data['preview_hidden_general'])){
            if($data['preview_hidden_general'] != ''){
                return true;
            }
        }        

        return false;
    }

    public function serializeSinglePost($attribute, $dataPost, $code = 200, $sendEmail = [])
    {
        $id = $dataPost['id'];
        $lang = $dataPost['lang'];

        $data = Db::table($attribute['table'])->where('id', $id)->first(); 
        // return $data;
        if(!$data){
            return abort(404);
        }

        $data = (array)$data;

        // Check if it is a preview
        $data = self::checkThePreviewData($data);
        $isPreview = self::checkTheIsPreview($data);
        /**
         * Check if need copy-from button
         * This happen when a user creates a localized post, the button must be always there
         * so then print a button to copy the content from the default post
         */
        $data = HelpersLang::addCopyDefaultFrom($data, $id);
        /** If the model is template get the slugs */
        $data = self::getTemplateSlugMultilanguage($data, $attribute['table']);
        
        // Check if the user has the authotization and remove fields if necessary
        $data = HelpersPermission::userPermissionFields($attribute['table'], $data);

        $delete = isset($attribute['delete']) ? $attribute['delete'] : false;
        $duplicate = isset($attribute['duplicate']) ? $attribute['duplicate'] : false;
        $preview = isset($attribute['preview']) ? $attribute['preview'] : false;

        $permission = HelpersPermission::userPermissionAction($attribute, $duplicate, $delete);

        // Extract Information from the post
        $info = HelpersInformation::getInformationFromPost($data);

        // Clean information from the post
        $data = HelpersInformation::cleanInformationFromPost($data);

        $defaultlang = Helperslang::getDefaultLang();

        $url = HelpersRoute::backofficeUrl($attribute, $data, $defaultlang, env('FRONT_URL'), env('FRONT_URL_LANG', true));

        if($url != ''){
            if(isset($data['sitemapimage_sitemapimage_sitemap'])){
                $data['sitemapimage_sitemapimage_sitemap'] = $url;
            }
            if($isPreview){
                $url = $url.'?olmopreview';
            }
        }

        if ($data) {

            $code = $code;

            $items = self::serializePost($data, $attribute, $lang, $id);
            // unset($items[5]);
            $response['items'] = $items;
            
            $response['pagename'] = explode('_', $attribute['table'])[1];
            $response['title'] =  $data['name_txt_general'] ?? explode('_', $attribute['table'])[1];
            // $response['url'] = ApiHelper::getSlugFront($model, $lang, $id);
            $response['id'] = (int)$id;
            $response['copy'] = isset($data['copytranslate']) ? true : false;
            $response['duplicate'] = $defaultlang == $lang ? $permission['duplicate'] : false;
            $response['delete'] = $permission['delete'];
            $response['create'] = $permission['create'];
            $response['multilang'] = isset($data['lang_langs_general']) ? HelpersLang::getLangsBymodelId($attribute['table'], $id) : false;
            $response['info'] = $info;
            $response['url'] = $url;            
            $response['statuscode'] = $code;
            $response['preview'] = $preview;
            $response['ispreview'] = $isPreview;
            $response['sendEmail'] = $sendEmail;
            $response['apicache'] = isset($attribute['cache']) ? $attribute['cache'] : false;
            $response['template'] = isset($data['template_id_general']) ? self::getTemplate($data['template_id_general']) : null;
            $response['translate'] = $permission['translate'];
            $response['visualblock'] = array("create" => $permission['visualblockcreate'], "delete" => $permission['visualblockdelete'], "modify" => $permission['visualblockmodify']);

            return $response;
        } else {
            return response(404);
        }
    }

    public function serializeSingleOrder($attribute, $dataPost, $order, $code = 200)
    {
        $id = $dataPost['id'];
        $lang = $dataPost['lang'];

        if(!$order){
            return abort(404);
        }
        
        foreach($order as $key => $item){
            $key_name = $item['name'];
            $array_assoc[$key_name] = $item['value'];
            if(!str_contains($key_name, '_information')){ // Clean information from the post
                $array[] = $item;
            }
            
        }
        $data = $array;

        // Check if it is a preview
        $data = self::checkThePreviewData($data);
        $isPreview = self::checkTheIsPreview($data);
        /**
         * Check if need copy-from button
         * This happen when a user creates a localized post, the button must be always there
         * so then print a button to copy the content from the default post
         */
        // $data = HelpersLang::addCopyDefaultFrom($data, $id);
        
        // Check if the user has the authotization and remove fields if necessary
        $data = HelpersPermission::userPermissionFields($attribute['table'], $data);

        $delete = isset($attribute['delete']) ? $attribute['delete'] : false;
        $duplicate = isset($attribute['duplicate']) ? $attribute['duplicate'] : false;
        $preview = isset($attribute['preview']) ? $attribute['preview'] : false;

        $permission = HelpersPermission::userPermissionAction($attribute, $duplicate, $delete);

        // Extract Information from the post
        $info = HelpersInformation::getInformationFromPost($array_assoc);

        $cart = Db::table('olmo_cart')->where('order_id', $id)->select('id')->first();
        if($cart){
            foreach($data as $key => $element){
                if($element['name'] == 'items_list'){
                    $data[$key]['value'] = Order::getCartObjects($cart->id);
                }
            }
        }

        $defaultlang = Helperslang::getDefaultLang();
        $url = HelpersRoute::backofficeUrl($attribute, $data, $defaultlang, env('FRONT_URL'));
        if($url != ''){
            if(isset($data['sitemapimage_sitemapimage_sitemap'])){
                $data['sitemapimage_sitemapimage_sitemap'] = $url;
            }
            if($isPreview){
                $url = $url.'?olmopreview';
            }
        }

        if ($data) {
            $response['items'] = (array) $data;
            $response['pagename'] = explode('_', $attribute['table'])[1];
            $response['title'] =  $data['name_txt_general'] ?? explode('_', $attribute['table'])[1];
            $response['id'] = (int)$id;
            $response['copy'] = isset($data['copytranslate']) ? true : false;
            $response['duplicate'] = $defaultlang == $lang ? $permission['duplicate'] : false;
            $response['delete'] = $permission['delete'];
            $response['create'] = $permission['create'];
            $response['multilang'] = isset($data['lang_langs_general']) ? HelpersLang::getLangsBymodelId($attribute['table'], $id) : false;
            $response['info'] = $info;
            $response['url'] = $url;            
            $response['statuscode'] = $code;
            $response['preview'] = $preview;
            $response['ispreview'] = $isPreview;
            $response['apicache'] = isset($attribute['cache']) ? $attribute['cache'] : false;
            $response['template'] = isset($data['template_id_general']) ? self::getTemplate($data['template_id_general']) : null;
            $response['translate'] = $permission['translate'];
            $response['visualblock'] = array("create" => $permission['visualblockcreate'], "delete" => $permission['visualblockdelete'], "modify" => $permission['visualblockmodify']);

            return $response;
        } else {
            return response(404);
        }
    }

    public function serializeListPosts($attribute, $dataPost)
    {
        $user = HelpersUser::getUser();
        $isAgent = HelpersAgent::isAgent($user->token_hidden_general, true);
        // return $isAgent;
        
        $name = explode('_', $attribute['table'])[1];
        $importexport = isset($attribute['importexport']) ? $attribute['importexport'] : false;
        $lang = $dataPost['lang'];
        $defaultlang = Helperslang::getDefaultLang();
        $orderby = isset($attribute['orderby']) ? $attribute['orderby'] : 'id';
        $order = isset($attribute['order']) ? $attribute['order'] : 'asc';

        $delete = isset($attribute['delete']) ? $attribute['delete'] : false;
        $duplicate = isset($attribute['duplicate']) ? $attribute['duplicate'] : false;

        $permission = HelpersPermission::userPermissionAction($attribute, $duplicate, $delete);
        $data = '';
        if ($name == 'user') {
            $data = HelpersUser::usersByRole();
        } else if($name == 'customer' && $isAgent == 1) {// per blindare da altri role, > 0
            $data = HelpersAgent::customersList($user);
        } else if($name == 'order' && $isAgent == 1) { // per blindare da altri role, > 0
            $data = HelpersAgent::customerOrderList($user);
        }
        if(!is_array($data)){
            if (isset($attribute['listing'])) {
                if (isset($attribute['lang']) && $attribute['lang']) {
                    
                    $data = Db::table($attribute['table'])
                            ->where('locale_hidden_general', $dataPost['lang'])
                            ->orderBy($orderby, $order)
                            ->select(DB::raw($attribute['listing']))
                            ->offset($dataPost['obj']['offset'])
                            ->limit($dataPost['obj']['pagesize'])
                            ->get();
                
                } else {
                    // TODO: 
                    /** 
                     * Check the select field and the orderby integrity
                     * the orderby value must be inside the listing
                     * */  
                    $data = Db::table($attribute['table'])
                            ->orderBy($orderby, $order)
                            ->select(DB::raw($attribute['listing']))
                            ->select(DB::raw($attribute['listing']))
                            ->offset($dataPost['obj']['offset'])
                            ->limit($dataPost['obj']['pagesize'])                            
                            ->get();
                }
            } else {
                $data = Db::table($attribute['table'])->get();
            }
        } else {
            $data = [];
        }

        $items  = [];
        $i      = 0;

        foreach ($data as $value) {

            foreach ($value as $key => $val) {

                $item = [];

                // $parts = explode('.',$key);

                $item['id'] = (string)$key;
                $item['label'] = HelpersData::setLabel((string)$key);
                $item['type'] = HelpersData::setTypeList((string)$key);
                $item['value'] = self::getListPostValue($val, $key, $attribute['table'], $value->id);

                // $item['label'] = 'locale';

                $items[$i][] = $item;
            }

            $i++;

        }

        $response['items'] = $items;
        $response['route'] = $attribute['table'];
        $response['page'] = 1;
        $response['pagesize'] = count($items);
        $response['totalpages'] = 1;
        $response['totalentries'] = 1;
        $response['generatepost'] = isset($attribute['generate-post']) ? $attribute['generate-post'] : true;
        $response['importexport'] = $importexport;
        $response['allmodelcachefrontend'] = isset($attribute['allmodelcachefrontend']) ? $attribute['allmodelcachefrontend'] : false;
        $response['order'] = $order;
        $response['orderby'] = $orderby;
        $response['duplicate'] = $defaultlang == $lang ? $permission['duplicate'] : false;
        $response['delete'] = $permission['delete'];
        $response['create'] = $permission['create'];
        $response['translate'] = $permission['translate'];

        return $response;
    }

    public function serializePostUpdating($request, $attribute, $preview = false, $order = false)
    {
        $required = HelpersRequiredField::checkRequired($request, $attribute);
        $rules = HelpersRulesField::checkRules($request, $attribute);
        
        if (count($required) > 0 or count($rules) > 0) {
            return count($required) ?
                response($required, 400) :
                response($rules, 400);
        } else {
            return $this->updateRequestPost($request, $attribute, $preview, $order);
        }
    }

    public function updateRequestPost($request, $attribute, $preview = false, $order = false)
    {
        $code = 200;
        $dataPost = HelpersData::idLangPost($request);

        /** Check if the request is a json */
        $isJson = HelpersData::isJson($request);
        if ($isJson) {
            return response('Body not a JSON', 400);
        }

        $body = HelpersData::normalizeBody($request);

        /** If necessary add lang to the body */
        $body = $this->addLang($body, $attribute, $dataPost['lang']);
        /** If the model is template update the slug in the templateslug table */
        $body = self::updateTemplateSlugMultilanguage($body, $attribute['table']);
        // Add update information to the body
        $body = HelpersInformation::updateInformation($body, $attribute['table']);

        /**
         * Understand what is need for
         */
        // if(isset($body['category_id_general'])){
        //     if($body['category_id_general'] == ''){
        //         $body['category_id_general'] = 0;
        //     }
        // }

        $orderstatus = HelpersOrder::getOrderStatus($attribute['table'], $dataPost['id']);
        
        /**
         * Questa cosa fa parte di un delirio al quale si dovrà ttrovare una soluzione
         * il lavoro è stato fatto per estego e la funzionalità agent
         */
        // if($order){
        //     $orderCompleted = false;
        //     Db::table('olmo_order')->update(['enabled_is_general' => $body['enabled_is_general']]);
            
        //     if(isset($body['payments_methods'])){
        //         if($body['payments_methods'] != ''){
        //             $code = Db::table('olmo_paymentmethod')->select('code_txt_general as code')->where('id', $body['payments_methods'])->first();
        //             Db::table($attribute['table'])->where('id', $dataPost['id'])
        //             ->update(['paymentmethod_txt_general' => $code->code]);
        //         }
        //         if($body['shipping_method'] != ''){
        //             $code = Db::table('olmo_shippingmethod')->select('code_txt_general as code')->where('id', $body['shipping_method'])->first();
        //             Db::table($attribute['table'])->where('id', $dataPost['id'])
        //             ->update(['shippingmethod_txt_general' => $code->code]);
        //         }
        //         if($body['shipping_address'] != ''){
        //             if($body['copy_shipping_to_billing']){
        //                 $body['billing_address'] = $body['shipping_address'];
        //             }
        //             unset($body['copy_shipping_to_billing']);
    
        //             $shipping_infos = Order::setShippingAddresses($body['shipping_address']);
        //             $billing_infos = Order::setBillingAddresses($body['billing_address']);
        //             $shipping_infos = array_merge($billing_infos, $shipping_infos); 

        //             Db::table($attribute['table'])->where('id', $dataPost['id'])->update($shipping_infos);
        //         }

        //     } else { // se entro qui, gli altri dati non esistono nel body perché non visualizzabili senza aver indicato il cliente
        //         if(isset($body['customer_read_general'])){
        //             if($body['customer_read_general']){ // qui dentro mi ritrovo l'id del cliente
        //                 $customer = HelpersCustomer::getCustomerbyId($body['customer_read_general']);
        //                 Db::table('olmo_order')->update(['customerid_hidden_customer' => $body['customer_read_general'], 'customer_read_general' => $customer->email_email_general]);
        //             }
        //         }
        //     }

        //     if($body['name_txt_general'] != ''){
        //         Db::table('olmo_order')->update(['name_txt_general' => $body['name_txt_general']]);
        //     }

            /**
             * Questa cosa fa parte di un delirio al quale si dovrà ttrovare una soluzione
             * il lavoro è stato fatto per estego e la funzionalità agent
             */
            // Questa prima parte era già commentata
            // $items = Cart::getCartForAgentOrder($dataPost['id']);
            // Db::table('olmo_order')->update(['orderitems_json_items' => $items]);
            
            // Questa seconda parte invece dava la risosta alla condizione $order come true
            // $order = HelpersOrder::backofficeOrder($request, 'it', $body['id']);
            // return $this->serializeSingleOrder($attribute, $dataPost, $order, $orderCompleted ? 201:200);

        // }else 
        if($preview){
            $lang = isset($body['locale_hidden_general']) ? $body['locale_hidden_general'] : 'base';
            $cacheKey = $attribute['table'].$body['name_txt_general'].$lang;
            $cacheKey = md5($cacheKey);
            $dbpath = "public/documents/preview/".$cacheKey.".json";
            if(isset($body['preview_hidden_general'])){
                Db::table($attribute['table'])->where('id', $dataPost['id'])->update(['preview_hidden_general' => $dbpath]);
                $body['preview_hidden_general'] = $dbpath;
                Storage::disk('local')->put($dbpath, json_encode($body));
            }
            // Cache::put($cacheKey, $body, now()->addMinutes(10));
        } else {
            if(isset($attribute['preview'])){
                if($attribute['preview'] == true){
                    if(isset($body['preview_hidden_general'])){
                        Storage::disk('local')->delete($body['preview_hidden_general']);
                        $body['preview_hidden_general'] = '';
                    }
                }
            }
            Db::table($attribute['table'])->where('id', $dataPost['id'])->update($body);

            /**
             * If there is a quantity field update the table olmo_quantity
             */
            if(isset($body['qty_spin_general'])){
                $update = Db::table('olmo_quantity')->where('id', $dataPost['id'])->update([
                    'quantity' => $body['qty_spin_general'],
                    'prod_id' => $dataPost['id'],
                    'type' => 'product'
                ]);
                if(!$update){
                    Db::table('olmo_quantity')->insertGetId([
                        'quantity' => $body['qty_spin_general'],
                        'prod_id' => $dataPost['id'],
                        'type' => 'product'
                    ]);                
                }
            }           
            /** Change all crosslanguage value if necessary and after row update */
            if(isset($body['parentid_hidden_general'])){
                if($body['parentid_hidden_general'] == ''){
                    HelpersLang::updateCrossLanguage($body, $attribute);
                }  
            }  
        }

        /**
         * EXTRA JOB FOR SPECIFIC DEFAULT MODEL
         */
        /** If the model is language add the db record in the templateslug table for the language added */
        HelpersLang::createSlugInEveryTemplate($attribute['table'], $body, $request);

        /**
         * If the model is Product and the User change the status like from pending to paid, or pending to shipped 
         * then the system perform a email sending 
         */
        $sendEmail = HelpersOrder::sendEmailIfStatusisChanged($attribute['table'], $orderstatus, $dataPost['id']);

        /**
         * EXTRA JOB FOR NO DEFAULT LANG POST
         */
        $namePost = isset($attribute['lang']) ? HelpersLang::checkNamePost($body, $dataPost['id'], $attribute) : null;
        if($namePost){
            HelpersLang::changeNamePost($body, $dataPost['id'], $attribute['table']);
        }

        if(isset($attribute['cache']) && isset($attribute['route'])){
            if($attribute['cache'] && $attribute['route']){

                $defaultlang = Helperslang::getDefaultLang();
                $url = HelpersRoute::backofficeUrl($attribute, $body, $defaultlang, env('APP_URL').'/api');
                $apiurl = rtrim($url, "/");
                $cacheKey = md5($url);     

                if(Cache::has($cacheKey)) {
                    Cache::forget($cacheKey);
                }                    
                
                // $response = Http::get($apiurl);
                $response = Http::withHeaders([
                    'front-token' => env('FRONT_TOKEN')
                ])->get($apiurl);                

                if($response->ok()){

                    $json = $response->json();                    

                    if(Cache::has($cacheKey)) {
                        Cache::forget($cacheKey);
                    }
                    Cache::put($cacheKey, $json, now()->addMinutes(10));
                    $code = 200;

                } else {      
                    $code = 204;
                }
            }
        }

        if($attribute['table'] == 'olmo_menu'){
            $key = $dataPost['lang']."navigationmenu".$body['name_txt_general'];
            $cacheKey = md5($key);
            Cache::forget($cacheKey);
        }

        return  $this->serializeSinglePost($attribute, $dataPost, $code, $sendEmail);
    }   

    public function addLang($body, $attribute, $lang)
    {
        if (isset($attribute['lang'])) {
            if ($attribute['lang'] == 'true') {
                $body['locale_hidden_general'] = $lang;
            }
        }

        return $body;
    }

    public static function updateTemplateSlugMultilanguage($data, $table)
    {
        if($table == 'olmo_template'){
            foreach($data as $key=>$item){
                $lang = strpos($key, '-') ? substr($key, 4, 5) : substr($key, 4, 2);
                $validLang = HelpersLang::checkIfvalidLang($lang);
                if(strpos($key, '_txt_slugs') !== false AND $validLang){
                    Db::table('olmo_templateslug')->where('defaultid', $data['id'])->where('lang', $lang)->update(['slug' => $data[$key]]);
                    unset($data[$key]);
                }
            }

            return $data;
        }        
        
        return $data;
    }    

    public static function getTemplateSlugMultilanguage($data, $table)
    {
        if($table == 'olmo_template'){
            $slugs = Db::table('olmo_templateslug')->where('defaultid', $data['id'])->get();

            foreach($slugs as $slug){
                if(HelpersLang::checkIfvalidLang($slug->lang)){
                    $data['slug'.$slug->lang.'_txt_slugs'] = $slug->slug;
                }
            }

            return $data;
        }

        return $data;
    }

}
