<?php

namespace Olmo\Core\App\Http\Controller;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;

use Olmo\Core\App\Helpers\HelpersRoute;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersBreadcrumb;
use Olmo\Core\App\Helpers\HelpersMultistructure;

class FrontController extends Controller
{

    public function getStructure(Request $request)
    {
        $res = [];
        //assets
        $res['assets']['media'] = env('FILESYSTEM_DRIVER') == 's3' ? env('AWS_DOMAIN') . '/public' : env('APP_URL') . "/storage";
        $res['assets']['local'] = env('APP_URL') . "/storage";

        // langs, api, analytics
        $res = array_merge($res, HelpersMultistructure::getStructureLangs($request));
        $res['api'] = array('name' => 'olmo', 'version' => '1.0.0');
        $res['analytics'] = $this->getAnalytics(HelpersMultistructure::getAnalisticId($request));

        $langs = $res['i18n']['locales'];
        $items = HelpersMultistructure::getStructureTemplates($request);
        $defaultLang = HelpersLang::getDefaultLang();
        $i = 0;
        foreach ($items as $item) {
            $res['routes'][$i]['id'] = $item->name_txt_general;
            foreach ($langs as $key => $value) {
                $res['routes'][$i]['slug'][$key] = $this->getSlug($key, $item, $defaultLang);
            }
            $i++;
        }

        return $res;
    }

    public function getSlug($lang, $template, $defaultLang)
    {
        if ($defaultLang != $lang) {
            $slugtranslated = Db::table('olmo_templateslug')
            ->where('lang', $lang)
            ->where('defaultid', $template->id)->first();
            return $slugtranslated->slug;
        } else {
            return $template->slug_txt_slugs;
        }
        return '';
    }

    public static function getAnalytics($id)
    {

        return @Db::table('olmo_analytics')->where('id', $id)->select(
            'gtmheader_txt_analytics as gtm_header',
            'gtmbody_txt_analytics as gtm_body',
            'gtmheaderprod_txt_analytics as gtm_header_prod',
            'gtmbodyprod_txt_analytics as gtm_body_prod'
        )->first();
    }



    public function getSitemap(Request $request)
    {

        $results = [];
        $lang = $request->lang;
        $base_url = env('FRONT_URL');
        $templates = Db::table('olmo_template')->where('enabled_is_general', 'true')->where('activate_is_sitemap', 'true')->get();
        $defaultLang = HelpersLang::getDefaultLang();
        $i = 0;

        $model = [];

        foreach ($templates as $template) {
            array_push($model, $template->model_select_general);
        }

        $models = array_unique($model);

        foreach ($models as $model) {

            $pages = Db::table('olmo_' . $model)
                ->where('enabled_is_general', 'true')
                ->where('disable_is_sitemap', 'false')
                ->where('locale_hidden_general', $lang)->get();

            foreach ($pages as $page) {

                $templatepage = Db::table('olmo_template')->where('id', $page->template_id_general)->first();

                if ($templatepage->activate_is_sitemap === 'true' && $page->disable_is_sitemap !== 'true') {
                    $slug_template = $this->getSlug($lang, $templatepage, $defaultLang);
                    $slug_page = $page->slug_txt_general;
                    $slug = str_replace(":slug", $slug_page, $slug_template);

                    if(str_contains($slug, ':category')){
                        $cat = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::theAttributes();
                        $categoryFolderUrl = isset($cat['category-folder-url']) ? $cat['category-folder-url'] : false;
                        if(!$categoryFolderUrl){
                            $category = Db::table('olmo_category')->where('id', $page->category_id_general)->first();
                        } else {
                            $modelCat = explode('_', $categoryFolderUrl);
                            $category = Db::table('olmo_'.$modelCat[0])->where('id', $page->{$cat['category-folder-url']})->first();
                        }
                        $slug = str_replace(":category", $category->slug_txt_general, $slug);
                    }

                    $folder = $slug_page;

                    /** Lastmode Conversion */
                    if(isset($page->lastmod_read_information)){
                        $dateConversion = $page->lastmod_read_information ? $page->lastmod_read_information : $page->create_read_information;
                    } else {
                        $dateConversion = $page->lastmod_read_general ? $page->lastmod_read_general : $page->create_read_general;
                    }
                    $date = new \DateTime($dateConversion);
                    $lastmod = $date->format('Y-m-d');

                    $results[$i]['baseurl']    = $base_url;
                    $results[$i]['lang']       = $page->locale_hidden_general;
                    $results[$i]['lastmod']    = $lastmod;
                    $results[$i]['slug']       = $slug;
                    $results[$i]['folder']     = $folder;
                    $results[$i]['type']       = $model;
                    $results[$i]['changefreq'] = $page->overwrite_is_sitemap == 'false' ? $templatepage->frequency_select_sitemap : $page->frequency_select_sitemap;
                    $results[$i]['priority']   = $page->overwrite_is_sitemap == 'false' ? $templatepage->priority_select_sitemap : $page->priority_select_sitemap;
                    $i++;
                }
            }
        }

        return $results;
    }

    public function getRobots()
    {

        $env = env('APP_ENV');

        if ($env === 'production') {
            $query = Db::table('olmo_analytics')->where('id', '1')->first();
            return response($query->robotsprod_txt_analytics)->withHeaders(['Content-Type' => 'text/plain']);
        } else {
            $query = Db::table('olmo_analytics')->where('id', '1')->first();
            return response($query->robots_txt_analytics)->withHeaders(['Content-Type' => 'text/plain']);
        }
    }

    public function getAllcategories($request)
    {
        $categories =  ApiHelper::FrontResponseChild(Category::where('locale_hidden_general', $request->lang)->where('category_id_general', '')->get());
        $response = [];
        foreach ($categories as $key => $children) {
            $response[$key] = $children;
            $child = ApiHelper::getChildren('category', $categories[$key]['id']);
            foreach ($child as $subKey => $subChildren) {
                $subChild = ApiHelper::getChildren('category', $child[$subKey]['id']);
                $child[$subKey]['children'] = $subChild;
            }
            $response[$key]['children'] = $child;
        }
        return $response;
    }

    public static function getFullListImages($data)
    {
        $images = [];
        $url = env('FILESYSTEM_DRIVER') == 's3' ? env('AWS_DOMAIN') . '/public' : env('APP_URL') . "/storage";
        foreach($data as $key=>$items){
            // print_r(gettype($items).' - '.$key);
            // echo '<br>';
            if(gettype($items) == 'array'){
                foreach($items as $ke=>$item){
                    if($ke == 'src'){
                        array_push($images, $url.$item);
                    }
                    if(gettype($item) == 'array'){
                        foreach($item as $k=>$i){
                            if($k == 'src'){
                                array_push($images, $url.$i);
                            }
                            if(gettype($i) == 'array'){
                                foreach($i as $n=>$x){
                                    if($n == 'src'){
                                        array_push($images, $url.$x);
                                    }
                                    if(gettype($x) == 'array'){
                                        foreach($x as $q=>$e){
                                            if($q == 'src'){
                                                array_push($images, $url.$e);
                                            }
                                            if(gettype($e) == 'array'){
                                                foreach($e as $l){
                                                    if($l == 'src'){
                                                        array_push($images, $url.$l);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $images;
    }

    public static function getStructuredData($structured, $data, $lang)
    {
        //TODO:
        /**
         * process images as array
         * dominio + folders
         */
        $pathimg = env('FILESYSTEM_DRIVER') == 's3' ? env('AWS_DOMAIN') . '/public' : env('APP_URL') . "/storage";
        $string = $structured;
        foreach($string as $key=>$item){
            if(is_string($item)){
                if(str_contains($item, '{$locale}') && str_contains($item, '{$route}')){
                    $string[$key] = str_replace('{$locale}', $lang, $item);
                    $string[$key] = str_replace('{$route}', $data['route']['slug'][$lang], $string[$key]);
                } else if(str_contains($item, '{$locale}')){
                    $string[$key] = str_replace('{$locale}', $lang, $item);
                } else if(str_contains($item, '-img}')){
                    $first = str_replace('{$', '', $item);
                    $second = str_replace('}', '', $first);
                    $third = str_replace('-img', '', $second);
                    $string[$key] = $pathimg.$data[$third]['original']['src'];
                } else if(str_contains($item, '{$imagelist}')) {
                    $string[$key] = self::getFullListImages($data);
                } else if(str_contains($item, '{$')){
                    $first = str_replace('{$', '', $item);
                    $second = str_replace('}', '', $first);
                    $string[$key] = $data[$second];
                }
            } else {
                foreach($string[$key] as $k=>$i) {
                    if(is_string($i)){
                        if(str_contains($i, '{$locale}') && str_contains($i, '{$route}')){
                            $string[$key][$k] = str_replace('{$locale}', $lang, $i);
                            $string[$key][$k] = str_replace('{$route}', $data['route']['slug'][$lang], $string[$key][$k]);
                        } else if(str_contains($i, '{$locale}')){
                            $string[$key][$k] = $lang;
                        } else if(str_contains($i, '-img}')){
                            $first = str_replace('{$', '', $item);
                            $second = str_replace('}', '', $first);
                            $third = str_replace('-img', '', $second);
                            $string[$key] = $pathimg.$data[$third]['original']['src'];
                        } else if(str_contains($i, '{$')){
                            $firsts = str_replace('{$', '', $i);
                            $seconds = str_replace('}', '', $firsts);
                            $string[$key][$k] = $data[$seconds];
                        }
                    } else {
                        foreach($string[$key][$k] as $ks=>$ii) {
                            if(str_contains($ii, '{$locale}')){
                                $string[$key][$k][$ks] = $lang;
                            } else if(str_contains($ii, '-img}')){
                                $first = str_replace('{$', '', $item);
                                $second = str_replace('}', '', $first);
                                $third = str_replace('-img', '', $second);
                                $string[$key] = $pathimg.$data[$third]['original']['src'];
                            } else if(str_contains($ii, '{$')){
                                $firstss = str_replace('{$', '', $ii);
                                $secondss= str_replace('}', '', $firstss);
                                $string[$key][$k][$ks] = $data[$secondss];
                            }
                        }
                    }
                }
            }
        }
        return $string;
    }

    public function getPage(Request $request)
    {
        $page = [];
        $lang = $request->lang;
        $cacheKey = $request->url();
        $last = substr($cacheKey, -1);
        $cacheKey = $last == '/' ? $cacheKey : $cacheKey.'/';
        $cacheKey = md5($cacheKey);
        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
            exit();
        }
        /**
         * Check the reqeust and determinate if is valid
         */
        $getPost = HelpersRoute::getThePostByRoute($request);
        if (!$getPost) {
            return abort(404);
        }

        $page = HelpersSerializer::routeResponse($getPost['data'], 'request', model: $getPost['model'], clean: env('CLEAN_ROUTE_JSON', false));
        $page['breadcrumb'] = HelpersBreadcrumb::breadcrumb($lang, $getPost['data'], $getPost['model'], $request);
        $page['prevpost'] = self::getNextPrevPost($getPost['model'], $getPost['data']['id'], 'prev');
        $page['nextpost'] = self::getNextPrevPost($getPost['model'], $getPost['data']['id'], 'next');
        $page['children'] = self::getLoopChildren($getPost['model'], $getPost['data']['id'], $lang);

        $page['seo'] = HelpersRoute::Seo($getPost['data']);

        $page['route'] = HelpersRoute::Route($lang, $getPost['data'], $getPost['model'], $request);
        
        if(isset($page['template']['structureddata'])){
            $page['structureddata'] = self::getStructuredData($page['template']['structureddata'], $page, $lang);
        }

        if(!Cache::has($cacheKey) AND $getPost['cache']){
            Cache::put($cacheKey, $page, now()->addMinutes(10));
        }

        if(isset($page['statuscode'])){
            if($page['statuscode'] != ''){
                return response($page, (int)$page['statuscode']);
            }
        }

        return response($page, 200);
    }

    public static function getNextPrevPost($table, $position, $type)
    {

        $getAllquery = Db::table('olmo_' . $table)->get()->toArray();

        usort($getAllquery, function ($a, $b) {
            return ($a->position_ord_general < $b->position_ord_general) ? -1 : 1;
        });

        $id = 0;

        foreach ($getAllquery as $index => $post) {
            if ($post->id == $position) {
                if ($type == 'next') {
                    $id = $index + 2;
                } else if ($type == 'prev') {
                    $id = $index;
                }
            }
        }

        $query = Db::table('olmo_' . $table)->where('id', $id)->first();
        if (!$query) {
            $id = $type == 'next' ? $getAllquery[0]->id : $getAllquery[count($getAllquery) - 1]->id;
            $query = Db::table('olmo_' . $table)->where('id', $id)->first();
        }
        $normalize = HelpersSerializer::cleanKeyObj($query);

        return $id;
    }

    public static function getLoopChildren($table, $id, $lang)
    {
        /**
         * Check if the column exist
         */
        $isColExist = Schema::connection("mysql")->hasColumn('olmo_' . $table, $table . '_id_general');
        if ($isColExist) {
            /** Extract the children */
            // $response = json_decode(json_encode(ApiHelper::getChildren($table, $id)), true);
            /**
             * Loop through the children and assign other children till now just one step
             * need to understand how to make infinite
             * */
            // foreach ($response as $key => $value) {
            //     $response[$key]['children'] = json_decode(json_encode(ApiHelper::getChildren($table, $response[$key]['id'])), true);
            // }
            // return $response;
        }
        return null;
    }

    public static function getChildren($table, $id)
    {
        $isColExist = Schema::hasColumn('olmo_' . $table, $table . '_id_general');
        if ($isColExist) {
            $response = DB::table('olmo_' . $table)->where($table . '_id_general', $id)->where('enabled_is_general', 'true')->get();
            return HelpersSerializer::routeResponse($response, 'stdClass');
        }
        return null;
    }

    public function getAllPosts(Request $request)
    {

        $lang = $request->lang;
        $model = $request->model;
        $fields = $request->fields ? $request->fields : [];
        $forcelang = $request->forcelang ? $request->forcelang : null;
        $conditions = isset($request->conditions) ? $request->conditions : [];
        $order = $request->order ? $request->order : 'ASC';
        $orderby = $request->orderby ? $request->orderby : 'position_ord_general';

        $page = isset($request->page) ? $request->page : 0;
        $offset = isset($request->offset) ? $request->offset : 0;
        // $salt = @$request->offset;
        // $q = @$request->q;
        // $sensibility = @$request->sensibility;
        // $select = @$request->select;

        $nextSkip = ($page * $offset);
        $skip = $nextSkip - $offset;

        $fullData = [];

        /**
         * Work with conditions
         */
        $arrayCondition = [];
        if(count($conditions) > 0){
            foreach($conditions as $item){
                $singleArrayCondition = [$item['key'], $item['operation'], $item['value']];
                array_push($arrayCondition, $singleArrayCondition);
            }
        }

        $table = 'olmo_' . $model;
        if(HelpersData::isDefaultModel($model)){
            $attribute = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::theAttributes();
        } else {
            $attribute = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::theAttributes();
        }

        $cache = $attribute['cache'];

        if($cache == true){            
            $cacheKey = 'allmodel-'.$model.md5($offset.$page.$orderby.$order.$forcelang.$lang).md5(json_encode($conditions, true)).md5(json_encode($fields, true));
            if(Cache::has($cacheKey)){
                return Cache::get($cacheKey);
                exit();
            }
        }

        if($attribute['lang'] && !$forcelang){
            if(count($conditions) == 0){
                $fullData = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->get();
                $data = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                $nextData = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->skip($nextSkip)->take($offset)->get();
            } else {
                $fullData = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->where($arrayCondition)->get();
                $data = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->where($arrayCondition)->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                $nextData = Db::table($table)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->where($arrayCondition)->skip($nextSkip)->take($offset)->get();
            }
        } else if($forcelang != '' OR $forcelang != null) {
            $valind = Helperslang::validLang($forcelang);
            if($valind){
                if(count($conditions) == 0){
                    $fullData = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->get();
                    $data = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                    $nextData = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->skip($nextSkip)->take($offset)->get();
                } else {
                    $fullData = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->where($arrayCondition)->get();
                    $data = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->where($arrayCondition)->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                    $nextData = Db::table($table)->where('locale_hidden_general', $forcelang)->where('enabled_is_general', 'true')->where($arrayCondition)->skip($nextSkip)->take($offset)->get();
                }
            }
        } else {
            if(count($conditions) == 0){
                $fullData = Db::table($table)->where('enabled_is_general', 'true')->get();
                $data = Db::table($table)->where('enabled_is_general', 'true')->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                $nextData = Db::table($table)->where('enabled_is_general', 'true')->skip($nextSkip)->take($offset)->get();
            } else {
                $fullData = Db::table($table)->where('enabled_is_general', 'true')->where($arrayCondition)->get();
                $data = Db::table($table)->where('enabled_is_general', 'true')->where($arrayCondition)->orderBy($orderby, $order)->skip($skip)->take($offset)->get();
                $nextData = Db::table($table)->where('enabled_is_general', 'true')->where($arrayCondition)->skip($nextSkip)->take($offset)->get();
            }
        }

        /**
         * Remove the fields are not in the request
         * */
        if(count($fields) > 0){
            foreach($data as $item){
                foreach($item as $key=>$value){
                    if(!in_array($key, $fields)){
                        unset($item->$key);
                    }
                }
            }
        }

        $data = json_decode(json_encode($data,true),true);

        if(env('CLEAN_ALLMODEL_JSON', false)){
            foreach($data as $key => $el){

                foreach ($el as $k => $element) {
    
                    if(is_iterable($data[$key][$k]) && !empty($data[$key][$k])){
                        $res = HelpersSerializer::cleanEmptyKey($data[$key][$k]);
                        if( empty($res) ){
                            unset($data[$key][$k]);
                        }else{
                            $data[$key][$k] = $res;
                        }
                    }else{
                        if( empty($data[$key][$k]) ){
                            unset($data[$key][$k]);
                        }
                    }
    
                }
    
            }
        }

        $response = [];
        $array = [];        
        foreach ($data as $item) {
            $item = HelpersSerializer::routeResponse($item, 'array', 0, 'allmodels');
            if(env('CLEAN_ALLMODEL_JSON', false)){
                foreach($item as $key => $el){
        
                    if(is_iterable($item[$key]) && !empty($item[$key])){
                        $res = HelpersSerializer::cleanEmptyKey($item[$key]);
                        if( empty($res) ){
                            unset($item[$key]);
                        }else{
                            $item[$key] = $res;
                        }
                    }else{
                        if( empty($item[$key]) ){
                            unset($item[$key]);
                        }
                    }
        
                }
            }
            array_push($array, $item);
        }        

        $response['response'] = $array;
        $response['total'] = count($data);
        $response['nextPage'] = count($nextData);
        $response['fulltotal'] = count($fullData);
        $response['pagecurrent'] = intval($page);
        $response['pagenumber'] = count($fullData) != 0 && count($data) != 0 ? ceil(count($fullData) / count($data)) : 0;
        
        if($cache == true){
            Cache::tags(['allmodel'])->put($cacheKey, $response, now()->addMinutes(10));
        }        

        return response($response, 200);
    }

}
