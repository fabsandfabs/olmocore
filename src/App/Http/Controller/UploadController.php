<?php

namespace Olmo\Core\App\Http\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersImage;
use Olmo\Core\App\Helpers\HelpersVideo;
use Olmo\Core\App\Helpers\HelpersGif;
use Olmo\Core\App\Http\Controller\StorageController;

trait UploadController
{

   public static function uploadChunk()
   {

      // $chunk_id     = @$_REQUEST['id'];
      $data = file_get_contents('php://input');
      $filename =  @$_REQUEST['fileName'];
      $temp_data = Cache::get($filename);
      $current_data = $temp_data . $data;
      Cache::put($filename, $current_data, 1200);

      $arr['data']         = null;
      $arr['isSuccess']    = true;
      $arr['errorMessage'] = null;

      return $arr;
   }

   // public static function insertMediaElement($truename, $filename, $model = 'filemanager', $public = 'true'){
   //    $checkTrueName = DB::table('olmo_storage')->where('truename', $truename)->get()->count();

   //    $storage_id = DB::table('olmo_storage')->insertGetId([
   //       'filename'    => $filename,
   //       'truename'    => $truename,
   //       'model'      => $model,
   //       'public'     => $public ? $public : 'true',
   //       'type'       => pathinfo($truename)['extension'],
   //    ]);

   //    if ($checkTrueName > 0) {
   //       [$name, $extension] = explode('.', $truename);
   //       $newTruename = $truename[0] . '-' . $storage_id . '.' . $truename[1];
   //       DB::table('olmo_storage')->where('id', $storage_id)->update(['truename' => $newTruename]);
   //       $public_path = $folders !== '' ? '/media/' . $newTruename : '/media/' . $newTruename;
   //       $truename = $truename[0].'.'.$truename[1];
   //    }
      
   //    return $storage_id;
   // }

   public static function completeChunkManager($request)
   {

      $filename = $request->input('fileName');
      $public = $request->input('public');
      $truename = HelpersData::normalizestring($request->input('trueName'));
      $folders = $request->input('foldersName');
      $current_data = Cache::get($filename);
      $path = 'public/media/' . substr($folders, 1) . '/' . $filename;
      $res = Storage::disk('local')->put($path, $current_data);

      $public_path = $folders !== '' ? '/media/' . substr($folders, 1) . $truename : '/media/' . $truename;

      if ($res) {
         $res = true;
      } else {
         $res = false;
      }

      $truename      = substr($folders, 1) . $truename;
      $checkTrueName = DB::table('olmo_storage')->where('truename', $truename)->get()->count();

      $storage_id = DB::table('olmo_storage')->insertGetId([
         'filename'    => $filename,
         'truename'    => $truename,
         'model'      => 'filemanager',
         'public'     => $public ? $public : 'true',
         'type'       => pathinfo($truename)['extension'],
      ]);

      $file = DB::table('olmo_storage')->where('id', $storage_id)->first();

      $assetOriginal = false;
      $defaultAssets = false;
      if(str_contains($folders, 'olmoemail')){
         $assetOriginal = false;
      } else if($file->type == 'mp4'){
         $assetOriginal = HelpersVideo::checkOriginalVersionsAndCreateIt($file);
      } else if($file->type == 'gif'){
         $assetOriginal = HelpersGif::checkOriginalVersionsAndCreateIt($file);
      } else if($file->type == 'jpg' OR $file->type == 'png' OR $file->type == 'jpeg'){
         $assetOriginal = HelpersImage::checkOriginalVersionsAndCreateIt($file);
         $defaultAssets = HelpersImage::checkDefaultVersionsAndCreateThem($file);
      }

      if ($checkTrueName > 0) {
         $truename = explode('.', $truename);
         $newTruename = $truename[0] . '-' . $storage_id . '.' . $truename[1];
         DB::table('olmo_storage')->where('id', $storage_id)->update(['truename' => $newTruename]);
         $public_path = $folders !== '' ? '/media/' . $newTruename : '/media/' . $newTruename;
         $truename = $truename[0].'.'.$truename[1];
      }

      $arr['data'] = $public_path;
      $arr['path'] = $truename;
      $arr['filename'] = $filename;
      $arr['id'] = $storage_id;
      $arr['isSuccess'] = $res;
      $arr['errorMessage'] = null;
      $arr['assetOriginal'] = $assetOriginal;
      $arr['assetsDefault'] = $defaultAssets;

      /**
       * Empty and generate cache
       */
      StorageController::storageClearCacheFolder(substr_replace($folders, '', -1));
      // StorageController::serializeAllFile();

      Cache::forget($filename);

      return $arr;
   }
}
