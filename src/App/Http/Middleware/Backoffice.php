<?php

namespace Olmo\Core\App\Http\Middleware;

use Closure;

class Backoffice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if ($request->input('token') !== 'my-secret-token') {
        //     return redirect('home');
        // }

        return $next($request);
    }
}