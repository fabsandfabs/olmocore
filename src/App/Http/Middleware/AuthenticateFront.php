<?php

namespace Olmo\Core\App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use \Illuminate\Http\Request;

class AuthenticateFront
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */

    public function handle(Request $request, Closure $next)
    {

        if(env('APP_PROTECTION', false) == 'true'){
            $token = $request->header('front-token');

            if($token){
                if($token == env('FRONT_TOKEN')){
                    return $next($request);
                }
            }
    
            $ip = $request->ip();
            $ipsallow = explode(',', env('IP_ALLOW', ''));
    
            if(in_array($ip, $ipsallow)){
                return $next($request);
            }
            
            $array = [
                'ip' => $ip,
                'token' => $token,
            ];

            Log::channel('authfront')->info($array);

            if(env('APP_ENV') == 'production' OR env('APP_ENV') == 'stage'){
                return abort(403);
            }
        }

        return $next($request);

    }

}