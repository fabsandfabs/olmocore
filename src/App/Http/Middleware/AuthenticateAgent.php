<?php

namespace Olmo\Core\App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Olmo\Core\App\Helpers\HelpersAgent; 

 
class AuthenticateAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {   
        $bool = HelpersAgent::Authenticate($request);
        if($bool){
            return $next($request);
        }
        return response([],403);
    }
}