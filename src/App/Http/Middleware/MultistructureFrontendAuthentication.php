<?php

namespace Olmo\Core\App\Http\Middleware;

use Closure;
use \Illuminate\Http\Request;
use Olmo\Core\App\Helpers\HelpersMultistructure as MultiStructure;

class MultistructureFrontendAuthentication
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */

    public function handle(Request $request, Closure $next)
    {
        $structure = MultiStructure::get($request);

        if ($structure != false) {
            $request->merge(['structure' => $structure]);
        }
        return $next($request);
    }

}
