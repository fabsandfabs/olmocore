<?php

namespace Olmo\Core\App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Olmo\Core\Loaders\OlmoLoadersContainers;

class OlmoSeedServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadMigrationsFromContainers();

    }    

    public function register()
    {
        //
    }

    /**
     * This is something must be changed
     */
    public function loadMigrationsFromContainers()
    {
        foreach (OlmoLoadersContainers::getAllContainerPaths() as $containerPath) {
            $containerMigrationPath = $containerPath."/Data/Seeders";       
            if(File::isDirectory($containerMigrationPath)){
                $this->loadMigrationsFrom($containerMigrationPath);
            }            
        }     
    }    
   
}