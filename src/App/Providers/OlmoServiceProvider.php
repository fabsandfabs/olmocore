<?php

namespace Olmo\Core\App\Providers;

use Illuminate\Support\ServiceProvider;

class OlmoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        // $this->loadMigrationsFrom(__DIR__.'/../../Container/Backoffice/Page/Data/Migrations');
    }

    public function register()
    {
        //
    }
}