<?php

namespace Olmo\Core\App\Crawlers;

use DOMDocument;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class MyCrawlObserver extends CrawlObserver {

    private $content;

    public function __construct() {
        $this->content = NULL;
    }  
    /**
     * Called when the crawler will crawl the url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {
        Log::info('willCrawl',['url'=>$url]);
    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    ): void
    {
        $doc = new DOMDocument();
        @$doc->loadHTML($response->getBody());

        $path = storage_path('app/public/media');
        $fp = fopen($path."temp-crawler-file.csv", 'a');
        fputcsv($fp, ["url+$url","code+".$response->getStatusCode()]);
        fclose($fp);

    }

     /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \GuzzleHttp\Exception\RequestException $requestException
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void
    {

        $path = storage_path('app/public/media');
        $fp = fopen($path."temp-crawler-file.csv", 'a');

        fputcsv($fp, ["url+$url", "code+".$requestException->getResponse()->getStatusCode()]);
        fclose($fp);

    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        Log::info("finishedCrawling");
    }
}