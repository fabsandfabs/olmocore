<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Illuminate\Support\Facades\Cache;

class HelpersFilter
{

    /**
     * Body Request example
     */
    
    // {
    //     "category": {
    //         "type": "category_multid_general",
    //         "values": [34,88]
    //     },
    //     "values": [
    //         [2,4],
    //         [5]
    //     ],
    //     "fields": [
    //          "id", 
    //          "slug_txt_general", 
    //          "template_id_general", 
    //          "cover_filemanager_general", 
    //          "abstract_editor_content", 
    //          "altcover_txt_general", 
    //          "property_props_general"
    //      ]
    // }    

    public static function filter($request)
    {

        $model = $request->model;
        $values = $request->values;
        $fields = $request->fields;
        $category = @$request->category;
        $macrocategory = @$request->macrocategory;
        $fieldselect = @$request->fieldselect;
        $page = isset($request->page) ? $request->page : 0;
        $offset = isset($request->offset) ? $request->offset : 0;        
        $lang = $request->lang;
        $ordertype = $request->ordertype ? $request->ordertype : 'position_ord_general';
        $filter = '';

        $nextSkip = ($page * $offset);
        $skip = $nextSkip - $offset;

        $offsetString = $offset != 0 ? 'LIMIT '.$offset : '' ;
        $offsetString = $page > 1 ? 'LIMIT '.$offset.' OFFSET '.(intval($offset)*intval($page-1)) : $offsetString;

        $fullData = [];

        $cache = env('FILTERS_CACHE', false);

        if($cache == true){
            $cacheKey = 'filter-'.$model.md5($offset.$page.$lang.json_encode($fieldselect, true)).md5(json_encode($values, true)).md5(json_encode($fields, true)).md5(json_encode($macrocategory, true)).md5(json_encode($category, true));
            if(Cache::has($cacheKey)){
                return Cache::get($cacheKey);
                exit();
            }            
        }

        foreach($values as $value){
            $regex   = "'";
            foreach($value as $v){
                $regex .= ','.$v.',|';
            }
            $regex = substr($regex, 0, -1)."'";
            $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex AND";
        }

        if(count($values) > 0){
            $filter = substr($filter, 0, -3);    
        }

        if($category != ''){
            if(!empty($category['values'])){
                $type = $category['type'];
                $values = $category['values'];
                if($filter){
                    $filter .=" AND CONCAT(',',".$type.",',') REGEXP ";
                } else {
                    $filter .=" CONCAT(',',".$type.",',') REGEXP ";
                }
                $regex = "'";
                foreach($values as $v){
                    $regex .= ','.$v.',|';
                }
                $regex = substr($regex, 0, -1)."'";
                $filter .= $regex;
            }
        }

        if($macrocategory != ''){
            if(!empty($macrocategory['values'])){
                $type = $macrocategory['type'];
                $values = $macrocategory['values'];
                if($filter){
                    $filter .=" AND CONCAT(',',".$type.",',') REGEXP ";
                } else {
                    $filter .=" CONCAT(',',".$type.",',') REGEXP ";
                }
                $regex = "'";
                foreach($values as $v){
                    $regex .= ','.$v.',|';
                }
                $regex = substr($regex, 0, -1)."'";
                $filter .= $regex;
            }
        }

        $fieldlist = '';
        if($fieldselect != ''){
            foreach($fieldselect as $key => $field){
                if($fieldlist == ''){
                    if(!empty($field)){
                        $fieldlist .= "".$key." IN (".implode(",", $field).") ";
                    }
                } else {
                    if(!empty($field)){
                        $fieldlist .= "AND ".$key." IN (".implode(",", $field).")";
                    }
                }
            }
        }

        /** Make the request */
        if($fieldlist != '' AND $filter != ""){
            if($offsetString){
                $sql = "SELECT * FROM olmo_".$model." WHERE ".$fieldlist." AND ".$filter." AND enabled_is_general = 'true' AND locale_hidden_general = '$lang' ORDER BY $ordertype ASC ";
                $fullData = DB::select($sql);                
            }            
            $sql = "SELECT * FROM olmo_".$model." WHERE ".$fieldlist." AND ".$filter." AND enabled_is_general = 'true' AND locale_hidden_general = '$lang' ORDER BY $ordertype ASC ".$offsetString;
            $response = DB::select($sql); 
        } else if($filter != ""){
            if($offsetString){
                $sql = "SELECT * FROM olmo_".$model." WHERE ".$filter." AND enabled_is_general = 'true' AND locale_hidden_general = '$lang' ORDER BY $ordertype ASC ";
                $fullData = DB::select($sql);                
            }            
            $sql = "SELECT * FROM olmo_".$model." WHERE ".$filter."AND enabled_is_general = 'true' AND locale_hidden_general = '$lang' ORDER BY $ordertype ASC ".$offsetString;
            $response = DB::select($sql); 
        } else {
            $response = DB::table("olmo_".$model)->where('enabled_is_general', 'true')->orderBy($ordertype)->where('locale_hidden_general', $lang)->skip($skip)->take($offset)->get();
        }        
        
        /** 
         * Remove the fields are not in the request 
         * */
        foreach($response as $item){
            foreach($item as $key=>$value){
                if(!in_array($key, $fields)){
                    unset($item->$key);
                }
            }
        }

        /** Clean it */
        /* TODO: verificare come mai una chiamata ai filtri con category id ma senza values restituisce tra i valids anche valore 0 */
        $responseClean = count($response) > 0 ? HelpersSerializer::cleanKeyArray($response) : [];
        
        
        /** 
         * Get all the active property 
         * and remove the duplicates value
         * */
        $propertyValue = [];
        foreach($responseClean as $item){
            if(isset($item['property'])){
                $values = explode(',', $item['property']);
                foreach($values as $value){
                    array_push($propertyValue, intval($value));
                }
            }
        }        
        $propertyValid = array_unique($propertyValue);    
        $propertyValid = array_values($propertyValid); // understand why value Zero

        /**
         * Create the fields relations
         */
        $responseSingleItem = [];
        foreach($response as $item){            
            $singleItem = HelpersSerializer::routeResponse($item);
            array_push($responseSingleItem, $singleItem);
        }

        $data['count'] = count($responseSingleItem);
        $data['elements'] = $responseSingleItem;
        $data['valids'] = $propertyValid;
        if($offsetString){
            $data['fulltotal'] = count($fullData);
            $data['pagenumber'] = count($fullData) != 0 && $offset != 0 ? ceil(count($fullData) / $offset) : 0;
            $data['pagecurrent'] = intval($page);
        }

        if($cache == true){
            Cache::tags(['filters'])->put($cacheKey, $data, now()->addMinutes(10));
        }

        return $data;

    }

}
