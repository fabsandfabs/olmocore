<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Http\Controller\BackController;

class HelpersItem
{

    public static function getItem($data)
    {

        $product = [];

        if ($data != '') {
            $ids = explode(',', $data);
            $items = [];
            $i      = 0;

            foreach ($ids as $id) {
                $id = Db::table('olmo_customeritem')->where('id', $id)->where('name_txt_general', '!=', '')->select(['name_txt_general', 'enabled_is_general', 'default_is_address',  'id'])->first();
                /**
                 * if name_txt_general is not filled means the user didn't saved it
                 */
                if($id){

                    foreach ($id as $key => $val) {

                        $item = [];
    
                        $item['id'] = (string)$key;
                        $item['label'] = HelpersData::setLabel((string)$key);
                        $item['type'] = HelpersData::setTypeList((string)$key);
                        $item['value'] = $val;
                        // $item['value'] = BackController::getListPostValue($val, $key, $attribute['table'], $value->id);
    
                        $items[$i][] = $item;
                    }
    
                    $i++;

                }

            }
            $product = $items;
        }

        return $product;
    }

    public static function createNewvalue($request, $attribute)
    {
        $lang = $request->lang;
        $id = $request->input('id');

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing($attribute['table']);

        /** Flip values in the keys */
        $columns_flip = array_flip($columns);
        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function (&$v, $k, $lang) {
            if (HelpersData::setLabel($k) == 'locale') {
                $v = $lang;
            } else if (HelpersData::getType($k) == 'number') {
                $v = 0;                
            } else if (HelpersData::getType($k) == 'readonly') {
                $v = Carbon::now();
            } else if (HelpersData::getType($k) == 'spinner') {
                $v = 0;
            } else {
                $v = "";
            }
        }, $lang);

        unset($columns_flip['id']);

        /**
         * Prefill some field to make the update possible
         */
        if (isset($columns_flip['postid_hidden_general'])) {
            $columns_flip['postid_hidden_general'] = $id;
        }
        if (isset($columns_flip['locale_hidden_general'])) {
            $columns_flip['locale_hidden_general'] = $lang;
        }
        if (isset($columns_flip['parentid_hidden_content'])) {
            $columns_flip['parentid_hidden_content'] = $id;
        }
        if (isset($columns_flip['customer_hidden_content'])) {
            $columns_flip['customer_hidden_content'] = $id;
        }

        /** Insert the post in the DB */
        $insert = Db::table($attribute['table'])->insertGetId($columns_flip);
        /** 
         * Update the product with the id 
         * First check if it is empty
         * */
        $product = Db::table($attribute['tablerelation'])->where('id', $id)->first();
        $product = json_decode(json_encode($product, true), true);
        $model = substr($attribute['table'], 5);        
        $table = $model.'_list_general';
        $productids = $product[$table] == '' ? $insert : $product[$table].','.$insert;
        Db::table($attribute['tablerelation'])->where('id', $id)->update([$table => $productids]);

        /**
         * If there is a quantity field update the table olmo_quantity
         */
        if(isset($columns_flip['qty_spin_general'])){
            Db::table('olmo_quantity')->insertGetId([
                'quantity' => 0,
                'prod_id' => $id,
                'type' => 'item'
            ]);
        }        

        /**
         * Retrieve the post and serialize it
         */
        // $data = Db::table($attribute['table'])->where('id', $insert)->first();

        $modelrelation = substr($attribute['tablerelation'], 5);
        $response['model'] = $modelrelation;
        $response['modelid'] = $id;
        $response['id'] = (int)$insert;        

        return response($response, 200);
    }

    public static function serializeValueUpdating($request, $attribute)
    {

        $required = HelpersRequiredField::checkRequired($request, $attribute);
        $rules = HelpersRulesField::checkRules($request, $attribute);

        if (count($required) > 0 or count($rules) > 0) {
            return count($required) ?
                response($required, 400) :
                response($rules, 400);
        } else {
            return self::updateRequestPost($request, $attribute);
        }
    }

    public static function updateRequestPost($request, $attribute)
    {
        $dataPost = HelpersData::idLangPost($request);

        /** Check if the request is a json */
        // $isJson = HelpersData::isJson($request);
        // if ($isJson) {
        //     return response('Body not a JSON', 400);
        // }

        $body = HelpersData::normalizeBody($request);

        $response = Db::table($attribute['table'])->where('id', $dataPost['id'])->update($body);

        /**
         * If there is a quantity field update the table olmo_quantity
         */
        if(isset($body['qty_spin_general'])){
            $update = Db::table('olmo_quantity')->where('id', $dataPost['id'])->update([
                'quantity' => $body['qty_spin_general'],
                'prod_id' => $dataPost['id'],
                'type' => 'item'
            ]);
            if(!$update){
                Db::table('olmo_quantity')->insertGetId([
                    'quantity' => $body['qty_spin_general'],
                    'prod_id' => $dataPost['id'],
                    'type' => 'item'
                ]);                
            }
        }        

        return  response($response, 200);
    }
}
