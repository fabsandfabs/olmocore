<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
// use App\Ship\Parents\Controllers\ApiCollection;
// use App\Ship\Parents\Controllers\ApiHelper;

use Olmo\Core\Containers\Backoffice\Template\Models\Template;
use Olmo\Core\Containers\Backoffice\Category\Models\Category;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersCollection;
use Olmo\Core\App\Helpers\HelpersFilemanager;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersSerializer;

class HelpersRoute
{


    public static function backofficeUrl($attribute, $data, $defaultlang, $urlbase, $fronturllang = true){
        $route = isset($attribute['route']) ? $attribute['route'] : false;
        if($route){
            $template = $data['template_id_general'];

            if($template != ''){
                $slug = $data['slug_txt_general'];
                $lang = $data['locale_hidden_general'] != '' ? $data['locale_hidden_general'] : $defaultlang;

                if($defaultlang == $lang) {
                    $temaplteData = Db::table('olmo_template')->where('id', $template)->first();
                    $temaplteSlug = $temaplteData->slug_txt_slugs;
                } else {
                    $temaplteData = Db::table('olmo_templateslug')->where('lang', $lang)->where('defaultid', $template)->first();
                    $temaplteSlug = $temaplteData->slug;
                }

                $pagePath = $temaplteSlug;

                if(str_contains($pagePath, ':subcategory') && str_contains($pagePath, ':category') && str_contains($pagePath, ':slug')){

                    $pagePath = str_replace(":slug", $slug, $temaplteSlug);
                    $frontlang = $fronturllang ? $lang.'/' : '';
                    $path = $urlbase.'/'.$frontlang.$slug;

                    $subcategory = isset($attribute['subcategory-folder-url']) ? $attribute['subcategory-folder-url'] : '';
                    $subcategoryModelName = explode('_', $subcategory);
                    $subcategoryModel = Db::table('olmo_'.$subcategoryModelName[0])->where('id', $data[$subcategory])->first();
                    if($subcategoryModel){
                        $pagePath = str_replace(":subcategory", $subcategoryModel->slug_txt_general, $pagePath);
                        $path = $urlbase.'/'.$lang.'/'.$pagePath;
    
                        $category = isset($attribute['category-folder-url']) ? $attribute['category-folder-url'] : '';
                        $categoryModelName = explode('_', $category);
                        $categoryModel = Db::table('olmo_'.$categoryModelName[0])->where('id', $data[$category])->first();
                        $pagePath = str_replace(":category", $categoryModel->slug_txt_general, $pagePath);
                        $frontlang = $fronturllang ? $lang.'/' : '';
                        $path = $urlbase.'/'.$frontlang.$pagePath.'/';
                        return $path;
                    }
                    return '';

                } else if(str_contains($pagePath, ':category') && str_contains($pagePath, ':slug')){

                    $pagePath = str_replace(":slug", $slug, $temaplteSlug);
                    $path = $urlbase.'/'.$lang.'/'.$slug;

                    $category = isset($attribute['category-folder-url']) ? $attribute['category-folder-url'] : '';
                    $categoryModelName = explode('_', $category);
                    $categoryModel = Db::table('olmo_'.$categoryModelName[0])->where('id', $data[$category])->first();
                    $pagePath = str_replace(":category", $categoryModel->slug_txt_general, $pagePath);
                    $frontlang = $fronturllang ? $lang.'/' : '';
                    $path = $urlbase.'/'.$frontlang.$pagePath.'/';
                    return $path;

                } else if(str_contains($pagePath, ':slug')){

                    $pagePath = str_replace(":slug", $slug, $temaplteSlug);
                    $frontlang = $fronturllang ? $lang.'/' : '';
                    $path = $urlbase.'/'.$frontlang.$pagePath.'/';
                    return $path;

                }

                $endingpath = $pagePath != "" ? $pagePath."/" : $pagePath;
                $frontlang = $fronturllang ? $lang.'/' : '';
                $path = $urlbase.'/'.$frontlang.$endingpath;

                return $path;
            }

        }

        return '';

    }

    public static function Seo($e){

        $seo = [];
        $seo['meta_title']       = $e['metatitle_txt_seo'];
        $seo['meta_description'] = $e['metadesc_txtarea_seo'];
        $seo['meta_keywords']    = $e['metakw_txt_seo'];
        $seo['follow']           = $e['follow_select_seo'];
        $seo['index']            = $e['index_select_seo'];
        $seo['og_title']         = $e['ogtitle_txt_seo'];
        $seo['og_description']   = $e['ogdesc_txtarea_seo'];
        $ogimg = HelpersFilemanager::getSinglePathFmanagerById($e['ogimg_filemanager_seo']);
        $seo['og_img']           =  $ogimg == '' ? HelpersFilemanager::getOgImg() : $ogimg;

        return $seo;

    }

    public static function transField($model_id,$lang,$model,$field){

        $default_lang  = ApiHelper::validLang()['default_lang'];
        $e             = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$model_id)->first();
        $parentid      = @$e->parentid_hidden_general;

        if($lang == $default_lang){
            if($parentid != '' || $parentid != ''){
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$parentid)->first();
            } else {
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$e->id)->first();
            }
        } else {
            if($parentid != '' || $parentid != '')
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$e->id)->first();
            else
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('parentid_hidden_general',$e->id)
                ->where('locale_hidden_general',$lang)->first();
        }

        return @$e1->{$field};

    }

    public static function Route($currentlang, $data, $model, $request){

        $route = [];
        $langs = HelpersLang::validLangFront();
        $templateDefault = Template::where('id', $data['template_id_general'])->first();
        $defaultlang = Helperslang::getDefaultLang();
        $route['id'] = @$templateDefault->name_txt_general;
        $route['locale'] = $currentlang;

        foreach($langs as $lang){
            if($lang == $defaultlang){
                $pattern = $templateDefault->slug_txt_slugs;
            } else {
                $templateSlug = Db::table('olmo_templateslug')->where('lang', $lang)->where('defaultid', $data['template_id_general'])->first();
                $pattern = $templateSlug->slug;
            }
            $slug = self::GetSlugTrans($data, $lang, $defaultlang, $model);
            $slug = str_replace(":slug", $slug, $pattern);

            // if(strpos($pattern, ':subcategory')  !== false) {
            //     $category     = Category::where('slug_txt_general', $request->subcategory)->first();
            //     $category     = self::GetSlugTrans($category,$ln,$lang,'category');
            //     $slug         = str_replace(":subcategory",$category,$slug);

            //     if(strpos($pattern,':category')  !== false) {
            //         $category = ('App\Containers\Backoffice\Category\Models\Category')::where('slug_txt_general', $request->subcategory)->first();
            //         $slug = str_replace(":category", $category->slug_txt_general, $slug);
            //     }

            // dd($slug);
            // } else

            if(strpos($pattern, ':category')  !== false) {

                $category = self::GetSlugCategoryTrans($data, $lang, $defaultlang, $model);
                $slug = str_replace(":category", $category, $slug);

            }

            $route['slug'][$lang] = $slug;

        }

        return $route;

    }

    public static function GetSlugCategoryTrans($data, $lang, $defaultlang, $model){

        $catmodel = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model));
        $attribute = $catmodel::theAttributes();
        $categoryid = $data[$attribute['category-folder-url']];
        $categorymodel = HelpersSerializer::splitKey($attribute['category-folder-url']);
        $category = ('App\Containers\Backoffice\\'.ucfirst($categorymodel['key']).'\\Models\\'.ucfirst($categorymodel['key']))::where('id', $categoryid)->first();
        if($lang == $defaultlang) {
            if($category->locale_hidden_general == $defaultlang){
                $slug = $category;
            } else {
                $slug = ('App\Containers\Backoffice\\'.ucfirst($categorymodel['key']).'\\Models\\'.ucfirst($categorymodel['key']))::where('id', $category->parentid_hidden_general)->first();
                // dd(json_decode($slug, true), $categorymodel['key'], $category->parentid_hidden_general, $lang);
            }
        } else {
            if($category->locale_hidden_general != $defaultlang){
                $slug = ('App\Containers\Backoffice\\'.ucfirst($categorymodel['key']).'\\Models\\'.ucfirst($categorymodel['key']))::where('locale_hidden_general', $lang)->where('parentid_hidden_general', $category->parentid_hidden_general)->first();
            } else {
                $slug = ('App\Containers\Backoffice\\'.ucfirst($categorymodel['key']).'\\Models\\'.ucfirst($categorymodel['key']))::where('locale_hidden_general', $lang)->where('parentid_hidden_general', $category->id)->first();
            }
        }
        $slug = isset($slug->slug_txt_general) ? $slug->slug_txt_general : $slug;
        return $slug;

    }

    public static function GetSlugTrans($data, $lang, $defaultlang, $model){

        $parentid = $data['parentid_hidden_general'];
        $id = $parentid !== '' ? $parentid : $data['id'];

        if($lang == $defaultlang) {
            $slug = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id', $id)->first();
        } else {
            $slug = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('locale_hidden_general', $lang)->where('parentid_hidden_general', $id)->first();
        }
        $slug =  isset($slug->slug_txt_general) ? $slug->slug_txt_general : $slug;

        return $slug;

    }

    public static function getThePostByRoute($request)
    {
        $lang = $request->lang;
        $response = [];
        $post = false;
        $preview = $request->has('olmopreview');
        $template = null;

        $isItvalidLang = Helperslang::checkIfvalidLang($lang);
        $defaultLang = Helperslang::getDefaultLang();
        if(!$isItvalidLang){
            return null;
        }

        $slugFirst = isset($request->category) ? $request->category : null;
        $slugSecond = isset($request->subcategory) ? $request->subcategory : null;
        $slugThird = isset($request->slug) ? $request->slug : null;

        $theSlug = $slugFirst;                

        /**
         * Get the last slug available
         * it will match with the slug in the DB
         */
        if($slugThird){
            $theSlug = $slugThird;            
            if($defaultLang == $lang){
                $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [$slugFirst.'/'.$slugSecond.'/:slug'])->where('enabled_is_general', 'true')->first();
                if(!$template){
                    $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [$slugFirst.'/:category/:slug'])->where('enabled_is_general', 'true')->first();
                }
                if(!$template){
                    $template = Db::table('olmo_template')->where('slug_txt_slugs', ':category/:subcategory/:slug')->where('enabled_is_general', 'true')->first();                    
                }
            } else {
                /** not default lang so check the slug first then get the template */
                $getIdBytranslateSlug = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', [$slugFirst.'/'.$slugSecond.'/:slug'])->first();
                if($getIdBytranslateSlug){
                    $template = Db::table('olmo_template')->where('id', $getIdBytranslateSlug->defaultid)->where('enabled_is_general', 'true')->first();
                } else {
                    $getIdBytranslateSlugDynamic = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', [$slugFirst.'/:category/:slug'])->first();
                    if($getIdBytranslateSlugDynamic){
                        $template = Db::table('olmo_template')->where('id', $getIdBytranslateSlugDynamic->defaultid)->where('enabled_is_general', 'true')->first();
                    } else {
                        return null;
                    }
                }
            }
        } else if($slugSecond){
            $theSlug = $slugSecond;
            $multipleteamplte = env('MULTIPLE_TEMPLATE_URL', false);                        
            if($multipleteamplte) {
                if($defaultLang == $lang){                    
                    $template = Db::table('olmo_template')->where('slug_txt_slugs', [$slugFirst.'/'.$slugSecond])->where('enabled_is_general', 'true')->first();                    
                    if(!$template){
                        $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [$slugFirst.'/:slug'])->where('enabled_is_general', 'true')->get();
                        if(count($template) == 1){
                            $template = $template[0];                            
                        } else if(count($template) > 1){                                                            
                            foreach($template as $t){
                                $singlepost = Db::table('olmo_'.$t->model_select_general)->where('slug_txt_general', $slugSecond)->first();                    
                                if($singlepost){
                                    $template = Db::table('olmo_template')->where('id', $singlepost->template_id_general)->where('enabled_is_general', 'true')->first();
                                }
                            }
                        } else if (!$template){
                            $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [':category/:slug'])->where('enabled_is_general', 'true')->first();
                        }                     
                    }                    
                } else {
                    /** not default lang so check the slug first then get the template */
                    $getIdBytranslateSlug = Db::table('olmo_templateslug')->where('slug', [$slugFirst.'/'.$slugSecond])->first();
                    if(!$getIdBytranslateSlug){
                        $getIdBytranslateSlugs = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', [$slugFirst.'/:slug'])->get();
                        
                        if(count($getIdBytranslateSlugs) == 1){
                            $template = Db::table('olmo_template')->where('id', $getIdBytranslateSlugs[0]->defaultid)->where('enabled_is_general', 'true')->first();
                        } else {
                            foreach($getIdBytranslateSlugs as $t){
                                $s_template = Db::table('olmo_template')->where('id', $t->defaultid)->where('enabled_is_general', 'true')->first();
                                if($s_template){
                                    $singlepost = Db::table('olmo_'.$s_template->model_select_general)->where('slug_txt_general', $slugSecond)->first();
                                    if($singlepost){
                                        $template = Db::table('olmo_template')->where('id', $singlepost->template_id_general)->where('enabled_is_general', 'true')->first();
                                    }
                                } 
                            }
                        }
    
                    }
    
                    if($getIdBytranslateSlug){
                        $template = Db::table('olmo_template')->where('id', $getIdBytranslateSlug->defaultid)->where('enabled_is_general', 'true')->first();
                    }
                }                
            } else {
                if($defaultLang == $lang){
                    $template = Db::table('olmo_template')->where('slug_txt_slugs', [$slugFirst.'/'.$slugSecond])->where('enabled_is_general', 'true')->first();
                    if(!$template){
                        $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [$slugFirst.'/:slug'])->where('enabled_is_general', 'true')->first();
                        if(!$template){
                            $template = Db::table('olmo_template')->whereIn('slug_txt_slugs', [':category/:slug'])->where('enabled_is_general', 'true')->first();
                        }
                    }
                } else {
                    /** not default lang so check the slug first then get the template */
                    $getIdBytranslateSlug = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', [$slugFirst.'/'.$slugSecond])->first();
                    if(!$getIdBytranslateSlug){
                        $getIdBytranslateSlug = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', [$slugFirst.'/:slug'])->first();
                    }
                    if($getIdBytranslateSlug){
                        $template = Db::table('olmo_template')->where('id', $getIdBytranslateSlug->defaultid)->where('enabled_is_general', 'true')->first();
                    }
                }
                   
            }
           
        } else if(!$theSlug){
            $theSlug = "home";
            if($defaultLang == $lang){
                $template = Db::table('olmo_template')->where('slug_txt_slugs', '')->where('enabled_is_general', 'true')->first();
            } else {
                $template = Db::table('olmo_templateslug')->where('lang', $lang)->where('slug', '')->first();
            }
        }  else {

            $dbTables = DB::select('SHOW TABLES');
            $dbTables = json_decode(json_encode($dbTables), true);
            $tables = [];

            foreach($dbTables as $table){
                foreach($table as $value){
                    if($value != 'migrations'){
                        if(HelpersData::isDefaultModel($value) == false){
                            array_push($tables, $value);
                        }
                    }
                }
            }

            foreach($tables as $table){
                $name = str_replace('olmo_', '', $table);
                $model = ('App\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name));
                $attribute = $model::theAttributes();
                if($attribute['route']){
                    $post = $model::where('slug_txt_general', $theSlug)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->first();
                    $modelName = $name;
                    if($post){
                        break;
                    }
                }
            }

        }                      

        if(!$post){            
            $modelName = 'page';
            if(isset($template->model_select_general)){
                $modelName = $template->model_select_general;
            }
            $model = ('App\Containers\Backoffice\\'.ucfirst($modelName).'\Models\\'.ucfirst($modelName));
            $attribute = $model::theAttributes();
            
            if(!$template){
                return null;
            }

            if($theSlug == 'home' AND $defaultLang != $lang){
                $templateid = $template->defaultid;
            } else {
                $templateid = $template->id;
            }            
            
            if($preview){
                $post = $model::where('slug_txt_general', self::checkItsHome($theSlug))->where('template_id_general', $templateid)->where('locale_hidden_general', $lang)->where('preview_hidden_general', '!=', '')->first();
            } else {
                $post = $model::where('slug_txt_general', self::checkItsHome($theSlug))->where('template_id_general', $templateid)->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->first();
            }

            /**
             * Check if the preview is requested than get the data in the storage
             */
            if(isset($post['preview_hidden_general']) AND $preview){
                if($post['preview_hidden_general'] != ''){
                    $storage = storage_path('app/'.$post['preview_hidden_general']);
                    $post = json_decode(file_get_contents($storage), true);
                }
            }            
        }

        if($post){
            $response = array('data' => $post, 'model' => $modelName, 'cache' => isset($attribute['cache']) ? $attribute['cache'] : false);
            return $response;
        }

        /**
         * Questo deve essere eliminato al più presto
         */
        // $models = HelpersCollection::getTemplates();
        // unset($models[0]);

        // $post = null;

        // foreach($models as $item){
        //     $model = ('App\Containers\Backoffice\\'.ucfirst($item['key']).'\Models\\'.ucfirst($item['key']));
        //     $isRoute = $model::theAttributes()['route'];
        //     if($isRoute AND $item['key'] == 'page'){
        //         $post = $model::where('slug_txt_general', self::checkItsHome($theSlug))->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->first();
        //         if($post){
        //             return $post;
        //         }
        //     }
        // }

        return null;
    }

    public static function checkItsHome($data){
        if($data == 'home'){
            return "/";
        }
        return $data;
    }

    public static function determinateIDpageByRoute(){

        $url = explode('/',$_SERVER['REQUEST_URI']);
        $langs = HelpersLang::ArrayValidLanguage();
        $lang = '';
        $start_deep = 0;

        for($i=0;$i <= ( count($url) - 1) ;$i++)
        {
           if(in_array($url[$i],$langs))
           {
               $lang       = $url[$i];
               $start_deep = $i;
           }
        }
        $lang  = @$url[2];
        $slug0 = '';
        $slug1 = @$url[$start_deep+1];
        $slug2 = @$url[$start_deep+2];
        $slug3 = @$url[$start_deep+4];

        $deep    = 0;
        $resPage = [];

        $final_slug = '/';
        //calcolo profondità della url e prendo lo slug finale
        for($i=( $start_deep + 1) ;$i< (count($url)) ;$i++)
        {
           if($url[$i] != ''){
             $final_slug =  $url[$i];
             $deep++;
           }
        }

        $models = HelpersCollection::getTemplates();
        $isFind = false;
        $Model   = '';
        $resModel = [];

        foreach($models as $model){
            if($model['value'] != ''){
                $Model = $model['value'];
                $modelPath = ('App\Containers\Backoffice\\'.ucfirst($Model).'\Models\\'.ucfirst($Model));
                $resModel = $modelPath::where('slug_txt_general',$final_slug)->where('locale_hidden_general',$lang)->where('enabled_is_general','true')->first();
                if($resModel){
                    $isFind = true;
                    break;
                }
           }
        }

        $res = array(
           'dataModel'  => $resModel ,
           'model'      => $Model,
           'deep_route' => $deep
         );
        return $res;


    }


}
