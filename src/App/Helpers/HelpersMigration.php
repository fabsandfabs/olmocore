<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class HelpersMigration
{

  private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

  private const CONTAINER_SLIDER = 'Slider';

  private const CONTAINER_DOT = 'Dot';

  private const CONTAINER_CUSTOMER = 'Customer';

  private const CONTAINER_VISUALBLOCK = 'VisualBlock';

  private const CONTAINER_FIELDREPEATER = 'FieldRepeater';

  public static function Customer()
  {

    $customerDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_CUSTOMER . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($customerDirectory);
    $datacustomer = null;

    if ($directory) {
      $sliderPath = $customerDirectory . 'customertable.json';
      $datacustomer = json_decode(file_get_contents($sliderPath), true);

      if (isset($datacustomer['table'])) {

        Schema::create('olmo_customer', function (Blueprint $table) use ($datacustomer) {
          // Create new table...
          $table->charset = 'utf8mb4';
          $table->collation = 'utf8mb4_unicode_ci';
          // General
          // HelpersMigration::Page($table);
          HelpersMigration::Default($table); // Please, use this migration in case dalete HelpersMigration::Page($table);
          // HelpersMigration::Locale($table); Please, use this migration in case dalete HelpersMigration::Page($table) and want to add multilang-post;
          // $table->text('page_id_general')->nullable(false);
          // Content
          $table->string('customerstatus_select_general', 100)->nullable(false);
          $table->string('token_read_general', 100)->nullable(false);
          $table->string('tokenactive_read_general', 100)->nullable(false);
          $table->string('endsession_read_general', 100)->nullable(false);
          $table->string('currentlocale_read_general', 100)->nullable(false);
          $table->string('currenttimezone_read_general', 100)->nullable(false);
          $table->text('email_email_general')->nullable(false);
          $table->text('password_pwd_general')->nullable(false);
          $table->text('confirmpassword_pwd_general')->nullable(false);
          $table->text('customerlang_txt_general')->nullable(false);
          $table->text('privacy_is_general')->nullable(false);
          $table->text('terms_is_general')->nullable(false);
          $table->text('customeritem_list_general')->nullable(false);  

          foreach ($datacustomer['table'] as $item) {
            if (strpos($item, 'enabled') === false) {
              $table->text($item)->nullable(false);
            } else {
              $table->string($item, 5)->nullable(false);
            }
          }
          // Example Profile account
          // $table->text('name_txt_profile')->nullable(false);
          // $table->text('surname_txt_profile')->nullable(false);
          // $table->text('company_txt_profile')->nullable(false);
          // $table->text('country_txt_profile')->nullable(false);
          // $table->string('vat_txt_profile', 100)->nullable(false);             
        });
      }      
    }
  }

  public static function Addresses()
  {

    Schema::create('olmo_customeritem', function (Blueprint $table) {
      // Create new table...
      $table->charset = 'utf8mb4';
      $table->collation = 'utf8mb4_unicode_ci';

      HelpersMigration::Default($table);

      $table->text('surname_txt_address')->nullable(false);
      $table->text('default_is_address')->nullable(false);
      $table->text('typeaddress_select_address')->nullable(false);
      $table->text('address_txt_address')->nullable(false);
      $table->text('company_txt_address')->nullable(false);
      $table->text('phone_txt_address')->nullable(false);
      $table->text('number_txt_address')->nullable(false);
      $table->text('city_txt_address')->nullable(false);
      $table->text('provincia_txt_address')->nullable(false);
      $table->string('zip_txt_address')->nullable(false);
      $table->string('region_txt_address')->nullable(false);
      $table->string('country_txt_address')->nullable(false);
      $table->string('customer_hidden_content')->nullable(false);
      
      $table->text('locale_hidden_content')->nullable(false);
      $table->text('parentid_hidden_content')->nullable(false);
      $table->text('postid_hidden_content')->nullable(false);
    });
  }

  public static function Dotdecimal()
  {

    $dotsDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_DOT . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($dotsDirectory);
    $datadot = null;

    if ($directory) {
      $sliderPath = $dotsDirectory . 'dottable.json';
      $datadot = json_decode(file_get_contents($sliderPath), true);
    }

    if ($datadot['table']) {

      Schema::create('olmo_dotsposition', function (Blueprint $table) use ($datadot) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';

        $table->increments('id')->unsigned();

        $table->text("locale_hidden_content")->nullable();
        $table->text("parentid_hidden_content")->nullable();
        $table->text("model_hidden_content")->nullable();
        $table->text("postid_hidden_content")->nullable();
        $table->text("columnname_hidden_content")->nullable();

        $table->text('name_txt_content')->nullable();
        $table->unsignedDecimal('left_num_content', $precision = 12, $scale = 2)->nullable();
        $table->unsignedDecimal('top_num_content', $precision = 12, $scale = 2)->nullable();
        $table->text('description_txt_content')->nullable();
        $table->text('image_filemanager_content')->nullable();
        $table->text('element_is_content')->nullable();      
        foreach ($datadot['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }        
                    
      });  

    }
  }

  public static function Slider()
  {

    $sliderDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SLIDER . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($sliderDirectory);
    $dataslider = null;

    if ($directory) {
      $sliderPath = $sliderDirectory . 'slidertable.json';
      $dataslider = json_decode(file_get_contents($sliderPath), true);
    }

    if ($dataslider['table']) {

      Schema::create('olmo_slideritem', function (Blueprint $table) use ($dataslider) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';
        $table->id();
        $table->text("locale_hidden_content")->nullable(false);
        $table->text("parentid_hidden_content")->nullable(false);
        $table->text("model_hidden_content")->nullable(false);
        $table->text("postid_hidden_content")->nullable(false);
        $table->text("columnname_hidden_content")->nullable(false);
        $table->text("name_txt_content")->nullable(false);
        $table->text("primary_filemanager_content")->nullable(false);
        foreach ($dataslider['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }
      });
    }
  }

  public static function FieldRepeater()
  {

    $sliderDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_FIELDREPEATER . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($sliderDirectory);
    $dataslider = null;

    if ($directory) {
      $sliderPath = $sliderDirectory . 'fieldtable.json';
      $dataslider = json_decode(file_get_contents($sliderPath), true);
    }

    if ($dataslider['table']) {

      Schema::create('olmo_fieldrepeateritem', function (Blueprint $table) use ($dataslider) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';
        $table->id();
        $table->text("locale_hidden_content")->nullable(false);
        $table->text("parentid_hidden_content")->nullable(false);
        $table->text("model_hidden_content")->nullable(false);
        $table->text("postid_hidden_content")->nullable(false);
        $table->text("columnname_hidden_content")->nullable(false);
        foreach ($dataslider['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }
      });
    }
  }

  public static function VisualBlock()
  {

    $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_VISUALBLOCK . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($menuDirectory);
    $dataslider = null;

    if ($directory) {
      $sliderPath = $menuDirectory . 'visualtable.json';
      $dataslider = json_decode(file_get_contents($sliderPath), true);
    }

    if ($dataslider['table']) {

      Schema::create('olmo_visualblock', function (Blueprint $table) use ($dataslider) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';
        $table->id();
        $table->text('name')->nullable(false);
        $table->text("locale_hidden_content")->nullable(false);
        $table->text("parentid_hidden_content")->nullable(false);
        $table->text("model_hidden_content")->nullable(false);
        $table->text("postid_hidden_content")->nullable(false);
        foreach ($dataslider['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }
      });
    }
  }

  public static function Default($table)
  {
    $table->increments('id')->unsigned();
    $table->string('enabled_is_general', 5)->nullable(false);
    $table->text('name_txt_general')->nullable(false);
    $table->integer('position_ord_general')->nullable(false);
    $table->text('trash_hidden_general')->nullable();
    $table->text('preview_hidden_general', 5)->nullable(false);
    self::Info($table);
  }

  public static function Page($table)
  {
    $table->increments('id')->unsigned();
    self::Locale($table);
    $table->string('enabled_is_general', 5)->nullable(false);
    $table->text('name_txt_general')->nullable(false);
    $table->text('slug_txt_general')->nullable(false);
    $table->text('template_id_general')->nullable(false);
    $table->integer('position_ord_general')->nullable(false);
    $table->string('menu_is_general')->nullable(false);
    $table->string('menu_multid_general')->nullable(false);
    $table->string('preview_hidden_general', 5)->nullable(false);
    $table->text('trash_hidden_general')->nullable();
    $table->integer('statuscode_select_general')->default(200)->nullable();
    self::Info($table);
  }

  public static function Info($table)
  {
    $table->dateTime('create_read_information')->useCurrent()->nullable();
    $table->text('createdby_read_information')->nullable(false);
    $table->dateTime('lastmod_read_information')->useCurrentOnUpdate()->nullable();
    $table->text('lastmodby_read_information')->nullable(false);
  }

  public static function Locale($table)
  {
    $table->text('lang_langs_general')->nullable(false);
    $table->text('locale_hidden_general')->nullable(false);
    $table->text('parentid_hidden_general')->nullable(false);
  }

  public static function Seo($table)
  {
    $table->text('metatitle_txt_seo')->nullable(false);
    $table->text('metadesc_txtarea_seo')->nullable(false);
    $table->text('metakw_txt_seo')->nullable(false);
    $table->text('ogtitle_txt_seo')->nullable(false);
    $table->text('ogdesc_txtarea_seo')->nullable(false);
    $table->text('ogimg_filemanager_seo')->nullable(false);
    $table->text('index_select_seo')->nullable(false);
    $table->text('follow_select_seo')->nullable(false);    
  }

  public static function Sitemap($table)
  {
    $table->string('disable_is_sitemap', 5)->nullable(false)->default('false');
    $table->string('overwrite_is_sitemap', 5)->nullable(false)->default('false');
    $table->text('frequency_select_sitemap')->nullable(false);
    $table->text('priority_select_sitemap')->nullable(false);
    $table->text('sitemapimage_sitemapimage_sitemap')->nullable(false);
  }

  // Ecommerce Migration to apply in different container
  public static function ECustomer($table)
  {
    $table->text('address_list_general')->nullable(false);
    $table->text('paymentmethod_hidden_general')->nullable(false);
    $table->text('shippingmethod_hidden_general')->nullable(false);
    $table->text('discountcode_hidden_general')->nullable(false);
    $table->text('shippingaddress_hidden_general')->nullable(false);
    $table->text('billingaddress_hidden_general')->nullable(false);
    // $table->text('meta_hidden_general');
  }

  public static function EcommerceProduct($table)
  {

    $table->text('category_id_general')->nullable(false);
    $table->text('property_props_general')->nullable(false);
    $table->text('sku_txt_general')->nullable(false);
    $table->unsignedDecimal('price_num_general', $precision = 12, $scale = 2)->nullable(false);
    $table->unsignedDecimal('weight_num_general', $precision = 12, $scale = 2)->nullable(false);
    $table->integer('qty_spin_general')->nullable(false);
    $table->text('items_productitems_general')->nullable(false);

    $table->text('title_txt_content')->nullable(false);
    $table->text('images_slider_content')->nullable(false);
    $table->text('cover_filemanager_content')->nullable(false);
    $table->text('description_editor_content')->nullable(false);
    $table->text('description_editor_content')->nullable(false);

  }

  public static function EcommerceProductItems($table)
  {
    $table->text('default_is_general')->nullable(false);
    $table->text('code_txt_general')->nullable(false);
    $table->text('sku_txt_general')->nullable(false);
    $table->text('property_props_general')->nullable(false);
    $table->text('title_txt_general')->nullable(false);
    $table->text('imagesfrom_sliderfromitem_general')->nullable(false);
    $table->text('images_slider_general')->nullable(false);
    $table->unsignedDecimal('price_num_general', $precision = 12, $scale = 2)->nullable(false);
    $table->unsignedDecimal('weight_num_general', $precision = 12, $scale = 2)->nullable(false);
  }

  public static function ProductItems($table)
  {
    $table->text('items_productitems_general')->nullable(false);
  }

  public static function Menu($table)
  {
    $table->text('menutitle_txt_menu')->nullable(false);
    $table->text('menuimg_filemanager_menu')->nullable(false);
  }
}
