<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;


class HelpersImportExport
{
    public static function outputCSV($data, $attribute, $filename)
    {
        $fp = fopen('php://temp', 'r+');
        $delimeter = $attribute['csvstructure']['separator'];
        $enclosure = '"';
        $contents = "";

        foreach ($data as $fields) {
            fputcsv($fp, $fields, $delimeter, $enclosure);
        }

        rewind($fp);
        while (!feof($fp)) {
             $contents .= fread($fp, 8192);
        }        
        
        fclose($fp);

        return $contents;
    }

    public static function export($model, $type)
    {
        $response  = [];
        $themodel = ('App\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model));
        $attribute = $themodel::theAttributes();
        $data = $themodel::all();
        $data = json_decode(json_encode($data), true);

        foreach($data as $index=>$item){
            foreach($item as $key=>$value){
                if(!in_array($key, $attribute['csvstructure']['mapping'])){
                    unset($data[$index][$key]);
                }
            }
        }
        
        $uuid = (string)Str::uuid();
        $date = Carbon::now();
        $truename = $model . '_export_' . str_replace(":", "-", $date) . '.csv';
        $filename = $model . '_export_' . $uuid . '.csv';
        
        $current_data = self::outputCSV($data, $attribute, $filename);

        $truename = str_replace(" ", "-", $truename);

        $path = 'public/documents/export/' . $model . '/' . $filename;
        Storage::put($path, $current_data);

        $storage_id = DB::table('olmo_storage')->insertGetId([
            'filename' => $filename,
            'truename' => $truename,
            'model' => $model,
            'public' => 'true',
            'type' => $type
        ]);

        Db::table('olmo_importexport')->insert([
            'store_id' => $storage_id,
            'creation_date' => date("Y-m-d H:i:s"),
            'type' => 'export',
            'model' => $model
        ]);

        $response['filepath'] = '/documents/export/' . $model . '/' . $truename;        

        return $response;
    }
}
