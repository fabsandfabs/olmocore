<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;

class HelpersCsv
{

    public static function getCsvFields($value)
    {
        if($value == ''){
            return '';
        } else{
            $slides = explode(',',$value);
            foreach($slides as $key=>$item){
                $slides[$key] = self::getCsvField($item);
            }
            return $slides;
        }        
    }

    public static function getCsvField($value)
    {
        $field = DB::table('olmo_formcsvfield')->where('id', $value)->first();

        return $field;
    }

    public static function updateCsvField($request)
    {
        $id = $request->id;
        $name = $request->input('name');

        Db::table('olmo_formcsvfield')->where('id', $id)->update(['name' => $name]);
        
        $getformid = Db::table('olmo_formcsvfield')->where('id', $id)->first();
        $getformid = Db::table('olmo_form')->where('id', $getformid->fieldid_hidden_general)->first();

        return self::getCsvFields($getformid->csvfield_csvfield_csv);
    }    

    public static function createCsvField($request)
    {
        $id = $request->id;

        $addcsvfield = Db::table('olmo_formcsvfield')->insertGetid(['name' => '', 'fieldid_hidden_general' => $id]);
        $getcsvfields = Db::table('olmo_form')->where('id', $id)->first();

        $newIds = '';

        if ($getcsvfields->csvfield_csvfield_csv == '') {
            $newIds = (string)$addcsvfield;
        } else {
            $newIds = $getcsvfields->csvfield_csvfield_csv . ',' . (string)$addcsvfield;
        }        
        
        Db::table('olmo_form')->where('id', $id)->update(['csvfield_csvfield_csv' => $newIds]);     

        return self::getCsvFields($newIds);
    }

    public static function deleteCsvField($request)
    {
        $id = $request->id;

        $formId = Db::table('olmo_formcsvfield')->where('id', $id)->first();
        $csvFields = Db::table('olmo_form')->where('id', $formId->fieldid_hidden_general)->first();
        
        $ids = explode(',', $csvFields->csvfield_csvfield_csv);
        foreach($ids as $key=>$field){
            if($field == $id){
                unset($ids[$key]);
            }
        }
        
        $implode = implode(',', $ids);
        Db::table('olmo_form')->where('id', $formId->fieldid_hidden_general)->update(['csvfield_csvfield_csv' => $implode]);
        Db::table('olmo_formcsvfield')->where('id', $id)->delete();
        $csvFields = Db::table('olmo_form')->where('id', $formId->fieldid_hidden_general)->first();

        return self::getCsvFields($csvFields->csvfield_csvfield_csv);
    }    

}