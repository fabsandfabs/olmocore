<?php
// https://www.php.net/manual/en/imagick.installation.php
namespace Olmo\Core\App\Helpers;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;

use Intervention\Image\Facades\Image;


class HelpersGif
{
    
    public static function checkOriginalVersionsAndCreateIt($file) {

        $versions_control = DB::table('olmo_storage_versions')->where('id_original', $file->id)->first();

        if(!$versions_control){
            $file_dirname = $file->truename;
            $dirname = pathinfo($file_dirname)['dirname'];
            $file_dirname_filename = $dirname .'/'. $file->filename;

            // $img = Image::make(env('APP_URL') . '/storage/media/' . $file_dirname);

            $imgget = Storage::disk('local')->get('public/media/' . $file_dirname_filename);
            Storage::put('public/assets/' . $file_dirname, $imgget);

            /**
             * If $assets_original_webp contains a folder that doen't exist
             * then create it before save the file
             */
            $dirname = pathinfo($file_dirname)['dirname'] != '' ? pathinfo($file_dirname)['dirname'] . '/' : '';
            $dirname = storage_path('app/public/assets/' . $dirname);
            HelpersFilemanager::checkFolderAndCreateIt($dirname);


            if(env('FILESYSTEM_DRIVER', 'local') == 's3'){
                Storage::disk('local')->delete('public/assets/' . $file_dirname);
            }

            DB::table('olmo_storage_versions')->insertGetId([
                'dirname' 	        => $file_dirname,
                'compression_path' 	=> '',
                'size'              => 0,
                'compression_size'  => 0,
                'width'             => 0,
                'height'            => 0,
                'compression'       => 0,
                'fit'               => '',
                'id_original'       => $file->id,
                'is_original'       => 'true'
            ]);

            //TODO:
            /**
             * aggiornare questa risposta con oggetto che contiene sempre path e boolean
             * farlo per tutti non solo
             */            
            return $file_dirname;
        }

        return false;

    }

}
