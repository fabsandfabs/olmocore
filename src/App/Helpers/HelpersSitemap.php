<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HelpersSitemap
{

    public static function getSiteImage($value)
    {
        $pageurl = $value;
        $getpage = Db::table('olmo_sitemapimages')->where('pageurl', $pageurl)->first();
        if($getpage){
            $storage = storage_path('app/'.$getpage->path);
            $json = json_decode(file_get_contents($storage), true);             
            return [
                'id' => $getpage->id,
                'date' => $getpage->date,
                'page' => $json['page'],
                'url' => $json['url'],
                "model" => $getpage->model,
                "idpost" => $getpage->idpost
            ];
        } else {
            return $value;
        }
    } 

}