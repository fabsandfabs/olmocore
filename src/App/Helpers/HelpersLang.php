<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersData;

class HelpersLang
{

    public static function checkNamePost($body, $id, $attribute)
    {
        $table = $attribute['table'];
        $lang = $attribute['lang'];
        $checkTable = HelpersData::isDefaultModel($table);

        $name = isset($body['name_txt_general']) && !$checkTable ? $body['name_txt_general'] : false;

        /**
         * Property is a special Default Model cause it acts like a generated one but it is still a default
         * so for some operations needs specifications like the name
         */
        $name = $table == 'olmo_property' ? true : $name;
        $name = $lang ? $name : $lang;

        if($name){
            $check = Db::table($table)->where('id', $id)->where('name_txt_general', $name)->first();
            $result = $check === $name ? false : true;

            return $result;
        }

        return $name;
    }

    public static function changeNamePost($body, $id, $table, $parentId = false)
    {
        if($parentId){
            $getname = Db::table($table)->where('id', $parentId)->first();;
            Db::table($table)->where('id', $id)->update(['name_txt_general' => $getname->name_txt_general]);            
        } else {
            $langs = self::validLang();
            foreach($langs as $lang){
                Db::table($table)->where('parentid_hidden_general', $id)->where('locale_hidden_general', $lang)->update(['name_txt_general' => $body['name_txt_general']]);
            }
        }
    }

    public static function createSlugInEveryTemplate($table, $body, $request)
    {
        $defaultLang = HelpersLang::getDefaultLang();
        if ($table == 'olmo_language' AND $defaultLang != $request->code_txt_general) {
            $templates = Db::table('olmo_template')->get();

            $body = (array)$body;

            foreach ($templates as $template) {
                $check = Db::table('olmo_templateslug')->where('defaultid', $template->id,)->where('lang', $body['code_txt_general'])->first();
                if(!$check){
                    $data = array(
                        'defaultid' => $template->id, 
                        'lang' => $body['code_txt_general'], 
                        'slug' => $template->slug_txt_slugs
                    );
                    Db::table('olmo_templateslug')->insert($data);
                }
            }
        }
    }

    public static function createTemplateSlugMultilanguage($table, $id)
    {
        if ($table == 'olmo_template') {
            $langs = Db::table('olmo_language')->get();
            $defaultlang = Helperslang::getDefaultLang();

            foreach ($langs as $lang) {
                if ($lang->code_txt_general != $defaultlang) {
                    $data = array('defaultid' => $id['id'], 'lang' => $lang->code_txt_general, 'slug' => '');
                    Db::table('olmo_templateslug')->insert($data);
                }
            }
        }
    }

    public static function getLangsBymodelId($table, $id)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */

        $langs = self::validLang();
        $res   = [];
        $i     = 0;

        $default_id  = self::getDefaultLangPostId($table, $id);
        $default_lang = self::getDefaultlang();

        foreach ($langs as $lang) {

            $exist = false;
            $id = $default_id;

            if ($default_lang == $lang) {
                $exist = true;
            } else {
                $check =  Db::table($table)->where('parentid_hidden_general', $default_id)->where('locale_hidden_general', $lang)->first();
                if ($check) {
                    $exist = true;
                    $id    = $check->id;
                }
            }

            $res[$i]['code']  = $lang;
            $res[$i]['exist'] = $exist;
            $res[$i]['id']    = (string)$id;
            $i++;
        }

        return $res;
    }

    public static function validLang()
    {
        $data =  Db::table('olmo_language')->where('enabled_is_general', 'true')->select('code_txt_general')->get()->toArray();
        $response = [];
        foreach ($data as $d) {
            $response[$d->code_txt_general] = $d->code_txt_general;
        }

        return $response;
    }

    public static function validLangFront()
    {
        $enabledfront = Schema::hasColumn('olmo_language', 'enabledfront_is_general');
        if($enabledfront){
            $data =  Db::table('olmo_language')->where('enabledfront_is_general', 'true')->select('code_txt_general')->get()->toArray();
        } else {
            $data =  Db::table('olmo_language')->where('enabled_is_general', 'true')->select('code_txt_general')->get()->toArray();
        }
        $response = [];
        foreach ($data as $d) {
            $response[$d->code_txt_general] = $d->code_txt_general;
        }

        return $response;
    }

    public static function checkIfvalidLang($lang)
    {
        $langs = self::validLang();
        return in_array($lang, $langs);
    }

    public static function checkIfdefaultLang($lang)
    {
        $langs = self::validLang();
        return in_array($lang, $langs);
    }    

    public static function getDefaultLang()
    {
        $default = Db::table('olmo_language')->where('default_is_general', 'true')->first();
        return $default->code_txt_general;
    }

    public static function getDefaultFrontLang()
    {
        $default = Db::table('olmo_language')->where('defaultfront_is_general', 'true')->first();
        return $default->code_txt_general;
    }    

    public static function checkIfTranslable($model)
    {
        if (HelpersData::isDefaultModel($model)) {
            $attribute = ('Olmo\Core\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model))::theAttributes();
        } else {
            $attribute = ('App\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model))::theAttributes();
        }
        return $attribute['lang'];
    }

    public static function getDefaultLangPostId($table, $id)
    {
        $data = DB::table($table)->where('id', $id)->first();
        $default_id = $data->parentid_hidden_general ? $data->parentid_hidden_general : $id;
        return $default_id;
    }

    /**
     * 
     * THIS IS DEPRECATED
     * 
     * Here you go!
     * Copy the default post values inside the current translated post
     * 
     * NEED TO DO
     * Copy correct relation on select and multiselect, what happen when there is no post created for that lang?
     */
    public static function copyDefautLang($request, $attribute)
    {
        $dataPost = HelpersData::idLangPost($request);

        $default_id = Db::table($attribute['table'])->where('id', $dataPost['id'])->first()->parentid_hidden_general;
        $default = Db::table($attribute['table'])->where('id', $default_id)->first();

        unset($default->id);
        unset($default->lang_langs_general);
        unset($default->locale_hidden_general);
        unset($default->parentid_hidden_general);
        unset($default->lastmod_read_general);
        unset($default->lastmod_read_information);

        $default = (array)$default;

        Db::table($attribute['table'])->where('id', $dataPost['id'])->update($default);
    }

    /**
     * Check if need copy-from button
     * This happen when a user creates a localized post, the button will be always
     * then print a button to copy the content from the default post
     */
    public static function addCopyDefaultFrom($data, $id)
    {
        $data = (array)$data;
        if (isset($data['parentid_hidden_general'])) {
            if (!$data['parentid_hidden_general'] == "") {
                $data = ["copytranslate" => $id] + $data;
            }
        }
        return $data;
    }

    /**
     * Create a translation of the requested slide copying everithing but the fields
     * who declare the post has translation of:
     * 
     * locale_hidden_content
     * parentid_hidden_content
     * postid_hidden_content     
     */
    public static function generateTranslatedRelation($id, $lang, $postid, $table)
    {
        $getSlide = Db::table($table)->where('id', $id)->first();
        $slide = (array)$getSlide;

        $slide['locale_hidden_content'] = $lang;
        $slide['parentid_hidden_content'] = $id;
        $slide['postid_hidden_content'] = $postid;
        unset($slide['id']);

        /**
         * Check if the value is already there, if so replace the values and do not add row
         */
        $check = Db::table($table)->where('locale_hidden_content', $lang)->where('parentid_hidden_content', $id)->where('postid_hidden_content', $postid)->first();
        if (!$check) {
            $insertSlide = Db::table($table)->insertGetId($slide);
        } else {
            Db::table($table)->where('locale_hidden_content', $lang)->where('parentid_hidden_content', $id)->where('postid_hidden_content', $postid)->update($slide);
            $insertSlide = $check->id;
        }

        return $insertSlide;
    }

    /**
     * THIS METHOD MUST BE DELETED AND MERGED WITH THE ONE ABOVE BUT THE 
     * CHANGE IS REALLY BREAKING, THE PROPITEM COLUMN NAME MUST BE CONVERTED TO _CONTENT INSTEAD _GEENRAL 
     * 
     * Create a translation of the requested slide copying everithing but the fields
     * who declare the post has translation of:
     * 
     * locale_hidden_content
     * parentid_hidden_content
     * postid_hidden_content     
     */
    public static function generateTranslatedPropVal($id, $lang, $postid, $table)
    {
        $getProp = Db::table($table)->where('id', $id)->first();
        if($getProp){
            $prop = (array)$getProp;

            $prop['locale_hidden_general'] = $lang;
            $prop['parentid_hidden_general'] = $id;
            $prop['postid_hidden_general'] = $postid;
            unset($prop['id']);
    
            /**
             * Check if the value is already there, if so replace the values and do not add row
             */
            $check = Db::table($table)->where('locale_hidden_general', $lang)->where('parentid_hidden_general', $id)->where('postid_hidden_general', $postid)->first();
            if (!$check) {                
                if(isset($prop['property_props_general'])){
                    $property = explode(',', $prop['property_props_general']);
                    $arrayProps = [];
                    foreach($property as $item){
                        $checkDefaultProperty = DB::table('olmo_propertyitem')->where('id', $item)->first();
                        $translatedProperty = DB::table('olmo_propertyitem')->where('locale_hidden_general', $lang)->where('parentid_hidden_general', $checkDefaultProperty->id)->first();
                        array_push($arrayProps, $translatedProperty->id);
                    }
                    $arrayPropsTranslated = implode(',', $arrayProps);
                    $prop['property_props_general'] = $arrayPropsTranslated;
                }
                $insertProp = Db::table($table)->insertGetId($prop);
            } else {
                $insertProp = $check->id;
            }
    
            return $insertProp;
        }
    }

    /**
     * Three options
     * $data -> the array of fileds the method has to translates
     * $lang -> the lang of the request
     * $postid -> the request post id
     * 
     * Basically the method takes care to recreate the relation 
     * between the original post and its translation in the request language
     * 
     * Big deal bro/sista
     */
    public static function translateRelations($data, $lang, $postid)
    {
        $toArray = (array)$data;
        foreach ($toArray as $key => $item) {
            $type = HelpersData::getType($key);
            $label = HelpersData::setLabel($key);
            if ($type == 'select' || $type == 'multiselect') {
                $table = $label == 'property' ? 'olmo_' . $label . 'item' : 'olmo_' . $label;
                $checkTable = Schema::hasTable($table);
                if ($checkTable) {
                    if ($type == 'select') {
                        $translable = HelpersLang::checkIfTranslable($label);
                        if ($translable) {
                            $getPost = Db::table($table)->where('parentid_hidden_general', $item)->where('locale_hidden_general', $lang)->first();
                            $toArray[$key] = isset($getPost->id) ? (string)$getPost->id : '';
                        }
                    } else if ($type == 'multiselect') {
                        $translable = HelpersLang::checkIfTranslable($label);
                        if ($translable) {
                            $ids = explode(',', $item);                            
                            foreach ($ids as $k => $id) {
                                $getPost = Db::table($table)->where('parentid_hidden_general', $id)->where('locale_hidden_general', $lang)->first();
                                if ($k == 0) {
                                    $toArray[$key] = isset($getPost->id) ? (string)$getPost->id : '';
                                } else {
                                    $toArray[$key] = isset($getPost->id) ? $toArray[$key] . ',' . (string)$getPost->id : $toArray[$key];
                                }
                            }
                        }
                    }
                }
            } else if($type == 'productitems'){
                $ids = explode(',', $item);                
                foreach ($ids as $k => $id) {
                    $getItemId = HelpersLang::generateTranslatedPropVal($id, $lang, $postid, 'olmo_productitem');
                    if($getItemId){
                        if ($k == 0) {
                            $toArray[$key] = (string)$getItemId;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getItemId;
                        }
                    }
                }
            } else if ($type == 'slider' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getSlideId = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_slideritem');
                    if($getSlideId){
                        if ($k == 0) {
                            $toArray[$key] = (string)$getSlideId;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                        }
                    }
                }
            } else if ($type == 'property' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getPropValue = HelpersLang::generateTranslatedPropVal($id, $lang, $postid, 'olmo_propertyitem');
                    if ($getPropValue) {
                        if ($k == 0) {
                            $toArray[$key] = (string)$getPropValue;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getPropValue;
                        }
                    }
                }
            } else if ($type == 'visual' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getSlideId = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_visualblock');
                    if($getSlideId){
                        if ($k == 0) {
                            $toArray[$key] = (string)$getSlideId;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                        }
                    }
                }
            } else if ($type == 'dotsposition' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getSlideId = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_dotsposition');
                    if($getSlideId){
                        if ($k == 0) {
                            $toArray[$key] = (string)$getSlideId;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                        }
                    }
                }
            }
        }

        return $toArray;
    }

    public static function ArrayValidLanguage()
    {

        $langs = Db::table('olmo_language')->where('enabled_is_general', 'true')->get();
        $validlang = [];

        foreach ($langs as $lang) {
            array_push($validlang, $lang->code_txt_general);
        }

        return $validlang;
    }

    public static function isCrossLanguage($column, $model, $type){
        if(isset($model['crosslanguagefields'])){
            if(in_array($column, $model['crosslanguagefields'], true)){
                return "disabled_".$type;
            }
        }
        
        return $type;
    }

    public static function updateCrossLanguage($body, $model){
        $update = [];
        if(isset($model['crosslanguagefields'])){
            if(count($model['crosslanguagefields']) > 0){
                $fields = $model['crosslanguagefields'];
                foreach($body as $column => $value){
                    if(in_array($column, $fields)){
                        $update[$column] = $value;
                    }
                }

                if(!empty($update)){ // Potrebbe essere interessante il numero di righe aggiornate? O é un controllo esagerato?
                    Db::table($model['table'])
                    ->where('parentid_hidden_general', $body['id'])
                    ->update($update);
                }
           
            }
        }
    }
}
