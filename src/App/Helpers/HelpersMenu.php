<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Olmo\Core\Containers\Backoffice\Property\Models\Property;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersFilemanager;

class HelpersMenu
{

    public static function getNavigationMenuLabel($lang, $value, $id)
    {

        $dbTables = DB::select('SHOW TABLES');
        $dbTables = json_decode(json_encode($dbTables), true);
        $tables = [];
        $labelmenu = [];
        $routemenu = [];
        $propertymenu = [];
        $modelmenu = [];
        
        $counter = 0;

        foreach($dbTables as $table){
            foreach($table as $value){
                if($value != 'migrations'){
                    if(HelpersData::isDefaultModel($value) == false){
                        array_push($tables, $value);

                        $name = str_replace('olmo_', '', $value);
                        $model = [];
                        $model['id'] = $counter++;
                        $model['table'] = $value;
                        $model['type'] = 'model';
                        $model['submenu'] = '';
                        $model['subsubmenu'] = '';   
                        $model['level'] = 0;                     
                        $model['name_txt_general'] = ucfirst($name);
                        array_push($modelmenu, $model);
                    }
                }
            }
        }
        
        foreach($tables as $table){
            $name = str_replace('olmo_', '', $table);
            $model = ('App\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name));
            $attribute = $model::theAttributes();            
            if(isset($attribute['menu'])){
                if($attribute['route'] && $attribute['menu']){
                    // $posts = $model::where('menu_is_general', 'true')->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->select(['id', 'name_txt_general'])->get()->toArray();
                    $posts = $model::where('menu_multid_general', 'LIKE', '%'.$id.'%')->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->select(['id', 'name_txt_general'])->get()->toArray();
                    if($posts){
                        foreach($posts as $key=>$post){
                            $posts[$key]['table'] = $table;
                            $posts[$key]['type'] = 'route';
                            $posts[$key]['submenu'] = '';
                            $posts[$key]['subsubmenu'] = '';
                            $posts[$key]['level'] = 0;
                            $posts[$key]['text'] = $name . ' - ' . $posts[$key]['name_txt_general'];
                        }
                        $routemenu = array_merge($routemenu, $posts);
                    }
                }
            }
        }

        $properties = Property::where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->select(['id', 'name_txt_general'])->get()->toArray();
        if($properties){
            foreach($properties as $key=>$post){
                $properties[$key]['table'] = 'olmo_property';
                $properties[$key]['type'] = 'property';
                $properties[$key]['submenu'] = '';
                $properties[$key]['subsubmenu'] = '';
                $properties[$key]['level'] = 0;
            }
            $propertymenu = $properties;
        }        

        $labelmenu['route'] = $routemenu;
        $labelmenu['property'] = $propertymenu;
        $labelmenu['tables'] = $modelmenu;

        return $labelmenu;
    }

    public static function getMenu($value)
    {

        $menu = $value == "" ? [] : json_decode($value, true);

        return $menu;
    }    

    /**
     * This method creates a navigation menu 
     * it can goes deep till 5 levels of child relation, no more.
     * 
     * IMPROVEMENT
     * Create a loop to manage infinite level of child relation
     */
    public static function getNavigationMenu($request)
    {   
        $lang = $request->lang;
        $name = $request->name;
        $cacheKey = md5($lang."navigationmenu".$name);
        if(Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }                

        $rawMenu = self::generateRawTheMenu($request, $request->lang);
        $menu = self::generateTheMenu($rawMenu, $request->lang);

        Cache::put($cacheKey, $menu, now()->addMinutes(10));

        return $menu;

    }  
    
    public static function generateTheMenu($menu, $lang)
    {

        function createLabelAndFolder($menu, $lang, $child = false) 
        {           
            $thelabel = [];
            $single = [];
            foreach($menu as $label){
                if($label['id'] != 'label'){
                    if($label['type'] == 'route'){
                        if(env('MENU_MULTID', false)){
                            //TODO: questa query sotto è da controllare meglio perchè in questo momento considera solo id sotto il 10
                            $getSingle = DB::table($label['table'])->where('name_txt_general', $label['name_txt_general'])->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->where('menu_multid_general', 'LIKE', '%'.$label['dbid'].'%')->first();
                        } else {
                            $getSingle = DB::table($label['table'])->where('name_txt_general', $label['name_txt_general'])->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->where('menu_multid_general', 'true')->first();
                        }
                        if($getSingle){
                            $newValue = HelpersSerializer::routeResponse($getSingle, 'stdClass', 0, 'route', '', false, $lang); 
                            $title = isset($getSingle->menutitle_txt_menu) ? $getSingle->menutitle_txt_menu : $getSingle->title_txt_content;
                            
                            $single['label'] = $title != "" ? $title : $getSingle->title_txt_content;
                            $single['template'] = $newValue['template']['name'];
                            $single['templateslug'] = @$newValue['template']['slug'];
                            $single['slug'] = $newValue['slug'];
                            $single['image'] = isset($newValue['menuimg']) ? $newValue['menuimg'] : $newValue['cover'];
                            $single['folder'] = $label['type'] == 'folder' ? true : false;
                            $single['lang'] = $lang;
                        }
                    } else if($label['type'] == 'property'){
                        $getSingle = DB::table($label['table'])->where('name_txt_general', $label['name_txt_general'])->where('locale_hidden_general', $lang)->where('enabled_is_general', 'true')->first();
                        if($getSingle){
                            $single['label'] = $getSingle->name_txt_general;
                            $single['template'] = '';
                            $single['templateslug'] = '';
                            $single['slug'] = '';
                            $single['image'] = HelpersFilemanager::getSinglePathFmanagerById($getSingle->primary_filemanager_general);
                            $single['folder'] = $label['type'] == 'folder' ? true : false;
                            $single['lang'] = $lang;

                            $getPropertyItem = DB::table('olmo_propertyitem')->orderby('position_ord_general', 'asc')->where('postid_hidden_general', $getSingle->id)->where('enabled_is_general', 'true')->get()->toArray();
                            $itemsProp = [];
                            foreach($getPropertyItem as $item){
                                $itemProp['label'] = $item->code_txt_general;
                                $itemProp['template'] = '';
                                $itemProp['templateslug'] = '';
                                $itemProp['slug'] = '?'.$item->postid_hidden_general.'='.$item->id;
                                $itemProp['image'] = HelpersFilemanager::getSinglePathFmanagerById($item->image_filemanager_general);
                                $itemProp['folder'] = false;
                                $itemProp['lang'] = $lang;
                                array_push($itemsProp, $itemProp);                             
                            }
                            $single['children'] = HelpersSerializer::cleanKeyArray($itemsProp);
                        }
                    } else if($label['type'] == 'model'){
                        $model = explode('_', $label['table']);
                        $model = ('App\Containers\Backoffice\\'.ucfirst($model[1]).'\Models\\'.ucfirst($model[1]));
                        if(env('MENU_MULTID', false)){
                            //TODO: questa query sotto è da controllare meglio perchè in questo momento considera solo id sotto il 10
                            $getSingle = $model::orderby('position_ord_general', 'asc')->where('enabled_is_general', 'true')->where('locale_hidden_general', $lang)->where('menu_multid_general', 'LIKE', '%'.$label['dbid'].'%')->get()->toArray();
                        } else {
                            $getSingle = $model::orderby('position_ord_general', 'asc')->where('enabled_is_general', 'true')->where('locale_hidden_general', $lang)->where('menu_is_general', 'true')->get()->toArray();
                        }

                        $fieldArray = ['template_id_general', 'title_txt_content', 'menutitle_txt_menu', 'cover_filemanager_content', 'menuimg_filemanager_menu', 'slug_txt_general', 'name_txt_general'];
                        $newArrayModel = [];
                        $makeTheElement = [];

                        foreach($getSingle as $k=>$single){
                            foreach($single as $key=>$item){
                                if(!in_array($key, $fieldArray)){
                                    unset($getSingle[$k][$key]);
                                }
                            }
                            $newValue = HelpersSerializer::routeResponse($getSingle[$k], 'stdClass', 0, 'route', '', false, $lang); 

                            $title = isset($newValue['menutitle']) ? $newValue['menutitle'] : $newValue['title'];
                            
                            $makeTheElement['label'] = $title != "" ? $title : $newValue['title'];
                            $makeTheElement['template'] = $newValue['template']['name'];
                            $makeTheElement['templateslug'] = @$newValue['template']['slug'];
                            $makeTheElement['slug'] = $newValue['slug'];
                            $makeTheElement['image'] = isset($newValue['menuimg']) ? $newValue['menuimg'] : $newValue['cover'];
                            $makeTheElement['folder'] = false;
                            $makeTheElement['lang'] = $lang;

                            array_push($newArrayModel, $makeTheElement);
                            $makeTheElement = [];
                        }

                        $single = $newArrayModel;
                    }                    
                } else {
                    $single['label'] = $label['name_txt_general'];
                    $single['folder'] = $label['type'] == 'folder' ? true : false;
                    $single['slug'] = $label['url'];
                }
                if(isset($label['children'])){
                    if(count($label['children']) > 0){
                        $single['children'] = createLabelAndFolder($label['children'], $lang, true);
                    } 
                    // else {
                        // $single['children'] = [];
                    // }
                }

                if(!$child){
                    if(count($single) > 0){
                        array_push($thelabel, $single);
                    }
                    $single = [];
                } else {
                    if($label['type'] == 'model'){
                        foreach($single as $s){
                            array_push($thelabel, $s);
                        }
                        $single = [];
                    } else {
                        array_push($thelabel, $single);
                        $single = [];
                    }
                    //$thelabel = $single; 
                }
            }

            return $thelabel;
        }

        return createLabelAndFolder($menu, $lang);
    }

    public static function generateRawTheMenu($request, $lang)
    {

        $firstline = [];
        $secondline = [];
        $thirdline = [];
        $fourthdline = [];
        $fivethdline = [];        

        $name = $request->name;
        $menudb = DB::table('olmo_menu')->where('name_txt_general', $name)->where('enabled_is_general', 'true')->where('locale_hidden_general', $lang)->first();          

        if(isset($menudb->menu_menu_menu)){            
            $menu = json_decode($menudb->menu_menu_menu, true);
        } else {
            $menu = [];
        }

        foreach($menu as $item){
            if($item['level'] == 0){
                array_push($firstline, $item);
            }
        }

        // $firstline = [];
        // foreach($firstline as $key=>$item) {
        //     generateTheLines($menu, $key, $item);
        // }

        // function generateTheLines($menu, $key, $item) {
        //     foreach($menu as $k=>$label){
        //         if($label['submenu'] == $item['name_txt_general']) {
        //             generateTheLines($menu, $key, $label);
        //         }   
        //     }  
        // }        

        foreach($firstline as $key=>$item){
            foreach($menu as $k=>$label){
                if($label['submenu'] == $item['name_txt_general']){
                    foreach($menu as $third){
                        if($third['submenu'] == $label['name_txt_general']){
                            foreach($menu as $fourth){
                                if($fourth['submenu'] == $third['name_txt_general']){
                                    foreach($menu as $fiveth){
                                        if($fiveth['submenu'] == $fourth['name_txt_general']){
                                            array_push($fivethdline, $fiveth);
                                        }
                                    }
                                    $fourth['children'] = $fivethdline;
                                    $fourth['dbid'] = $menudb->id;
                                    array_push($fourthdline, $fourth);
                                    $fivethdline = [];
                                }
                            }
                            $third['children'] = $fourthdline;
                            $third['dbid'] = $menudb->id;
                            array_push($thirdline, $third);
                            $fourthdline = [];
                        }
                    }
                    $label['children'] = $thirdline;
                    $label['dbid'] = $menudb->id;
                    array_push($secondline, $label);
                    $thirdline = [];
                }
            }
            $firstline[$key]['children'] = $secondline;
            $firstline[$key]['dbid'] = $menudb->id;
            $secondline = [];
        }
        

        return $firstline;

    }

}