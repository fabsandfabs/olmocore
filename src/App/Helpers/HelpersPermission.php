<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersUser;

class HelpersPermission
{

  private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

  private const CONTAINER_MENU = 'Menu';

  private const CONTAINER_PERMISSION = 'Permission';

  public static function getMenu(Request $request)
  {

    $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_MENU . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($menuDirectory);

    if ($directory) {

      $token = $request->header('x-token');
      $user = self::userRole($token);

      if (!$user) {
        return response(['No user provide'], 403);
      }

      $menuPath = $menuDirectory . 'menu.json';
      $menuRaw = json_decode(file_get_contents($menuPath), true);

      return response(self::userPermissionMenu($menuRaw, $user), 200);
    }

    return response(['No directory provide'], 400);
  }

  public static function userPermissionMenu($data, $user)
  {

    $permissionDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_PERMISSION . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($permissionDirectory);

    if ($directory) {

      $permissionPath = $permissionDirectory . 'permissionmenu.json';
      $permissionRaw = json_decode(file_get_contents($permissionPath), true);

      if ($user['name_txt_general'] == 'admin') {
        $data = self::permissions($data, $permissionRaw['admin']);
      } else if ($user['name_txt_general'] == 'editor') {
        $data = self::permissions($data, $permissionRaw['editor']);
      } else if ($user['name_txt_general'] == 'publisher') {
        $data = self::permissions($data, $permissionRaw['publisher']);
      }
    }

    return $data;
  }

  public static function userRole($token)
  {

    $getUser = Db::table('olmo_user')
      ->where('token_hidden_general', $token)
      ->join('olmo_role', 'olmo_user.role_id_general', '=', 'olmo_role.id')
      ->select('olmo_user.role_id_general', 'olmo_role.name_txt_general')->first();

    $userRole = null;
    if ($getUser) {
      $userRole = $getUser;
    }

    return (array)$userRole;
  }

  public static function permissions($data, $permissionRaw)
  {

    foreach ($permissionRaw as $permission) {
      if ($permission['type'] == 'dropdown') {
        $data = self::dropdownPermission($data, $permission['subheader'], $permission['items']);
      }
      if ($permission['type'] == 'linear') {
        $data = self::linearPermission($data, $permission['subheader'], $permission['items']);
      }
    }

    return $data;
  }

  public static function dropdownPermission($data, $subheader, $items)
  {
    $index = 0;
    $menu = [];


    if (count($items) > 0) {
      foreach ($data as $key => $label) {
        if ($label['subheader'] == $subheader) {
            if($subheader == "website" || $subheader == "file manager"){
                foreach ($label['items'] as $kset => $setting) {
                    if (in_array($setting['name'], $items)) {
                        unset($data[$key]['items'][$kset]);
                        $index = $key;
                    }
                }
                $menu = array_values($data[$index]['items']);
                $data[$index]['items'] = $menu;

            }else{
                foreach ($label['items'] as $kset => $setting) {
                    $itemslabel = $setting['items'];
                    
                    foreach ($itemslabel as $k => $item) {
                        if (in_array($item['name'], $items)) {
                            unset($data[$key]['items'][$kset]['items'][$k]);
                            $index = $key;
                            $indexsub = $kset;
                        }
                    }
                }

                $menu = array_values($data[$index]['items'][$indexsub]['items']);
                $data[$index]['items'][$indexsub]['items'] = $menu;
            }
        }
      }

    } else {
      foreach ($data as $key => $label) {
        if ($label['subheader'] == $subheader) {
          array_splice($data, $key, 1);
        }
      }
    }

    return $data;
  }

  public static function linearPermission($data, $subheader, $permissionRaw)
  {
    $index = 0;
    if (count($permissionRaw) > 0) {
      foreach ($data as $key => $label) {
        if ($label['subheader'] == $subheader) {

          foreach ($label['items'] as $kset => $website) {

            if (in_array($website['name'], $permissionRaw)) {
              // array_splice($data[$key]['items'], $kset, 1);
              unset($data[$key]['items'][$kset]);
              $index = $key;
            }
          }
        }
      }

      $menu = array_values($data[$index]['items']);
      $data[$index]['items'] = $menu;
    } else {
      foreach ($data as $key => $label) {
        if ($label['subheader'] == $subheader) {
          array_splice($data, $key, 1);
        }
      }
    }

    return $data;
  }

  public static function userPermissionFields($table, $data)
  {
    $header = getallheaders();
    $token =  @$header['x-token'];
    if ($token == '') {
      $token =  @$header['X-Token'];
    }

    $user = self::userRole($token);
    $attribute = false;

    $name = str_replace('olmo_', '', $table);
    if(HelpersData::isDefaultModel($name) AND Schema::hasTable($table)){
      $model = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name));
      if(class_exists($model)){
        $attribute = $model::theAttributes();
      }
    } else if(Schema::hasTable($table)) {
      $model = ('App\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name));
      $attribute = $model::theAttributes();
    }

    if (!$user) {
      return response(['No user provide'], 403);
    }

    $permissionDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_PERMISSION . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($permissionDirectory);

    if ($directory and $user['name_txt_general'] != 'superadmin' AND $attribute) {

      $permissionPath = $permissionDirectory . 'permissionfield.json';
      $permissionRaw = json_decode(file_get_contents($permissionPath), true);

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == 'global-model') {
          foreach ($item['fields'] as $v) {
            if(!in_array($v, $attribute['requiredbackoffice'])){
              unset($data[$v]);
            }
          }
        }
      }

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == $table) {
          foreach ($item['fields'] as $v) {
            if(!in_array($v, $attribute['requiredbackoffice'])){
              unset($data[$v]);
            }
          }
        }
      }
    }

    return $data;
  }

  public static function userPermissionFieldsDisabled($table, $field)
  {
    $header = getallheaders();
    $token =  @$header['x-token'];
    if ($token == '') {
      $token =  @$header['X-Token'];
    }

    $user = self::userRole($token);

    if (!$user) {
      return response(['No user provide'], 403);
    }

    $permissionDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_PERMISSION . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($permissionDirectory);

    if ($directory and $user['name_txt_general'] != 'superadmin') {

      $permissionPath = $permissionDirectory . 'permissionfield.json';
      $permissionRaw = json_decode(file_get_contents($permissionPath), true);

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == 'global-model') {
          foreach ($item['fields'] as $v) {
            if($field == $v){
              return true;
            }
          }
        }
      }

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == $table) {
          foreach ($item['fields'] as $v) {
            if($field == $v){
              return true;
            }
          }
        }
      }
    }

  }

  public static function userPermissionAction($attribute, $duplicatevalue, $deletevalue)
  {
    $header = getallheaders();
    $token =  @$header['x-token'];
    if ($token == '') {
      $token =  @$header['X-Token'];
    }

    $user = self::userRole($token);

    if (!$user) {
      return response(['No user provide'], 403);
    }

    $permissionDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_PERMISSION . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($permissionDirectory);

    $create = true;
    $translate = isset($attribute['lang']) ? $attribute['lang'] : false;
    $duplicate = $duplicatevalue;
    $delete = $deletevalue;
    $visualblockcreate = true;
    $visualblockdelete = true;
    $visualblockmodify = true;

    if ($directory and $user['name_txt_general'] != 'superadmin') {

      $permissionPath = $permissionDirectory . 'permissionaction.json';
      $permissionRaw = json_decode(file_get_contents($permissionPath), true);

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == 'global-model') {
          foreach ($item['actions'] as $v) {
            if($v == "duplicate"){
              $duplicate = false;
            }
            if($v == "create"){
              $create = false;
            }
            if($v == "delete"){
              $delete = false;
            }
            if($v == "translate"){
              $translate = false;
            }
            if($v == "visualblock-create"){
              $visualblockcreate = false;
            }
            if($v == "visualblock-delete"){
              $visualblockdelete = false;
            }
            if($v == "visualblock-modify"){
              $visualblockmodify = false;
            }
          }
        }
      }

      foreach ($permissionRaw[$user['name_txt_general']] as $k => $item) {
        if ($item['model'] == $attribute['table']) {
          foreach ($item['actions'] as $v) {
            if($v == "duplicate"){
              $duplicate = false;
            }
            if($v == "create"){
              $create = false;
            }
            if($v == "delete"){
              $delete = false;
            }
            if($v == "translate"){
              $translate = false;
            }
            if($v == "visualblock-create"){
              $visualblockcreate = false;
            }
            if($v == "visualblock-delete"){
              $visualblockdelete = false;
            }
            if($v == "visualblock-modify"){
              $visualblockmodify = false;
            }
          }
        }
      }
    }

    $data['create'] = $create;
    $data['duplicate'] = $duplicate;
    $data['delete'] = $delete;
    $data['translate'] = $translate;
    $data['visualblockcreate'] = $visualblockcreate;
    $data['visualblockdelete'] = $visualblockdelete;
    $data['visualblockmodify'] = $visualblockmodify;

    return $data;
  }
}
