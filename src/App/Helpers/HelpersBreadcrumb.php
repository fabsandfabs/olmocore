<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\Helperslang;
use Olmo\Core\Containers\Backoffice\Template\Models\Template;


class HelpersBreadcrumb
{

    public static function templateSlugTranslate($lang, $data, $defaultlang) 
    {      

      if($defaultlang == $lang){
        $templateSlug = Template::where('id', $data['template_id_general'])->first()->slug_txt_slugs;
      } else {
        $templateSlug = Db::table('olmo_templateslug')->where('defaultid', $data['template_id_general'])->where('lang', $lang)->first()->slug;
      }

      return $templateSlug;

    }

    public static function getTemplate($lang, $templateSlug, $defaultlang, $data = null, $key = 0)
    {
      
      if($defaultlang == $lang){
        if($data AND $key != 0){
          $templateSlug = Template::where('id', $data->template_id_general)->first();
        } else {
          $templateSlug = Template::where('slug_txt_slugs', $templateSlug)->first();
        }
        if(isset($data['category_id_general']) && !$templateSlug){         
          if($data['category_id_general'] !== ""){
            $category = Db::table('olmo_category')->where('id', $data['category_id_general'])->first();            
            $templateSlug = Template::where('id', $category->template_id_general)->first();
          }
        }

      } else {    
        if($data AND $key != 0){
          $templateId = Db::table('olmo_templateslug')->where('lang', $lang)->where('defaultid', $data->template_id_general)->first();          
        } else {
          $templateId = Db::table('olmo_templateslug')->where('slug', $templateSlug)->first();
        }
        $templateSlug = null;
        if($templateId){
          $templateId = $templateId->defaultid;
          $templateSlug = Template::where('id', $templateId)->first();
        }

      }

      return $templateSlug;

    }

    public static function breadcrumb($lang, $data, $modelName, $request) {
        $page = [];
        $defaultlang = Helperslang::getDefaultLang();
        $templateSlug = self::templateSlugTranslate($lang, $data, $defaultlang);       
        $templates = explode('/', $templateSlug);
        $model = ('App\Containers\Backoffice\\'.ucfirst($modelName).'\Models\\'.ucfirst($modelName))::theAttributes();
        $breadcrumbs = isset($model['breadcrumbs']) ? $model['breadcrumbs'] : false;

        if($breadcrumbs){

          $page[0] = [
            'name'  => 'home',
            'title' => 'home',
            'slug'  => '',
            'singlepage' => false,
            'template' => HelpersSerializer::cleanKeyObj(Template::where('name_txt_general','home')->first()->toArray())
          ];
    
          $i = 1;          
  
          foreach ($templates as $key=>$slug) {            
            // print_r("<br>------- slug --------<br>");
            // print_r($slug);
            // print_r("<br>------- slug --------<br>");
    
              if (str_contains($slug, ':slug')) {
                // $template = Template::where('slug_txt_slugs',$templateSlug)->first();
                $template = self::getTemplate($lang, $templateSlug, $defaultlang);
    
                $page[$i] = [
                    'name'       => $data->name_txt_general,
                    'title'      => isset($data->title_txt_content) ? $data->title_txt_content : $data->name_txt_general,
                    'slug'       => $data->slug_txt_general,
                    'singlepage' => str_contains($slug,':slug') ? true : false,
                    'template'   => HelpersSerializer::cleanKeyObj($template->toArray())
                ];
    
                $i++;

              } else if($key == 1 AND str_contains($slug, ':category')) {

                if($lang == $defaultlang){
                  $template = Template::where('slug_txt_slugs', $templates[0].'/:slug')->first();
                } else {
                  $template = Db::table('olmo_templateslug')->where('slug', $templates[0].'/:slug')->where('lang', $lang)->first();
                  $template = Template::where('id', $template->defaultid)->first();
                }
                $model    = $template->model_select_general;          
                $table    = 'olmo_'.$model;
                $realSlug = $slug == ':category' ? self::getCategory($data) : $slug;   
                $pages    = Db::table($table)->where('locale_hidden_general',$lang)->where('slug_txt_general', $realSlug)->first();
                $page[$i] = [
                  'name' => $pages->name_txt_general,
                  'title' => isset($pages->title_txt_content) ? $pages->title_txt_content : $pages->name_txt_general,
                  'slug' => $pages->slug_txt_general,
                  'singlepage' => true,
                  'template' => HelpersSerializer::cleanKeyObj($template->toArray())
                ];
    
                $i++;                
    
              } else if(str_contains($slug, ':category')) {
                
                // $template = Template::where('slug_txt_slugs', ':slug')->where('model_select_general', $modelName)->first();
                $template = self::getTemplate($lang, '/:slug', $defaultlang, $data);
                $model    = $template->model_select_general;          
                $table    = 'olmo_'.$model;
                $realSlug = $slug == ':category' ? self::getCategory($data) : $slug;                   
                $pages    = Db::table($table)->where('locale_hidden_general',$lang)->where('slug_txt_general', $realSlug)->first();
                $page[$i] = [
                  'name' => $pages->name_txt_general,
                  'title' => isset($pages->title_txt_content) ? $pages->title_txt_content : $pages->name_txt_general,
                  'slug' => $pages->slug_txt_general,
                  'singlepage' => true,
                  'template' => HelpersSerializer::cleanKeyObj($template->toArray())
                ];
    
                $i++;

              } else if($key == 1) {
                // $template = Template::where('slug_txt_slugs',$templates[0].'/:slug')->first();
                $template = self::getTemplate($lang, $templates[0].'/:slug', $defaultlang);
                if(!$template){
                  $template = self::getTemplate($lang, $templates[0].'/'.$templates[1], $defaultlang);
                }
                $model    = $template->model_select_general;          
                $table    = 'olmo_'.$model;
                $realSlug = $slug == ':category' ? self::getCategory($data) : $templates[0].'/'.$slug;
                $pages    = Db::table($table)->where('locale_hidden_general',$lang)->where('slug_txt_general', $realSlug)->first();
                if(!$pages){
                  $pages = Db::table($table)->where('locale_hidden_general',$lang)->where('slug_txt_general', $templates[1])->first();
                }
                $page[$i] = [
                  'name' => $pages->name_txt_general,
                  'title' => isset($pages->title_txt_content) ? $pages->title_txt_content : $pages->name_txt_general,
                  'slug' => $pages->slug_txt_general,
                  'singlepage' => true,
                  'template' => HelpersSerializer::cleanKeyObj($template->toArray())
                ];
    
                $i++;
    
              } else {      
                // $template = Template::where('slug_txt_slugs',$slug)->first();
                $template = self::getTemplate($lang, $slug, $defaultlang, $data, $key);                
                $model    = $template->model_select_general;
                $table    = 'olmo_'.$model;
                $pages    = Db::table($table)->where('locale_hidden_general',$lang)->where('template_id_general', $template->id)->first();
    
                $page[$i] = [
                    'name'     => $pages->name_txt_general,
                    'title'    => isset($pages->title_txt_content) ? $pages->title_txt_content : $pages->name_txt_general,
                    'slug'     => $pages->slug_txt_general,
                    'singlepage' => false,
                    'template'   => HelpersSerializer::cleanKeyObj($template->toArray())
                ];
    
                $i++;
              }
    
          }

          return $page;

        }

        return null;


    }

    public static function getCategory($data)
    {

      $getCategory = Db::table('olmo_category')->where('id', $data->category_id_general)->first();

      return $getCategory->slug_txt_general;
    }


}