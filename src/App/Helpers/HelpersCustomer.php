<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

class HelpersCustomer
{
    public static function createCustomerToken($user)
    {
        
        $token = md5(time() . 'OLMO');
        if(env('CUSTOMER_SESSION') == 'multisession'){
            $session = DB::table('olmo_sessions')->insertGetId(['customer_id' => $user->id, 'token' => $token]);
        } else {
            $session = DB::table('olmo_customer')->where('id', $user->id)->update(['token_read_general' => $token]);
        }

        if($session){
            return $token;
        }

    }

    public static function getCustomerbyOrderId($order_id){
        $order_info = Db::table('olmo_order')->where('id', $order_id)->first();
        $customer = DB::table('olmo_customer')->where('id', $order_info->customerid_hidden_customer)->first();
        unset($order_info->customerid_hidden_customer);
        return [$order_info, $customer];      
    }

    public static function endCustomerSession()
    {

        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if ($X_token == '') {
            $X_token =  @$header['X-Token'];
        }

        if(env('CUSTOMER_SESSION') == 'multisession'){
            $response = DB::table('olmo_sessions')->where('token', $X_token)->first();
            DB::table('olmo_sessions')->where('token', $response->token)->update(['token' => '']);
        } else {
            $date = date('Y-m-d H:i:s');
            $response = DB::table('olmo_customer')
                ->where('token_read_general', $X_token)
                ->update(['token_read_general' => '', 'endsession_read_general' => $date]);
        }

        return $response;
    }

    public static function getCustomer($request){

        $header = $request;
        $xtoken =  $header->hasHeader('x-token');
        $guesttoken = $header->hasHeader('x-guest');
        $agenttoken = $header->hasHeader('x-agent');

        $x_Token =  $header->hasHeader('x-Token');
        $guest_Token = $header->hasHeader('x-Guest');  
            
        if($agenttoken && $xtoken){

            $agent_token = $header->header('x-token');
            $customer_email = $header->header('x-agent');
            $response = self::getCustomerFromEmail($customer_email, $agent_token);
            return $response;
        }

        if($guesttoken && $xtoken){
            $token = $header->header('x-token');
            $response = self::getCustomerFromToken($token);
            $response['x-guest'] = $request->header('x-guest');
            return $response;
        }

        if($xtoken){
            $token = $header->header('x-token');
            $response = self::getCustomerFromToken($token);
            if(!$response){
                return null;
            }
            return $response;
        }

        if($guesttoken){
            return $request->header('X-Guest');
        }

        if($x_Token){
            $x_Token = $header->header('x-token');
            $response = self::getCustomerFromToken($x_Token);
            if(!$response){
                return null;
            }            
            return $response;
        }

        if($guest_Token){            
            return $request->header('x-guest');
        }

        return null;

    }

    public static function getCustomerFromEmail($email, $agent_id = false){
        // the query has only email because the request will only start from the order pipeline of backoffice
        // and if the request starts from an admin, the agent_agent_general column will not have the user id in it
        if($agent_id == false){
            return DB::table('olmo_customer')->where('email_email_general', $email)->first();
        }else{
            return DB::table('olmo_customer')->where('email_email_general', $email)->where('agent_agent_general',$agent_id)->first();
        }
    }

    public static function getCustomerbyId($id, $agent_id = false){
        if($agent_id == false){
            return DB::table('olmo_customer')->where('id', $id)->first();
        }else{
            return DB::table('olmo_customer')->where('id', $id)->where('agent_agent_general',$agent_id)->first();
        }
    }

    public static function resetCustomerToken($customer, $request)
    {
        if(env('CUSTOMER_SESSION') == 'multisession'){
            $token = $request->token;
            $response = DB::table('olmo_sessions')->where('token', $token)->delete();
        } else {
            $response = DB::table('olmo_customer')->where('id', $customer['id'])->update(['tokenactive_read_general' => '']);
        }
        return $response;
    }

    public static function getCustomerFromToken($token)
    {
        if(env('CUSTOMER_SESSION') == 'multisession'){
            $response = DB::table('olmo_sessions')->where('token', $token)->first();
            if($response){
                $response = DB::table('olmo_customer')->where('id', $response->customer_id)->first();
            }
        } else {
            $response = DB::table('olmo_customer')->where('tokenactive_read_general', $token)->first();  
        }

        $response = json_decode(json_encode($response), true);

        return $response;
    }

    public static function getActiveToken($request)
    {
        $token = $request->input('token');

        $customer = self::getCustomerFromToken($token);

        if($customer){
            return $customer;
        } else {
            return null;
        }
    }
    
    public static function changeStatus($userid, $status)
    {
        $user = Db::table('olmo_customer')->where('id', $userid)->first();
        if($user){
            $userstatus = $user->customerstatus_select_general;
            if($userstatus == $status){
                return 409;
            }
        }
        $updatestatus = Db::table('olmo_customer')->where('id', $userid)->update(['customerstatus_select_general' => $status]);
        if($updatestatus){
            return 200;
        } else {
            return 400;
        }
    }

    public static function getPartsField($field) {
        $object = [];

        $item = explode('_', $field);

        $f = self::getTypeFront(@$item[0], @$item[1]);

        $object['name']       = @$item[0];
        $object['element']    = $f['element'];
        $object['typology']   = $f['typology'];
        $object['label']      = @$item[0];
        $object['required']   = true;
        $object['option']     = $f['option'];

        return $object;
    }
    
    public static function getValidFieldForms($field, $type, $exclude) {
        $valid = false;
        $group  = @explode('_',$field)[2];
        if($group != '') {
            if(in_array($group,$type)){
                $valid = true;
            }
            if(in_array($field,$type)){
                 $valid = true;
            }
        }

        if(in_array($field,$exclude)){
            $valid = false;
        }

        return $valid;
    }    

    public static function getTypeFront($element, $type) {
        $t = 'text';
        $e = 'Input';
        $option = [];

        if($type == 'pwd')
            $t = 'password';

        if($type == 'is')
            $t = 'checkbox';

        if($element == 'country'){
            $e = 'InputList';
            $t = 'select';
            $fulltable = Db::table('olmo_'.$element)->get();
            if($fulltable){
                $fullTableCleaned = HelpersSerializer::cleanKeyArray($fulltable);
                foreach($fullTableCleaned as $key=>$field){
                    $fullTableCleaned[$key]['key'] = $field['title'];
                    $fullTableCleaned[$key]['value'] = $field['name'];
                }
            }            
            $option = $fullTableCleaned;
        }

        return [
            'typology' => $t,
            'element'  => $e,
            'option'   => $option
        ];
    }    
    

    public static function getPaymentMethods($customer){
        return HelpersDBRelation::dropdownListValuePair(Db::table('olmo_paymentmethod')->where('enabled_is_general', 'true')->get());
    }

    /**
     * For backoffice, it return value-key pair obj
     */
    public static function getTypeAddresses($customer, $type = 'billing'){
        $addresses = Db::table('olmo_customeritem')
        // ->orderby('default_is_address', '') Da capire se 
        ->where([
            'customer_hidden_content' => $customer->id,
            'enabled_is_general' => 'true',
            'typeaddress_select_address' => $type
        ])->get();
            
        $array = [];
        foreach($addresses as $address){
            $data = HelpersSerializer::cleanCreatedModby($address);
            $data->name_txt_general = $data->address_txt_address;
            if($data){
                array_push($array, $data);
            }
        }
        // return $array;
        $array = HelpersDBRelation::dropdownListValuePair($array);
        return $array;
    }

    public static function getBillingMethods($customer_id){
        return Db::table('olmo_customeritem')->where('');
    }
}