<?php

namespace Olmo\Core\App\Helpers;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Http\Controller\BackController;


class HelpersDotsPosition
{

    public static function createNewDots($request, $attribute) 
    {

        $lang = $request->lang;
        $model = $request->input('model');
        $column = $request->input('name');
        $id = $request->input('id');

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing($attribute['table']);
        /** Flip values in the keys */
        $columns_flip = array_flip($columns);
        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function(&$v, $k, $lang){
            if(HelpersData::setLabel($k) == 'locale'){
                $v = $lang;  
            } else if(HelpersData::getType($k) == 'readonly' || HelpersData::getType($k) == 'select') {
                $v = null;
            } else if(HelpersData::getType($k) == 'number') {
                $v = 0.00;                
            } else if($k == 'created_at' || $k == 'updated_at'){
                $v = null;
            } else {
                $v = "";  
            }
        }, $lang);

        unset($columns_flip['id']);

        /**
         * Prefill some field to make the update possible
         */
        if(isset($columns_flip['model_hidden_content'])){
            $columns_flip['model_hidden_content'] = $model;
        }
        if(isset($columns_flip['postid_hidden_content'])){
            $columns_flip['postid_hidden_content'] = $id;
        }   
        if(isset($columns_flip['columnname_hidden_content'])){
            $columns_flip['columnname_hidden_content'] = $column;
        }                

        /** Insert the post in the DB */
        $response = Db::table($attribute['table'])->insertGetId($columns_flip);
        $check = Db::table('olmo_'.$model)->where('id', $id)->first();

        if($check->{$column} == ''){
            Db::table('olmo_'.$model)->where('id', $id)->update([$column => $response]);
        } else {
            Db::table('olmo_'.$model)->where('id', $id)->update([$column => $check->{$column}.','.$response]);
        }

        $response = array('id' => $response);

        return response($response, 200);  

    }

    public static function getPost($request)
    {
        $id = $request->id;
        $dot = Db::table('olmo_dotsposition')->where('id', $id)->first();
        $dot = HelpersSerializer::routeResponse($dot);

        return response($dot, 200);
    }

    public static function deleteDot($attribute, $request)
    {
        $model = $request->input('model');
        $column = $request->input('name');
        $id = $request->input('id');
        $postid = $request->input('postid');

        $check = Db::table('olmo_'.$model)->where('id', $postid)->first();
        if($check){
            $ids = explode(',', $check->{$column});
            if (($key = array_search($id, $ids)) !== false) {
                unset($ids[$key]);
            }
            $ids = implode(',', $ids);

            Db::table('olmo_'.$model)->where('id', $postid)->update([$column => $ids]);
            Db::table($attribute['table'])->where('id', $id)->delete();

            return response(200);
        }
        return response(400);

    }

    /**
     * Kind of tricky method, could be inproved
     * 
     * Get the 
     */
    public static function serializeDotspostionUpdating($request, $attribute, $dataPost)
    {

        $id = $dataPost['id'];

        $body = HelpersData::normalizeBody($request);        
        Db::table($attribute['table'])->where('id', $id)->update($body);
        $data = Db::table($attribute['table'])->where('id', $id)->first();

        if ($data) {          

            return response(200);

        } else {
            return response(404);
        }       

    }   

    /**
     * This method is a clone of BackController serializeSinglePost
     * but with few adjusments to fit with the dotsposition options
     * it could be merged with the BackController serializeSinglePost 
     * but I think could be more clean and stable keep it alone
     */
    public static function serializeSingleDotsPosition($request, $attribute)
    {
        $id = $request->id;
        $lang = $request->lang;
        $data = Db::table('olmo_dotsposition')->where('id', $id)->first();

        if ($data) {

            unset($data->created_at);
            unset($data->updated_at);
    
            $items = [];
            $i = 0;

            foreach ($data as $key => $value) {

                $item = [];

                $item['name'] = (string)$key;
                $item['label'] = HelpersData::setLabel((string)$key);
                $item['tab'] = HelpersData::setTab((string)$key);
                $item['type'] = HelpersData::getType((string)$key, $key);
                $item['required'] = false;
                
                $item['values'] = BackController::getValues($key, (string)$value, $item['type'], $lang, $attribute['table'], $id);
                $item['value'] = BackController::getValue($key, (string)$value, $item['values'], $item['type'], $attribute['table'], $id);

                $items[$i] = $item;
                $i++;

            }

            return $items;

        } else {
            return response(404);
        }        

    }

    public static function getDots($value, $type = null)
    {
        if($value == ''){
            return '';
        } else{
            $slides = explode(',',$value);
            foreach($slides as $key=>$item){
                if($type){
                    $slides[$key] = self::getDotFront($item, $type);
                } else {
                    $slides[$key] = self::getDot($item, $type);
                }
            }
            return $slides;
        }        
    }

    public static function getDot($value)
    {
        $dot = DB::table('olmo_dotsposition')->where('id', $value)->select('id','name_txt_content as name','left_num_content as left','top_num_content as top','image_filemanager_content as image')->first();
        
        if($dot) {
            $dot->image = HelpersFilemanager::getSinglePathFmanagerById($dot->image, 'media');
            return $dot;
        } else {
            return '';
        }
    }    

    public static function getDotFront($value)
    {
        $dot = DB::table('olmo_dotsposition')->where('id', $value)->first();
        
        if($dot) {
            $dot = HelpersSerializer::routeResponse($dot);
            return $dot;
        } else {
            return '';
        }
    }
    
    public static function getDotslist($thekey, $value)
    {
        if($thekey['key'] == 'list'){            
            return self::getDots($value, 'front');
        }
    }
}
