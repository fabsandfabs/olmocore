<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

class HelpersRulesField
{

    // This is the pattarn to use to create rules
    //
    // ----- Unique
    // [
    //     'type' => 'unique',
    //     'field' => 'slug_txt_general',
    //     'table' => 'olmo_service',
    //     'lang' => true, default: true
    //     'errortxt' => 'Slug duplicate'
    // ] 
    // ----- Equal
    // [
    //     'type' => 'unique',
    //     'field_one' => 'password_pwd_general',
    //     'field_two' => 'confirmpassword_pwd_general',
    //     'errortxt' => 'Password not match'
    // ] 
    // ----- Check slug length
    // ----- Check slug length


    public static function checkRules($request, $attribute, $type = 'back')
    {

        $errors = [];        

        $rules = [];
        if(isset($attribute['rules'])){
            $rules = $attribute['rules'];
        }

        if($type == 'front'){
            if(isset($attribute['rulesform'])){
                $rules = $attribute['rulesform'];
            }
        }
    
        foreach($rules as $rule){

            if($rule['type'] == 'equal') {
                if(self::checkEqual($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'unique') {
                if(self::checkUnique($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'default') {
                if(self::checkDefault($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'ifthen') {
                if($pt = self::checkIfThen($request, $rule)){
                    // array_push($errors, $rule['errortxt']);
                    array_push($errors, $pt);
                }                
            }            

        }

        return $errors;
       
    }

    public static function checkIfThen($request, $rule)
    {        

        $condition_one = $rule['condition_one'];
        $condition_two = $rule['condition_two'];

        $filed_one_request = $request->input($rule['field_one']);
        if(!is_array($filed_one_request)){
            $filed_one_request = var_export($filed_one_request, true);
        }
        
        $filed_two_request = $request->input($rule['field_two']);
        if(!is_array($filed_two_request)){
            $filed_two_request = var_export($filed_two_request, true);
        }

        if($filed_one_request == $condition_one){
            switch($condition_two){
                case 'notEmpty':
                    if(is_array($filed_two_request)){ // for dropdowns options
                        if(empty($filed_two_request['key'])){ // TODO: for other purposes, add logic
                            return $rule['errortxt'];
                        }
                    }else if($filed_two_request == ""){
                        return $rule['errortxt'];
                    }
                    break;
                default:
                    if($filed_two_request != $condition_two){
                        return $rule['errortxt'];
                    }
                    break;
            }
        }

    }

    public static function checkDefault($request, $rule)
    {
        $id = $request->input('id');
        // $request = json_decode(json_encode($request), true);
        $default = $request[$rule['field']];

        if($default){
            /** Check if a different post has been already set as default */
            $check = Db::table($rule['table'])->where('id', '!=', $id)->where($rule['field'], 'true')->get()->count();
            if($check){
                return $rule['errortxt'];
            }

        }
    }

    public static function checkEqual($request, $rule)
    {

        $a = $request->input($rule['field_one']);
        $b = $request->input($rule['field_two']);

        if($a != $b){
            return $rule['errortxt'];
        }
        
    }

    public static function checkUnique($request, $rule)
    {

        $isLang = false;
        $lang = $request->lang;
        $field = $request->input($rule['field']);
        
        /** Check if the lang is set */
        if(isset($rule['lang'])){
            $isLang = $rule['lang'];
        }

        /** If the lang is set check the post in the same language if not to all */
        if($isLang){
            $check = Db::table($rule['table'])->where($rule['field'],$field)->where('locale_hidden_general', $lang)->where('id','!=',$request->id)->get()->count();
        } else {
            $check = Db::table($rule['table'])->where($rule['field'],$field)->where('id','!=',$request->id)->get()->count();                    
        } 
                        
        /** If the query result is different than 0 print the error */
        if($check > 0){
            return $rule['errortxt'];
        }
        
    }    

}
