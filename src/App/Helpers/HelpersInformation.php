<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersUser;

class HelpersInformation
{
    public static function getInformationFromPost($data)
    {
        $items = [];
        $i = 0;

        foreach ($data as $key => $value) {
            $type = HelpersData::getType((string)$key, $key);
            $tab = HelpersData::setTab((string)$key);
            if($type == 'readonly' AND $tab == 'information'){
                $item = [];

                $item['name'] = (string)$key;
                $item['label'] = HelpersData::setLabel((string)$key);
                $item['tab'] = $tab;
                $item['type'] = $type;
    
                $item['value'] = $value;
    
                $items[$i] = $item;

                $i++;
            }            

        }

        return $items;           
    }

    public static function cleanInformationFromPost($data)
    {
        foreach ($data as $key => $value) {
            $type = HelpersData::getType((string)$key, $key);
            $tab = HelpersData::setTab((string)$key);
            if($type == 'readonly' AND $tab == 'information'){
                unset($data[$key]);
            }            

        }

        return $data;
    }

    public static function updateInformation($body, $table)
    {
        $columns = Schema::getColumnListing($table);
        if(in_array("lastmodby_read_information", $columns)){
            $userEmail = HelpersUser::getUserEmail();
            $body['lastmodby_read_information'] = $userEmail;
            $body['lastmod_read_information'] = Carbon::now();
        }
        return $body;
    }

}