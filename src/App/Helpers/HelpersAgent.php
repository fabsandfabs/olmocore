<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersUser;
use Olmo\Core\Containers\Backoffice\Order\Models\Order;
use Olmo\Core\Containers\Backoffice\Customer\Models\Customer;

class HelpersAgent
{   
    const rolesWithAgentPermission = ['agent', 'admin', 'superadmin'];

    public static function Authenticate($request){
        $token = HelpersUser::checkToken($request);
        // its required the agent role
        return self::isAgent($token, true) ? true : false;
    }

    public static function isAgent($token, $include_admins = false): bool{ // capire perchè adnrea ha scritto $include_admins potrebbe essere tolto
        // $role = false; capire perchè adnrea ha messo sta cosa
        
        if($token){
            $role = DB::table('olmo_user')
            ->where('token_hidden_general',$token)
            ->join('olmo_role', 'olmo_user.role_id_general', '=', 'olmo_role.id')
            ->select('olmo_role.name_txt_general as role')
            ->first();

            if(isset($role->role)){
                if($include_admins){
                    return in_array($role->role, self::rolesWithAgentPermission) ? true : false;
                }
                return $role->role == 'agent' ? true : false;
            }
        } 

        return false;
    }
    
    public static function getAgentRoleId(){
        return DB::table('olmo_role')->where('name_txt_general','agent')->select('id')->first();
    }
    // TODO: questo metodo deve essere utilizzato per il dropdown di assegnazione per il customer
    public static function getAgents(){

        $agents = DB::table('olmo_user')
        ->where('role_id_general', '=', DB::raw("(select id from olmo_role where name_txt_general = 'agent')"))
        ->orWhere('role_id_general', '=', "2") // id 2 = admin
        ->get();

        return $agents;
    }

    public static function customersList($agent, $listing = null){
        $attribute = Customer::theAttributes();

        if($attribute['agent']){
            $customers = DB::table('olmo_customer')
            ->whereIn('agent_agent_general', explode(',', $agent->id))
            ->select($listing ?? DB::raw($attribute['listing']))
            ->get();
            
            if(blank($customers)){ /** qui non ho bisogno di altri controlli, se l'utente che arriva all'ordine tramite isAgent non é un agent, é per forza dentro self::rolesWithAgentPermission */ 
                 // quindi qui prendo tutti i customers
                $customers = DB::table('olmo_customer')
                ->select($listing ?? DB::raw($attribute['listing']))
                ->get();
            }
            return $customers;
        }
    }

    public static function customerOrderList($agent){
        $attribute = Customer::theAttributes();

        if($attribute['agent']){
            $customers = self::customersList($agent, '*');
            $attribute = Order::theAttributes();
            $orderby = isset($attribute['orderby']) ? $attribute['orderby'] : 'id';
            $orderDir = isset($attribute['order']) ? $attribute['order'] : 'asc';

            $customer_ids = [];
            foreach($customers as $customer){
                $customer_ids[] = $customer->id;
            }

            $orders = DB::table('olmo_order')
            ->orderBy($orderby, $orderDir)
            ->whereIn('customerid_hidden_customer', $customer_ids)
            ->select(DB::raw($attribute['listing']))
            ->get();

            return $orders;
        }
    }

    public static function getSingleCustomer($id, $user){
        // TODO: da rivedere
        if(in_array($id, explode(',', $user->customer_multid_general))){
            $customer = HelpersCustomer::getCustomerById($id);
            return [
                BackController::serializePost($customer, Customer::theAttributes(), $customer->customerlang_txt_general, $customer->id)
            ];
        }
        return abort(403);
    }

    public static function getAgent($request){
        return HelpersUser::getUser();
    }


    public static function setOlmoInformation(){
        $order['code_read_general'] = time().rand(100,999);
        $order['createdonutc_read_general'] = date("Y-m-d H:i:s", time() - date("Z"));
        $order['lastmod_read_information'] = date("Y-m-d H:i:s");        
        $order['paidonutc_read_general'] = date("Y-m-d H:i:s");     
        return $order;
    }

    public static function setBillingAddresses($billingId, $empty = false){
        $order = [];
        if($empty){
            $order['billingname_read_billing'] = '';
            $order['billingsurname_read_billing'] = '';
            $order['billingphone_read_billing'] = '';
            $order['billingcompany_read_billing'] = '';
            $order['billingaddress_read_billing'] = '';
            $order['billingaddressnumber_read_billing'] = '';
            $order['billingcity_read_billing'] = '';
            $order['billingregion_read_billing'] = '';
            $order['billingstate_read_billing'] = '';
            $order['billingcountry_read_billing'] = '';
            $order['billingZIP_read_billing'] = '';
            $order['billingprovincia_read_billing'] = '';
        }else{
            $billing = Db::table('olmo_customeritem')->where('id', $billingId)->first();
            $billing = HelpersSerializer::cleanKeyObj($billing);
            $order['billingname_read_billing'] = $billing['name'];
            $order['billingsurname_read_billing'] = $billing['surname'];
            $order['billingphone_read_billing'] = $billing['phone'];
            $order['billingcompany_read_billing'] = $billing['company'];
            $order['billingaddress_read_billing'] = $billing['address'];
            $order['billingaddressnumber_read_billing'] = $billing['number'];
            $order['billingcity_read_billing'] = $billing['city'];
            $order['billingregion_read_billing'] = $billing['region'];
            $order['billingstate_read_billing'] = '';
            $order['billingcountry_read_billing'] = $billing['country'];
            $order['billingZIP_read_billing'] = $billing['zip'];
            $order['billingprovincia_read_billing'] = $billing['provincia'];
        }

        return $order;
    }

    public static function setShippingAddresses($shippingId, $empty = false){
        $order = [];
        if($empty){
            $order['courierlink_txt_shipping'] = '';
            $order['tracknumber_txt_shipping'] = '';
            $order['shippingname_read_shipping'] = '';
            $order['shippingsurname_read_shipping'] = '';
            $order['shippingphone_read_shipping'] = '';
            $order['shippingcompany_read_shipping'] = '';
            $order['shippingaddress_read_shipping'] = '';
            $order['shippingaddressnumber_read_shipping'] = '';
            $order['shippingcity_read_shipping'] = '';
            // $order['shippingregion_read_shipping'] = $shipping['region'];
            $order['shippingstate_read_shipping'] = '';
            $order['shippingcountry_read_shipping'] = '';
            $order['shippingZIP_read_shipping'] = '';        
            $order['shippingprovincia_read_shipping'] = '';  
        }else{
            $shipping = Db::table('olmo_customeritem')->where('id', $shippingId)->first();
            $shipping = HelpersSerializer::cleanKeyObj($shipping);        
            $order['courierlink_txt_shipping'] = '';
            $order['tracknumber_txt_shipping'] = '';
            $order['shippingname_read_shipping'] = $shipping['name'];
            $order['shippingsurname_read_shipping'] = $shipping['surname'];
            $order['shippingphone_read_shipping'] = $shipping['phone'];
            $order['shippingcompany_read_shipping'] = $shipping['company'];
            $order['shippingaddress_read_shipping'] = $shipping['address'];
            $order['shippingaddressnumber_read_shipping'] = $shipping['number'];
            $order['shippingcity_read_shipping'] = $shipping['city'];
            // $order['shippingregion_read_shipping'] = $shipping['region'];
            $order['shippingstate_read_shipping'] = '';
            $order['shippingcountry_read_shipping'] = $shipping['country'];
            $order['shippingZIP_read_shipping'] = $shipping['zip'];        
            $order['shippingprovincia_read_shipping'] = $shipping['provincia'];  
        }

        return $order;
    }

    // public static function setOrderInfo($agent, $customer, $order_id){
    //     $orders = Db::table('olmo_order')->get();

    //     $order['name_txt_general'] = $order_id;
    //     $order['trash_hidden_general'] = '';
    //     $order['createdby_read_information'] = $agent['email'];
    //     $order['lastmodby_read_information'] = $agent['email'];
    //     $order['customerid_hidden_customer'] = $customer['id'];
    //     $order['customer_read_general'] = $customer['email'];  
        
    //     return $order;
    // }
}
