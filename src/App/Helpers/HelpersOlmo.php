<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\Helperslang;
use Olmo\Core\App\Helpers\HelpersFilemanager as Filemanager;
use Olmo\Core\Containers\Backoffice\Template\Models\Template;


class HelpersOlmo
{
    // public static function deleteFormLogs(){
    //     // $days = Config::get('log_retention.days');
    //     $logPath = storage_path('logs');
    //     try {
    //         $files = glob($logPath . '/*.log');
    //         // echo print_r($files."\n", true);
    //         foreach ($files as $file) {
    //             // echo $file;
    //             // $lastModified = filemtime($file);
    //             // $threshold = time() - ($days * 24 * 60 * 60);
    //             // if ($lastModified < $threshold) {
    //             unlink($file);
    //             // }
    //         }
    //     } catch (\Exception $e) {
    //     }
    // }

    public static function createFolderStructure():array{
        $media[] = storage_path('app/public/mail');
        $media[] = storage_path('app/public/media');
        $media[] = storage_path('app/public/media/olmoemail');
        $media['assets'] = storage_path('app/public/media/olmoemail/assets');
        $media['csv'] = storage_path('app/public/media/olmoemail/csv');
        $media['images'] = storage_path('app/public/media/olmoemail/images');
        $media['strings'] = storage_path('app/public/media/olmoemail/strings');
        $media['template'] = storage_path('app/public/media/olmoemail/template');
        $media[] = storage_path('app/public/media/olmosetting');
        $media['translation'] = storage_path('app/public/media/olmosetting/strings');
        $media[] = storage_path('app/public/assets');

        foreach($media as $path){
            Filemanager::checkFolderAndCreateIt($path);
        }
        $storage_id = 'None';
        $translation_truename = $translation_filename = $media['translation']."/translations.csv";
        // $translation_filename = $media['translation']."/dsiophfgfSIUFnWCRPOQC2IA.csv";
        if(!File::exists($translation_filename)){
            File::put($translation_filename, 'code,it,en,fr,es,ru');

            $storage_id = DB::table('olmo_storage')->insertGetId([
                'filename'    => explode("/storage/", $translation_filename)[1],
                'truename'    => explode("/storage/",$translation_truename)[1],
                'model'      => 'filemanager',
                'public'     => 'true',
                'type'       => pathinfo($translation_filename)['extension'],
             ]);
        }
        
        

        return ['id_file' => $storage_id];
    }



}
