<?php

namespace Olmo\Core\App\Helpers;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use FFMpeg;

use Olmo\Core\App\Helpers\HelpersFilemanager;


class HelpersVideo
{
    public static function checkOriginalVersionsAndCreateIt($file, $only_copy = false)
    {
        $FFMPEG = (bool)env('FFMPEG', false);
        $versions_control = DB::table('olmo_storage_versions')->where([['id_original', $file->id], ['is_original', 'true']])->get()->count();
        if ($versions_control == 0) {
            $assets_url = 'app/public/assets/';
            $file_dirname = $file->truename;
            $file_encoded_name = $file->filename;
            $destination_path = (pathinfo($file_dirname)['dirname'] != '.') ? pathinfo($file_dirname)['dirname'] . '/' : '';
            $dirname = storage_path($assets_url . $destination_path);

            /** @param assets_original - the name of the media copy */
            $assets_original = storage_path($assets_url . $file_dirname);

            HelpersFilemanager::checkFolderAndCreateIt($dirname);

            $media_path = 'public/media/' . $destination_path . $file_encoded_name;
            $videoget = Storage::disk('local')->get($media_path);
            $videosize = Storage::disk('local')->size($media_path);
            Storage::put('public/assets/' . $file_dirname, $videoget);
            
            $videoInfo = self::get_video_attributes(storage_path($media_path));
 
            DB::table('olmo_storage_versions')->insertGetId(
                [
                    'dirname' => $file_dirname,
                    'compression_path' => '',
                    'size' => $videosize,
                    'compression_size' => 0,
                    'width' => $videoInfo['width'], 'height' => $videoInfo['height'],
                    'compression' => 0,
                    'duration' => $videoInfo['hours'] . ':' . $videoInfo['mins'] . ':' . $videoInfo['secs'],
                    'fit' => '',
                    'id_original' => $file->id,
                    'is_original' => 'true'
                ]);  

            // condizione creata per getListVersion, dove solo la copia deve essere rigenerata forzosamente
            // if(!$only_copy){
            //     if($FFMPEG){
            //         self::createOriginalWebm($file_dirname);
            //     }
            // }

            if ( env('FILESYSTEM_DRIVER') == 's3' ){
                Storage::disk('local')->delete('public/assets/' . $file_dirname);
            }
            
            return true;
        } else {
            return false;
        }
    }

    /** @param file_dirname - contains the path and filename*/
    public static function createOriginalWebm($file_dirname){
        $webm_dirname = str_replace(pathinfo($file_dirname)['extension'], 'webm', $file_dirname);
        $assets_original_webm = str_replace($file_dirname, $webm_dirname, storage_path('app/public/assets/' . $file_dirname));
        $destination_path = (pathinfo($file_dirname)['dirname'] != '.') ? pathinfo($file_dirname)['dirname'] . '/' : '';
        
        $versions_control = DB::table('olmo_storage_versions')->where('compression_path', $webm_dirname)->get()->count();
        $version = DB::table('olmo_storage_versions')->where('dirname', $file_dirname)->first();
        $original = DB::table('olmo_storage')->where('id', $version->id_original)->first();

        if($versions_control == 0){
           try {
                FFMpeg::fromDisk('local')
                ->open('public/media/' . $destination_path . $original->filename)
                ->export()->inFormat(new \FFMpeg\Format\Video\WebM)
                ->save('public/assets/' . $webm_dirname);

                $videosize = Storage::disk('local')->size('public/assets/' . $webm_dirname);
                // TODO: da capire se va bene dirname, senno va passato l'id
                DB::table('olmo_storage_versions')->where('dirname', $file_dirname)->update(
                    [
                        'compression_path' => $webm_dirname,
                        'compression_size' => $videosize
                    ]);
                    
            } catch (Exception $e) {
                return false;
            }
        } else {
            return false;
        }


        if ( env('FILESYSTEM_DRIVER', 'local') == 's3' ){
            $webmget = Storage::disk('local')->get('public/assets/' . $webm_dirname);
            Storage::put('public/assets/' . $webm_dirname, $webmget);
            Storage::disk('local')->delete('public/assets/' . $webm_dirname);
        }

        return true;
    }

    public static function get_video_attributes($video)
    {
        $ffmpeg = env('FFMPEG_BINARIES', 'ffmpeg');
        $command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';

        exec($command, $output);
        $output = implode('',$output);
        
        $regex_sizes = "/Video: ([^\r\n]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";

        if (preg_match($regex_sizes, $output, $regs)) {
            $codec = $regs[1] ? $regs[1] : null;
            $width = $regs[3] ? $regs[3] : null;
            $height = $regs[4] ? $regs[4] : null;
        }

        $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
        if (preg_match($regex_duration, $output, $regs)) {
            $hours = $regs[1] ? $regs[1] : null;
            $mins = $regs[2] ? $regs[2] : null;
            $secs = $regs[3] ? $regs[3] : null;
            $ms = $regs[4] ? $regs[4] : null;
        }
        
        return [
            'codec' => $codec ?? '',
            'width' => $width ?? 0,
            'height' => $height ?? 0,
            'hours' => $hours ?? 0,
            'mins' => $mins ?? 0,
            'secs' => $secs ?? 0,
            'ms' => $ms ?? 0
        ];
    }

    public static function checkIfAssetExist($params, $dirname, $pathinfo)
    {

        $checkParams = self::checkParams($params);
        if (!$checkParams) {
            return array(
                'fileexist' => false,
                'fileNameWithParams' => false
            );
        }

        $fileNameWithParams = $pathinfo['filename'] . '-' . self::generateParams($params) . '.' . $pathinfo['extension'];
        $pathFile = 'public/assets/' . $dirname . '/' . $fileNameWithParams;
        $fileexist = Storage::exists($pathFile);

        return array(
            'fileexist' => $fileexist,
            'fileNameWithParams' => $fileNameWithParams
        );
    }

    public static function checkParams($params)
    {
        $rightParams = array('fit', 'width', 'height', 'compression');
        foreach ($params as $key => $item) {
            if (!in_array($key, $rightParams)) {
                return false;
            }
        }
        return true;
    }

    public static function generateParams($params)
    {
        return implode('_', $params);
    }

    public static function  checkExtention($pathinfo)
    {
        $extention = $pathinfo['extension'];
        if ($extention == 'png' or $extention == 'jpg' or $extention == 'jpeg') {
            return true;
        }
        return false;
    }

    public static function checkParamsImages($param)
    {
        $defaultPixel = 100;
        $permittedFit = ['clip', 'clamp', 'crop'];

        if ($param['height'] < 1) {
            $param['height'] = $defaultPixel;
        }

        if ($param['width'] < 1) {
            $param['width'] = $defaultPixel;
        }

        if ($param['compression'] < 0 or !$param['compression']) {
            $param['compression'] = explode(',', env('IMAGES_COMPRESSION'))[0];
        } else if ($param['compression'] > 100) {
            $param['compression'] = 100;
        }

        if (!in_array($param['fit'], $permittedFit)) {
            $param['fit'] = 'clip';
        }
        return $param;
    }
}
