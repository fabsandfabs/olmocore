<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use Carbon\Carbon;
use DateTime;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersProduct;
use Olmo\Core\App\Helpers\HelpersVisualBlock;
use Olmo\Core\App\Helpers\HelpersLang;

class HelpersSerializer
{
    public static function splitKey($data)
    {
        $key = [];
        if($data != 'id'){
            $key['key'] = @explode('_',$data)[0];
            $key['type'] = @explode('_',$data)[1];
            $key['tab'] = @explode('_',$data)[2];
        } else {
            $key['key'] = 'id';
            $key['type'] = 'databaseid';
            $key['tab'] = 'databaseid';
        }

        return $key;
    }

    public static function cleanKeyObj($data)
    {
        if($data){
            $response = [];
            foreach($data as $key=>$value){

                $thekey = self::splitKey($key);
                $response[$thekey['key']] = $value;

            }

            return $response;
        }
    }

    public static function cleanKeyArray($data)
    {
        if($data){
            $response = [];
            foreach($data as $index=>$item){
                foreach($item as $key=>$value){

                    $thekey = self::splitKey($key);
                    $response[$index][$thekey['key']] = $value;

                }
            }

            return $response;
        }
    }

    public static function cleanCreatedModby($data)
    {
        if(isset($data->createdby_read_information)){
            unset($data->createdby_read_information);
        }
        if(isset($data->lastmodby_read_information)){
            unset($data->lastmodby_read_information);
        }
        return $data;
    }

    public static function cleanEmptyKey($obj){

        /* TODO:
            Verificare perché si bugga la ricorsiva, questo if é messo apposta
        */
        if (is_iterable($obj) ? True:False){
            foreach ($obj as $key => $value) {
                $iterable = (is_iterable($value)) ? True:False;
                // echo print_r([$obj[$key], $iterable]);
                switch ($obj[$key]) {
                    case '':
                        unset($obj[$key]);
                        break;
                    case empty($obj[$key]):
                        unset($obj[$key]);
                        break;
                    case $iterable:
                        $cleaned = self::cleanEmptyKey($value);
                        if(empty($cleaned)){
                            unset($obj[$key]);
                        }else{
                            $obj[$key] = self::cleanEmptyKey($value);
                        }

                        break;
                }
            }
        }

        return $obj;
    }

    public static function routeResponse($data, $type = 'stdClass', $time = 0, $reqeust = 'route', $model = '', $clean = false, $lang = '')
    {
        $response = [];
        if($type != 'stdClass'){
            $data = is_array($data) ? $data : $data->toArray();
        }

        foreach($data as $key=>$value){

            $thekey = self::splitKey($key);

            if($thekey['type'] == 'databaseid'){
                $response['id'] = $value;
            } else if($thekey['type'] == 'id'){
                $response[$thekey['key']] = self::select($value, $key, $time, $model, $lang);
            } else if($thekey['type'] == 'multid'){
                $response[$thekey['key']] = self::multiselect($value, $key, $time, $model);
            } else if($thekey['type'] == 'sliderfromitem'){
                $response[$thekey['key']] = self::getSliderFrom($value, $key);
            } else if($thekey['type'] == 'property' || $thekey['type'] == 'props'){
                // TODO
                // capire come fare funzionare questa cosa per le proprietà del prodotto e allo stesso tempo farlo funzionare per gli items
                if($reqeust == 'route' OR $time == 0){
                    $response[$thekey['key']] = self::property($value, $time);
                } else if($reqeust == 'allmodels') {
                    $response[$thekey['key']] = self::propertyOrder($data, $value, $time);
                }
            // } else if($thekey['type'] == 'dnd'){
            // } else if($thekey['type'] == 'multimg'){
            }else if($thekey['type'] == 'dotsposition'){
                $response[$thekey['key']] = HelpersDotsPosition::getDotslist($thekey, $value);
            } else if($thekey['type'] == 'img'){
                $response[$thekey['key']] = HelpersFilemanager::getSinglePathById($value);
            } else if($thekey['type'] == 'filemanager'){
                $response[$thekey['key']] = HelpersFilemanager::getSinglePathFmanagerById($value);
            // } else if($thekey['type'] == 'media'){
            } else if($thekey['type'] == 'slider'){
                $response[$thekey['key']] = self::getSingleSlides($value);
            } else if($thekey['type'] == 'fields'){
                $response[$thekey['key']] = self::getSingleFields($value);
            } else if($thekey['type'] == 'visual'){
                $response[$thekey['key']] = self::getVisualBlock($value, $time);
            } else if($thekey['type'] == 'productitems'){
                $items = self::getSingleProduct($value);
                $response[$thekey['key']] = $items;
                if(count($items) > 0){
                    /**
                     * Feeds the frontend framework
                     */
                    $response['properties'] = HelpersProduct::getProperties($items);
                }
            } else if($thekey['type'] == 'list'){
                $response[$thekey['key']] = self::getListing($value, $key);
            } else {
                $response[$thekey['key']] = $value;
            }
        }

        if($clean){
            foreach($response as $key => $element){

                if(is_iterable($response[$key]) && !empty($response[$key])){
                    $res = self::cleanEmptyKey($response[$key]);
                    if( empty($res) ){
                        unset($response[$key]);
                    }else{
                        $response[$key] = $res;
                    }
                }else{
                    if( empty($response[$key]) ){
                        unset($response[$key]);
                    }
                }
            }
        }

        if(isset($response['create'])){
            unset($response['create']);
        }
        
        if(isset($response['createdby'])){
            unset($response['createdby']);
        }
        /**
         * These cannot be unset cause is used in to the structured data functionality
         */
        // if(isset($response['lastmod'])){
        //     unset($response['lastmod']);
        // }
        if(isset($response['lastmodby'])){
            unset($response['lastmodby']);
        }
        if(isset($response['lang'])){
            unset($response['lang']);
        }

        return $response;
    }
    

    public static function getSliderFrom($value, $key)
    {
        if($value != ""){
            $response = DB::table('olmo_productitem')->where('id', $value)->first();
            $slider = explode(',', $response->images_slider_general);
            $slides = [];
            foreach($slider as $key=>$slide){
                $singleSlide = self::getSingleSlides($slide);
                $slides[$key] = $singleSlide[0];
            }
            return $slides;
        }
        return $value;
    }

    public static function getListing($value, $key)
    {
        $model = str_replace('item_list_general','',$key);
        if($value){
            $slider = explode(',',$value);
            $array = [];
            foreach($slider as $item){
                $val = self::getSingleFullItem($item, $model);
                array_push($array, $val);
            }
            return $array;
        }

        return [];
    }

    public static function getSingleFullItem($s, $model)
    {
        $res = [];
        if($s){
            $slider = DB::table('olmo_'.$model.'item')->where('id',$s)->first();
            if($slider){
                $i = self::routeResponse($slider, 'stdClass');
                return $i;
            }
        }
        return null;
    }

    public static function getSingleProduct($value)
    {
        if($value){
            $slider = explode(',',$value);
            $array = [];
            foreach($slider as $item){
                $val = self::getSingleFullProduct($item);
                if($val){
                    array_push($array, $val);
                }
            }
            return $array;
        }

        return [];
    }

    public static function getSingleFullProduct($s)
    {
        $res = [];
        if($s){
            $product = DB::table('olmo_productitem')->where('enabled_is_general', 'true')->where('id',$s)->first();
            if($product){
                $i = self::routeResponse($product, 'stdClass');
                return $i;
            }
        }
        return null;
    }

    /**
     * This is a changing paradigm
     * most of the time olmo use the sequence of id in the post field to make a relation
     * this time things are different, it's using the postid field in the item (child) to be related with the mather post
     */
    public static function propertyOrder($data, $value, $time)
    {
        $array = [];
        if($value){
            $query = Db::table('olmo_propertyitem')->where('postid_hidden_general', $data->id)->orderBy('position_ord_general', 'asc')->where('enabled_is_general', 'true')->get();
            foreach($query as $item){
                $parent = Db::table('olmo_property')->where('id', $item->postid_hidden_general)->where('enabled_is_general', 'true')->first();
                $parent = self::cleanEmptyValue($parent);
                $i = self::routeResponse($item, 'array', $time);
                $i['parent'] = self::cleanKeyObj($parent);
                array_push($array, $i);
            }
        }

        return $array;
    }

    public static function cleanEmptyValue($data) {
        $data = json_decode(json_encode($data), true);

        if(env('CLEAN_ROUTE_JSON', false) AND is_array($data)){
            $data = array_filter($data, function ($var){
                return ($var !== NULL && $var !== FALSE && $var !== "");
            } );
        }

        return $data;
    }

    public static function property($value, $time)
    {
        $array = [];
        if($value){
            $splitid = explode(',', $value);
            foreach($splitid as $id){
                $query = Db::table('olmo_propertyitem')->where('id', $id)->where('enabled_is_general', 'true')->first();

                $query = self::cleanEmptyValue($query);

                if($query){
                    $item = self::routeResponse($query, 'array', $time);
                    $parent = Db::table('olmo_property')->where('id', $query['postid_hidden_general'])->where('enabled_is_general', 'true')->first();
                    $parent = self::cleanCreatedModby($parent);
                    $parent = self::cleanEmptyValue($parent);
                    $item['parent'] = self::cleanKeyObj($parent);
                    array_push($array, $item);
                }
            }
        }

        return $array;
    }

    public static function getStructuredData($data)
    {
        $str = preg_replace("/[\r\n]*/", "", $data);
        $string = preg_replace('/\s+/', '', $str);
        $json = json_decode($string, true);
        return $json;
    }

    public static function getSelectParams($model, $key){
        $select_field = "*";
        if($model != ''){
            $attribute_key = "model_".HelpersData::setKey($key);
            $model_attributes = ('App\Containers\Backoffice\\'.ucfirst($model).'\Models\\'.ucfirst($model) )::theAttributes();

            if(isset($model_attributes[$attribute_key])){
                $select_field = $model_attributes[$attribute_key];
            }
        }
        return $select_field;
    }

    public static function select($value, $key, $time, $model, $lang = '')
    {
        if($value && $time <= 2){
            $time = $time + 1;
            $label = HelpersData::setLabel($key);
            $table = 'olmo_'.$label;

            $select_field = self::getSelectParams($model, $key);

            $data = Db::table($table)->select($select_field)->where('enabled_is_general', 'true')->where('id', $value)->first();
            $data = self::cleanCreatedModby($data);
            $data = $data ? self::cleanEmptyValue($data) : [];

            if($label == 'template'){
                if(isset($data['structureddata_txtarea_structured'])){
                    $data['structureddata_txtarea_structured'] = self::getStructuredData($data['structureddata_txtarea_structured']);
                }
                if($lang != Helperslang::getDefaultLang()){
                    $slug = Db::table('olmo_templateslug')->where('defaultid', $value)->first();
                    if($slug){
                        $slug = json_decode(json_encode($slug), true);
                        $data = json_decode(json_encode($data), true);
                        $data['slugs_txt_slugs'] = $slug['slug'];
                    }
                }
            }
            return self::routeResponse($data, 'array', $time);
        }
        return "";

    }

    public static function multiselect($value, $key, $time, $model)
    {
        if($value){
            $time = $time + 1;
            $table = 'olmo_'.HelpersData::setLabel($key);

            $select_field = self::getSelectParams($model, $key);

            $splitid = explode(',', $value);
            $array = [];
            foreach($splitid as $id){
                $data = Db::table($table)->select($select_field)->where('enabled_is_general', 'true')->where('id', $id)->first();
                $data = self::cleanCreatedModby($data);
                $data = self::cleanEmptyValue($data);

                if($data){
                    if($time <= 2){
                        $val = self::routeResponse($data, 'array', $time);
                    } else {
                        $val = self::cleanKeyObj($data);
                    }
                    array_push($array, $val);
                }
            }
            return $array;
        }
        return [];
    }

    public static function getSingleFields($value)
    {
        if($value){
            $slider = explode(',',$value);
            $array = [];
            foreach($slider as $item){
                $val = self::getSingleFullField($item);
                array_push($array, $val);
            }
            return $array;
        }

        return [];
    }

    public static function getSingleFullField($s)
    {
        $res = [];
        if($s){
            $slider = DB::table('olmo_fieldrepeateritem')->where('id',$s)->first();
            if($slider){
                $i = self::routeResponse($slider, 'stdClass');
                return $i;
            }
        }
        return null;
    }

    public static function getSingleSlides($value)
    {
        if($value){
            $slider = explode(',',$value);
            $array = [];
            foreach($slider as $item){
                $val = self::getSingleFullSlide($item);
                array_push($array, $val);
            }
            return $array;
        }

        return [];
    }

    /**
     * Change this method to get every filemanager fields dynamically
     */
    public static function getSingleFullSlide($s)
    {
        $res = [];
        if($s){
            $slider = DB::table('olmo_slideritem')->where('id',$s)->first();
            $slider = $slider ? self::cleanEmptyValue($slider) : [];            
                  
            if($slider){
                $i = self::routeResponse($slider, 'array');
                return $i;
            }
        }
        return null;
    }

    public static function getVisualBlock($data, $time)
    {

        $blocks = [];
        $data = $data != "" ? explode(',', $data) : [];

        foreach($data as $block){
            $row = DB::table('olmo_visualblock')->where('id', $block)->first();
            $row = self::cleanEmptyValue($row);
            // $removeNull = HelpersData::removeNullKey($row);
            if($row){
                if($time <= 2){
                    $clearRow = self::routeResponse($row, 'stdClass', $time);
                } else {
                    $clearRow = self::routeResponse($row, 'stdClass', $time);
                }
                array_push($blocks, $clearRow);
            }

        }

        return $blocks;

    }

}
