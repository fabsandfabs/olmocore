<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Forms\App\Http\Controller\FormController;
use Olmo\Forms\App\Helpers\HelpersDataForm;
use Olmo\Core\App\Helpers\HelpersSerializer;

class HelpersRequestMedia
{

  public static function requestCode($request)
  {
	$locale = $request->input('locale');
	$code = $request->input('code');
	$model = $request->input('model');
	$name = $request->input('name');

	$media = Db::table('olmo_'.$model)->where('name_txt_general', $name)->where('locale_hidden_general', $locale)->where('code_txt_general', $code)->first();
	
	if($media){
		$media = HelpersSerializer::routeResponse($media);
		return response($media, 200);
	}
	return response("Code not found", 400);
  }

  public static function requestAssets($request)
  {

  	$truename = $request->truename;
	$locale = $request->input('locale');
	$type = $request->input('type');
	$formid = $request->input('formid');

	$content = HelpersDataForm::getFormData($request);

  	$media = Db::table('olmo_storage')->where('truename', $truename)->where('model','filemanager')->first();	  
	if(!$media) {
  	  return response("the media doesn't exist", 403);
  	  exit();
    }
  	
	$tokens = Db::table('olmo_tokens')->insertGetId([
		'token' => md5(rand(1, 10) . microtime()),
		'model' => 'filemanager',
		'modelid' => $media->truename,
		'created_at' => date('Y-m-d H:i:s')
	]);	

	$data = Db::table('olmo_tokens')->where('id', $tokens)->first();
    if(!$data) {
  	  return response("token not found", 403);
  	  exit();
    }	

  	$url_media = env('APP_URL')."/storage/media/".$media->truename."?olmotoken=".$data->token;

  	//Invio la mail
  	$params = [
  		'a'      => $request->email,
		'email'  => $request->email,
		'privacy'=> $request->privacy,
  		'url'  	 => $url_media,
		'domain' => env('APP_URL'),
		'locale' => $locale
  	];

	$send = null;

	if($formid){
		$form = Db::table('olmo_form')->where('id', $formid)->get();
		$params['id'] = $formid;
	} else if(!$type == 'url-noemail'){
		$forms = Db::table('olmo_form')->get();		
	
		foreach ($forms as $form) {
			$ids = explode(',', $form->formtype_multid_general);
			if (in_array('28', $ids)) {
				$formid = $form->id;
				$params['id'] = $formid;
			}
		}
	}

	if(isset($params['id'])){
		$params = array_merge($params, $content);
		$send = FormController::sendingFormData($params, 'internal');
	}
  
	if($type == "url" OR $type == 'url-noemail'){
		return [
			"url" => $url_media,
			"send" => $send,
		];
	}

	if(isset($params['id'])){
  		return $send;
	}
   
  }

}