<?php

namespace Olmo\Core\App\Helpers;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Intervention\Image\Facades\Image;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;


class HelpersImage
{
    private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

    private const CONTAINER_FILEMANAGER = 'Filemanager';
  
    public static function checkParams($params) {
        $rightParams = array('fit', 'width', 'height', 'compression');
        foreach($params as $key=>$item){
            if(!in_array($key, $rightParams)) {
                return false;
            }
        }
        return true;
    }

    public static function checkImagesParamsFromJson($obj){
        $rightParams = ['size', 'fit', 'compression'];
        foreach($obj as $key=>$item){
            if(!in_array($key, $rightParams)) {
                return false;
            }
        }
        return true;
    }
    
    public static function generateParams($params) {
        return implode('_',$params);
    }

    /*
    *
    * This code defines a static function to check whether the original versions of an image is present or not, and creates it if necessary. The given parameter is a file.
    * First, the directory name and filename of the file are assigned to variables. Then, the paths to the folder containing the original images and their webp version is defined using the storage_path() function.
    * Next, the image file is loaded to a variable with the Image intervention package. This variable is then cloned to another variable to create a webp version of the image. If the original image is not present in the public folder, it is saved there.
    * The webp version is saved to the folder that contains the original image, and then stored in storage. The webp file is then removed from the public folder.
    * Information about the created original image is then stored in the database in a row in a specific table.
    * Finally, the function returns true, indicating that it has completed successfully.
    *
    *
    * This script is a function that takes a file object as a parameter.
    * It generates a webp version of the image, saves it in a directory,
    * and then creates a new database entry to store the image's metadata.
    *
    */
    public static function checkOriginalVersionsAndCreateIt($file) {
        $aiuto = [];
        $versions_control = DB::table('olmo_storage_versions')->where([['id_original', $file->id],['is_original', 'true']])->get()->count();

        if($versions_control == 0){
            $file_dirname = $file->truename;
            $dirname = pathinfo($file_dirname)['dirname'];
            $file_dirname_filename = $dirname .'/'. $file->filename;
            $assets_original = storage_path('app/public/assets/'. $file_dirname);

            $dirname = pathinfo($file_dirname)['dirname'] != '' ? pathinfo($file_dirname)['dirname'] : '';
            $dirname = storage_path('app/public/assets/' . $dirname);
            HelpersFilemanager::checkFolderAndCreateIt($dirname);
            File::copy(storage_path('app/public/media/' . $file_dirname_filename), $assets_original);
            // $getImgoriginal = Storage::disk('local')->get('public/media/' . $file_dirname);
            // Storage::put('public/assets/' . $file_dirname, $getImgoriginal);            
            if(env('FILESYSTEM_DRIVER', 'local') == 's3'){
                $getImg = Storage::disk('local')->get('public/assets/' . $file_dirname);
                Storage::put('public/assets/' . $file_dirname, $getImg);
            }

            /** Webp paths */
            $webp_dirname = str_replace(pathinfo($file_dirname)['extension'], 'webp', $file_dirname);
            $assets_original_webp = str_replace($file_dirname, $webp_dirname, $assets_original);

            $flag = true;
            $try = 1;
            while ($flag && $try <= 3):
                // try {

                    /**
                     *  TODO:
                     *  Piccole osservazioni
                     *  - L'errore nasce da qui: $webp_copy->encode('webp', ); 
                     *  - ini_set('memory_limit', '') permette la gestione di immagini piú pesanti 
                     *  - La soluzione sembra essere la limitazione della dimensione del primo resizing, la libreria non propone dimensioni massime ma specifica che passare da 3000x2000 a 300x200 occupa 32mb
                     *  - Il try/catch maschera l'errore o blocca qualcosa (condizione da verificare, sto riavendo problemi con lo stesso codice sul server di sviluppo)
                     *  - 2G space su back.cbm / fino a 23mb / 15000 x 8000 (Facendo una proporzione, occupati sono 1600mb durante il processo)
                     *  - 512M space == / fino a 4.2 / 5000 x 2700 (Occupati 533mb)
                     *         
                     *  - Bisogna capire come catchare l'errore dell'encode per riuscire a displayare qualcosa di senso compiuto
                     */

                    ini_set('memory_limit', env('MEMORY_LIMIT', '512M'));
                    $limit = env('MAX_COMPRESSION_SIZE', 2000);
                    $img = Image::make($assets_original);

                    if($img->width() > $limit){
                        $n = $img->width() / $limit;
                        $aiuto = [$img->width(), $img->height()];
                        $img = $img->resize($img->width() / $n, $img->height() / $n);
                    }

                    $webp_copy = $img;
                    $webp_copy->encode('webp', 100 / $try); // Ho letto che diminuire la compression diminuisce anche i problemi di conversione. Se riusciamo ad utilizzare i try/catch diventa sfruttabile come cosa.
                    
                    /**
                     * If $assets_original_webp contains a folder that doen't exist
                     * then create it before save the file
                     */
                    $dirname = pathinfo($file_dirname)['dirname'] != '' ? pathinfo($file_dirname)['dirname'] . '/' : '';
                    $dirname = storage_path('app/public/assets/' . $dirname);
                    HelpersFilemanager::checkFolderAndCreateIt($dirname);
                    /** Then save the new image */
                    $webp_copy->save($assets_original_webp);

                    $getImgWebp = Storage::disk('local')->get('public/assets/' . $webp_dirname);
                    Storage::put('public/assets/' .$webp_dirname, $getImgWebp);
        
                    if(env('FILESYSTEM_DRIVER', 'local') == 's3'){
                        Storage::disk('local')->delete('public/assets/' . $webp_dirname);                        
                    }

                    $flag = false;
                // } catch (\Throwable $th) {
                //     $webp_dirname = "";
                //     $img = null;
                //     $webp_copy = null;
                // }
                $try++;
            endwhile;           

            // $imgget = Storage::disk('local')->get('public/media/' . $file_dirname_filename);
            // Storage::put('public/assets/' . $file_dirname, $imgget);
            if(env('FILESYSTEM_DRIVER', 'local') == 's3'){
                Storage::disk('local')->delete('public/assets/' . $file_dirname);                        
            }

            $dataImage = [
                'dirname' 	        => $file_dirname,
                'compression_path'  => $webp_dirname,
                'size'              => $img ? $img->filesize() : 0,
                'compression_size'  => $webp_copy ? $webp_copy->filesize() : 0,
                'width'             => $img ? $img->width() : 0,
                'height'            => $img ? $img->height() : 0,
                'compression'       => 0,
                'fit'               => 'clip',
                'id_original'       => $file->id,
                'is_original'       => 'true'
            ];

            DB::table('olmo_storage_versions')->insertGetId($dataImage);
            return [
                "src" => $assets_original,
                "webp" => $assets_original_webp,
                'info' => $aiuto
            ];
        }

        return false;
    }

    /**
     * This method creates the versions indicated in the settings/Filemanager/ folder,
     * It return an array that contains information about the process
     */
    public static function checkDefaultVersionsAndCreateThem($file){
        $FilemanagerDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_FILEMANAGER . DIRECTORY_SEPARATOR);
        $directory = File::isDirectory($FilemanagerDirectory);

        $result = [];
        if( $directory ){
            $truename = $file->truename;
            $id = $file->id;

            $result['default_images_created'] = 'Not implemented!';
            if(env("IMAGES_DEFAULT_SIZE", false)){
                $result['default_images_created'] = self::CreateVersionsFromImagesSettings($FilemanagerDirectory, $truename, $id);
            }

            $result['folder_images_created'] = 'Not implemented!';
            if(env("FOLDER_DEFAULT_SIZE", false)){
                $result['folder_images_created']  = self::CreateVersionsFromFoldersSettings($FilemanagerDirectory, $truename, $id);
            }
        }
        return $result;
    }

    

    /**
     * this method takes the settings indicated inside settings/Filemanager/folder.json, checks 
     * the path of the loaded image and creates a version of the image as indicated inside the json
     */
    public static function CreateVersionsFromFoldersSettings($pathSettings, $truename, $id) {
        $filemanagerPath = $pathSettings . 'folders.json';
        $filemanagerJson = json_decode(file_get_contents($filemanagerPath), true);

        // get path information
        $imagePath = pathinfo($truename)['dirname'];
        $ImagePathExploded = explode('/', $imagePath);
        $pathSubfolderNumberImage = count($ImagePathExploded);
        $count = 0;
        // explode all jsons key
        foreach($filemanagerJson as $folderPattern => $folderParams){
            $folderPathExploded = explode('/', $folderPattern);
            $n = count($folderPathExploded);
            
            // compare subfolders numbers
            if( $pathSubfolderNumberImage == $n ){
                $i = 0; // if the counter becomes equal to the number of subfolders, it means that the path is correct
                foreach($folderPathExploded as $key => $subfolder){
                    if($subfolder == '*'){ // with %, the developer means that it is a dynamic subfolder
                        $i++;
                    }else if($subfolder == $ImagePathExploded[$key]){
                        $i++;
                    }
                }

                if($i == $n){ // if the counter becomes equal to the number of subfolders, creates the specified version
                    if( !isset($folderParams['size']) ) {
                        foreach($folderParams as $options){
                            if ( self::checkImagesParamsFromJson($options) ){
                                $sizes = explode('x', $options['size']);
                                $params = [
                                    'fit' => $options['fit'], 
                                    'compression' => $options['compression'],
                                    'width' => $sizes[0], 
                                    'height' => $sizes[1] 
                                ];
                                $count += self::generateImage($truename, $params, 'writeDB', $id) ? 1 : 0;
                            }else{
                                // return "Error: Json object is incorrect!";
                            }
                        }
                    } else {
                        if ( self::checkImagesParamsFromJson($folderParams) ){
                            $sizes = explode('x', $folderParams['size']);
                            $params = [
                                'fit' => $folderParams['fit'], 
                                'compression' => $folderParams['compression'],
                                'width' => $sizes[0], 
                                'height' => $sizes[1] 
                            ];
                            $count += self::generateImage($truename, $params, 'writeDB', $id) ? 1 : 0;
                        }else{
                            return "Error: Json object is incorrect!";
                        }
                    }
  
                }else{
                    return "The image path does not match anything in the settings";
                }
            }
            
        } 

        return ['Created' => $count];
    }
    /**
     * this method takes the settings indicated in settings/Filemanager/images.json and creates the indicated versions
     */
    public static function CreateVersionsFromImagesSettings($pathSettings, $truename, $id) {
        $response = $errors = [];
        $count = 0;

        $filemanagerPath = $pathSettings . 'images.json';
        $filemanagerJson = json_decode(file_get_contents($filemanagerPath), true);
        
        foreach($filemanagerJson as $key => $defaultParams){
            if ( self::checkImagesParamsFromJson($defaultParams) ){
                $sizes = explode('x', $defaultParams['size']);
                $params = [
                    'fit' => $defaultParams['fit'], 'compression' => $defaultParams['compression'],
                    'width' => $sizes[0], 'height' => $sizes[1] 
                ];
                $count += self::generateImage($truename, $params, 'writeDB', $id) ? 1 : 0;
            }else{
                $errors[$key] = 'Error: Json object is incorrect!';
            }
        } 
        $response = ['Created' => $count, 'Errors' => $errors];
        return $response;
    }

    public static function checkIfAssetExist($params, $pathinfo) {

        $params = self::checkParamsImages($params);
        $checkParams = self::checkParams( $params );
        
        if(!$checkParams){
            return array(
                'fileexist' => false,
                'fileNameWithParams' => false
            );
        }

        $fileNameWithParams = $pathinfo['filename'] . '-' . self::generateParams($params) . '.' . $pathinfo['extension'];
        $pathFile = 'public/assets/' . HelpersFilemanager::getDirname($pathinfo['dirname'])  . $fileNameWithParams;
        $fileexist = Storage::exists($pathFile);

        return array(
            'path' => $pathFile,
            'fileexist' => $fileexist,
            'fileNameWithParams' => $fileNameWithParams
        );
    }

    public static function generateImage($truename, $params, $database = null, $id_original = null, $originalsize = 'false')
    {   
        $pathinfo = pathinfo($truename);
        if(!self::checkExtention($pathinfo)) {
            return false;
        }
        $extension = $pathinfo['extension'];

        $params = self::checkParamsImages($params);
        
        /** @param folder_path - contains the storage path of the file */
        $folder_path = HelpersFilemanager::getDirname($truename);

        /** @param original_path - contains the path of the original file in media */
        $original_path = $folder_path . $pathinfo['basename'];
        $img = Image::make( env('APP_URL') . '/storage/media/' . $original_path );

        //if($params['fit'] == 'clip'){
        if ($params['fit'] == 'clip'){
            $img->resize($params['width'], $params['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
        }else if ($params['fit'] == 'clamp'){
            $img->fit($params['width'], $params['height']);
        } else if ($params['fit'] == 'crop'){
            $img->crop($params['width'], $params['height']);
        } else {
            $img->resize($params['width'], $params['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $webp_copy = $img;

        $img->encode($extension, $params['compression']);
        $webp_copy->encode('webp', $params['compression']);

        HelpersFilemanager::checkFolderAndCreateIt( storage_path( 'app/public/assets/' . $folder_path ) );

        $filename_with_param_no_extension = $pathinfo['filename'] . '-' . self::generateParams($params) . '.';
        
        /** @param path_webp/file_path  - contains the storage path for laravel's method */
        $path_webp = $folder_path . $filename_with_param_no_extension . 'webp';
        $file_path = $folder_path . $filename_with_param_no_extension . $extension;

        $local_path =  storage_path( 'app/public/assets/' . $file_path); 

        if (Storage::exists($local_path)) {
            // File exists
            return false;
        } else {
            $save = $img->save($local_path);

            if($save) {
                $webp_copy->save(storage_path( 'app/public/assets/' . $path_webp));
    
                $imgget = Storage::disk('local')->get('public/assets/'.$file_path);
                Storage::put('public/assets/' . $file_path, $imgget);
    
                $getImgWebp = Storage::disk('local')->get('/public/assets/' . $path_webp);
                Storage::put('public/assets/' . $path_webp, $getImgWebp);
    
                if($database){
                    DB::table('olmo_storage_versions')->insertGetId([
                        'dirname' 	       => $file_path,
                        'compression_path' => $path_webp,
                        'size'             => $img->filesize(),
                        'compression_size' => $webp_copy->filesize(),
                        'width'            => $params['width'],
                        'height'           => $params['height'],
                        'compression'      => $params['compression'],
                        'fit'              => $params['fit'],
                        'id_original'      => $id_original,
                        'is_original'      => 'false',
                        'is_originalsize'  => $originalsize,
                    ]);
                }
    
                if(env('FILESYSTEM_DRIVER', 'local') == 's3'){
                    Storage::disk('local')->delete('public/assets/' . $file_path);
                    Storage::disk('local')->delete('public/assets/' . $path_webp);
                }
    
                return true;
            }
        }


        return false;

    }

    public static function  checkExtention($pathinfo) {
        $extention = $pathinfo['extension'];
        if($extention == 'png' OR $extention == 'jpg' OR $extention == 'jpeg'){
            return true;
        }
        return false;

    }

    public static function checkParamsImages($param){
        $defaultPixel = 100;
        $permittedFit = ['clip', 'clamp', 'crop'];

        if(isset($param['height'])){
            if($param['height'] < 1){
                $param['height'] = $defaultPixel;
            }
        }

        if(isset($param['width'])){
            if($param['width'] < 1){
                $param['width'] = $defaultPixel;
            }
        }

        if(isset($param['compression'])){
            if($param['compression'] < 0 OR !$param['compression']){
                $param['compression'] = explode(',', env('IMAGES_COMPRESSION'))[0];
            }else if($param['compression'] > 100){
                $param['compression'] = 100;
            }
        }

        if(isset($param['fit'])){
            if(!in_array($param['fit'], $permittedFit)){
                $param['fit'] = 'clip';
            }
        }
        return $param;
    }

}
