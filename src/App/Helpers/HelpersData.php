<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersPermission;
use Illuminate\Support\Facades\Schema;

use App\Containers\Frontend\Quantity\Controllers\Controller as Quantity;

class HelpersData
{

    public static function normalizeBody($request)
    {

        /**
         * PLEASE FIX ME I SUCK!!
         * This method has not been modify, so take a moment to verify it
         * when all default models are done.
         */

        //Normalizzo il body
        $body = $request->toArray();
        $params = [];
        foreach ($body as $key => $value) {
            if (is_array($value)) {
                //conrolla se esiste la chiave key
                if (isset($value['value'])) {
                    $params[$key] = @$value['value'];
                } else {
                    $rel = '';
                    foreach ($value as $v) {
                        $rel .= @$v['value'] . ',';
                    }
                    $params[$key] = rtrim($rel, ',');
                }
            } else {
                if (strpos($key, 'password') !== false) {
                    if ($value != '') {
                        $params[$key] = $value;
                    }
                } else if (strpos($key, '_is') !== false) {
                    // $params[$key] = $value;
                    if ($value == 1 || $value == "true" || $value == "1") {
                        $params[$key] = "true";
                    } else {
                        $params[$key] = "false";
                    }
                } else if (strpos($key, '_filemanager') !== false) {
                    if($value){
                        $imageid = Db::table('olmo_storage')->where('truename', $value)->first();
                        if($imageid){
                            $id = $imageid->id;
                        } else {
                            $value = str_replace('/media/', '', $value);
                            $imageid = Db::table('olmo_storage')->where('truename', $value)->first();
                            $id = $imageid->id;
                        }
                    }
                    $params[$key] = $value ? $id : "";
                } else {
                    if ($value == '') {
                        $value = "";
                    }
                    $params[$key] = $value;
                }
            }
        }

        return $params;
    }

    public static function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function removeNullKey($data)
    {
        foreach($data as $key=>$value) {
            if($value === null){
                unset($data[$key]);
            }
        }

        return $data;
    }

    public static function idLangPost(Request $request)
    {

        $item = [];

        $id = $request->id ? $request->id : 1;
        $lang = $request->lang;
        $obj = [
            "searchterm" => $request->input('searchterm'),
            "pagesize" => $request->input('pagesize'),
            "offset" => $request->input('offset'),
        ];

        $item['id'] = $id;
        $item['lang'] = $lang;
        $item['obj'] = $obj;

        return $item;
    }

    public static function getType2($field, $lang = '', $singlepost = false, $defaultlang = '', $required = null, $attribute = null)
    {

        if (strpos($field, '_id_')) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_select';
            } else {
                $type = 'select';
            }
        } else if (strpos($field, '_multid_')) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_multiselect';
            } else {
                $type = 'multiselect';
            }
        } else if (strpos($field, '_collection_')) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_select';
            } else {
                $type = 'select';
            }
        } else if (strpos($field, '_select_') !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_select';
            } else {
                $type = 'select';
            }
        } else if (strpos($field, '_multiselect_') !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_select';
            } else {
                $type = 'multiselect';
            }
        } else if (strpos($field, '_email_') !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_email';
            } else {
                $type = 'email';
            }
        } else if (strpos($field, '_media_') !== false) {
            $type = 'media';
        } else if (strpos($field, 'orderitems_json_') !== false) {
            $type = 'orderitems';
        } else if (strpos($field, '_link_') !== false) {
            $type = 'link';
        } else if (strpos($field, '_multimg_') !== false) {
            $type = 'multiimage';
        } else if (strpos($field, '_img_') !== false) {
            $type = 'singleimage';
        } else if (strpos($field, '_is_') !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_checkbox';
            } else {
                $type = 'checkbox';
            }
        } else if (strpos($field, '_editor_') !== false) {
            $type = 'texteditor';
        } else if (strpos($field, '_txt_') !== false) {
            $lang = $lang == '' ? $defaultlang : $lang;

            if($field == 'name_txt_general' && $lang != $defaultlang && $singlepost){
                $type = 'disabled_text';
            } else if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_text';
            } else {
                $type = 'text';
            }
        } else if (strpos($field, '_txtarea_')    !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_textarea';
            } else {
                $type = 'textarea';
            }
        } else if (strpos($field, '_fields_') !== false) {
            $type = 'fields';
        } else if (strpos($field, '_num_') !== false) {
            $type = 'number';
        } else if (strpos($field, '_date_') !== false) {
            $type = 'date';
        } else if (strpos($field, '_euro_') !== false) {
            $type = 'euro';
        } else if (strpos($field, '_hidden_') !== false) {
            $type = 'hidden';
        } else if (strpos($field, '_list_') !== false) {
            $type = 'list';
        } else if (strpos($field, '_ord_') !== false) {
            $type = 'number';
        } else if (strpos($field, '_div_') !== false) {
            $type = 'divider';
        } else if (strpos($field, '_down_') !== false) {
            $type = 'downloader';
        } else if (strpos($field, '_spin_') !== false) {
            $type = 'spinner';
        } else if (strpos($field, '_read_') !== false) {
            $type = 'readonly';
        } else if (strpos($field, '_color_') !== false) {
            $type = 'colorpicker';
        } else if (strpos($field, '_dnd_') !== false) {
            $type = 'order';
        } else if (strpos($field, '_menu_') !== false) {
            $type = 'menu';
        } else if (strpos($field, '_spin_') !== false) {
            $type = 'spinner';
        } else if (strpos($field, '_pwd_') !== false) {
            $type = 'password';
        } else if (strpos($field, '_slider_') !== false) {
            $type = 'slider';
        } else if (strpos($field, '_sliderfromitem_') !== false) {
            $type = 'select';
        } else if (strpos($field, '_sitemapimage_') !== false) {
            $type = 'sitemapimage';
        } else if (strpos($field, '_visual_') !== false) {
            $type = 'visual';
        } else if (strpos($field, '_label_') !== false) {
            $type = 'label';
        } else if (strpos($field, '_props_') !== false) {
            if($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_multiselect';
            } else {
                $type = 'multiselect';
            }
        } else if (strpos($field, '_filemanager_') !== false) {
            $type = 'filemanager';
        } else if (strpos($field, '_langs_') !== false) {
            $type = 'lang';
        } else if (strpos($field, 'copy') !== false) {
            $type = 'copy';
        } else if (strpos($field, '_property_')   !== false) {
            $type = 'property';
        } else if (strpos($field, '_formfield_')  !== false) {
            $type = 'formfield';
        } else if (strpos($field, '_txtdis_')  !== false) {
            $type = 'disabled';
        } else if (strpos($field, '_optionfield_')  !== false) {
            $type = 'optionfield';
        } else if (strpos($field, '_csvfield_')  !== false) {
            $type = 'csvfield';
        } else if (strpos($field, '_productitems_')  !== false) {
            $type = 'productitems';
        } else if (strpos($field, '_dotsposition_')  !== false) {
            $type = 'dotsposition';
        } else {
            $type = 'hidden';
        }

        return (string)$type;
    }

    public static function getType($field, $lang = '', $singlepost = false, $defaultlang = '', $required = null, $attribute = null)
    {
        $fieldMap = [
            '_id_' => ['select', 'disabled_select'],
            '_multid_' => ['multiselect', 'disabled_multiselect'],
            '_collection_' => ['select', 'disabled_select'],
            '_select_' => ['select', 'disabled_select'],
            '_agent_' => ['agent', 'disabled_agent'],
            '_multiselect_' => ['multiselect', 'disabled_multiselect'],
            '_email_' => ['email', 'disabled_email'],
            '_media_' => 'media',
            'orderitems_json_' => 'orderitems',
            '_link_' => 'link',
            '_multimg_' => 'multiimage',
            '_img_' => 'singleimage',
            '_is_' => ['checkbox', 'disabled_checkbox'],
            '_editor_' => 'texteditor',
            '_txt_' => ['text', 'disabled_text'],
            '_txtarea_' => ['textarea', 'disabled_textarea'],
            '_fields_' => 'fields',
            '_num_' => 'number',
            '_date_' => 'date',
            '_euro_' => 'euro',
            '_hidden_' => 'hidden',
            '_list_' => 'list',
            '_ord_' => 'number',
            '_div_' => 'divider',
            '_down_' => 'downloader',
            '_spin_' => 'spinner',
            '_read_' => 'readonly',
            '_color_' => 'colorpicker',
            '_dnd_' => 'order',
            '_menu_' => 'menu',
            '_pwd_' => 'password',
            '_slider_' => 'slider',
            '_sliderfromitem_' => 'select',
            '_sitemapimage_' => 'sitemapimage',
            '_visual_' => 'visual',
            '_label_' => 'label',
            '_props_' => ['multiselect', 'disabled_multiselect'],
            '_filemanager_' => 'filemanager',
            '_langs_' => 'lang',
            'copy' => 'copy',
            '_property_' => 'property',
            '_formfield_' => 'formfield',
            '_txtdis_' => 'disabled',
            '_optionfield_' => 'optionfield',
            '_csvfield_' => 'csvfield',
            '_productitems_' => 'productitems',
            '_dotsposition_' => 'dotsposition'
        ];

        $type = 'hidden'; // Default type

        foreach ($fieldMap as $key => $value) {
            if (strpos($field, $key) !== false) {
                if (is_array($value)) {
                    if ($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                        $type = $value[1];
                    } else {
                        $type = $value[0];
                    }
                } else {
                    $type = $value;
                }
                break;
            }
        }

        if (strpos($field, '_txt_') !== false) {
            $lang = $lang == '' ? $defaultlang : $lang;
                /**
                 * Scopri perchè hai fatto sta cosa e metti il commento
                 * 1- La gestione dei default Container
                 * 2- da capire perchè avevo messo $lang come type = al posto di 'disable'
                 */
            if ($field == 'name_txt_general' && $lang != $defaultlang && $singlepost) {
                $type = 'disabled_text';
            } else if ($required && HelpersPermission::userPermissionFieldsDisabled($attribute['table'], $field)) {
                $type = 'disabled_text';
            } else {
                $type = 'text';
            }
        }

        return (string)$type;
    }


    public static function setTypeList($value)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $type = self::getType($value);
        // explode('_', $value)[0]

        if ($type == 'checkbox' || $type == 'enabled' || $type == 'spinner' || $type == 'label') {
            $type = 'label';
        } else if ($type == 'singleimage' || $type == 'filemanager') {
            $type = 'image';
        } else if ($type == 'select' || $type == 'multiselect' || $type == 'role' || $type == 'text' || $type == 'number') {
            $type = 'text';
        } else if ($type == 'email') {
            $type = 'email';
        } else if ($type == 'lang') {
            $type = 'lang'; // delete it
        } else if ($type == 'date') {
            $type = 'date';
        } else if ($type == 'euro') {
            $type = 'euro';
        } else {
            $type = 'hidden';
        }

        return $type;
    }

    public static function setKey($value)
    {
        if ($value == 'copytranslate') {
            $label = array(0 => 'Copy content post');
        } else {
            $label = explode('_', $value);
        }
        return isset($label[0]) ? $label[0] : $value;
    }

    public static function setLabel($value)
    {
        if ($value == 'copytranslate') {
            $label = array(0 => 'Copy content post');
        } else if (str_contains($value, '-')) {
            $label = explode('_', $value);
            $label = explode('-', $label[0]);
        } else {
            $label = explode('_', $value);
        }
        return isset($label[0]) ? $label[0] : $value;
    }

    public static function setTab($value)
    {
        $defaultTab = 'general';
        $tab = explode('_', $value);
        return isset($tab[2]) ? $tab[2] : $defaultTab;
    }

    public static function getDefaultValue($required, $k)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $res = "";
        if (isset($required[$k])) {
            $res =  $required[$k];
        }
        return $res;
    }

    /**
     * This method is use inside the BackController serializeSinglePost
     * to print-out a required condition inside a field
     */
    public static function isRequired($required, $key)
    {
        $response = "false";
        if (is_array($required)) {
            if (in_array($key, $required)) {
                $response = "true";
            }
        }
        return $response;
    }

    public static function currentValueSelect($collection, $value)
    {
        // return $collection;
        foreach ($collection as $item) {
            if ($item['value'] == $value) {
                $val = [];
                $val['value'] = $item['value'];
                $val['key'] = $item['key'];
                return  $val;
            }
        }

        return $value;
    }

    public static function currentValueMultiSelect($value, $key)
    {

        $table = 'olmo_'.self::setLabel($key);
        $items = [];

        if ($value) {
            $ids = explode(',', $value);
            foreach ($ids as $id) {
                $item = [];
                if($table == 'olmo_property'){
                    $data = Db::table('olmo_propertyitem')->where('id', $id)->select('id', 'name_txt_general as value', 'postid_hidden_general')->first();
                    $property = Db::table('olmo_property')->where('id', $data->postid_hidden_general)->select('name_txt_general as property')->first();
                    $item['key'] = @$property->property.' - '.@$data->value;
                    $item['value'] = (string)@$data->id ?? '';
                } else {
                    $checkTable = Schema::hasTable($table);
                    if($checkTable){
                        $data = Db::table($table)->where('id', $id)->first();
                        $item['key'] = @$data->name_txt_general ?? @$data->username_txt_general ?? @$data->email_email_general ?? '';
                        $item['value'] = (string)@$data->id ?? '';
                    } else {
                        $el = HelpersCollection::jsoncollection(self::setLabel($key));
                        foreach($el as $e){
                            if($e['value'] == $id){
                                $item['key'] = $e['key'];
                            }
                        }
                        $item['value'] = $id;
                    }
                }
                array_push($items, $item);
            }
        }

        return $items;
    }

    public static function normalizestring($text, $checkextension = true)
    {
        $textfile     = pathinfo($text);
        $filename     = $textfile['filename'];
        $filename     = self::clean($filename);
        $filename     = strtolower($filename);
        if(!$checkextension){
            return $filename;
        } else {
            $extension    = $textfile['extension'];
            return $filename . '.' . $extension;
        }
    }

    public static function normalizePathString($path, $pathinfo = false){

        $pathinfo = pathinfo($path);

        $all_folder = explode('/', $pathinfo['dirname']);
        $normalizedpath = '';
        foreach($all_folder as $folder_name){
            $normalizedpath .= self::normalizestring($folder_name, false)."/";
        }
        $normalizedpath .= self::normalizestring($pathinfo['basename']);
        
        $normalizedpath = ltrim($normalizedpath, '/');
        if($pathinfo){
            $array = pathinfo($normalizedpath);
            $array['relativepath'] = $normalizedpath;
            return $array;
        }
        return $normalizedpath;
    }

    public static  function clean($string)
    {
        if(strpos($string, '.blade') === false){
            $string = str_replace(' ', '-', $string);
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
            return preg_replace('/-+/', '-', $string);
        }
        $string = str_replace(' ', '-', $string);
        return preg_replace('/-+/', '-', $string);
    }

    public static function str_replace_first($search, $replace, $subject)
    {
        $search = '/' . preg_quote($search, '/') . '/';
        return preg_replace($search, $replace, $subject, 1);
    }

    public static function isDefaultModel($model)
    {
        $olmo = strpos($model, 'olmo_') !== false ? 'olmo_' : '';
        if(
            $model == $olmo.'order' ||
            $model == $olmo.'property' ||
            $model == $olmo.'template' ||
            $model == $olmo.'slider' ||
            $model == $olmo.'menu' ||
            $model == $olmo.'user' ||
            $model == $olmo.'form' ||
            $model == $olmo.'formtype' ||
            $model == $olmo.'language' ||
            $model == $olmo.'visualblock' ||
            $model == $olmo.'analytics' ||
            $model == $olmo.'formauth' ||
            $model == $olmo.'formcsv' ||
            $model == $olmo.'formcsvfield' ||
            $model == $olmo.'formfield' ||
            $model == $olmo.'formfieldoptions' ||
            $model == $olmo.'formfieldoptionstype' ||
            $model == $olmo.'formmailchimp' ||
            $model == $olmo.'formmailchimpfield' ||
            $model == $olmo.'formsmtp' ||
            $model == $olmo.'importexport' ||
            $model == $olmo.'propertyitem' ||
            $model == $olmo.'role' ||
            $model == $olmo.'slideritem' ||
            $model == $olmo.'storage' ||
            $model == $olmo.'storage_versions' ||
            $model == $olmo.'templateslug' ||
            $model == $olmo.'tokens' ||
            $model == $olmo.'regions' ||
            $model == $olmo.'country' ||
            $model == $olmo.'customer' ||
            $model == $olmo.'customeritem' ||
            $model == $olmo.'address' ||
            $model == $olmo.'fillform' ||
            $model == $olmo.'cart' ||
            $model == $olmo.'cartitem' ||
            $model == $olmo.'productitem' ||
            $model == $olmo.'wishlist' ||
            $model == $olmo.'quantity' ||
            $model == $olmo.'shippingmethod' ||
            $model == $olmo.'paymentmethod' ||
            $model == $olmo.'formlog' ||
            $model == $olmo.'sessions' ||
            $model == $olmo.'dotsposition' ||
            $model == $olmo.'mailchimp' ||
            $model == $olmo.'fieldrepeateritem' ||
            $model == $olmo.'sitemapimages' ||
            $model == $olmo.'smtp' ||
            $model == 'cache'
        ){
            return true;
        }
        return false;
    }

    public static function spinNumber($key, $value, $values, $type, $table, $id)
    {
        if(self::setLabel($key) == 'qty' AND Schema::hasTable('olmo_quantity')){
            if(env('CHECK_QUANTITY_CUSTOM', false)){
                if($type == 'item'){
                    $product = DB::table('olmo_productitem')->where('id', $id)->first();
                } else {
                    $product = DB::table('olmo_product')->where('id', $id)->first();
                }
                $qty = Quantity::getQuantity($product);
            } else {
                $qty = DB::table('olmo_quantity')->where('prod_id', $id)->first();
            }            
            if($qty){
                $qty = json_decode(json_encode($qty, true), true);
                return $qty['quantity'];
            }
            return (int)$value;
        }
        return (int)$value;
    }

    public static function convertKeyToStrings($array) {
        $string = "";
        foreach (array_keys($array) as $key) {
          $string .= strval($key) . ",";
        }
        // Remove the trailing comma
        return rtrim($string, ",");
      }

}
