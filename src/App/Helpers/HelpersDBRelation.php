<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersCollection;
use Olmo\Core\App\Helpers\HelpersAgent;

class HelpersDBRelation
{

    public static function singleRelation($table, $value)
    {
        $data = Db::table($table)->where('id', $value)->first();
        // $data->name_txt_general ?? $data->username_txt_general ?? $data->email_email_general
        return $data;
    }

    /**
     * This method could be useless
     */
    public static function arrayRelation($table, $value)
    {
        $data = Db::table($table)->where('id', $value)->get();
        return $data;
    }

    /**
     * This method has been used in BackController getListPostValue method
     * to retreive the name if the field type is select (_id_ or _select__)
     * 
     * ----- WORKING ON ------
     * others besides name_txt_general could be possible
     */
    public static function selectRelation($table, $values, $type, $key = null)
    {
        $collection = explode('_', $key);
        if($collection[1] == 'collection'){
            return $values;
        } elseif(!empty($values)){
            $data = Db::table($table)->where('id', $values)->first();
            if($data){
                return $data->name_txt_general;
            }
        }

        return "";
    }

    public static function dropdownListValuePair($data, $firstElementEmpty = false)
    {
        $items = [];
        $i = 0;
        if($firstElementEmpty){
            $item = [];
            $item['key'] = '';
            $item['value'] = '';
            $items[$i] = $item;
            $i++;
        }
        
        
        foreach ($data as $value) {
            $item = [];
            $item['key']    = $value->name_txt_general ?? $value->username_txt_general ?? $value->email_email_general;
            $item['value']  = (string)$value->id;
            $items[$i]      = $item;
            $i++;
        }
        return $items;
    }

    /**
     * listRelation method is generally used to backoffice porpuse
     * it gets the relation between two tables by the name of the table
     * which can be retrieved by the column name extracting the key like template_id_general
     * in this case template is the key and the name of the table like olmo_template
     * so use it to create self-generated relation between model
     */
    public static function listRelation($table, $lang, $model, $id, $key, $modelid = null)
    {
        /**
         * Select Relation can be done in three ways
         * 1- to a table 
         * 2- to a colelction, this concept must be changed as soon as possible
         * 3- to a property item in the table olmo_propertyitem
         */
        $steps = [];
        $items = [];
        
        $steps[] = $table;
        $checkTable = Schema::hasTable($table);
        $isAgent = false;
        if(!$checkTable){
            $isAgent =  $table == 'olmo_agent' ? true:false;
        }
        /** to a table */
        if ($checkTable || $isAgent) {
            $steps[] = 'checktable';
            
            /**
             * Check if locale_hidden_general column is in the table, if so make do a specific query
             */
            if($isAgent){
                $response = true;
            }else{
                $response = Db::select("SHOW COLUMNS FROM $table LIKE 'locale_hidden_general'");
                $response = count($response) > 0 ? true : false;   
            }
 
                   
            if ($response) {
                /** 
                 * Specific query to the table and lang 
                 * but if the relation is with template needs a different query
                 * and if it is property point the query to olmo_property
                 */                
                if($table == 'olmo_productitem') {
                    $data = Db::table($table)->where('postid_hidden_general', $modelid)->get();
                } else if ($table == 'olmo_template') {
                    $data = Db::table($table)->where('model_select_general', $model)->get();
                } else if ($table == 'olmo_agent') {
                    $data = HelpersAgent::getAgents();
                } else if($table == 'olmo_property') {
                    $data = Db::table('olmo_propertyitem')->where('enabled_is_general', 'true')->where('locale_hidden_general', $lang)->get();
                    foreach($data as $key=>$item){
                        $property = Db::table('olmo_property')->where('enabled_is_general', 'true')->where('id', $item->postid_hidden_general)->first();
                        if($item->name_txt_general == ''){
                            unset($data[$key]);
                            /**
                             * ALERT THIS IS A DELETE  
                             * Delete property item without name 
                             * */
                            Db::table('olmo_propertyitem')->where('id', $item->id)->delete();
                        } else {
                            /** Check if the property exist if not delete the relation in the previous table */
                            if($property){
                                $item->name_txt_general = $property->name_txt_general.' - '.$item->name_txt_general;
                            } else {
                                unset($data[$key]);
                                /**
                                 * ALERT THIS IS A DELETE 
                                 * Delete property item without property relation 
                                 * */
                                Db::table('olmo_propertyitem')->where('id', $item->id)->delete();
                            }
                        }
                    }
                } else {
                    $data = Db::table($table)->where('locale_hidden_general', $lang)->get();
                }                
            } else {
                /** general query to the table */
                $data = Db::table($table)->get();
            }
            // return $steps;
            /** 
             * Create che array for the list of values by key and value 
             * the first value must be always empty
             * */
            $items = self::dropdownListValuePair($data, true);
        }
        /** 
         * To a colelction 
         * The collection has been extracted from the table which is the table builded from the key
         */
        else {
            $fieldname = explode('_', $key)[0];
            if($fieldname == 'dependentfield'){
                $items = HelpersForm::dependentfield($id);
            } else {
                $model = explode('_', $table)[1];
                $items = HelpersCollection::get($model);
                if(!$items){
                    
                    $items = HelpersCollection::jsoncollection($model);
                    return $items;
                    // $items = [
                    //     [
                    //         "key" => '',
                    //         "value" => '',
                    //     ],                        
                    //     [
                    //         "key" => 'key one',
                    //         "value" => 'value one',
                    //     ],
                    //     [
                    //         "key" => 'key two',
                    //         "value" => 'value two',
                    //     ] 
                    // ];
                }
            }
        }
        // return $steps;
        return $items;
    }


    public static function makeRelation($table, $value, $type)
    {

        $data  = Db::table($table)->whereIn('id', $value)->get();
        $i = 0;

        foreach ($data as $value) {
            $item = [];
            $item['key']    =  $value->name_txt_general ?? @$value->username_txt_general ?? @$value->email_email_general;
            $item['value']  = (string)$value->id;
            $items[$i]      = $item;
            $i++;
        }

        return $items;
    }

    public static function currentRelation(
        $table,
        $values,
        $type
    ) {

        /**
         * THIS IS A DISASTER
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         * 
         * WORKING ON
         * 
         */

        $items  = [];
        $i      = 0;

        // if ($copyid != 0) {
        //     $values = Db::table($model)->where('id', $copyid)->first()->{$field};
        // }

        // if ($values == '') {
        //     if ($type == 'multiselect') {
        //         $items = [];
        //     } else {
        //         $item['key']    = '';
        //         $item['value']  = '';
        //         $items          = $item;
        //     }
        // } else {
        //     $ids = explode(',', $values);
        //     //Recupero gli id in lingua
        //     if ($copyid != 0) {
        //         $temp = [];
        //         foreach ($ids as $id) {
        //             $row = Db::table($table)->where('parentid_hidden_general', $id)->first();
        //             if ($row)
        //                 $temp[] =  $row->id;
        //         }
        //         $ids = $temp;
        //     }

        // }

        $data = Db::table($table)->where('id', $values)->first();

        return $data->name_txt_general;       

    }
}
