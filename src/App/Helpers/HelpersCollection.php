<?php

namespace Olmo\Core\App\Helpers;

use Olmo\Core\Loaders\OlmoLoadersContainers;

class HelpersCollection
{

    private const CONTAINERS_DIRECTORY_NAME = 'app'; 

    private const CONTAINER_SECTION = 'Settings';        

    public static function jsoncollection($type)
    {

        $collectionDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR . "Collection");
        $collectionPath = $collectionDirectory.'/'.'collection.json';
        $collectionRaw = json_decode(file_get_contents($collectionPath), true);
        
        return isset($collectionRaw[$type]) ? $collectionRaw[$type] : [['key' => '', 'value' => '']];

    }

    public static function get($type)
    {
        // TODO:
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $res = [];

        $collection['customerprop'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'vat', 'value' => 'vat']
        ];

        $collection['customercondition'] = [
            ['key' => '', 'value' => ''],
            ['key' => '=', 'value' => '='],
            ['key' => '=', 'value' => '!='],
            ['key' => '>', 'value' => '>'],
            ['key' => '<', 'value' => '<'],
            ['key' => '>=', 'value' => '>='],
            ['key' => '<=', 'value' => '<=']
        ];

        $collection['ordercondition'] = [
            ['key' => '', 'value' => ''],
            ['key' => '=', 'value' => '='],
            ['key' => '=', 'value' => '!='],
            ['key' => '>', 'value' => '>'],
            ['key' => '<', 'value' => '<'],
            ['key' => '>=', 'value' => '>='],
            ['key' => '<=', 'value' => '<=']
        ];

        $collection['ordernumber'] = [
            ['key' => '', 'value' => '']
        ];

        $collection['model'] = [
            ['key' => '',          'value' => ''],
            ['key' => 'page',      'value' => 'page'],
            ['key' => 'product',   'value' => 'product'],
            ['key' => 'arredo',  'value' => 'arredo'],
            ['key' => 'blog',      'value' => 'blog'],
            ['key' => 'sector',   'value' => 'sector'],
            ['key' => 'project',   'value' => 'project'],
            ['key' => 'whyifi',  'value' => 'whyifi'],
            ['key' => 'technology',  'value' => 'technology'],
            ['key' => 'formatpers',  'value' => 'formatpers']
        ];

        $collection['index'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'index', 'value' => 'index'],
            ['key' => 'noindex', 'value' => 'noindex'],
        ];

        $collection['follow'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'follow', 'value' => 'follow'],
            ['key' => 'nofollow', 'value' => 'nofollow']
        ];

        $collection['frequency'] = [
            ['key' => '',        'value' => ''],
            ['key' => 'always',  'value' => 'always'],
            ['key' => 'hourly',  'value' => 'hourly'],
            ['key' => 'daily',   'value' => 'daily'],
            ['key' => 'weekly',  'value' => 'weekly'],
            ['key' => 'monthly', 'value' => 'monthly'],
            ['key' => 'never',   'value' => 'never']
        ];

        $collection['priority'] = [
            ['key' => '', 'value' => ''],
            ['key' => '0', 'value' => '0'],
            ['key' => '0.1', 'value' => '0.1'],
            ['key' => '0.2', 'value' => '0.2'],
            ['key' => '0.3', 'value' => '0.3'],
            ['key' => '0.4', 'value' => '0.4'],
            ['key' => '0.5', 'value' => '0.5'],
            ['key' => '0.6', 'value' => '0.6'],
            ['key' => '0.7', 'value' => '0.7'],
            ['key' => '0.8', 'value' => '0.8'],
            ['key' => '0.9', 'value' => '0.9'],
            ['key' => '1', 'value' => '1'],
        ];

        $collection['formtype'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'login', 'value' => 'login'],
            ['key' => 'register', 'value' => 'register'],
            ['key' => 'resetpassword', 'value' => 'resetpassword'],
            ['key' => 'neworder', 'value' => 'neworder'],
            ['key' => 'orderconfirmed', 'value' => 'orderconfirmed'],
            ['key' => 'ordershipped', 'value' => 'ordershipped'],
            ['key' => 'orderrefunded', 'value' => 'orderrefunded'],
            ['key' => 'customer creation', 'value' => 'customercreation'],
            ['key' => 'customer not active', 'value' => 'customernotactive'],
            ['key' => 'customer active', 'value' => 'customeractive'],
            ['key' => 'customer disabled', 'value' => 'customerdisabled'],
            ['key' => 'customer blocked', 'value' => 'customerblocked'],
            ['key' => 'customer password recovery', 'value' => 'customerpasswordrecovery'],
            ['key' => 'customer password changed', 'value' => 'customerpasswordchanged'],
            ['key' => 'request asstet', 'value' => 'requestasset']
        ];

        $collection['typeaddress'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'shipping', 'value' => 'shipping'],
            ['key' => 'billing', 'value' => 'billing']
        ];

        $collection['customerstatus'] = [
            ['key' => 'Not active', 'value' => 'notactive'],
            ['key' => 'Active', 'value' => 'active'],
            ['key' => 'Disabled', 'value' => 'disabled'],
            ['key' => 'Blocked', 'value' => 'blocked']
        ];

        $collection['orderstatus'] = [
            ['key' => 'Pending', 'value' => 'pending'],
            ['key' => 'Paid', 'value' => 'paid'],
            ['key' => 'Shipped', 'value' => 'shipped'],
            ['key' => 'Completed', 'value' => 'completed'],
            ['key' => 'Canceled', 'value' => 'canceled'],
            ['key' => 'Refunded', 'value' => 'refunded']
        ];

        $collection['element'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Input', 'value' => 'input'],
            ['key' => 'Textarea', 'value' => 'textarea'],
            ['key' => 'Inputlist', 'value' => 'inputlist'],
            ['key' => 'Text', 'value' => 'text']
        ];
        
        $collection['typology'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Text', 'value' => 'text'],
            ['key' => 'Email', 'value' => 'email'],
            ['key' => 'Number', 'value' => 'number'],
            ['key' => 'Checkbox', 'value' => 'checkbox'],
            ['key' => 'Button', 'value' => 'button'],
            ['key' => 'Date', 'value' => 'date'],
            ['key' => 'Datetime-local', 'value' => 'datetime-local'],
            ['key' => 'File', 'value' => 'file'],
            ['key' => 'Hidden', 'value' => 'hidden'],
            ['key' => 'Image', 'value' => 'image'],
            ['key' => 'Password', 'value' => 'password'],
            ['key' => 'Submit', 'value' => 'submit'],
            ['key' => 'Tel', 'value' => 'tel'],
            ['key' => 'Url', 'value' => 'url'],
            ['key' => 'Time', 'value' => 'time'],
            ['key' => 'Newsletter', 'value' => 'newsletter'],
            ['key' => 'Checkbox', 'value' => 'checkbox'],
            ['key' => 'Radio', 'value' => 'radio'],
            ['key' => 'Select', 'value' => 'select'],
            ['key' => 'Multi Select', 'value' => 'multiselect'],
            ['key' => 'Country', 'value' => 'country'],
            ['key' => 'Province', 'value' => 'province'],                   
            ['key' => 'Region', 'value' => 'region'],                   
        ];

        $collection['typologyinput'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Text', 'value' => 'text'],
            ['key' => 'Email', 'value' => 'email'],
            ['key' => 'Number', 'value' => 'number'],
            ['key' => 'Checkbox', 'value' => 'checkbox'],
            ['key' => 'Button', 'value' => 'button'],
            ['key' => 'Date', 'value' => 'date'],
            ['key' => 'Datetime-local', 'value' => 'datetime-local'],
            ['key' => 'File', 'value' => 'file'],
            ['key' => 'Hidden', 'value' => 'hidden'],
            ['key' => 'Image', 'value' => 'image'],
            ['key' => 'Password', 'value' => 'password'],
            ['key' => 'Submit', 'value' => 'submit'],
            ['key' => 'Tel', 'value' => 'tel'],
            ['key' => 'Url', 'value' => 'url'],
            ['key' => 'Time', 'value' => 'time'],
            ['key' => 'Newsletter', 'value' => 'newsletter']
        ];        

        $collection['typologyinputlist'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Checkbox', 'value' => 'checkbox'],
            ['key' => 'Radio', 'value' => 'radio'],
            ['key' => 'Select', 'value' => 'select'],
            ['key' => 'Multi Select', 'value' => 'multiselect'],
            ['key' => 'Country', 'value' => 'country'],
            ['key' => 'States', 'value' => 'states'],
            ['key' => 'Province', 'value' => 'province']
        ];    

        $collection['typologytext'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Long Text', 'value' => 'longtext'],
            ['key' => 'Short Text', 'value' => 'shorttext']
        ];    
        
        $collection['dependentaction'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Show', 'value' => 'show'],
            ['key' => 'Hide', 'value' => 'hide']
        ];      
        
        $collection['formfieldoptionstype'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'Custom', 'value' => 'custom'],
            ['key' => 'Country', 'value' => 'country'],
            ['key' => 'Province', 'value' => 'province'],
            ['key' => 'Region', 'value' => 'region'],
            ['key' => 'USA States', 'value' => 'usastate'],
            ['key' => 'Canada States', 'value' => 'canadanation'],
            ['key' => 'Products', 'value' => 'product']
        ];      
        
        $collection['auth'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'True', 'value' => 'true'],
            ['key' => 'False', 'value' => 'false']
        ]; 

        $collection['tls'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'SSL', 'value' => 'ssl'],
            ['key' => 'TLS', 'value' => 'tls']
        ]; 

        $collection['statuscode'] = [
            ['key' => '200', 'value' => '200'],
            ['key' => '404', 'value' => '404'],
            ['key' => '410', 'value' => '410']
        ];         

        if($type == 'model'){
            return self::getTemplates();
        } else if(isset($collection[$type])) {
            $res = $collection[$type];
        }

        return $res;
    }    

    public static function getTemplates()
    {

        $containers = OlmoLoadersContainers::getAllContainerPaths();
        $model = [];

        $empty = array('key' => '', 'value' => '');
        array_push($model, $empty);

        foreach($containers as $item){
            $container = last(explode('/', $item));
            $value = [];
            $value['key'] = strtolower($container);
            $value['value'] = strtolower($container);
            array_push($model, $value);
        }

        return $model;

    }

}