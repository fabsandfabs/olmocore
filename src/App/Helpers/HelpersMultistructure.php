<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Http\Controller\FrontController;
use Olmo\Core\Containers\Backoffice\Template\Models\Template;

use Olmo\Core\App\Helpers\HelpersUser;

class HelpersMultistructure
{

    private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

    private const CONTAINER_STRUCTURE = 'Structure';

            // {
            // "1":{
            //     "ip":"127.0.0.1",
            //     "domain":"landing.test",
            //     "langs":{
            //         "default": "en",
            //         "en": "en",
            //         "it": "it"
            //     },
            //     "analysticsid":"",
            //     "value":""
            // }
            // }

    public static function get(Request $request){

        if($request->headers->has('structure-token') ){

            $structureDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_STRUCTURE . DIRECTORY_SEPARATOR);
            
            $directory = File::isDirectory($structureDirectory);
            
            if( $directory){
                
                $file_structure_name = $request->headers->get('structure-token').".json";
                $structure_path = $structureDirectory . $file_structure_name;
                if(File::exists($structure_path)){
                    $structure = json_decode(file_get_contents($structure_path), true);
                    return $structure;
                }
            }
        }

        return false;
    }

    public static function getStructureLangs(Request $request){
        $res = [];
        $res['i18n']['default_locale'] = @HelpersLang::getDefaultFrontLang();

        if(isset($request['structure'])){
            $structure_langs = $request['structure']['langs'];
            $res['i18n']['locales'] = $structure_langs;
        }else{
            $res['i18n']['locales'] = HelpersLang::validLangFront();;
        }

        return $res;
    }

    public static function getAnalisticId(Request $request){
        $id = 1;
        if(isset($request['structure'])){
            $id = $request['structure']['analysticsid'];
        }
        return $id;
    }

    public static function getStructureTemplates(Request $request){
        $items = [];

        if(isset($request['structure'])){
            $structure_id = $request['structure']['value'];
            $items = Template::where('enabled_is_general', 'true')->where('structure_id_general', $structure_id)->get();
        }else{
            $items = Template::where('enabled_is_general', 'true')->where('structure_id_general', '')->get();
        }

        return $items;
    }



}
