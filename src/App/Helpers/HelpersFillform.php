<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HelpersFillform
{

    public static function sendMailFillform($formid, $params = array())
    {

        $token = env('FILLFORM_TOKEN');
        $url = env('FILLFORM_API_URL').$token.'/sendMailV2/'.$formid;
        $response = Http::asForm()->post($url, $params);

        return $response;

    }
    
}
