<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;

class HelpersForm
{

    private const CONTAINERS_DIRECTORY_NAME = 'app'; 

    private const CONTAINER_SECTION = 'Settings';    

    public static function retrieveSetting() {

        $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR . "Form");
        $settingPath = $menuDirectory.'/'.'form.json';
        $settingRaw = json_decode(file_get_contents($settingPath), true);     

        return response($settingRaw, 200);

    }

    public static function getFields($value)
    {
        if($value == ''){
            return '';
        } else{
            $slides = explode(',',$value);
            foreach($slides as $key=>$item){
                $slides[$key] = self::getField($item);
            }
            return $slides;
        }        
    }

    public static function getField($value)
    {
        $field = DB::table('olmo_formfield')->where('id', $value)->select(
            'id',
            'name_txt_general as name',
            'attribute_txt_general as attribute',
            'class_txt_general as class',
            'dependentaction_select_general as dependentaction',
            'dependentfield_select_general as dependentfield',
            'dependenttrigger_txt_general as dependenttrigger',
            'element_select_general as element',
            'hidden_is_general as hidden',
            'idfield_txt_general as idfield',
            'label_txt_general as label',
            'placeholder_txt_general as placeholder',
            'option_optionfield_general as option',
            'required_is_general as required',
            'typology_select_general as typology'
            )->first();

        return $field;
    }  
    
    public static function dependentfield($id) 
    {
        $field = DB::table('olmo_formfield')->where('id', $id)->first();
        $activeFields = DB::table('olmo_form')->where('id', $field->postid_hidden_general)->first();
        $splitField = explode(',', $activeFields->formfield_formfield_fields);

        $listField = [];
        foreach($splitField as $singleField){
            $thefield = DB::table('olmo_formfield')->where('id', $singleField)->first();
            if($thefield){
                array_push($listField, $thefield);
            }
        }

        $i = 0;
        $item = [];
        $item['key'] = '';
        $item['value'] = '';
        $items[$i] = $item;
        $i++;

        foreach ($listField as $value) {
            $item = [];
            $item['key']    = $value->name_txt_general;
            $item['value']  = $value->name_txt_general;
            $items[$i]      = $item;
            $i++;
        }        

        return $items;
    }


    public static function defaultForms(){
        $presets = [
            'contact' => [
                'fields' => ['name_txt_general', 'element_select_general', 'typology_select_general', 'idfield_txt_general', 'required_is_general', 'hidden_is_general'],
                'rows' => [
                            ['usertype', 'inputlist', 'select', 'usertype', 'true', 'false'],
                            ['name', 'input', 'text', 'name', 'true', 'false'],
                            ['surname', 'input', 'text', 'surname', 'true', 'false'],
                            ['email', 'input', 'email', 'email', 'true', 'false'],
                            ['company', 'input', 'text', 'company', 'false', 'false'],
                            ['telephone', 'input', 'text', 'telephone', 'true', 'false'],
                            ['mobilenum', 'input', 'text', 'mobilenum', 'false', 'false'],
                            ['city', 'input', 'select', 'city', 'false', 'false'],
                            ['nation', 'input', 'select', 'nation', 'true', 'false'],
                            ['requesttype', 'inputlist', 'select', 'requesttype', 'true', 'false'],
                            ['file', 'input', 'file', 'file', 'false', 'false'],
                            ['message', 'textarea', '', 'message', 'true', 'false'],
                            ['newsletter', 'input', 'checkbox', 'newsletter', 'false', 'false'],
                            ['privacy', 'input', 'checkbox', 'privacy', 'true', 'false']
                        ]
                ],
            'workwithus' => ['fields' => ['name_txt_general', 'element_select_general', 'typology_select_general', 'idfield_txt_general', 'required_is_general', 'hidden_is_general'],
                'rows' => [
                            ['name', 'input', 'text', 'name', 'true', 'false'],
                            ['surname', 'input', 'text', 'surname', 'true', 'false'],
                            ['email', 'input', 'email', 'email', 'true', 'false'],
                            ['telephone', 'input', 'text', 'telephone', 'true', 'false'],
                            ['city', 'input', 'select', 'city', 'false', 'false'],
                            ['nation', 'input', 'select', 'nation', 'true', 'false'],
                            ['file', 'input', 'file', 'file', 'false', 'false'],
                            ['message', 'textarea', '', 'message', 'true', 'false'],
                            ['newsletter', 'input', 'checkbox', 'newsletter', 'false', 'false'],
                            ['privacy', 'input', 'checkbox', 'privacy', 'true', 'false']
                        ]
                    ], 
            'newsletter' => [
                'fields' => ['name_txt_general', 'element_select_general', 'typology_select_general', 'idfield_txt_general', 'required_is_general', 'hidden_is_general'],
                'rows' => [
                            ['email', 'input', 'email', 'email', 'true', 'false'],
                            ['privacy', 'input', 'checkbox', 'privacy', 'true', 'false']
                        ]
            ]
        ];

        return $presets;
        
    }

    public static function jsonSeeding($name)
    {
        $SeedingDirectory = base_path("app/Settings/Form/Seeding");
        $SeedingPath = $SeedingDirectory.'/'.$name.'.json';
        $SeedingRaw = false;
        if(file_exists($SeedingPath)){
            $SeedingRaw = json_decode(file_get_contents($SeedingPath), true);
        }
        
        return $SeedingRaw;
    }

}