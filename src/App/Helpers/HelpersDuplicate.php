<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersData;

class HelpersDuplicate
{
    public static function duplicateSinglePost($table, $lang, $id)
    {
        $post = Db::table($table)->where('id', $id)->first();
        if($post){
            unset($post->id);
            
            $insertpostId = Db::table($table)->insertGetId((array)$post);
            $insertpost = Db::table($table)->where('id', $insertpostId)->first();

            /** Generate the relation creating new post */
            $response = HelpersDuplicate::duplicateRelations($insertpost, $lang, $insertpostId);

            $id = $response['id'];
            $response['name_txt_general'] = $response['name_txt_general']. ' Copy '.$id;
            unset($response['id']);
            Db::table($table)->where('id', $id)->update((array)$response);

            $response = array('id' => $insertpostId);

            return response($response, 200);
        }
        return response(404);
    }

    public static function duplicateRelations($data, $lang, $postid)
    {
        $toArray = (array)$data;
        foreach ($toArray as $key => $item) {
            $type = HelpersData::getType($key);
            $label = HelpersData::setLabel($key);
            if ($type == 'slider' && $item != '') {
                // TODO:
                // $ids = explode(',', $item);
                // foreach ($ids as $k => $id) {
                //     $getSlideId = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_slideritem');
                //     if($getSlideId){
                //         if ($k == 0) {
                //             $toArray[$key] = (string)$getSlideId;
                //         } else {
                //             $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                //         }
                //     }
                // }
            } else if ($type == 'visual' && $item != '') {
                // TODO:
                // $ids = explode(',', $item);
                // foreach ($ids as $k => $id) {
                //     $getSlideId = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_visualblock');
                //     if($getSlideId){
                //         if ($k == 0) {
                //             $toArray[$key] = (string)$getSlideId;
                //         } else {
                //             $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                //         }
                //     }
                // }
            } else if ($type == 'fields' && $item != '') {
                // TODO:
            } else if ($type == 'formfield' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getSlideId = self::generateFormfieldRelation($id, $lang, $postid, 'olmo_formfield');
                    if($getSlideId){
                        if ($k == 0) {
                            $toArray[$key] = (string)$getSlideId;
                        } else {
                            $toArray[$key] = $toArray[$key] . ',' . (string)$getSlideId;
                        }
                    }
                }                
            }
        }

        return $toArray;
    }    

    public static function generateFormfieldRelation($id, $lang, $postid, $table)
    {
        $getProp = Db::table($table)->where('id', $id)->first();
        $prop = (array)$getProp;

        // $prop['locale_hidden_general'] = $lang;
        // $prop['parentid_hidden_general'] = $id;
        $prop['postid_hidden_general'] = $postid;
        unset($prop['id']);

        /**
         * Check if the value is already there, if so replace the values and do not add row
         */
        $insertProp = Db::table($table)->insertGetId($prop);
        
        self::generateFormfieldOptionsRelation($id, $insertProp);
        
        return $insertProp;
    }    
    
    public static function generateFormfieldOptionsRelation($idfrom, $idto)
    {
        $options = Db::table('olmo_formfieldoptions')->where('fieldid_hidden_general', $idfrom)->get();
        foreach($options as $option){
            unset($option->id);
            $option->fieldid_hidden_general = $idto;
            Db::table('olmo_formfieldoptions')->insert((array)$option);
        }

        $optionsType = Db::table('olmo_formfieldoptionstype')->where('fieldid_hidden_general', $idfrom)->first();
        if($optionsType){
            unset($optionsType->id);
            $optionsType->fieldid_hidden_general = $idto;        
            Db::table('olmo_formfieldoptionstype')->insert((array)$optionsType);
        }

    }
}
