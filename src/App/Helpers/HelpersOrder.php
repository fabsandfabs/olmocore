<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Olmo\Forms\App\Http\Controller\FormController;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersCustomer as Customer;
use Olmo\Core\App\Http\Controller\BackController;
// use Olmo\Core\Containers\Backoffice\Order\Controllers\Controller as Order;
use Olmo\Core\Containers\Backoffice\Order\Models\Order as OrderModel;
use Olmo\Ecommerce\Containers\Ecommerce\Order\Controllers\Controller as Order;

class HelpersOrder
{
    public static function getOrderStatus($table, $id)
    {
        if ($table == 'olmo_order') {
            $order = Db::table($table)->where('id', $id)->first();

            return $order->orderstatus_select_general;
        }
    }

    public static function getShippingMethods(){
        return HelpersDBRelation::dropdownListValuePair(Db::table('olmo_shippingmethod')->where('enabled_is_general', 'true')->get());
    }

    public static function sendEmailIfStatusisChanged($table, $orderstatus, $id)
    {
        
        if ($table == 'olmo_order') {

            $order = Db::table('olmo_order')->where('id', $id)->first();        

            $formtypeId = null;
            if($orderstatus == 'pending' AND $order->orderstatus_select_general == 'paid') {
                $formtypeId = '5';
            } else if ($orderstatus == 'paid' AND $order->orderstatus_select_general == 'shipped') {
                $formtypeId = '6';
            }
    
            $customer = Db::table('olmo_customer')->where('name_txt_general', $order->customer_read_general)->first();
    
            $params = [
                'a'      => $order->customer_read_general,
                'email'  => $order->customer_read_general,
                'domain' => env('APP_URL'),
                'locale' => $customer->currentlocale_read_general,
            ];
    
            $customer = HelpersSerializer::cleanKeyObj($customer);
            $order = HelpersSerializer::cleanKeyObj($order);

            $customerOrder = array_merge($order, $customer);
    
            $forms = Db::table('olmo_form')->get();
    
            foreach ($forms as $form) {
                $ids = explode(',', $form->formtype_multid_general);
                if (in_array($formtypeId, $ids)) {
                    $formid = $form->id;
                    $params['id'] = $formid;
                }
            }
    
            if($formtypeId){
                $send = FormController::sendingFormData(array_merge($customerOrder, $params), 'internal');
                return $send;
            }

            return 'ciao';

        }

        return 'off';

    }

    public static function removeUnwantedFields($order){
        $remove_fields = [    
            "billingname_read_billing",
            "billingsurname_read_billing",
            "billingphone_read_billing",
            "billingcompany_read_billing",
            "billingaddress_read_billing",
            "billingaddressnumber_read_billing",
            "billingregion_read_billing",
            "billingcity_read_billing",
            "billingstate_read_billing",
            "billingcountry_read_billing",
            "billingZIP_read_billing",
            "billingprovincia_read_billing",

            "shippingname_read_shipping",
            "shippingsurname_read_shipping",
            "shippingphone_read_shipping",
            "shippingcompany_read_shipping",
            "shippingaddress_read_shipping",
            "shippingaddressnumber_read_shipping",
            "shippingcity_read_shipping",
            "shippingstate_read_shipping",
            "shippingcountry_read_shipping",
            "shippingZIP_read_shipping",
            "shippingprovincia_read_shipping",
            
            "subtotal_read_general",
            "discountvalue_read_general",
            "totalitems_read_general",
            "currency_read_general",
            "shippingcost_read_general",
            "paymentmethod_txt_general",
            "shippingmethod_txt_general",
            "paypalid_txt_general",
            "courierlink_txt_shipping",
            "tracknumber_txt_shipping",
            "totalvat_read_general"
        ];

        foreach($remove_fields as $key){
            if(isset($order->$key)){
                unset($order->$key);
            }
        }

        return $order;
    }

    public static function backofficeOrder(Request $request, $lang, $order_id){
        $disabled = ['orderstatus_select_general', 'name_txt_general'];
        
        [$order, $customer] = Customer::getCustomerbyOrderId($order_id);
        $order = self::removeUnwantedFields($order);
        $order = BackController::serializePost($order, OrderModel::theAttributes(), $lang, $order_id);
        if($customer){
            $shippingMethods = [
                "name" => "shipping_method",
                "label" => "shipping method",
                "tab" => "cart",
                "type" => "select",
                "required" => "true",
                "value" => Order::getShippingMethod($order_id)
            ];
            $shippingMethods['values'] = HelpersOrder::getShippingMethods();
            
            $copyShippingToBilling = [
                "name" => "copy_shipping_to_billing",
                "label" => "Use shipping in billing",
                "tab" => "cart",
                "type" => "checkbox",
                "required" => "false",
                "value" => 'false',
                "values" => 'false'
            ];
            
            // address-id
            $billingAddresses = [
                "name" => "billing_address",
                "label" => "billing",
                "tab" => "cart",
                "type" => "select",
                "required" => "false",
                "value" => Order::getAddressesValueId($order_id, 'billing')
            ];
            $billingAddresses['values'] = Customer::getTypeAddresses($customer, 'billing');
    
            $shippingAddresses = [
                "name" => "shipping_address",
                "label" => "shipping",
                "tab" => "cart",
                "type" => "select",
                "required" => "true",
                "value" => Order::getAddressesValueId($order_id, 'shipping')
            ];
            $shippingAddresses['values'] = Customer::getTypeAddresses($customer, 'shipping');
            
            $paymentsMethods = [
                "name" => "payments_methods",
                "label" => "payment",
                "tab" => "cart",
                "type" => "select",
                "required" => "true",
                "value" => Order::getPaymentValueId($order_id)
            ];
            $paymentsMethods['values'] = Customer::getPaymentMethods($customer);
    
            $items = [
                "name" => "items_list",
                "label" => "items",
                "tab" => "cart",
                "type" => "items",
                "required" => "false",
                "value" => HelpersOrder::getCartItems($customer, $order_id, 2)
            ];

            $order = array_merge($order, [$shippingMethods], [$shippingAddresses], [$copyShippingToBilling], [$billingAddresses], [$paymentsMethods], [$items]);
        }else{
            foreach($order as $key => &$field){
                if($field['name'] == 'customer_read_general'){
                    if($field['value'] == ''){
                        $field['required'] == 'true';
                        $field['value'] = ['key' => '', 'value' => ''];
                        $field['type'] = 'select';
                        $field['values'] = HelpersDBRelation::dropdownListValuePair(HelpersAgent::customersList(HelpersUser::getUser()));
                    }
                }
                if($field['name'] == 'total_read_general'){
                    unset($order[$key]);
                }
            }
        }
        
        foreach($order as $key => &$value){
            if( in_array($value['name'], $disabled) ){
                $value['type'] = 'disabled_'.$value['type'];
            }
            if($value['name'] == "orderitems_json_items"){
                if($value['value'] == "[]"){
                    unset($order[$key]);
                }else{
                    $json_order = $value; // faccio cosí per ordinare le tabs con items alla fine
                    unset($order[$key]);
                    $order[] = $json_order;
                }
            }
        }

        return $order;
    }
    // TODO: cosa mi torna?
    public static function getCartItems($customer, $order_id, $active = 2){
        $cart_items = Db::raw(
            "SELECT pi.id, pi.name_txt_general FROM olmo_cartitem as ci
             RIGHT JOIN olmo_productitem as pi ON ci.item_id = pi.id
             WHERE ci.cart_id = (SELECT id FROM olmo_cart WHERE order_id = ? AND active = ? AND customer_id = ?)", [$order_id, $active, $customer->id]
        );

        return HelpersDBRelation::dropdownListValuePair($cart_items);
    }
}
