<?php

namespace Olmo\Core\App\Helpers;

use Olmo\Core\App\Helpers\HelpersSerializer;

class HelpersService
{  
    public static function endpointReply($request)
    {
        $model = $request->model;
        $lang = $request->lang;
        $id = $request->id;
        $fieldname = $request->field;

        
        $themodel = ('App\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model));
        $attribute = $themodel::theAttributes();
        
        $isEndpoint = isset($attribute['service-endpoint']) ? $attribute['service-endpoint'] : false;

        if($isEndpoint){
            if($attribute['lang']){
                $data = $themodel::where('locale_hidden_general', $lang)->where($fieldname, $id)->first();
            } else {
                $data = $themodel::where($fieldname, $id)->first();
            }
    
            return HelpersSerializer::routeResponse($data, 'request', 0);
        } else {
            return response('Sorry! This is not a valid endpoint', 409);
        }

    }
}