<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class HelpersPreview
{

    public static function deletePreview($request, $attribute)
    {
      $id = $request->id;
      $table = $attribute['table'];

      $getPreview = Db::table($table)->where('id', $id)->first();
      Storage::disk('local')->delete($getPreview->preview_hidden_general);

      $deletePreview = Db::table($table)->where('id', $id)->update(['preview_hidden_general' => '']);

      return $deletePreview;
    }


}