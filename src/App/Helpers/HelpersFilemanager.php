<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class HelpersFilemanager
{

    /**
     * The asset goes in to media folder but if it is among the assets-logic
     * definend in the upload method you will find it in the assets folder
     * by the way id the metod as string media as second param it goes straigth in to the media folder
     */
    public static function getSinglePathFmanagerById($id, $media = ''){

        $original_media = DB::table('olmo_storage')->where('id', $id)->first();

        if($original_media){

            $assets = DB::table('olmo_storage_versions as versions')
                ->where('id_original', $original_media->id)
                ->get();

            if($assets->count() != 0 AND $media != 'media'){

                $extension = pathinfo($original_media->filename)['extension'];
                $obj_default['src'] = '/'.$original_media->truename;

                $obj = [];
                $obj = self::getAssetsObject($assets, $extension);

                return $obj ?? $obj_default;

            } else {
                $mediapath = '/media/';
                return $mediapath.$original_media->truename;

            }

        } else {
            return '';
        }

    }

    public static function getOgImg(){
        $ogimg = Db::table('olmo_storage_versions')->where('dirname', 'LIKE', 'olmosetting/ogimg/ogimg.%')->get();

        if($ogimg){
            $obj = '';
            $obj = self::getAssetsObject($ogimg, 'jpg');
            return empty($obj) ? '':$obj;
        }
        
        return '';
    }

    public static function getSinglePathById($id, $media = ''){

        return self::getSinglePathFmanagerById($id, $media);

    }

    public static function checkFolderAndCreateIt($dirname) {

        if(!File::isDirectory($dirname)){
            File::makeDirectory($dirname, 0755, true, true);
        }

    }

    public static function getDirname($path){
        $dirname = dirname($path);
        return ($dirname != '.') ? $dirname.'/':'';
    }

    public static function getAssetsObject($assets, $extension){
        $asset_path = "/assets/";
        $obj = [];
        $add_alt = env('ALT_IMG', false);

        foreach($assets as $asset){
            $key = $asset->width."x".$asset->height;

            switch($extension){
                case 'mp4':
                    if($asset->is_original == 'true'){
                        $obj['original']['src'] = $asset_path.$asset->dirname;
                        $obj['original']['compressed'] = $asset_path.$asset->compression_path;
                        $obj['original']['width'] = $asset->width;
                        $obj['original']['height'] = $asset->height;
                    }else{
                        $obj[$key]['src'] = $asset_path.$asset->dirname;
                        $obj[$key]['compressed'] = $asset_path.$asset->compression_path;
                        $obj[$key]['width'] = $asset->width;
                        $obj[$key]['height'] = $asset->height;
                    }
                    break;
                case 'gif':
                    if($asset->is_original == 'true'){
                        $obj['original']['src'] = $asset_path.$asset->dirname;
                        // $obj['original']['optimized'] = $asset_path.$asset->optimized;
                        // $obj['original']['width'] = $asset->width;
                        // $obj['original']['height'] = $asset->height;
                    }else{
                        $obj[$key]['src'] = $asset_path.$asset->dirname;
                        // $obj[$key]['optimized'] = $asset_path.$asset->optimized;
                        // $obj[$key]['width'] = $asset->width;
                        // $obj[$key]['height'] = $asset->height;
                    }
                    break;
                default:
                    if($asset->is_original == 'true'){
                        $obj['original']['src'] = $asset_path.$asset->dirname;
                        $obj['original']['compressed'] = $asset_path.$asset->compression_path;
                        $obj['original']['width'] = $asset->width;
                        $obj['original']['height'] = $asset->height;
                        $key = 'original';
                    }else{
                        $obj[$key]['src'] = $asset_path.$asset->dirname;
                        $obj[$key]['compressed'] = $asset_path.$asset->compression_path;
                        $obj[$key]['width'] = $asset->width;
                        $obj[$key]['height'] = $asset->height;
                    }

                    if($add_alt){
                        $obj[$key]['alt'] = pathinfo($asset->dirname)['filename'];
                    }else{
                        $obj[$key]['alt'] = '';
                    }
                    break;
                }
            }

        return $obj;
    }

    public static function getSinglePathStorageById($id){

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
          ];

        $im = DB::table('olmo_storage')->where('id',$id)->first();
        if($im){
             return '/'.$categories_files[$im->type].'/'.$im->model.'/'.$im->truename;
        }else{
         return '';
        }
    }


}
