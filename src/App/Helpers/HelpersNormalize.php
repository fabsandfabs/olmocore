<?php

namespace Olmo\Core\App\Helpers;

class HelpersNormalize
{

    public static function cleanKeyArray($datas) 
    {
        $datas = json_decode(json_encode($datas), true);
        $array = [];
        foreach($datas as $items){
            $res = [];
            if(is_array($items) || is_object($items)){
                foreach($items as $k=>$v){                 
    
                    if($k == '_default' || $k == '_type'){
                        $key = $k;
                    }else{
                        $key  = @explode('_',$k)[0];
                    }
            
                    $res[$key] = $v;
                }
            }
            array_push($array,$res);
        }
        return $array;        
    }   

    public static function cleanKeyString($datas) 
    {
        $key  = @explode('_',$datas)[0];
        return $key;
    } 

    public static function normalizeObject($datas) 
    {
        $datas = json_decode(json_encode($datas), true);
        $res = [];
        foreach($datas as $k=>$v){

            $key  = @explode('_',$k)[0];
            
            $res[$key] = $v;
        }
        return $res;        
    }

}