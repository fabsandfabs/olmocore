<?php

namespace Olmo\Core\Containers\Backoffice\Property\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\Property\Models\Property;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersProperty;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Property::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Property::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Property::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Property::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    } 
    
    public function getPropertyValue(Request $request)
    {
        $attribute = Property::theAttributes();
        $attribute['table'] = 'olmo_propertyitem';
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }    

    public function createPropertyValue(Request $request)
    {
        $attribute = Property::theAttributes();
        $attribute['table'] = 'olmo_propertyitem';

        return HelpersProperty::createNewvalue($request, $attribute);
    }    

    public function updatePropertyValue(Request $request)
    {
    	$attribute = Property::theAttributes();
        $attribute['table'] = 'olmo_propertyitem';
        $attribute['requiredbackofficevalue'] = [
            'name_txt_general'
        ];            

        return HelpersProperty::serializeValueUpdating($request, $attribute);
    }      

}
