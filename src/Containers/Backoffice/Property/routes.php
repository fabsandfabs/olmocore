<?php

use Olmo\Core\Containers\Backoffice\Property\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/property', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the property post
Route::get('{lang}/property/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the property post
Route::put('{lang}/property/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the property post
Route::post('{lang}/property/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// Get list of value associated to a property
Route::get('{lang}/property/value/{id}', [Controller::class, 'getPropertyValue'])->middleware(['auth:api']);

// Get list of value associated to a property
Route::post('{lang}/property/value/create', [Controller::class, 'createPropertyValue'])->middleware(['auth:api']);

// Update the property post
Route::put('{lang}/property/value/{id}', [Controller::class, 'updatePropertyValue'])->middleware(['auth:api']);


// Route::get('{lang}/property/{id}', [Controller::class, 'findById']);

// Route::get('{lang}/property/{id}/value/{value_id}', [Controller::class, 'findByValue']);

// Route::put('{lang}/property/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

// Route::put('{lang}/{model}/{id_model}/value/{id_property}', [Controller::class, 'createUpdateValue']);

// Route::post('{lang}/property', [Controller::class, 'getListV1'])->middleware(['auth:api']);