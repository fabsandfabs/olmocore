<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersMigration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_property', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Locale($table);
            HelpersMigration::Default($table);
            $table->text('primary_filemanager_general')->nullable(false);
            $table->text('optional_is_general')->nullable(false);
            $table->text('title_editor_general')->nullable(false);
            $table->text('pivot_is_general')->nullable(false);
            $table->text('changeimage_is_general')->nullable(false);
            $table->text('property_property_general')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_property');
    }
}
