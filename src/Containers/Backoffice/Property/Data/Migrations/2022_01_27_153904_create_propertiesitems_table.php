<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreatePropertiesitemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_propertyitem', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Default($table);
            $table->text('locale_hidden_general')->nullable(false);
            $table->text('parentid_hidden_general')->nullable(false);
            $table->text('postid_hidden_general')->nullable(false);
            $table->text('code_txt_general')->nullable(false);
            $table->text('image_filemanager_general')->nullable(false);
            $table->text('optional_is_general')->nullable(false);
            $table->text('description_editor_general')->nullable(false);
            $table->text('color_color_general')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_propertyitem');
    }
}
