<?php

namespace App\Containers\Backoffice\General\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class GeneralRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
