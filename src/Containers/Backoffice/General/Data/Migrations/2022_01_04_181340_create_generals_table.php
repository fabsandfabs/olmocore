<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersMigration;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_language', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->string('enabled_is_general', 5)->nullable(false);
            $table->string('enabledfront_is_general', 5)->nullable(false);
            $table->string('code_txt_general', 10)->nullable(false);
            $table->string('name_txt_general', 255)->nullable(false);
            $table->string('displayname_txt_general', 255)->nullable(false);
            $table->string('default_is_general', 5)->nullable(false);
            $table->string('defaultfront_is_general', 5)->nullable(false);
            $table->string('menu_is_general', 5)->nullable(false);
            $table->smallInteger('position_ord_general')->nullable(false);
            HelpersMigration::Info($table);
        });
        
        Schema::create('olmo_analytics', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->text('gtmheader_txt_analytics')->nullable();
            $table->text('gtmbody_txt_analytics')->nullable();
            $table->text('gtmheaderprod_txt_analytics')->nullable();
            $table->text('gtmbodyprod_txt_analytics')->nullable();
            $table->text('robots_txt_analytics')->nullable();
            $table->text('robotsprod_txt_analytics')->nullable();
        });

        Schema::create('olmo_importexport', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id')->unsigned();         
            $table->text('store_id')->nullable(false);
            $table->text('creation_date')->nullable(false);
            $table->text('type')->nullable(false);
            $table->text('model')->nullable(false);
                        
        });      

        Schema::create('olmo_sitemapimages', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id')->unsigned();         
            $table->text('path')->nullable(false);
            $table->integer('active')->nullable(false);
            $table->text('pageurl')->nullable(false);
            $table->datetime('date')->nullable(false);
            $table->datetime('model')->nullable(false);
            $table->datetime('idpost')->nullable(false);
                        
        });      

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('generals');
    }
}
