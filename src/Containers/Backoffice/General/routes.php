<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\General\Controllers\Controller;
use Olmo\Core\Containers\Backoffice\General\Controllers\SitemapController;

// Items
Route::post('{lang}/{model}/item/create', [Controller::class, 'createItem'])->middleware(['auth:api']);
Route::get('{lang}/{model}/{model_id}/item/{id}', [Controller::class, 'getItem'])->middleware(['auth:api']);
Route::put('{lang}/{model}/{model_id}/item/{id}', [Controller::class, 'updateItem'])->middleware(['auth:api']);

// Get the list
Route::post('{lang}/language', [Controller::class, 'getPosts'])->middleware(['auth:api']);
Route::post('{lang}/language/setting', [Controller::class, 'getLangs'])->middleware(['auth:api']);

// Get the general post
Route::get('{lang}/language/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);
Route::get('{lang}/analytics', [Controller::class, 'getAnalytic'])->middleware(['auth:api']);

// Update the general post
Route::put('{lang}/language/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);
Route::put('{lang}/analytics', [Controller::class, 'updateAnalytic'])->middleware(['auth:api']);

// Create the general post
Route::post('{lang}/language/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// Get current column list for translate porpuse
Route::get('thetable/{table}', [Controller::class, 'theTable'])->middleware(['auth:api']);

// Copy the requested field in the new post
Route::post('{lang}/thetable/{table}/{id}', [Controller::class, 'copyColumns'])->middleware(['auth:api']);

// Sitemap images
Route::get('imagesitemap/{lang}', [SitemapController::class, 'getImagesitemap'])->middleware(['auth:api']);
Route::get('imagesitemaplinks/{lang}', [SitemapController::class, 'getImagesitemaplinks'])->middleware(['auth:api']);
Route::post('imagesitemapfile/{lang}', [SitemapController::class, 'getImagesitemapfile'])->middleware(['auth:api']);
Route::delete('{lang}/imagesitemapfile/{id}/{idpost}/{model}', [SitemapController::class, 'getImagesitemapfileDelete'])->middleware(['auth:api']);

//Importexpor
Route::get('importexport/import/list/{table}', [Controller::class, 'getImport'])->middleware(['auth:api']);
Route::get('importexport/export/{table}', [Controller::class, 'export'])->middleware(['auth:api']);
Route::get('importexport/export/list/{table}', [Controller::class, 'getExport'])->middleware(['auth:api']);
Route::post('importexport/uploadchunks/{table}', [Controller::class, 'uploadChunkimportexport'])->middleware(['auth:api']);
Route::post('{lang}/importexport/uploadcomplete/{table}', [Controller::class, 'completeChunkimportexport'])->middleware(['auth:api']);

// Duplicate post
Route::post('{lang}/duplicate/{table}/{id}', [Controller::class, 'duplicatePost'])->middleware(['auth:api']);
Route::post('{lang}/duplicate/{table}', [Controller::class, 'duplicatePosts'])->middleware(['auth:api']);

// Empty cache
Route::post('cache/frontend/clear/single-route', [Controller::class, 'cacheSingleRoute']);//->middleware(['auth:api']);
Route::get('cache/frontend/clear/data', [Controller::class, 'cacheClearData'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/strict', [Controller::class, 'cacheClearStrict'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/traslation', [Controller::class, 'cacheClearTraslation']);//->middleware(['auth:api']);
Route::get('cache/frontend/clear/route/{id}', [Controller::class, 'cacheClearRouteID'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/model/{id}', [Controller::class, 'cacheClearModelsName'])->middleware(['auth:api']);
Route::get('cache/frontend/clear', [Controller::class, 'cacheClearStructure'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/custom', [Controller::class, 'cacheClearCustom'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/forms', [Controller::class, 'cacheClearCustom'])->middleware(['auth:api']);
Route::get('cache/frontend/clear/filters', [Controller::class, 'cacheFilters'])->middleware(['auth:api']);

Route::get('cache/frontend/data/image/clear', [Controller::class, 'cacheImageClear'])->middleware(['auth:api']);
Route::get('cache/backend/data/model/clear/{model}', [Controller::class, 'cacheClearApiModel'])->middleware(['auth:api']);
Route::get('cache/backend/data/full/clear', [Controller::class, 'cacheClearApiFull'])->middleware(['auth:api']);

// Empty Nextjs
Route::post('cache/nextjs', [Controller::class, 'cacheNextjsClear'])->middleware(['auth:api']);

// Edit CSV
Route::post('csv/edit', [Controller::class, 'editCsv']);
// Edit PHP
Route::post('php/edit', [Controller::class, 'editPhp']);

// crawler for model
Route::post('olmo/crawler', [Controller::class, 'startCrawling']);
/**
 * Just for management porpuse
 */
// Route::get('_/containersBackoffice/list', [Controller::class, 'containersList'])->middleware(['auth:api']);
// Route::get('_/clearTable/toempty', [Controller::class, 'toEmpty'])->middleware(['auth:api']);
// Route::get('_/fixcolumvisual', [Controller::class, 'fixcolumn'])->middleware(['auth:api']);
