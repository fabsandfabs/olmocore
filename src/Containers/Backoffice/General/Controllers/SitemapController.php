<?php

namespace Olmo\Core\Containers\Backoffice\General\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersRoute;

class SitemapController extends BackController
{   

    public function getImagesitemap(Request $request)
    {
        $lang = $request->lang;

        $defaultlang = Helperslang::getDefaultLang();

        $dbTables = DB::select('SHOW TABLES');

        $array = [
            'migrations'
        ];

        $routes = [];
        foreach($dbTables as $item){
            foreach($item as $value){
                if(!in_array($value, $array) AND HelpersData::isDefaultModel($value) == false){
                    $model = str_replace('olmo_', '', $value);
                    $el = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model));
                    $attribute = $el::theAttributes();
                    if(isset($attribute['route'])){
                        if($attribute['route']){
                            $list = Db::table($value)->where('enabled_is_general', 'true')->where('locale_hidden_general', $lang)->select('id', 'template_id_general', 'locale_hidden_general', 'sitemapimage_sitemapimage_sitemap', 'slug_txt_general')->get();                            
                            $serializedRoute = [];
                            foreach($list as $route){
                                $routearray = json_decode(json_encode($route, true), true);
                                $listRelation = HelpersSerializer::routeResponse($route);
                                $listRelation['slug'] = HelpersRoute::backofficeUrl($attribute, $routearray, $defaultlang, env('FRONT_URL'), env('FRONT_URL_LANG', true));
                                $listRelation['model'] = $model;
                                $listRelation['idsitemapimage'] = '';
                                if($listRelation['sitemapimage'] != ''){
                                    $post = Db::table('olmo_sitemapimages')->where('pageurl', $listRelation['slug'])->first();
                                    if($post){
                                        $listRelation['idsitemapimage'] = $post->id;
                                    }
                                }
                                array_push($serializedRoute, $listRelation);
                            }
                            array_push($routes, $serializedRoute);
                        }
                    }
                }
            }
        }

        return $routes;
    }

    public function getImagesitemapfileDelete(Request $request)
    {
        $id = $request->id;
        $idpost = $request->idpost;
        $model = $request->model;
        $data = DB::table('olmo_sitemapimages')->where('id', $id)->first();
        $storage = Storage::disk('local')->delete($data->path);

        if($storage){
            DB::table('olmo_sitemapimages')->where('id', $id)->delete();
            DB::table('olmo_'.$model)->where('id', $idpost)->update(['sitemapimage_sitemapimage_sitemap' => '']);
        }

        return $this->getImagesitemap($request);
    }

    public function getImagesitemaplinks(Request $request)
    {

        $langs = HelpersLang::validLangFront();
        $lang = $request->lang;        

        if (in_array($lang, $langs)) {

            $page = file_get_contents(env('APP_URL') . '/api/sitemap/' . $lang);
            $sitemap = json_decode($page, true);
    
            $sitemapArray = [];
    
            foreach ($sitemap as $item) {
                if ($item['type'] == 'page') {
                    if ($item['slug'] == '') {
                        array_push($sitemapArray, $item['baseurl'] . '/' . $item['lang'] . '/');
                    } else {
                        array_push($sitemapArray, $item['baseurl'] . '/' . $item['lang'] . '/' . $item['slug'] . '/');
                    }
                } else {
                    array_push($sitemapArray, $item['baseurl'] . '/' . $item['lang'] . '/' . $item['slug'] . '/');
                }
            }
            return [
                "links" => $sitemapArray,
                "counter" => count($sitemapArray)
            ];

        }

    }

    public function getImagesitemapfile(Request $request)
    {
        $langs = HelpersLang::validLangFront();
        $lang = $request->lang;     
        $url = $request->input('url');
        $model = $request->input('model');
        $idpost = $request->input('idpost');

        if (in_array($lang, $langs)) {
            $urls = self::getpageurl($url);

            $urlpath = str_replace(env('FRONT_URL')."/", "", $urls['page']);
            $path = str_replace("/", "-", $urlpath);
            $dbpath = "public/documents/sitemap/".$path.".json";

            $data = [
                "active" => true,
                "path" => $dbpath,
                "pageurl" => $urls['page'],
                "date" => Carbon::now(),
                "lang" => $lang,
                "model" => $model,
                "idpost" => $idpost
            ];

            $check = DB::table('olmo_sitemapimages')->where('path', $dbpath)->first();
            if($check){
                $db = DB::table('olmo_sitemapimages')->where('path', $dbpath)->update($data);
                $storage = Storage::disk('local')->put($dbpath, json_encode($urls));
            } else {
                $db = DB::table('olmo_sitemapimages')->insertGetId($data);
                $storage = Storage::disk('local')->put($dbpath, json_encode($urls));
            }

            DB::table('olmo_'.$model)->where('id', $idpost)->update(['sitemapimage_sitemapimage_sitemap' => $dbpath]);

            return [
                "db" => $db,
                "storage" => $storage,
                "content" => $urls,
                "date" => Carbon::now(),
                "links" => $this->getImagesitemap($request)
            ];
        }

        return abort(404);
    }

    public static function getpageurl($url)
    {
        $env = env('FILESYSTEM_DRIVER') == 's3' ? env('AWS_DOMAIN') . '/public/assets' : env('APP_URL') . "/storage";

        $doc = new \DOMDocument;
        $doc->preserveWhiteSpace = false;
        $doc->strictErrorChecking = false;
        $doc->recover = true;

        ini_set('user_agent', 'Olmo-imagesitemap');
        libxml_use_internal_errors(true);
        $doc->loadHTMLFile($url);
        $xpath = new \DOMXPath($doc);
        $images = $xpath->query('//img');

        $imgtags = [];
        foreach ($images as $image) {
            $img = $image->getAttribute('src');
            if(str_contains($img, $env)){
                array_push($imgtags, $img);
            }
        }

        return [
            'page' => $url,
            'url' => $imgtags
        ];
    }
}
