<?php

namespace Olmo\Core\Containers\Backoffice\General\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\General\Models\General;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersImportExport;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersItem;

use Olmo\Core\App\Crawlers\MyCrawlObserver;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use Olmo\Core\App\Helpers\HelpersRoute;

class Controller extends BackController
{

    public function updatePost(Request $request)
    {
        $attribute = General::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }

    public function getPost(Request $request)
    {
        $attribute = General::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return  $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = General::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function getLangs(Request $request)
    {
        $attribute = General::theAttributes();
        $langs = Db::table($attribute['table'])->get();
        $response = HelpersSerializer::cleanKeyArray($langs);

        return  $response;
    }

    public function updateAnalytic(Request $request)
    {
        $attribute  =  [
            'table' => 'olmo_analytics',
            'required' => []
        ];
        $dataPost = [
            'id' => 1,
            'lang' => $request->lang
        ];
        return $this->serializePostUpdating($request, $attribute);
    }


    public function getAnalytic(Request $request)
    {
        $attribute  =  [
            'table' => 'olmo_analytics',
            'required' => []
        ];
        $dataPost = [
            'id' => 1,
            'lang' => $request->lang
        ];

        return  $this->serializeSinglePost($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = General::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function theTable(Request $request)
    {
        return HelpersExtra::theTable($request);
    }

    public function copyColumns(Request $request)
    {
        return HelpersExtra::copyColumnsFromDefault($request);
    }

    public function duplicatePost(Request $request)
    {
        return HelpersExtra::duplicatePost($request);
    }

    public function duplicatePosts(Request $request)
    {
        /**
         * Recuperate il modello attraverso la table
         * determinare se il modello è default e eseguire la richiesta del modello
         * estrapolare gli attributi e controllare gli elementi unici
         * */
        return HelpersExtra::duplicatePosts($request);
    }

    public function cacheNextjsClear(Request $request)
    {
        $lang = $request->input('lang');
        $model = $request->input('model');
        $path = $request->input('path');
        $tag = $request->input('tag');
        
        $data = [
            "lang" => $lang,
            "model" => $model,
            "path" => $path,
            "tag" => $tag
        ];

        $response = Http::post(env('FRONT_URL')."/api/cache", $data);
        return response([$response->body()], $response->getStatusCode());
    }

    public function cacheClearData(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-data/?forceclear');
    }

    public function cacheClearStrict(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-strict/?forceclear');
    }

    public function cacheClearTraslation(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-translations/?forceclear');
    }

    public function cacheClearRouteID(Request $request)
    {
        $routeId = $request->id;
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-routes/'.$routeId.'?forceclear');
    }

    public function cacheClearModelsName(Request $request)
    {
        $routeId = $request->id;
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-models/'.$routeId.'?forceclear');
    }

    public function cacheClearStructure(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-structure/?forceclear');
    }

    public function cacheClearCustom(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-custom/?forceclear');
    }

    public function cacheClearForms(Request $request)
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-forms/?forceclear');
    }

    public function cacheFilters(Request $request)
    {        
        return Cache::tags(['filters'])->flush();
    }

    public function cacheSingleRoute(Request $request)
    {        
        $requestUrl = env('FRONT_URL') . '/_/hooks/cache/clear-route/?forceclear';
        $array = [
            "locale" => $request->input('locale'),
            "path" => $request->input('path')
        ];
        $response = Http::post($requestUrl, $array);
        return response($response, 200);
    }

    public function cacheClearApiModel(Request $request)
    {
        // $model = $request->model;
        //TODO:
        /**
         * This is wrong we supposed to do a cache archive with tags to delete them with
         * group action and not by singular event or full cache deletion
         * Laravel 10 in combination with swayok/alternative-laravel-cache and league/flysystem-aws-s3-v3
         * does not support league/flysystem version so wait till the package is updated
         */
        // Cache::tags([$model])->flush();
        Cache::flush();

        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-data/?forceclear');
    }

    public function cacheClearApiFull()
    {
        return Cache::flush();
    }

    public function cacheImageClear()
    {
        return file_get_contents(env('FRONT_URL') . '/_/hooks/cache/clear-img/?forceclear');
    }

    public function export(Request $request)
    {
        $table = $request->table;
        return HelpersImportExport::export($table, 'csv');
    }

    public function getImport(Request $request)
    {
        $i = 0;
        $res = [];
        $model = $request->table;
        $import = Db::table('olmo_importexport')->where('model', $model)->where('type', 'import')->orderBy('creation_date', 'desc')->get();
        foreach ($import as $imp) {
            $res[$i]['date'] = $imp->creation_date;
            $store                    = Db::table('olmo_storage')->where('id', $imp->store_id)->first();
            $truename                 = $store->truename;
            $folder                   = $store->model;
            $res[$i]['public']        = $store->public;
            $res[$i]['path']          = '/documents/import/' . $folder . '/' . $truename;

            $i++;
        }

        return $res;
    }

    public function getExport(Request $request)
    {
        $i      = 0;
        $res    = [];
        $model  = $request->table;
        $import = Db::table('olmo_importexport')->where('model', $model)->where('type', 'export')->orderBy('creation_date', 'desc')->get();

        foreach ($import as $imp) {
            $res[$i]['date'] = $imp->creation_date;
            $store = Db::table('olmo_storage')->where('id', $imp->store_id)->first();
            $truename = $store->truename;
            $folder = $store->model;
            $res[$i]['public'] = $store->public;
            $res[$i]['path'] = '/documents/export/' . $folder . '/' . $truename;

            $i++;
        }

        return $res;
    }

    public function uploadChunkimportexport(Request $request)
    {

        $chunk_id     = @$_REQUEST['id'];
        $filename     = @$_REQUEST['fileName'];
        $data         = file_get_contents('php://input');
        $temp_data    = Cache::get($filename);
        $current_data = $temp_data . $data;
        Cache::put($filename, $current_data, 1200);

        $arr['data']         = null;
        $arr['isSuccess']    = true;
        $arr['errorMessage'] = null;

        return $arr;
    }


    public function completeChunkimportexport(Request $request)
    {

        $chunk_id = @$request->id;

        $filename = @$_REQUEST['fileName'];
        $truename = @$_REQUEST['trueName'];
        $fieldname = @$_REQUEST['fieldName'];
        $model = $request->table;
        $lang = $request->lang;
        $defaultlang = HelpersLang::getDefaultLang();
        $current_data = Cache::get($filename);

        $elementArrayWalk = array('lang' => $lang, 'defaultlang' => $defaultlang);

        $pagename = $model;
        $type = pathinfo($truename, PATHINFO_EXTENSION);

        $categories_files = [
            'csv'  => 'documents'
        ];

        $date = (string)Str::uuid();
        $path = 'public/' . $categories_files[$type] . '/import/' . $pagename . '/' . $filename;
        $res = Storage::put($path, $current_data);

        $physical_path = '/' . $categories_files[$type] . '/import/' . $pagename . '/' . $filename;

        if ($res) {
            $res = true;
        } else {
            $res = false;
        }

        $storage_id = DB::table('olmo_storage')->insertGetId([
            'filename'  => $filename,
            'truename'  => $model . '_import_' . $date . '.csv',
            'model'     => $pagename,
            'meta'      => $chunk_id . $fieldname,
            'public'    => 'true',
            'type'      => $type
        ]);
        Cache::forget($filename);


        //Importazione
        //Storelocator::truncate();
        $err         = null;
        $themodel = ('App\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model));
        $attribute   = $themodel::theAttributes();
        $mapping     = $attribute['csvstructure']['mapping'];
        $separator   = $attribute['csvstructure']['separator'];
        $idcsv       = $attribute['csvstructure']['id'];
        $row = 1;

        if (($handle = fopen(storage_path('app/' . $path), "r")) !== FALSE) {

            Db::table('olmo_importexport')->insert([
                'store_id' => $storage_id,
                'creation_date' => date("Y-m-d H:i:s"),
                'type' => 'import',
                'model' => $model
            ]);

            while (($data = fgetcsv($handle, 1000, $separator)) !== FALSE) {

                /**
                 *  Declare the unique value in the csv to check if it already exist in the database
                 *  so fieldname and the value
                 * */
                $fieldname = $mapping[$idcsv];
                $uniqueValue = $data[$idcsv];
                $record = $themodel::where($fieldname, $uniqueValue)->first();

                /** Get the current columns path of the table to use in the else condition */
                $columns = Schema::getColumnListing($attribute['table']);
                /** Flip values in the keys */
                $columns_flip = array_flip($columns);

                /** If it exist update it, if not create a new one */
                if ($record) {
                    foreach ($mapping as $k => $v) {
                        if ($record->{$v} != @$data[$k]) {
                            $record->{$v} = @$data[$k];
                        }
                    }
                    $record->save();
                } else {
                    $record = new $themodel;

                    /** Set every value to empty string with exceptions */
                    array_walk($columns_flip, function (&$v, $k, $elementArrayWalk) {
                        if (HelpersData::getType($k) == 'number') {
                            $v = 0;
                        } else if (HelpersData::getType($k) == 'readonly') {
                            $v = Carbon::now();
                        } else if ($k == 'locale_hidden_general') {
                            $v = $elementArrayWalk['lang'];
                        } else if ($k == 'parentid_hidden_general') {
                            $v = $elementArrayWalk['defaultlang'] == $elementArrayWalk['lang'] ? '' : 'number';
                        } else {
                            $v = '';
                        }
                    }, $elementArrayWalk);

                    unset($columns_flip['id']);

                    foreach ($mapping as $key => $value) {
                        $columns_flip[$value] = @$data[$key];
                    }

                    Db::table($attribute['table'])->insertGetId($columns_flip);
                }
            }
            fclose($handle);
        } else {
            $error = "File notfound";
            return response($error, 400);
        }

        $response['data']         = null;
        $response['isSuccess']    = true;
        $response['errorMessage'] = [];

        return response($response, 200);
    }

    public function getItem(Request $request)
    {   
        $id = $request->id;
        $modelid = $request->model_id;
        $model = $request->model;
        $lang = $request->lang;
        //TODO 
        // check this because looks like usless
        $data = Db::table('olmo_' . $model . 'item')->where('id', $id)->where('postid_hidden_content', $modelid)->first();
        if(!$data){
            $data = Db::table('olmo_' . $model . 'item')->where('id', $id)->first();
        }
        $modelpost = Db::table('olmo_' . $model)->where('id', $modelid)->first();

        if($request->model == 'customer'){
            $modelattribute = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($request->model).'\\Models\\'.ucfirst($request->model));
        } else {
            $modelattribute = ('App\Containers\Backoffice\\'.ucfirst($request->model).'\\Models\\'.ucfirst($request->model));
        }
        $attribute = $modelattribute::theAttributes();
        $attribute['table'] = 'olmo_'.$model.'item';

        $data = json_decode(json_encode($data, true), true);

        $product = [];

        $product['items'] = $this->serializePost($data, $attribute, $lang, $id, $modelid);
        $product['pagename'] = $model;
        $product['namepost'] = $modelpost->name_txt_general;
        $product['nameblock'] = $data['name_txt_general'];
        $product['create'] = true;

        return $product;
    }

    public function createItem(Request $request)
    {
        return false;
        if($request->model == 'customer'){
            $model = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($request->model).'\\Models\\'.ucfirst($request->model));
        } else {
            $model = ('App\Containers\Backoffice\\'.ucfirst($request->model).'\\Models\\'.ucfirst($request->model));
        }
        $attribute = $model::theAttributes();
        $attribute['table'] = 'olmo_'.$request->model.'item';
        $attribute['tablerelation'] = 'olmo_'.$request->model;

        return HelpersItem::createNewvalue($request, $attribute);
    }

    public function updateItem(Request $request)
    {
        $id = $request->id;
        $modelid = $request->model_id;
        $model = $request->model;
        $lang = $request->lang;

        $body = HelpersData::normalizeBody($request);

        if($request->model == 'customer'){
            $modelattribute = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($request->table).'\\Models\\'.ucfirst($request->table));
        } else {
            $modelattribute = ('App\Containers\Backoffice\\'.ucfirst($request->model).'\\Models\\'.ucfirst($request->model));
        }
    	$attribute = $modelattribute::theAttributes();
        $attribute['table'] = 'olmo_' . $model . 'item';
        $attribute['requiredbackofficevalue'] = [
            'name_txt_general'
        ];

        Db::table('olmo_' . $model . 'item')->where('id', $id)->update($body);

        if(isset($body['qty_spin_general'])){
            $updateQuantity = Db::table('olmo_quantity')->where('prod_id', $id)->first();
            if($updateQuantity){
                Db::table('olmo_quantity')->where('prod_id', $id)->update(['quantity' => (int)$body['qty_spin_general']]);
            } else {
                //TODO: Bisogna trovare un modo per rendere 'type' => 'item' dinamico perché la quantità potrebbe essere di un prodotto e non di un item
                Db::table('olmo_quantity')->insertGetId([
                    'quantity' => (int)$body['qty_spin_general'],
                    'prod_id' => $id,
                    'type' => 'item',
                ]);
            }
        }

        $data = Db::table($attribute['table'])->where('id', $id)->first();
        $modelpost = Db::table('olmo_' . $model)->where('id', $modelid)->first();
        $data = json_decode(json_encode($data, true), true);

        $product['items'] = $this->serializePost($data, $attribute, $lang, $id);
        $product['pagename'] = $model.'item';
        $product['namepost'] = $modelpost->name_txt_general;
        $product['nameblock'] = $data['name_txt_general'];

        return response($product, 200);
    }

    public static function outputCSV($data)
    {
        $fp = fopen('php://temp', 'r+');
        $enclosure = '"';
        $contents = "";

        foreach ($data as $fields) {
            fputcsv($fp, $fields, ',', $enclosure);
        }

        rewind($fp);
        while (!feof($fp)) {
             $contents .= fread($fp, 8192);
        }

        fclose($fp);

        return $contents;
    }

    public static function convertJsonToCSV($jsonFile, $newpath)
    {
        $jsonString = $jsonFile['data'];
        $current_data = self::outputCSV($jsonString);

        $fileexist = Storage::disk('local')->exists($newpath);
        if($fileexist){
            Storage::disk('local')->delete($newpath);
        }

        return Storage::disk('local')->put($newpath, $current_data);
    }

    public function editCsv(Request $request)
    {

        $data = $request->input('data');
        $id = $request->input('id');

        $storage = Db::table('olmo_storage')->where('id', $id)->first();
        $path = $storage->truename;
        $filename = $storage->filename;

        $jsonFile = json_decode($data, true);
        $csvFile = 'file.csv';

        $arrayFile = explode('/', $path);
        $arrayFile[count(explode('/', $path)) - 1] = $filename;
        $newpath = 'public/media/'.implode('/', $arrayFile);

        return [
            'storage' => self::convertJsonToCSV($jsonFile, $newpath),
            'path' => $path,
            'pathnoname' => explode('/', $path),
            'count' => count(explode('/', $path)),
            'filename' => $filename,
            'newpath' => $newpath,
        ];
    }

    public function editPhp(Request $request)
    {

        $data = $request->input('php');
        $id = $request->input('id');

        $storage = Db::table('olmo_storage')->where('id', $id)->first();
        $path = $storage->truename;
        $filename = $storage->filename;

        $arrayFile = explode('/', $path);
        $arrayFile[count(explode('/', $path)) - 1] = $filename;
        $filePath = storage_path('app/public/media/'.implode('/', $arrayFile));

        $file_handle = fopen($filePath, 'w');
        fwrite($file_handle, $data);
        fclose($file_handle);

        return response([], 200);
    }

    /* check if the model passed exists, than return the model name and his attribute schema */
    public static function getModel($database, $columnFind, $modelFind){
        try {
            $models = DB::select("SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ? AND TABLE_SCHEMA = ?;", [$columnFind, $database]);

            foreach($models as $key=>$model){
                $name = str_replace("olmo_", "", $model->TABLE_NAME);
                if($name == $modelFind){
                    $attribute = ('App\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name) )::theAttributes();
                    return [$model->TABLE_NAME, $attribute];
                }
            }
        } catch (\Exception $e) {
            Log::error('Database query error: '.$e->getMessage());
            return response()->json(['error' => 'An error occurred while retrieving data'], 500);        
        }
        return false;
    }

    public static function getUrls($tablename, $attribute, $column, $lang, $apiUrl){
        $all_routes = json_decode(json_encode(Db::table($tablename)->where($column, "!=", "")->where('locale_hidden_general', $lang)->get()), true);

        $allUrls = [];
        foreach($all_routes as $route){
            $allUrls[] = HelpersRoute::backofficeUrl($attribute, $route, $lang, $apiUrl, env('FRONT_URL_LANG', true));
        }
        return $allUrls;
    }
    
    public function startCrawling(Request $request){

        // creo il file temporaneo
        $path = storage_path('app/public/media');
        $path = $path."temp-crawler-file.csv"; // Non ho usato php:://temp ma si potrebbe valutare
        $fp = fopen($path, 'w');
        fclose($fp);

        $lang_list = HelpersLang::validLang();
        $lang = $request->input('lang', false);
        $model = $request->input('model', false);
        $column = "slug_txt_general";
        $database = env("DB_DATABASE", false);
        $apiUrl = env("APP_URL", false)."/api";

        if($lang == false){
            $response = "Lang missing";
        }else if(!in_array($lang, $lang_list)){
            $response = "Lang is invalid";
            $lang = false;
        }

        if($model == false){
            $response = "Model missing";
        }

        if($lang && $model){
            [$tablename, $attribute] = self::getModel($database, $column, $model);
            if($tablename && $attribute){
                $allUrls = self::getUrls($tablename, $attribute, $column, $lang, $apiUrl);
            }else{
                return response("Error with retrieved data!", 200);
            }   
    
            foreach($allUrls as $url){
                // [RequestOptions::ALLOW_REDIRECTS => true, RequestOptions::TIMEOUT => 30]]
                Crawler::create()
                // ->acceptNofollowLinks()
                // ->ignoreRobots()
                ->setParseableMimeTypes(['application/json'])
                ->setCrawlObserver(new MyCrawlObserver())
                ->setMaximumDepth(0)
                ->setConcurrency(1) // all urls will be crawled one by one
                ->setDelayBetweenRequests(1000)
                ->startCrawling($url);
            }
    
            $fp = fopen($path, 'r');
    
            $response = [];
            while (false != ($line = fgetcsv($fp))) {
                $columns = [];
                foreach($line as $key => $column){
                    $keyValue = explode('+', $column);
                    $columns[$keyValue[0]] = $keyValue[1];
                }
                $response[] = $columns;
            }
            fclose($fp);
            unlink($path);
        }
        
        if(is_string($response)){
            return response($response, 200);
        }else{
            return response()->json($response, 200);
        }
        
    }
}
