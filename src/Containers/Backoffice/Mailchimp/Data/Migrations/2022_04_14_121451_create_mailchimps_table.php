<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateMailchimpsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_mailchimp', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->text('enabled_is_general')->nullable(false);
            $table->text('name_txt_general')->nullable(false);
            $table->text('apikey_txt_general')->nullable(false);
            $table->text('server_txt_general')->nullable(false);
            $table->text('doubleoptin_is_general')->nullable(false);
            $table->text('fieldname_txt_general')->nullable(false);
            $table->text('fieldvalue_txt_general')->nullable(false);
            $table->text('mailchimplistid_fields_general')->nullable(false);
            $table->text('mailchimpfield_fields_general')->nullable(false);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_mailchimp');
    }
}
