<?php

use Olmo\Core\Containers\Backoffice\Mailchimp\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list mailchimp
Route::post('{lang}/mailchimp', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the mailchimp post
Route::get('{lang}/mailchimp/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the mailchimp post
Route::put('{lang}/mailchimp/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the mailchimp post
Route::post('{lang}/mailchimp/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

Route::fallback(function () {
    return view("404");
});