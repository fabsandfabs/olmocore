<?php

namespace Olmo\Core\Containers\Backoffice\Form\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FormSeeder extends Seeder
{
	public function run()
	{

		DB::table('olmo_formsmtp')->truncate();
		DB::table('olmo_formauth')->truncate();
		DB::table('olmo_formcsv')->truncate();

		DB::table('olmo_formauth')->insert([
			'token_txtdis_general' => (string)Str::uuid(),
			'ipaddressforserver_txtarea_general' => '',
			'ipuseraddressblacklist_txtarea_general' => '',
			'domainaddressforclient_txtarea_general' => ''
		]);
		
		DB::table('olmo_formsmtp')->insert([
			'displayname_txt_general' => '',
			'smtp_txt_general' => '',
			'email_txt_general' => '',
			'username_txt_general' => '',
			'password_pwd_general' => '',
			'port_txt_general' => '',
			'tls_select_general' => '',
			'auth_select_general' => ''
		]);		

		DB::table('olmo_formcsv')->insert([
			'fields_csvfield_general' => ''
		]);

	}
}