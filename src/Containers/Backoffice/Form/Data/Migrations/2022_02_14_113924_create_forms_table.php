<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersMigration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('olmo_form', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('enabled_is_general')->nullable(false);
            $table->text('name_txt_general')->nullable(false);
            $table->text('formtype_multid_general')->nullable(false);
            $table->text('mailchimp_multid_general')->nullable(false);
            $table->text('smtp_id_general')->nullable(false);
            $table->text('a_txt_setting')->nullable(false);
            $table->text('cc_txt_setting')->nullable(false);
            $table->text('bcc_txt_setting')->nullable(false);
            $table->text('replyto_txt_setting')->nullable(false);
            $table->text('subject_txt_setting')->nullable(false);
            $table->text('subjecttemplate_filemanager_setting')->nullable(false);
            $table->text('subjectautoresponse_txt_setting')->nullable(false);
            $table->text('subjectautoresponsetemplate_filemanager_setting')->nullable(false);
            $table->text('strings_filemanager_setting')->nullable(false);            
            $table->text('formfield_formfield_fields')->nullable(false);
            $table->text('csvfield_csvfield_csv')->nullable(false);
            $table->text('trash_hidden_general')->nullable();
            HelpersMigration::Info($table);
        });

        Schema::create('olmo_formfield', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('element_select_general')->nullable(false);
            $table->text('typology_select_general')->nullable(false);
            $table->text('name_txt_general')->nullable(false);
            $table->text('idfield_txt_general')->nullable(false);
            $table->text('label_txt_general')->nullable(false);
            $table->text('default_txt_general')->nullable(false);
            $table->text('placeholder_txt_general')->nullable(false);
            $table->text('class_txt_general')->nullable(false);
            $table->text('attribute_txt_general')->nullable(false);
            $table->text('required_is_general')->nullable(false);
            $table->text('hidden_is_general')->nullable(false);
            $table->text('dependentfield_select_general')->nullable(false);
            $table->text('dependentaction_select_general')->nullable(false);
            $table->text('dependenttrigger_txt_general')->nullable(false);
            $table->text('option_optionfield_general')->nullable(false);
            $table->text('filesize_txt_general')->nullable(false);
            $table->text('postid_hidden_general')->nullable(false);
        });      

        Schema::create('olmo_formfieldoptionstype', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('formfieldoptionstype_select_general')->nullable(false);
            $table->text('fieldid_hidden_general')->nullable(false);   
        });

        Schema::create('olmo_formfieldoptions', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('value')->nullable(false);
            $table->text('key')->nullable(false); 
            $table->text('fieldid_hidden_general')->nullable(false);           
        });

        Schema::create('olmo_formauth', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('token_txtdis_general')->nullable(false);
            $table->text('ipaddressforserver_txtarea_general')->nullable(false);
            $table->text('ipuseraddressblacklist_txtarea_general')->nullable(false);
            $table->text('domainaddressforclient_txtarea_general')->nullable(false);
        });

        Schema::create('olmo_formsmtp', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('displayname_txt_general')->nullable(false);
            $table->text('smtp_txt_general')->nullable(false);
            $table->text('email_txt_general')->nullable(false);
            $table->text('username_txt_general')->nullable(false);
            $table->text('password_pwd_general')->nullable(false);
            $table->text('port_txt_general')->nullable(false);
            $table->text('tls_select_general')->nullable(false);
            $table->text('auth_select_general')->nullable(false);
        });

        Schema::create('olmo_formcsv', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('fields_csvfield_general')->nullable(false);
        });        

        Schema::create('olmo_formcsvfield', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('name')->nullable(false);
            $table->text('fieldid_hidden_general')->nullable(false);           
        });  
        
        Schema::create('olmo_formlog', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('name')->nullable();
            $table->text('date')->nullable();
            $table->text('status')->nullable();
        });        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_form');
        Schema::dropIfExists('olmo_formfield');
        Schema::dropIfExists('olmo_formfieldoptions');
    }
}
