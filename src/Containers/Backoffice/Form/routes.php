<?php

use Olmo\Core\Containers\Backoffice\Form\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/form', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the form post
Route::get('{lang}/form/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the form post
Route::put('{lang}/form/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the form post
Route::post('{lang}/form/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// Create the field
Route::post('form/field/create', [Controller::class, 'createFormfield'])->middleware(['auth:api']);

// Get the field
Route::get('form/field/{id}', [Controller::class, 'getFormfield'])->middleware(['auth:api']);

// Update the field
Route::put('form/field/{id}', [Controller::class, 'updateFormfield'])->middleware(['auth:api']);

// Get the field typology by element
Route::post('form/field/typology', [Controller::class, 'getFormfieldTypology'])->middleware(['auth:api']);

// Create the field options
Route::post('form/field/options/create', [Controller::class, 'createFormfieldOptions'])->middleware(['auth:api']);

// Get the field options
Route::get('form/field/options/{id}', [Controller::class, 'getFormfieldOptions'])->middleware(['auth:api']);

// Update the field options
Route::put('form/field/options/{id}', [Controller::class, 'updateFormfieldOptions'])->middleware(['auth:api']);

// Delete the field options
Route::delete('form/field/options/{id}', [Controller::class, 'deleteFormfieldOptions'])->middleware(['auth:api']);

// Delete the field options
Route::post('form/field/options/type/{id}', [Controller::class, 'updateFormfieldOptionsType'])->middleware(['auth:api']);

// Put the csv field
Route::post('{lang}/form/csv/field/{id}', [Controller::class, 'createCsvField'])->middleware(['auth:api']);

// delete the csv field
Route::delete('{lang}/form/csv/field/{id}', [Controller::class, 'deleteCsvField'])->middleware(['auth:api']);

// Update the csv field
Route::put('{lang}/form/csv/field/{id}', [Controller::class, 'updateCsvField'])->middleware(['auth:api']);

// Get the csv id from form Id
Route::get('{lang}/formid/csvid/{id}', [Controller::class, 'getCsvId'])->middleware(['auth:api']);

// Get form setting option
Route::get('form/setting', [Controller::class, 'getFormSetting'])->middleware(['auth:api']);

// Get form smtp setting option
Route::get('form/setting/smtp', [Controller::class, 'getFormSettingSmtp'])->middleware(['auth:api']);

// Get form smtp setting option
Route::put('form/setting/smtp', [Controller::class, 'updateFormSettingSmtp'])->middleware(['auth:api']);

// Get form auth setting option
Route::get('form/setting/auth', [Controller::class, 'getFormSettingAuth'])->middleware(['auth:api']);

// Get form auth setting option
Route::put('form/setting/auth', [Controller::class, 'updateFormSettingAuth'])->middleware(['auth:api']);

// get setting csv
Route::get('form/setting/csv', [Controller::class, 'getSettingCsv'])->middleware(['auth:api']);

// Create setting csv
Route::post('form/setting/csv/create', [Controller::class, 'createSettingCsvField'])->middleware(['auth:api']);

// Update setting csv
Route::put('form/setting/csv', [Controller::class, 'updateSettingCsv'])->middleware(['auth:api']);

// Update setting csv field
Route::put('form/setting/csv/{id}', [Controller::class, 'updateSettingCsvField'])->middleware(['auth:api']);

// Delete setting csv field
Route::delete('form/setting/csv/{id}', [Controller::class, 'deleteSettingCsvField'])->middleware(['auth:api']);