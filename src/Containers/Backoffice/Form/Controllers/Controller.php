<?php

namespace Olmo\Core\Containers\Backoffice\Form\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Olmo\Core\Containers\Backoffice\Form\Models\Form;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersCollection;
use Olmo\Core\App\Helpers\HelpersForm;
use Olmo\Core\App\Helpers\HelpersCsv;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Form::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Form::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Form::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Form::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }

    public function getCsvId(Request $request)
    {
        $id = $request->id;
        $getCsvId = Db::table('olmo_storage')->where('truename', 'olmoemail/csv/form-' . $id . '.csv')->first();
        if($getCsvId){
            return response($getCsvId->id, 200);
        }
        return response(400);
    }

    public function createFormfield(Request $request)
    {
        $table = 'olmo_formfield';
        $id = $request->input('id');

        $columns = Schema::getColumnListing($table);

        $columns_flip = array_flip($columns);

        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function (&$v, $k, $id) {
            if ($k == 'postid_hidden_general') {
                $v = $id;
            } else {
                $v = '';
            }
        }, $id);

        unset($columns_flip['id']);     

        $response = Db::table($table)->insertGetId($columns_flip);
        $row = Db::table('olmo_form')->where('id', $id)->first();

        $rowField = $row->formfield_formfield_fields == "" ? $response : $row->formfield_formfield_fields.','.$response;
        Db::table('olmo_form')->where('id', $id)->update(['formfield_formfield_fields' => $rowField]);        

        if($row){
            return response(200);
        } 

        return response(403);
    }   
    
    public function getFormfield(Request $request)
    {
        $attribute['table'] = 'olmo_formfield';
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function updateFormfield(Request $request)
    {
    	$attribute['table'] = 'olmo_formfield';
    	$attribute['lang'] = false;

        return $this->serializePostUpdating($request, $attribute);
    }    

    public function getFormfieldTypology(Request $request)
    {
        $element = $request->input('element');

        return HelpersCollection::get('typology'. $element);
    }   
    
    public function createFormfieldOptions(Request $request)
    {
        $id = $request->input('id');

        $columns = array(
            'key' => '',
            'value' => '',
            'fieldid_hidden_general' => $id
        );

        $option = Db::table('olmo_formfieldoptions')->insertGetId($columns);
        $options = Db::table('olmo_formfieldoptions')->where('fieldid_hidden_general', $id)->select('value', 'key', 'id')->get();

        $optionFieldForm = Db::table('olmo_formfield')->where('id', $id)->first();
        if($optionFieldForm){
            $optionFieldFormValue = $optionFieldForm->option_optionfield_general == '' ? $option : $optionFieldForm->option_optionfield_general.','.$option;
            Db::table('olmo_formfield')->where('id', $id)->update(['option_optionfield_general' => $optionFieldFormValue]);
        }

        return $options;
    }

    public function getFormfieldOptions(Request $request)
    {
        $id = $request->id;
        $options = Db::table('olmo_formfieldoptions')->where('fieldid_hidden_general', $id)->select('value', 'key', 'id')->get();
        $optionType = Db::table('olmo_formfieldoptionstype')->where('fieldid_hidden_general', $id)->first();

        $type = array('key' => '', 'value' => '');
        $typelist = HelpersCollection::get('formfieldoptionstype');        

        if($optionType){
            $type = self::getValue('formfieldoptionstype_select_general', $optionType->formfieldoptionstype_select_general, $typelist, 'select', 'olmo_formfieldoptionstype', $id);
        } else {
            $columns = array('formfieldoptionstype_select_general' => '', 'fieldid_hidden_general' => $id);
            Db::table('olmo_formfieldoptionstype')->insertGetId($columns);
        }

        $optionsSetup = array(
            'id' => $id,
            'type' => $type,
            'typelist' => $typelist,
            'items' => $options
        );

        return $optionsSetup;
    }

    public function deleteFormfieldOptions(Request $request) 
    {
        $id = $request->id;
        $fieldid = $request->input('fieldid');
        Db::table('olmo_formfieldoptions')->where('id', $id)->delete();
        $options = Db::table('olmo_formfieldoptions')->where('fieldid_hidden_general', $fieldid)->select('value', 'key', 'id')->get();

        $optionFieldFormValue = Db::table('olmo_formfield')->where('id', $fieldid)->first();
        $arrayValues = explode(',', $optionFieldFormValue->option_optionfield_general);
        if (($key = array_search($id, $arrayValues)) !== false) {
            unset($arrayValues[$key]);
        }
        $newOptionValue = implode(',', $arrayValues);
        Db::table('olmo_formfield')->where('id', $fieldid)->update(['option_optionfield_general' => $newOptionValue]);

        if($options){
            return response($options, 200);
        }

        return response(404);
    }

    public function updateFormfieldOptions(Request $request)
    {
        $id = $request->id;
        $inputKey = $request->input('inputKey');
        $inputValue = $request->input('inputValue');
        $options = Db::table('olmo_formfieldoptions')->where('id', $id)->update(['value' => $inputValue, 'key' => $inputKey]);

        if($options){
            return response(200);
        }

        return response(404);        
    }

    public function updateFormfieldOptionsType(Request $request) 
    {
        $id = $request->id;
        $type = $request->input('type');
        $options = Db::table('olmo_formfieldoptionstype')->where('fieldid_hidden_general', $id)->update(['formfieldoptionstype_select_general' => $type]);

        if($options){
            return response(200);
        }

        return response(404);
    }

    public function createCsvField(Request $request)
    {
        return HelpersCsv::createCsvField($request);
    }

    public function deleteCsvField(Request $request)
    {
        return HelpersCsv::deleteCsvField($request);
    }

    public function updateCsvField(Request $request)
    {
        return HelpersCsv::updateCsvField($request);
    }

    public function getFormSetting()
    {
        return HelpersForm::retrieveSetting();
    }

    public function getFormSettingSmtp()
    {
        $attribute['table'] = 'olmo_formsmtp';
        $dataPost = array('id' => '1', 'lang' => 'en');

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function updateFormSettingSmtp(Request $request)
    {
        $attribute['table'] = 'olmo_formsmtp';
        $attribute['required'] = [];
        $attribute['route'] = false;

        return $this->serializePostUpdating($request, $attribute);
    }

    public function getFormSettingAuth()
    {
        $attribute['table'] = 'olmo_formauth';
        $dataPost = array('id' => '1', 'lang' => 'en');

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function updateFormSettingAuth(Request $request)
    {
        $attribute['table'] = 'olmo_formauth';
        $attribute['required'] = [];
        $attribute['route'] = false;

        return $this->serializePostUpdating($request, $attribute);
    }

    public function getSettingCsv()
    {
        $csvfield = Db::table('olmo_formcsv')->where('id', '1')->first();

        $ids = explode(',', $csvfield->fields_csvfield_general);
        $array = [];
        foreach($ids as $id){
            $csvfields = Db::table('olmo_formcsvfield')->where('id', $id)->select('name', 'id')->first();
            if($csvfields){
                array_push($array, $csvfields);
            }
        }

        $csv = array('fields' => $array, 'csv' => $csvfield);

        return response($csv, 200);
    }

    public function createSettingCsvField()
    {
        $addcsvfield = Db::table('olmo_formcsvfield')->insertGetid(['name' => '', 'fieldid_hidden_general' => 'setting']);
        // $csvfields = Db::table('olmo_formcsvfield')->select('name', 'id')->get();
        $getcsvfields = Db::table('olmo_formcsv')->where('id', '1')->first();

        $newIds = '';

        if ($getcsvfields->fields_csvfield_general == '') {
            $newIds = (string)$addcsvfield;
        } else {
            $newIds = $getcsvfields->fields_csvfield_general . ',' . (string)$addcsvfield;
        }        
        
        Db::table('olmo_formcsv')->where('id', '1')->update(['fields_csvfield_general' => $newIds]);        

        return $this->getSettingCsv();
    }

    public function updateSettingCsv(Request $request)
    {
        $value = $request->input('name');

        Db::table('olmo_formcsv')->where('id', '1')->update(['fields_csvfield_general' => $value]);

        return $this->getSettingCsv();
    }

    public function updateSettingCsvField(Request $request)
    {
        $id = $request->id;
        $name = $request->input('name');

        Db::table('olmo_formcsvfield')->where('id', $id)->update(['name' => $name]);

        return $this->getSettingCsv();
    }

    public function deleteSettingCsvField(Request $request)
    {
        $id = $request->id;

        Db::table('olmo_formcsvfield')->where('id', $id)->delete();

        return $this->getSettingCsv();
    }

    public function addMailchimpRecord(Request $request)
    {
        $attribute = Form::theAttributes();
        $attribute['table'] = 'olmo_formmailchimp';

        return $this->createNewPost($request, $attribute);        
    }

    public function getMailchimpList(Request $request)
    {
        $attribute['table'] = 'olmo_formmailchimp';
        $attribute['required'] = [];
        $attribute['route'] = false;
        $attribute['listing'] = 'name_txt_general, apikey_txt_general, id';
        
        $dataPost['id'] = 1;
        $dataPost['lang'] = '';
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function getMailchimpPost(Request $request)
    {
        $attribute = Form::theAttributes();
        $attribute['table'] = 'olmo_formmailchimp';
        $attribute['required'] = [];
        $attribute['route'] = false;        

        $dataPost = HelpersData::idLangPost($request);
        $dataPost['lang'] = '';

        return  $this->serializeSinglePost($attribute, $dataPost);    
    }

    public function getSettingMailchimp()
    {

        $attribute['table'] = 'olmo_formmailchimp';
        $dataPost = array('id' => '1', 'lang' => 'en');

        return $this->serializeSinglePost($attribute, $dataPost);
    }       
    
    public function updateSettingMailchimp(Request $request)
    {
        $attribute['table'] = 'olmo_formmailchimp';
        $attribute['required'] = [];
        $attribute['route'] = false;

        return $this->serializePostUpdating($request, $attribute);        
    }   

    public function getSettingMailchimpField(Request $request)
    {

        $idfield = $request->id;

        $attribute['table'] = 'olmo_formmailchimp';

        $mailchimpfield = Db::table($attribute['table'])->where('id', $idfield)->first();

        $array = [];
        if($mailchimpfield){
            $ids = explode(',', $mailchimpfield->mailchimpfield_fields_general);
            foreach($ids as $id){
                $mailchimpfields = Db::table($attribute['table'].'field')->where('id', $id)->first();
                if($mailchimpfields){
                    $mailchimpfields->thisisemail = $mailchimpfields->thisisemail == '0' ? false : true;
                    $mailchimpfields->thisistag = $mailchimpfields->thisistag == '0' ? false : true;
                    array_push($array, $mailchimpfields);
                }
            }
        }

        return $array;
    }

    public function updateSettingMailchimpField(Request $request)
    {
        $attribute['table'] = 'olmo_formmailchimpfield';

        $id = $request->id;
        $name = $request->input('value');
        $field = $request->input('field');
        $email = $request->input('email');
        $tag = $request->input('tag');   

        Db::table($attribute['table'])->where('id', $id)->update([
            'name' => $name ? $name : "",
            'namemailchimp' => $field ? $field : "",
            'thisisemail' => $email,
            'thisistag'   => $tag,
        ]);

        return $this->getSettingMailchimpField($request);
    }    

    public function createSettingMailchimpField(Request $request)
    {

        $idpost = $request->id;

        $attribute['table'] = 'olmo_formmailchimp';

        $addmailchimpfield = Db::table($attribute['table'].'field')->insertGetid([
            'name' => '', 
            'namemailchimp' => '',
            'thisisemail' => false,
            'thisistag' => false
        ]);

        $getmailchimpfields = Db::table($attribute['table'])->where('id', $idpost)->first();

        $newIds = '';

        if ($getmailchimpfields->mailchimpfield_fields_general == '') {
            $newIds = (string)$addmailchimpfield;
        } else {
            $newIds = $getmailchimpfields->mailchimpfield_fields_general . ',' . (string)$addmailchimpfield;
        }
        
        Db::table($attribute['table'])->where('id', $idpost)->update(['mailchimpfield_fields_general' => $newIds]);

        // $request->id = intval($getmailchimpfields);

        return $this->getSettingMailchimpField($request);
    }     

    public function deleteSettingMailchimpField(Request $request)
    {
        $id = $request->id;
        $attribute['table'] = 'olmo_formmailchimp';

        Db::table($attribute['table'].'field')->where('id', $id)->delete();

        return $this->getSettingMailchimpField($request);
    }    

}
