<?php

namespace Olmo\Core\Containers\Backoffice\Form\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Form extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Form';

    public $table = 'olmo_form';
    
    public static function theAttributes(){
        return [
            // Those two keys are mandatory
            'table' => 'olmo_form',
            'required' => [],
            'route' => false,
            'duplicate' => true,
            'cache' => false,

            // Use this to setup columns in the listing query
            'listing' => 'name_txt_general, enabled_is_general, a_txt_setting, cc_txt_setting, id',

            // Use this setup for multilanguage posts
            'lang'  => false,

            // Use this setup for posts with endpoint
            'requiredbackoffice' => [],

        ];
    }    
}
