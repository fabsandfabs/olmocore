<?php

namespace Olmo\Core\Containers\Backoffice\FieldRepeater\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\FieldRepeater\Models\FieldRepeater;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersFieldsRepeater;

class Controller extends BackController
{

    public function getPost(Request $request)
    {
        $attribute = FieldRepeater::theAttributes();

        HelpersExtra::managerExtra($request, $attribute);

        return HelpersFieldsRepeater::serializeSingleSlide($request, $attribute);
    }

    public function createPost(Request $request)
    {
        $attribute = FieldRepeater::theAttributes();

        return HelpersFieldsRepeater::createNewField($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = FieldRepeater::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return HelpersFieldsRepeater::serializeSlideUpdating($request, $attribute, $dataPost);
    }   

}
