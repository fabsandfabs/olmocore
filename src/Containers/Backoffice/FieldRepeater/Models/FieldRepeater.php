<?php

namespace Olmo\Core\Containers\Backoffice\FieldRepeater\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class FieldRepeater extends OlmoMainModel
{

    public $timestamps = false;
    
    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'FieldRepeater';

    public $table = 'olmo_fieldrepeateritem';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_fieldrepeateritem',
            'required' => [],
            'route' => false
        ];
    }

}