<?php

use Olmo\Core\Containers\Backoffice\FieldRepeater\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
// Route::post('{lang}/slider', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the page post
Route::get('{lang}/fieldrepeater/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the slider post
Route::put('{lang}/fieldrepeater/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the slider post
Route::post('{lang}/fieldrepeater/create', [Controller::class, 'createPost'])->middleware(['auth:api']);