<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateFieldRepeatersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {        
        HelpersMigration::FieldRepeater();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_fieldrepeateritem');
    }
}
