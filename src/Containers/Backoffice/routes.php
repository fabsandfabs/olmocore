<?php

// Backoffice routes import
require "General/routes.php";
require "Slider/routes.php";
require "Filemanager/routes.php";
require "Template/routes.php";
require "User/routes.php";
require "Property/routes.php";
require "Menu/routes.php";
require "VisualBlock/routes.php";
require "Fillform/routes.php";
require "Form/routes.php";
require "Formtype/routes.php";
require "Customer/routes.php";
require "Order/routes.php";
require "Dotsposition/routes.php";
require "FieldRepeater/routes.php";
require "Smtp/routes.php";
require "Mailchimp/routes.php";