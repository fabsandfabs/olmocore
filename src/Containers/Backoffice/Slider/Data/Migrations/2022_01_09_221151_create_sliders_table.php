<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {        
        HelpersMigration::Slider();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_slideritem');
    }
}
