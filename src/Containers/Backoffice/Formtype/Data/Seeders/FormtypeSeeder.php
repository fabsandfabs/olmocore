<?php

namespace Olmo\Core\Containers\Backoffice\Formtype\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FormtypeSeeder extends Seeder
{
	public function run()
	{

		DB::table('olmo_formtype')->truncate();

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'login',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'register',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'resetpassword',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'neworder',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'orderconfirmed',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'ordershipped',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'orderrefunded',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer creation',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer not active',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer disabled',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer blocked',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer password recovery',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer password changed',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'customer asstet',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'contact form',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);		

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'smtp',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);	

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'newsletter',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);		

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'mailchimp',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);		

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'csv',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'csv-global',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);		

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'salesforce',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'sendinblue',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);	
		
		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'postmark',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'billing',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'shipping',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'profile',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			
		
		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'requestasset',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'orderplaced',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'shippingcost',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'smtp-multiconf',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'step',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'final-step',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

		DB::table('olmo_formtype')->insert([
			'name_txt_general' => 'brevo',
			'enabled_is_general' => 'true',
			'createdby_read_information' => 'seeder',
			'lastmodby_read_information' => Carbon::now()
		]);			

	}
}
