<?php

use Olmo\Core\Containers\Backoffice\Formtype\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/formtype', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the formtype post
Route::get('{lang}/formtype/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the formtype post
Route::put('{lang}/formtype/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the formtype post
Route::post('{lang}/formtype/create', [Controller::class, 'createPost'])->middleware(['auth:api']);