<?php

namespace Olmo\Core\Containers\Backoffice\Formtype\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Formtype extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Form';

    public $table = 'olmo_formtype';
    
    public static function theAttributes(){
        return [
            // Those two keys are mandatory
            'table' => 'olmo_formtype',
            'required' => [],
            'route' => false,
            'cache' => false,

            // Use this to setup columns in the listing query
            'listing' => 'name_txt_general, id',

            // Use this setup for multilanguage posts
            'lang'  => false,

        ];
    }    
}
