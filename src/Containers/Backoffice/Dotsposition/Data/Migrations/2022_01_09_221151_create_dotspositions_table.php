<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateDotspositionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {        
        HelpersMigration::Dotdecimal();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_dotsposition');
    }
}
