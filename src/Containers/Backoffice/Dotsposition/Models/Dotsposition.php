<?php

namespace Olmo\Core\Containers\Backoffice\Dotsposition\Models;

use Olmo\Core\App\Models\OlmoMainModel;

// use Illuminate\Support\Facades\DB;

class Dotsposition extends OlmoMainModel
{

    public $timestamps = false;
    
    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Dotsposition';

    public $table = 'olmo_dotsposition';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_dotsposition',
            'required' => [],
            'route' => false,
        ];
    }

}
