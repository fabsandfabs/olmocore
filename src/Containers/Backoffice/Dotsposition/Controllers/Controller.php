<?php

namespace Olmo\Core\Containers\Backoffice\Dotsposition\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\Dotsposition\Models\Dotsposition;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersDotsPosition;

class Controller extends BackController
{

    public function getPost(Request $request)
    {
        $attribute = Dotsposition::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return HelpersDotsPosition::serializeSingleDotsPosition($request, $attribute);
    }

    public function deletePost(Request $request)
    {
        $attribute = Dotsposition::theAttributes();
        
        return HelpersDotsPosition::deleteDot($attribute, $request);
    }

    public function createPost(Request $request)
    {
        $attribute = Dotsposition::theAttributes();

        return HelpersDotsPosition::createNewDots($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Dotsposition::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return HelpersDotsPosition::serializeDotspostionUpdating($request, $attribute, $dataPost);
    }   

}
