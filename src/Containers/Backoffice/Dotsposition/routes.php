<?php

use Olmo\Core\Containers\Backoffice\Dotsposition\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
// Route::post('{lang}/dotposition', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the dotposition post
Route::get('{lang}/dotsposition/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the dotposition post
Route::put('{lang}/dotsposition/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the dotposition post
Route::post('{lang}/dotsposition/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

Route::delete('{lang}/dotsposition/delete', [Controller::class, 'deletePost'])->middleware(['auth:api']);