<?php

namespace App\Containers\Backoffice\Smtp\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class SmtpRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
