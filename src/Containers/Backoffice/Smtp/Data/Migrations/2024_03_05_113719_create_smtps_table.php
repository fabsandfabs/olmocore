<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateSmtpsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_smtp', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Default($table); //Please, use this migration in case dalete HelpersMigration::Page($table);
            HelpersMigration::Locale($table); //Please, use this migration in case dalete HelpersMigration::Page($table) and want to add multilang-post;
            // Content
            $table->text('displayname_txt_general')->nullable(false);
            $table->text('smtp_txt_general')->nullable(false);
            $table->text('email_txt_general')->nullable(false);
            $table->text('username_txt_general')->nullable(false);
            $table->text('password_pwd_general')->nullable(false);            
            $table->text('port_txt_general')->nullable(false);
            $table->text('tls_select_general')->nullable(false);
            $table->text('auth_select_general')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_smtp');
    }
}
