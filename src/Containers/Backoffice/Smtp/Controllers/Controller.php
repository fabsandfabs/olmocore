<?php

namespace Olmo\Core\Containers\Backoffice\Smtp\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\Smtp\Models\Smtp;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersPreview;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Smtp::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Smtp::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        $attribute['listing'] = 'name_txt_general, enabled_is_general, id';
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Smtp::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Smtp::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }

    public function previewPost(Request $request)
    {
        $attribute = Smtp::theAttributes();
        $preview = true;

        return $this->serializePostUpdating($request, $attribute, $preview);
    }      

    public function previewPostDelete(Request $request)
    {
        $attribute = Smtp::theAttributes();
        $dataPost = HelpersData::idLangPost($request);  
        HelpersPreview::deletePreview($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }  

}
