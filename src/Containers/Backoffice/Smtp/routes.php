<?php

use Olmo\Core\Containers\Backoffice\Smtp\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/smtp', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the smtp post
Route::get('{lang}/smtp/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the smtp post
Route::put('{lang}/smtp/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Save preview the product post
Route::put('{lang}/smtp/{id}/preview', [Controller::class, 'previewPost'])->middleware(['auth:api']);

// Delete preview the product post
Route::post('{lang}/smtp/{id}/preview', [Controller::class, 'previewPostDelete'])->middleware(['auth:api']);

// Create the smtp post
Route::post('{lang}/smtp/create', [Controller::class, 'createPost'])->middleware(['auth:api']);