<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersMigration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_template', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->string('enabled_is_general', 5)->nullable(false);
            $table->text('name_txt_general')->nullable(false);
            $table->text('slug_txt_slugs')->nullable(false);
            $table->text('model_select_general')->nullable(false);
            $table->text('structure_id_general')->nullable(false);
            // Sitemap
            $table->string('activate_is_sitemap', 5)->nullable(false);
            $table->text('frequency_select_sitemap')->nullable(false);
            $table->text('priority_select_sitemap')->nullable(false);
            $table->text('structureddata_txtarea_structured')->nullable(false);
            $table->text('trash_hidden_general')->nullable();
            HelpersMigration::Info($table);
        });

        Schema::create('olmo_templateslug', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->text('slug', 5)->nullable(false);
            $table->text('lang')->nullable(false);
            $table->text('defaultid')->nullable(false);
        });        

        // Schema::create('olmo_model', function (Blueprint $table) {
        //     // Create new table...
        //     $table->charset = 'utf8mb4';
        //     $table->collation = 'utf8mb4_unicode_ci';
        //     // General                
        //     $table->increments('id')->unsigned();
        //     $table->text('name_txt_general')->nullable();

        //     $table->timestamps();
        
        // });        

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('generals');
    }
}
