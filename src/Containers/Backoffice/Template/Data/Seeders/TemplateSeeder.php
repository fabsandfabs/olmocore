<?php

namespace Olmo\Core\Containers\Backoffice\Template\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TemplateSeeder extends Seeder
{
    public function run()
    {

        // ...
        DB::table('olmo_template')->truncate();
       
		DB::table('olmo_template')->insert([
			'enabled_is_general' => 'true',
			'name_txt_general' => 'home',
			'slug_txt_slugs' => '',
			'frequency_select_sitemap' => '',
			'priority_select_sitemap' => '',
			'trash_hidden_general' => '',
			'structure_id_general' => '',
			'structureddata_txtarea_structured' => '',
			'model_select_general' => 'page',
			'activate_is_sitemap' => 'false',
			'createdby_read_information' => 'super@super.com',
			'lastmodby_read_information' => 'super@super.com'
		]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general' => 'product'
		// ]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general' => 'category'
		// ]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general' => 'blog'
		// ]);

        // DB::table('olmo_model')->insert([
		// 	'name_txt_general' => 'casehistory'
		// ]);

        // DB::table('olmo_model')->insert([
		// 	'name_txt_general' => 'service'
		// ]);         

    }
}
