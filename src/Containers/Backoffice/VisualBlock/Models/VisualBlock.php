<?php

namespace Olmo\Core\Containers\Backoffice\VisualBlock\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class VisualBlock extends OlmoMainModel
{

    public $timestamps = false;
    
    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Visuals';

    public $table = 'olmo_visualblock';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_visualblock', 
            'required' => [],
            'route' => false,
            'lang' => false,
        ];
    }    
}
