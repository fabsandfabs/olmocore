<?php

namespace Olmo\Core\Containers\Backoffice\VisualBlock\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersData;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\VisualBlock\Models\VisualBlock;

class Controller extends BackController
{

    private const CONTAINERS_DIRECTORY_NAME = 'app';

    private const CONTAINER_SECTION = 'Settings';

    public function getComponentVisual()
    {
        $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR . "VisualBlock");
        $directory = File::isDirectory($menuDirectory);

        if ($directory) {

            $visualPath = $menuDirectory . '/' . 'visualblock.json';
            $visualRaw = json_decode(file_get_contents($visualPath), true);

            return response($visualRaw, 200);
        }

        return response(['No directory provide'], 400);
    }

    public function addVisual(Request $request)
    {

        $model       = $request->model;
        $table       = 'olmo_' . $model;
        $id           = $request->id;
        $lang           = $request->lang;
        $model_field = $request->model_field;
        $blocks = $request->input('blocks');

        $columns = Schema::getColumnListing('olmo_visualblock');

        $columns_flip = array_flip($columns);

        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function (&$v, $k, $blocks) {
            if ($k == 'name') {
                $v = $blocks;
            } else {
                $v = '';
            }
        }, $blocks);

        unset($columns_flip['id']);

        if (isset($columns_flip['model_hidden_content'])) {
            $columns_flip['model_hidden_content'] = $model;
        }
        if (isset($columns_flip['postid_hidden_content'])) {
            $columns_flip['postid_hidden_content'] = $id;
        }
        if (isset($columns_flip['locale_hidden_content'])) {
            $columns_flip['locale_hidden_content'] = $lang;
        }

        $response = Db::table('olmo_visualblock')->insertGetId($columns_flip);
        $row = Db::table($table)->where('id', $id)->first();

        $rowBlock = $row->$model_field == "" ? $response : $row->$model_field . ',' . $response;
        $block = Db::table($table)->where('id', $id)->update([$model_field => $rowBlock]);

        if ($block) {
            return response(200);
        }

        return response(403);
    }

    public function saveVisual(Request $request)
    {
        $id           = $request->id;

        $body = HelpersData::normalizeBody($request);

        // foreach ($blocks as $key => $value) {

        //     $type = @explode('_', $key)[1];

        //     if (is_null($value)) {
        //         $blocks[$key] = '';
        //     }

        //     if ($type === 'filemanager' && $value) {
        //         $image = Db::table('olmo_storage')->where('truename', $value)->first();
        //         if ($image) {
        //             $blocks[$key] = (string)$image->id;
        //         }
        //     } else if($type === 'filemanager'){

        //     }
        // }

        $response = Db::table('olmo_visualblock')->where('id', $id)->update($body);

        if ($response) {
            return response(200);
        }

        return response(403);
    }

    public function getVisual(Request $request)
    {
        $id = $request->id;
        $modelid = $request->model_id;
        $model = $request->model;
        $lang = $request->lang;
        $data = Db::table('olmo_visualblock')->where('id', $id)->where('postid_hidden_content', $modelid)->first();
        if(!$data){
            $data = Db::table('olmo_visualblock')->where('id', $id)->first();
        }
        $modelpost = Db::table('olmo_' . $model)->where('id', $modelid)->first();
        $attribute = VisualBlock::theAttributes();

        $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR . "VisualBlock");
        $visualPath = $menuDirectory . '/' . 'visualblock.json';
        $visualRaw = json_decode(file_get_contents($visualPath), true);

        $data = json_decode(json_encode($data, true), true);

        foreach ($data as $k => $v) {
            if ($k != 'id' && $k != 'name' && $k != 'created_at' && $k != 'updated_at') {
                foreach ($visualRaw as $field) {
                    if ($data['name'] == $field['name']) {
                        if (!in_array(explode('_', $k)[0], $field['blocks'])) {
                            unset($data[$k]);
                        }
                    }
                }
            }
        }

        $visual = [];

        $visual['items'] = $this->serializePost($data, $attribute, $lang, $id);
        $visual['pagename'] = $model;
        $visual['namepost'] = $modelpost->name_txt_general;
        $visual['nameblock'] = $data['name'];

        return $visual;
    }
}