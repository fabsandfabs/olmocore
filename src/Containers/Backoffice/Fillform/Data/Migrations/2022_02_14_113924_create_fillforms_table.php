<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersMigration;

class CreateFillformsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('olmo_fillform', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->text('code_txt_general')->nullable(false);
            $table->text('name_txt_general')->nullable(false);
            $table->text('formtype_select_general')->nullable(false);
            HelpersMigration::Info($table);
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_fillform');
    }
}
