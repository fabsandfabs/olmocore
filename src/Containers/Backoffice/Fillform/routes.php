<?php

use Olmo\Core\Containers\Backoffice\Fillform\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/fillform', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the fillform post
Route::get('{lang}/fillform/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the fillform post
Route::put('{lang}/fillform/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the fillform post
Route::post('{lang}/fillform/create', [Controller::class, 'createPost'])->middleware(['auth:api']);