<?php

namespace Olmo\Core\Containers\Backoffice\Order\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\Order\Models\Order;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersAgent as Agent;
use Olmo\Core\App\Helpers\HelpersUser as User;
use Olmo\Core\App\Helpers\HelpersOrder;
use Olmo\Ecommerce\Containers\Ecommerce\Order\Controllers\Controller as Ecommerce;
use Olmo\Ecommerce\Containers\Ecommerce\Cart\Controllers\Controller as Cart;
// use Olmo\Core\Containers\Backoffice\Order\Controllers\Controller as Order;
use Olmo\Core\Containers\Backoffice\Order\Models\Order as OrderModel;
// use Olmo
class Controller extends BackController
{ 

    public function getPost(Request $request, $lang, $id)
    {
        $token = User::checkToken();
        // $isAgent = Agent::isAgent($token, true);
        $attribute = Order::theAttributes();
        $dataPost = HelpersData::idLangPost($request);
        
        //TODO: 
        /**
         * la questione degli agenti è completametne da rivedere
         * per come è stata pensata se sei admin o super admin caschi sempre nella prima condizione,
         * che non va assolutamente bene perchè ricostruendo l'array non vengono considerati diversi campi 
         * è inoltre molti vengono esposti come disabled, quindi mettiti l'anima in pace fabs e rifallo.
         */
        // if($isAgent){            
        //     $order = HelpersOrder::backofficeOrder($request, $lang, $id);
        //     return $this->serializeSingleOrder($attribute, $dataPost, $order);
        // } else {
            HelpersExtra::managerExtra($request, $attribute);
            return $this->serializeSinglePost($attribute, $dataPost);
        // }
    }

    public function getPosts(Request $request)
    {
        $attribute = Order::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        return Ecommerce::createNewOrder($request);
    }   
    
    public function orderAgentComplete(Request $request){
        return Ecommerce::orderCompleted($request);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Order::theAttributes();
        return $this->serializePostUpdating($request, $attribute, false, true);
    }       

    public function getItemsList(Request $request){
        return Ecommerce::allProduct($request);
    }

    public function getVariantList(Request $request, $lang, $id){
        return Ecommerce::allVariant($id);
    }

    public function getCart(Request $request){
        return Ecommerce::getCartByRoute($request);
    }

    public function createCartAgentItem(Request $request){
        return Ecommerce::createCartItem($request);
    }

    public function UpdateProductAgentCart(Request $request){
        return Ecommerce::UpdateProductAgentCart($request);
    }

    public function DeleteProductFromAgentCart(Request $request){
        return Ecommerce::DeleteProductFromAgentCart($request);
    }

    public function sendEmail(Request $request){
        return Ecommerce::orderConfirmEmail($request);
    }

}
