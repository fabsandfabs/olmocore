<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // HelpersMigration::Cart();
        Schema::create('olmo_order', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            // ------------------------
            HelpersMigration::Default($table);
            // ------------------------
            $table->text('customerid_hidden_customer')->nullable(false);
            $table->integer('emailsent_hidden_general')->default(0);
            $table->text('code_read_general')->nullable(false);
            $table->text('customer_read_general')->nullable(false);
            $table->dateTime('createdonutc_read_general')->nullable(false);
            $table->text('orderstatus_select_general')->nullable(false);
            // ------------------------
            $table->dateTime('paidonutc_read_general')->nullable(false);
            $table->text('paymentmethod_txt_general')->nullable(false);
            $table->text('shippingmethod_txt_general')->nullable(false); 
            // ------------------------
            $table->text('discount_read_general')->nullable(false);
            $table->text('totalitems_read_general')->nullable(false); 
            $table->text('currency_read_general')->nullable(false); // da gestire il valore
            $table->unsignedDecimal('subtotal_read_general', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('discountvalue_read_general', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('total_read_general', $precision = 12, $scale = 2)->nullable();
            $table->unsignedDecimal('totalvat_read_general', $precision = 12, $scale = 2)->nullable();
            $table->text('paypalid_txt_general')->nullable(false);
            // ------------------------
            $table->text('billingname_read_billing')->nullable(false);
            $table->text('billingsurname_read_billing')->nullable(false);
            $table->text('billingphone_read_billing')->nullable(false);
            $table->text('billingcompany_read_billing')->nullable(false);
            $table->text('billingaddress1_read_billing')->nullable(false);
            $table->text('billingaddress2_read_billing')->nullable(false);
            $table->text('billingcity_read_billing')->nullable(false);
            $table->text('billingstate_read_billing')->nullable(false);
            $table->text('billingprovincia_read_billing')->nullable(false);
            $table->text('billingcountry_read_billing')->nullable(false);
            $table->text('billingZIP_read_billing')->nullable(false);
            $table->text('billingaddressnumber_read_billing')->nullable(false);
            $table->text('billingregion_read_billing')->nullable(false);
            // ------------------------
            $table->text('shippingcost_read_shipping')->nullable(false);
            $table->text('shippingname_read_shipping')->nullable(false);
            $table->text('shippingsurname_read_shipping')->nullable(false);
            $table->text('shippingphone_read_shipping')->nullable(false);
            $table->text('shippingcompany_read_shipping')->nullable(false);
            $table->text('shippingaddress1_read_shipping')->nullable(false);
            $table->text('shippingaddress2_read_shipping')->nullable(false);
            $table->text('shippingcity_read_shipping')->nullable(false);
            $table->text('shippingstate_read_shipping')->nullable(false);
            $table->text('shippingprovincia_read_shipping')->nullable(false);
            $table->text('shippingcountry_read_shipping')->nullable(false);
            $table->text('shippingZIP_read_shipping')->nullable(false);
            $table->text('shippingregion_read_shipping')->nullable(false);
            $table->text('courrierlink_txt_shipping')->nullable(false);
            $table->text('tracknumber_txt_shipping')->nullable(false);
            // ------------------------
            $table->text('orderitems_json_items')->nullable(false);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_order');
    }
}
