<?php

use Olmo\Core\Containers\Backoffice\Order\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/order', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the order post
Route::get('{lang}/order/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the order post
Route::put('{lang}/order/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the order post
Route::post('{lang}/order/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// nuovo ordine
Route::get('{lang}/agent/product/list', [Controller::class, 'getItemsList'])->middleware(['auth.agent']);
Route::get('{lang}/agent/variant/{id}', [Controller::class, 'getVariantList'])->middleware(['auth.agent']);

Route::post('{lang}/agent/cart', [Controller::class, 'getCart'])->middleware(['auth.agent']);
Route::post('{lang}/agent/cart/create', [Controller::class, 'createCartAgentItem'])->middleware(['auth.agent']);
Route::patch('{lang}/agent/cart/update', [Controller::class, 'UpdateProductAgentCart'])->middleware(['auth.agent']);
Route::delete('{lang}/agent/cart/delete', [Controller::class, 'DeleteProductFromAgentCart'])->middleware(['auth.agent']);
// TODO: costruisci metodo
Route::post('{lang}/agent/order/complete', [Controller::class, 'orderAgentComplete'])->middleware(['auth.agent']);
Route::post('{lang}/agent/order/sendemail', [Controller::class, 'sendEmail'])->middleware(['auth.agent']);





