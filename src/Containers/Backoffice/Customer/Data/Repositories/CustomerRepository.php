<?php

namespace App\Containers\Backoffice\Customer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CustomerRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
