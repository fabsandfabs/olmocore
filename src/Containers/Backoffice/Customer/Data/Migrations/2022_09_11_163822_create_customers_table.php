<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        HelpersMigration::Customer();
        HelpersMigration::Addresses();
        
        Schema::create('olmo_sessions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('customer_id')->nullable();
            $table->text('token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_customer');
    }
}
