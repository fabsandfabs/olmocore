<?php

use Olmo\Core\Containers\Backoffice\Customer\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/customer', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the customer post
Route::get('{lang}/customer/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the customer post
Route::put('{lang}/customer/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the customer post
Route::post('{lang}/customer/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// Route::put('en/{model}/{id_model}/address/{id_address}', [Controller::class, 'createUpdateAddressV1'])->middleware(['auth:api']);
