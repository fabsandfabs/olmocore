<?php

namespace Olmo\Core\Containers\Backoffice\Customer\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\Customer\Models\Customer;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Illuminate\Support\Facades\DB;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Customer::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Customer::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Customer::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Customer::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }             

}
