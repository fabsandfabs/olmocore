<?php

namespace Olmo\Core\Containers\Backoffice\Menu\Controllers;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersPermission;
use Olmo\Core\Containers\Backoffice\Menu\Models\Menu;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;

use Illuminate\Http\Request;

class Controller extends BackController
{

    public function getMenu(Request $request)
    {
      return HelpersPermission::getMenu($request);
    }  
    
    public function getPost(Request $request)
    {
        $attribute = Menu::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Menu::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Menu::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Menu::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }       

}