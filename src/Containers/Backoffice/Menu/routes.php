<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\Menu\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| The Backoffice Menu - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * This is the navigation menu in the backoffice
 */
Route::get('menu', [Controller::class, 'getMenu'])->middleware(['auth:api']);

// Get the list
Route::post('{lang}/menu', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the menu post
Route::get('{lang}/menu/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the menu post
Route::put('{lang}/menu/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the menu post
Route::post('{lang}/menu/create', [Controller::class, 'createPost'])->middleware(['auth:api']);