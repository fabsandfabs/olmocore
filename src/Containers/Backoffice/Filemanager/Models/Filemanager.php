<?php

namespace Olmo\Core\Containers\Backoffice\Filemanager\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Filemanager extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Filemanager';

    public static function theAttributes(){
        return [
            // Those two keys are mandatory
            'table' => 'olmo_form',
            'required' => [],
            'route' => false,
            'duplicate' => false,

            // Use this setup for multilanguage posts
            'lang'  => false,

            // Use this setup for posts with endpoint
            'requiredbackoffice' => [],

        ];
    }     
    
}
