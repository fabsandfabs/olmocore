<?php

use Olmo\Core\Containers\Backoffice\Filemanager\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('filemanager/get/folder', [Controller::class, 'getFolder'])->middleware(['auth:api']);
Route::get('filemanager/get/all', [Controller::class, 'getAllFiles'])->middleware(['auth:api']);
Route::get('filemanager/get/all/folder', [Controller::class, 'getAllFolders'])->middleware(['auth:api']);
Route::get('filemanager/clear/cache', [Controller::class, 'clearCacheFilemanager'])->middleware(['auth:api']);

// Uploader
Route::post('filemanager/uploadchunks', [Controller::class, 'uploadChunk'])->middleware(['auth:api']);
Route::post('filemanager/uploadcomplete', [Controller::class, 'uploadComplete'])->middleware(['auth:api']);

//Create folder
Route::put('filemanager/folder/create', [Controller::class, 'folderCreate'])->middleware(['auth:api']);

//Edit file
Route::patch('filemanager/file/edit/{idfile}', [Controller::class, 'fileEdit'])->middleware(['auth:api']);

//Edit folder
Route::patch('filemanager/folder/edit', [Controller::class, 'folderEdit'])->middleware(['auth:api']);

//Move file
Route::patch('filemanager/files/move', [Controller::class, 'moveFiles'])->middleware(['auth:api']); 

//Move folder
Route::patch('filemanager/folder/move', [Controller::class, 'moveFolder']);

//Delete
Route::patch('filemanager/delete', [Controller::class, 'deleteFile'])->middleware(['auth:api']);

//Search
Route::post('filemanager/search', [Controller::class, 'makeSearch'])->middleware(['auth:api']);

//Post
Route::post('filemanager/file/token', [Controller::class, 'getToken']);

Route::get('filemanager/csv/{id}', [Controller::class, 'getCsv'])->middleware(['auth:api']);
Route::get('filemanager/php/{id}', [Controller::class, 'getPhp'])->middleware(['auth:api']);

// Upload single file or multiple files not serchable
Route::post('{lang}/{pagename}/{id}/uploadchunks', [Controller::class, 'uploadChunks'])->middleware(['auth:api']);
Route::post('{lang}/{pagename}/{id}/uploadcomplete', [Controller::class, 'completeChunk'])->middleware(['auth:api']);

// Images resizing and compression
Route::get('fileversions', [Controller::class, 'fileVersions'])->middleware(['auth:api']);
Route::get('fileversion/{id}', [Controller::class, 'fileVersion'])->middleware(['auth:api']);
Route::delete('deleteversion/{id}', [Controller::class, 'deleteFileVersion'])->middleware(['auth:api']);
Route::post('filesizing/{id}', [Controller::class, 'fileResizing'])->middleware(['auth:api']);
Route::post('filesizingmassive', [Controller::class, 'fileResizingMassive'])->middleware(['auth:api']);

Route::post('defaultfilesizingmassive', [Controller::class, 'defaultfileResizingMassive'])->middleware(['auth:api']);

// bulk generation
Route::get('generatefileversion', [Controller::class, 'generateFileversionMassive']);

// route to fix storage conflicts
Route::get('checkfilemanager', [Controller::class, 'checkFilemanager']);//->middleware(['auth:api']);
