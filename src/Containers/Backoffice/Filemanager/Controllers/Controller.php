<?php

namespace Olmo\Core\Containers\Backoffice\Filemanager\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Http\Controller\StorageController;
use Olmo\Core\App\Http\Controller\UploadController;

use Olmo\Core\App\Helpers\HelpersImage;

class Controller extends BackController
{

    use StorageController, UploadController;

    public function getFolder(Request $request)
    {
        return $this->getFolderContent($request);
    }

    public function getAllFiles()
    {
        return $this->serializeAllFile();
    }

    public function getAllFolders()
    {
        return $this->serializeAllFolders();
    }

    public function clearCacheFilemanager()
    {
        return $this->clearCacheFilemanagerMethod();
    }

    public function fileEdit(Request $request)
    {
        return $this->fileEditMethod($request);
    }

    public function folderEdit(Request $request)
    {
        return $this->folderEditMethod($request);
    }

    public function moveFiles(Request $request)
    {
        return $this->moveFilesMethod($request);
    }

    public function deleteFile(Request $request)
    {
        return $this->deleteFileMethod($request);
    }

    public function uploadChunks(Request $request)
    {
        return $this->uploadChunk($request);
    }

    public function makeSearch(Request $request)
    {
        return $this->makeSearchMethod($request);
    }

    public function uploadComplete(Request $request)
    {
        return $this->completeChunkManager($request);
    }

    public function folderCreate(Request $request)
    {
        return $this->folderCreateMethod($request);
    }

    public function getToken(Request $request)
    {
        $data = [
            'token'     => md5(time() . 'tokenolmo'),
            'model'     => $request->model,
            'modelid'   => $request->modelid,
            'created_at' => date('Y-m-d H:i:s')
        ];

        return Db::table('olmo_tokens')->insert($data);
    }

    public function checkFilemanager(Request $request){
        return $this->checkFilemanagerStatus($request);
    }

    public function getCsv(Request $request)
    {
        return self::getCsvfile($request);
    }

    public function getPhp(Request $request)
    {
        return self::getPhpfile($request);
    }

    /**
     * ---------------------------
     * WORK IN PROGRESS
     * ---------------------------
     */
    public function moveFolder(Request $request)
    {
        return $this->moveFolderMethod($request);

        // $foldersrc   = 'uno/due';
        // $folderdest  = 'uno';
        // $src         = storage_path('app/public/media/' . $foldersrc);
        // $dest        = storage_path('app/public/media/' . $folderdest);

        // return Storage::move($src, $dest);
    }

    public function completeChunk(Request $request)
    {

        $chunk_id     = @$request->id;
        $filename     = @$_REQUEST['fileName'];
        $truename     = @$_REQUEST['trueName'];
        $fieldname    = @$_REQUEST['fieldName'];
        $current_data = Cache::get($filename);

        $pagename     =  $request->pagename;
        $type = pathinfo($truename, PATHINFO_EXTENSION);

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
        ];


        $path  = 'public/'.$categories_files[$type].'/'.$pagename.'/'.$filename;
        $res   = Storage::put($path, $current_data);

        $public_path = '/'.$categories_files[$type].'/'.$pagename.'/'.$truename;

        if($res){
            $res = true;
        }else{
            $res = false;
        }

        $storage_id = DB::table('olmo_storage')->insertGetId([
           'filename' 	=> $filename,
           'truename' 	=> $truename,
           'model'     => $pagename,
           'meta'     => $chunk_id.$fieldname,
           'public'    => 'true',
           'type'      => $type
        ]);


        if(strpos($fieldname,'_multimg')    !== false) {

           //storage id incrementale
           $entity     = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->first();

           if($entity){
              $storage_id = $entity->{$fieldname}.','.$storage_id;
              $storage_id = trim($storage_id,",");
           }

           $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
              $fieldname 	=> $storage_id
           ]);

        }else{
           $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
              $fieldname 	=> $storage_id
           ]);
        }

        $arr['data']        = $public_path;
        $arr['id']        = $storage_id;
        $arr['isSuccess']   = $res;
        $arr['isUpdate']   = $update;
        $arr['errorMessage'] = null;

        Cache::forget($filename);
        return $arr;

    }

    public function getFilesForBackOffice($data){
        return $this->getListVersions($data);
    }

    public function fileResizingMassive(Request $request){
        return $this->generateFileResizingMassive($request);
    }

    public function defaultfileResizingMassive(Request $request){
        return $this->generateDefaultFileResizingMassive($request);
    }

    public function fileVersion(Request $request){
        return $this->getFilesForBackOffice($request);
    }

    public function generateFileversionMassive(Request $request){
        return $this->generateFileMassive($request);
    }

    public function fileVersions(){

        $sizeOptions = explode(',', env('IMAGES_SIZES', '200x200'));
        $fitOptions = explode(',', env('IMAGES_FIT', 'clip'));
        $compressionOptions = explode(',', env('IMAGES_COMPRESSION', 75));
        $countersize = 0;

        $versionsArray = [];
        $versionsSizeArray = [];

        array_unshift($sizeOptions, 'original size');
        array_unshift($sizeOptions, 'webm');

        foreach($sizeOptions as $item){
            if($item == 'webm'){
                $obj = [
                    'checkbox' => false,
                    'compression' => ['value'=>null],
                    'fit' => ['value'=>null],
                    'id' => 'webm',
                    'size' => 'webm'
                ];
            } else {
                $obj = $this->createImageVersionObject($item, $fitOptions, $compressionOptions);
            }
            
            
            array_push($versionsArray, $obj);
        }

        return $versionsArray;
    }

    public function fileResizing(Request $request){
        return $this->singleFileCreation($request);
    }

    public function deleteFileVersion(Request $request){
        $id_version = $request->id;
        $version = DB::table('olmo_storage_versions')->where('id', $id_version)->first();
        if($version){
            $return = self::deleteSingleVersion($version);
            $id_original = $version->id_original;
            return $this->getFilesForBackOffice((object)['id' => $id_original]);
        }else{
            return false;
        }
        
    }
    
}
