<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\User\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('auth/login', [Controller::class, 'createUserSessionLogin']);

Route::post('auth/logout', [Controller::class, 'removeSessionlogin'])->middleware(['auth:api']);

Route::post('{lang}/user', [Controller::class, 'getPosts'])->middleware(['auth:api']);

Route::get('{lang}/user/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

Route::put('{lang}/user/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

Route::post('{lang}/user/create', [Controller::class, 'createPost'])->middleware(['auth:api']);