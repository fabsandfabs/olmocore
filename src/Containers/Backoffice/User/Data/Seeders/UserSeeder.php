<?php

namespace Olmo\Core\Containers\Backoffice\User\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {

        DB::table('olmo_user')->truncate();
       
		DB::table('olmo_user')->insert([
			'enabled_is_general' 	=> 'true',
			'name_txt_general'		=> 'super',
			'email_email_general' 	=> 'super@super.com',
			'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
			'confirmpassword_pwd_general' => '0cc175b9c0f1b6a831c399e269772661',
			'role_id_general'       => '1',
			'token_hidden_general'  => '1045e2086fda7ba429d6d2feb3562346',
			'avatar_filemanager_general' => '',
            'createdby_read_information' => 'seeder',
            'lastmodby_read_information'=> Carbon::now()			
		]);

		DB::table('olmo_user')->insert([
			'enabled_is_general' 	=> 'true',
			'name_txt_general'		=> 'admin',
			'email_email_general' 	=> 'admin@admin.com',
			'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
			'confirmpassword_pwd_general' => '0cc175b9c0f1b6a831c399e269772661',
			'role_id_general'       => '2',
			'token_hidden_general'  => '1045e2086fda7ba429d6d2feb3562345',
			'avatar_filemanager_general' => '',
			'createdby_read_information' => 'seeder',
            'lastmodby_read_information'=> Carbon::now()
		]);		

        DB::table('olmo_user')->insert([
       		'enabled_is_general' 	=> 'true',
			'name_txt_general'		=> 'editor',
       		'email_email_general' 	=> 'editor@editor.com',
       		'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
			'confirmpassword_pwd_general' => '0cc175b9c0f1b6a831c399e269772661',
       		'role_id_general'       => '3',
       		'token_hidden_general'  => '1045e2086fda7ba429d6d2feb3562344',
			'avatar_filemanager_general' => '',
			'createdby_read_information' => 'seeder',
            'lastmodby_read_information'=> Carbon::now()
       	]);

		DB::table('olmo_user')->insert([
			'enabled_is_general' 	=> 'true',
			'name_txt_general'		=> 'publisher',			
			'email_email_general' 	=> 'publisher@publisher.com',
			'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
			'confirmpassword_pwd_general' => '0cc175b9c0f1b6a831c399e269772661',
			'role_id_general'       => '4',
			'token_hidden_general'  => '1045e2086fda7ba429d6d2feb35623453',
			'avatar_filemanager_general' => '',
			'createdby_read_information' => 'seeder',
            'lastmodby_read_information'=> Carbon::now()
		]);	
		
		
        DB::table('olmo_role')->truncate();
       
        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'superadmin',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'admin',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'editor',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'publisher',
        ]);		

    }
}
