<?php

namespace Olmo\Core\Containers\Storage\Filemanager\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Http\Controller\StorageController;

class Controller extends BackController
{

    use StorageController;

    public function getFileManager(Request $request){

        return $this->getFileManagerMethod($request);

    }
    
    public function getFile(Request $request){

        return $this->getFileMethod($request);
        
    }

    public function getBarcode(Request $request){

        return $this->getBarcodeMethod($request);

    }

    public function updateStorageForAssets(Request $request){

        return $this->alignStorage();
        
    }    
    
    public function updateStorageForAssetsImage(Request $request){

        return $this->alignStorageImage();

    }    

    public function updateStorageForAssetsVideo(Request $request){

        return $this->alignStorageVideo();

    }    

    public function updateStorageForAssetsGif(Request $request){

        return $this->alignStorageGif();

    }    

    public function uploadStorageFromFolder(Request $request){
        
        return $this->uploadLocalFolder();
    }

}
