<?php

namespace Olmo\Core\Containers\Storage\Filemanager\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Filemanager extends OlmoMainModel
{

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Filemanager';

    public $table = 'olmo_filemanager';
    
    public static function theAttributes(){
        return [
            // Those two keys are mandatory
            'table' => 'olmo_filemanager',
            'required' => [],

            // Use this setup for multilanguage posts
            'lang'  => true,
            
            // Use this setup for posts with endpoint
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],

            // // Use this setup for posts with endpoint
            // 'rules' => [
            //     [
            //         'type' => 'unique',
            //         'field' => 'slug_txt_general',
            //         'table' => 'olmo_Testing',
            //         'errortxt' => 'Slug duplicate in page'
            //     ]               
            // ],  

            // // Use this setup for User container 
            // 'requiredbackoffice' => [
            //     'email_email_general'
            // ],
            // 'rules' => [
            //     [
            //         'type' => 'unique',
            //         'field' => 'email_email_general',
            //         'table' => 'olmo_user',
            //         'lang' => false,
            //         'errortxt' => 'Email duplicate'
            //     ],
            //     [
            //         'type' => 'equal',
            //         'field_one' => 'password_pwd_general',
            //         'field_two' => 'confirmpassword_pwd_general',
            //         'errortxt' => 'Password not match'
            //     ]                
            // ], 

            // // Use this setup for Customer container 
            // 'required' => [
            //     'customerstatus_select_general',
            //     'email_email_general',
            //     'password_pwd_general',
            //     'confirmpassword_pwd_general',
            //     'name_txt_register',
            //     'surname_txt_register',
            //     'profession_txt_register',
            //     'company_txt_register',
            //     'country_txt_register',
            //     'vat_txt_register',
            //     'privacy_is_register',
            //     'terms_is_register'
            // ],
            // 'requiredbackoffice' => [
            //     'email_email_general'
            // ],
            // 'forms' => [
            //     'register' => [
            //          'groups' => ['register','email_email_general','password_pwd_general','confirmpassword_pwd_general'],
            //          'exclude' => [],
            //          'submit'  => true
            //     ],
            //     'profile' => [
            //         'groups' => ['register','profile'],
            //         'exclude' => ['privacy_is_register','terms_is_register'],
            //         'submit'  => true
            //     ]
            // ],
            // 'rules' => [
            //     [
            //         'type' => 'unique',
            //         'field' => 'email_email_general',
            //         'table' => 'olmo_customer',
            //         'lang' => false,
            //         'errortxt' => 'Email duplicate'
            //     ],
            //     [
            //         'type' => 'equal',
            //         'field_one' => 'password_pwd_general',
            //         'field_two' => 'confirmpassword_pwd_general',
            //         'errortxt' => 'Password not match'
            //     ]                
            // ],
            // 'cart' =>[
            //     'discountglobalfield' => 'vat_txt_register'
            // ],                   

        ];
    }    
}
