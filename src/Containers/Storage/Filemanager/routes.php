<?php

use Olmo\Core\Containers\Storage\Filemanager\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('media/{any}', [Controller::class, 'getFileManager'])->where('any', '.*');
Route::get('documents/{any}', [Controller::class, 'getFile'])->where('any', '.*');

Route::get('public-link', function () {
    Artisan::call('storage:link');
    return 'done';
});

// Route::get('{path}/{folder}/{truename}', [Controller::class, 'getFile']);
Route::middleware(['auth.front'])->group(function () {

    Route::get('barcode/{code}', [Controller::class, 'getBarcode']);

    // align old storage to new olmo
    Route::get('updatestorage', [Controller::class, 'updateStorageForAssets']);
    Route::get('updatestorage/image', [Controller::class, 'updateStorageForAssetsImage']);
    Route::get('updatestorage/video', [Controller::class, 'updateStorageForAssetsVideo']);
    Route::get('updatestorage/gif', [Controller::class, 'updateStorageForAssetsGif']);
    // import automatically folder image
    // functional only in local
    Route::get('uploadfolder', [Controller::class, 'uploadStorageFromFolder']);

});