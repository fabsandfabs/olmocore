<?php

namespace Olmo\Core\Containers\Frontend\Base\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Olmo\Core\App\Http\Controller\FrontController;
use Olmo\Core\App\Helpers\HelpersRequestMedia;
use Olmo\Core\App\Helpers\HelpersFilter;
use Olmo\Core\App\Helpers\HelpersService;
use Olmo\Core\App\Helpers\HelpersMenu;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersSerializer;
use App\Containers\Frontend\Quantity\Controllers\Controller as Quantity;

class Controller extends FrontController
{
    public function requestAssets(Request $request)
    {
        return HelpersRequestMedia::requestAssets($request);
    }

    public function requestCode(Request $request)
    {
        return HelpersRequestMedia::requestCode($request);
    }

    public function filters(Request $request)
    {
        return HelpersFilter::filter($request);
    }

    public function filtersCache(Request $request)
    {
        return HelpersFilter::filter($request);
    }

    public function getServiceEndpoint(Request $request)
    {
        return HelpersService::endpointReply($request);
    }

    public function navigationMenu(Request $request)
    {
        return HelpersMenu::getNavigationMenu($request);
    }

    public function getImagesitemap(Request $request)
    {
        $lang = $request->lang;

        $data = DB::table('olmo_sitemapimages')->where('lang', $lang)->get();

        $map = [];

        foreach($data as $d){
            $storage = storage_path('app/'.$d->path);
            $json = json_decode(file_get_contents($storage), true);
            array_push($map, $json);
        }

        return [
            "map" => $map,
            "data" => $data
        ];
    }

    public function autocompleteSearch(Request $request)
    {
        $lang = $request->lang;
        $table = $request->input('table');
        $field = $request->input('field');

        $field = $field ? $field : "label_txt_general";

        $model = ('App\Containers\Backoffice\\'.ucfirst($table).'\Models\\'.ucfirst($table))::theAttributes();

        $language = $model['lang'];

        if($language){
            $data = Db::table('olmo_'.$table)->where('locale_hidden_general', $lang)->select($field)->get();
        } else {
            $data = Db::table('olmo_'.$table)->select($field)->get();
        }

        $arrayData = [];
        foreach($data as $value){
            array_push($arrayData, $value->{$field});
        }
        array_values($arrayData);

        return $arrayData;
    }

    public function itemVersion($items, $props)
    {
        $product = [];
        $singleProperty = [];
        $propertyIds = [];
        $productSelected = [];
        $count = count($props);

        foreach($items as $item) {
            $versions = HelpersSerializer::routeResponse($item);

            foreach($versions['property'] as $prop){
                if(!in_array($prop, $singleProperty)){
                    array_push($singleProperty, $prop);
                }
            }
        }
        foreach($items as $item) {
            $version = HelpersSerializer::routeResponse($item);

            $getIn = 0;
            foreach($version['property'] as $property){
                if(in_array($property['id'], $props)){
                    $getIn++;
                }
            }
            if($count == $getIn){
                array_push($productSelected, $version);
            }
        }

        $item = $productSelected[0];
        $item['property'] = $singleProperty;
        return $item;
    }

    public function getQuantity(Request $request)
    {
        $id = $request->id;
        $lang = $request->lang;
        $type = $request->type;

        if($type == 'item'){
            $product = DB::table('olmo_productitem')->where('locale_hidden_general', $lang)->where('id', $id)->first();
            if(env('CHECK_QUANTITY_CUSTOM', false)){
                $qty = Quantity::getQuantity($product);
            } else {
                $qty = DB::table('olmo_quantity')->where('prod_id', $id)->first();
            }
        } else {
            $product = DB::table('olmo_product')->where('locale_hidden_general', $lang)->where('id', $id)->first();
            if(env('CHECK_QUANTITY_CUSTOM', false)){
                $qty = Quantity::getQuantity($product);
            } else {
                $qty = DB::table('olmo_quantity')->where('prod_id', $id)->first();
            }
        }

        if($qty){
            $qty = json_decode(json_encode($qty, true), true);
            $response = [
                'item' => $product,
                'available' => intVal($qty['quantity']) > 0 ? true : false,
                'quantity' => intVal($qty['quantity']),
            ];
    
            return response($response, 200);
        }
        return response([], 400);

    }

    public function getQuantityOld(Request $request)
    {
        $model = $request->input('model');
        $property = $request->input('property');
        $id = $request->id;
        $lang = $request->lang;

        $defaultLang = HelpersLang::getDefaultLang();

        if($defaultLang != $lang){
            $product = DB::table('olmo_'.$model)->where('locale_hidden_general', $lang)->where('id', $id)->first();
            $product = DB::table('olmo_'.$model)->where('id', $product->parentid_hidden_general)->first();
            $id = $product->id;

            /** Convert properties */
            $propArray = [];
            foreach($property as $p){
                $currentproperty = DB::table('olmo_propertyitem')->where('id', $p['value'])->first();
                $dafaultproperty = DB::table('olmo_propertyitem')->where('id', $currentproperty->parentid_hidden_general)->first();
                $val = [];
                $val['value'] = $dafaultproperty->id;
                array_push($propArray, $val);
            }
            $property = $propArray;

        } else {
            $product = Db::table('olmo_'.$model)->where('locale_hidden_general', $lang)->where('id', $id)->first();
        }

        if(isset($product->qty_spin_general)){
            if($product->id){

            }
        } else {
            // $item = Db::table('olmo_'.$model.'item')->where('locale_hidden_general', $lang)->where('id', $id)->first();

            $props = [];
            foreach($property as $prop){
                array_push($props, $prop['value']);
            }

            if(count($props) > 0){
                $filter  = '';
                $regex   = "'";
                foreach($props as $value){
                    $regex .= ','.$value.',|';
                }
                $regex = substr($regex, 0, -1)."'";
                $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex ";

                // $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND postid_hidden_general = '$product->id'";
                $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND locale_hidden_general = '$defaultLang' AND postid_hidden_general = $id";
                $version = DB::select($sql);

                $version = self::itemVersion($version, $props);

                if(isset($version['qty'])){
                    return $version['qty'];
                }
            }

        }

    }

    public function getItemToUpdateInfo(Request $request)
    {
        $property = $request->input('property');
        $mode = $request->input('mode');
        $lang = $request->lang;
        $id = $request->id;

        // $defaultLang = HelpersLang::getDefaultLang();

        $product = Db::table('olmo_product')->where('locale_hidden_general', $lang)->where('id', $id)->first();
        // if($defaultLang != $lang){
        //     $product = Db::table('olmo_product')->where('id', $product->parentid_hidden_general)->first();
        // }

        $props = [];
        foreach($property as $prop){
            array_push($props, $prop['value']);
        }

        if(count($props) > 0){
            $filter  = '';
            $regex   = "'";
            if($mode == "item-property"){

                foreach($props as $value){
                    $regex .= ','.$value.',|';
                }
                $regex = substr($regex, 0, -1)."'";
                $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex ";

                $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND locale_hidden_general = '$lang' AND postid_hidden_general = '$id'";

                $version = DB::select($sql);
                $version = self::itemVersion($version, $props);
                $version['cartbutton'] = false;
                return $version;
            } else {
                foreach($props as $value){
                    $regex .= ','.$value.'';
                }
                $regex = $regex."'";
                $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex ";

                $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND postid_hidden_general = '$product->id'";
                $version = DB::select($sql);

                $version = HelpersSerializer::routeResponse($version[0]);
                $version['cartbutton'] = $version['qty'] > 0 ? true : false;
                return $version;
            }
        }

        return response(404);

    }

    public function getOgImg(Request $request){

        $ogimg = Db::table('olmo_storage_versions')->where('truename', 'LIKE', 'olmosetting/ogimg/ogimg.webp')->first();
        $dir = '';
        if($ogimg){
            $dir = 'assets/'.$ogimg->dirname;
        }else{
            $ogimg = Db::table('olmo_storage')->where('truename', 'LIKE', 'olmosetting/ogimg/ogimg.j%')->first();
            if($ogimg){
                $dir = 'assets/'.$ogimg->truename;
            }
        }
        return $dir;
    }

    public function getStrings(Request $request)
    {
        $lang = $request->lang;

        $valind = Helperslang::validLang($lang);

        if(!isset($valind[$lang])){
            return response('no valid lang', 404);
        }

        $data = Db::table('olmo_storage')->where('truename', 'olmosetting/strings/translations.csv')->first();

        if($data){
            $path = $data->truename;
            $filename = $data->filename;
            $arrayFile = explode('/', $path);
            $arrayFile[count(explode('/', $path)) - 1] = $filename;
            $file = storage_path('app/public/media/'.implode('/', $arrayFile));
            $csv = file_get_contents($file);
            $array = array_map("str_getcsv", explode("\n", $csv));

            $lanfIndex = 0;
            $header = $array[0];

            foreach($header as $key=>$item) {
                if($item == $lang){
                    $lanfIndex = $key;
                }
            }

            $translationRoute = [];
            foreach($array as $item) {
                if(isset($item[$lanfIndex])) {
                    $translationRoute[$item[0]] = $item[$lanfIndex];
                }
            }

            return response($translationRoute, 200);

        }

        return response('no file found', 404);
    }

    public function getImagesitemapfileDelete(Request $request)
    {
        $id = $request->id;
        $data = DB::table('olmo_sitemapimages')->where('id', $id)->first();
        $storage = Storage::disk('local')->delete($data->path);

        if($storage){
            DB::table('olmo_sitemapimages')->where('id', $id)->delete();
        }

        return true;
    }

}
