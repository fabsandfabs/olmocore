<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Frontend\Base\Controllers\Controller;

Route::middleware(['auth.front'])->group(function () {
    /** Autocomplete */
    Route::post('{lang}/autocomplete', [Controller::class, 'autocompleteSearch']);

    /** Menu */
    Route::get('{lang}/navigationmenu/{name}', [Controller::class, 'navigationMenu']);

    // Route::get('{lang}/allcategories', [Controller::class, 'getAllcategories']);    
    Route::post('{lang}/allmodel/{model}', [Controller::class, 'getAllPosts']);
    Route::get('structure', [Controller::class, 'getStructure'])->middleware(['auth.multistructure']);
    Route::get('sitemap/{lang}', [Controller::class, 'getSitemap']);
    Route::get('robots.txt', [Controller::class, 'getRobots']);
    Route::get('string-translations/{lang}', [Controller::class, 'getStrings']);
    Route::get('getogimg', [Controller::class, 'getOgImg']);

    // Sitemap images
    Route::get('imagesitemap/{lang}', [Controller::class, 'getImagesitemap']);

    /** Fitelrs */
    Route::post('{lang}/filters/{model}', [Controller::class, 'filters']);

    /** Quantity */
    Route::get('{lang}/{type}/quantity/{id}', [Controller::class, 'getQuantity']);

    /** Item */
    Route::post('{lang}/versionitem/getitem/{id}', [Controller::class, 'getItemToUpdateInfo']);

    /** Base */
    Route::get('{lang}/{category?}/{subcategory?}/{slug?}', [Controller::class, 'getPage']);
});

/** Get a record in the DB **/
Route::post('{lang}/service-endpint/{model}/{id}', [Controller::class, 'getServiceEndpoint']);
/** Request assets **/
Route::post('request/assets', [Controller::class, 'requestAssets']);
Route::post('request/code', [Controller::class, 'requestCode']);

Route::fallback(function () {
    return view("404");
});