<?php

namespace Olmo\Core\Containers\Frontend\Auth\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

use Olmo\Core\App\Http\Controller\FrontController;
use Olmo\Core\App\Helpers\HelpersRequestMedia;
use Olmo\Core\App\Helpers\HelpersFilter;
use Olmo\Core\App\Helpers\HelpersService;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersData;

use Olmo\Core\Containers\Backoffice\Customer\Models\Customer;
use Olmo\Forms\App\Http\Controller\FormController;
use Olmo\Core\App\Helpers\HelpersRulesField;

class Controller extends FrontController
{
    public function register(Request $request)
    {

        $err = [];
        $valid = true;
        $code = 200;
        $request->id = '0';
        $status = 'notactive';
        $attribute = Customer::theAttributes();
        $rules = $attribute['rulesform'];
        $level_registration = env('CUSTOMER_REGISTRATION_LEVEL');
        $locale = $request->current_locale;
        $token = null;
        $send = null;
        $customer = null;

        $rules = HelpersRulesField::checkRules($request, $attribute, 'front');
        if (count($rules) > 0) {
            $err = $rules;
            $valid = false;
            $code = 400;
        } else {

            /** Check the email */
            $email = $request->input('email');
            $customerCheck = Db::table($attribute['table'])->where('email_email_general', $email)->first();

            if ($customerCheck) {
                $err = ['message' => 'User already exist'];
                $valid = false;
                $code = 409;
            } else {

                /**
                 * Get the current columns in the requested table
                 */
                $columns = Schema::getColumnListing('olmo_customer');

                /** Flip values in the keys */
                $columns_flip = array_flip($columns);

                $user = $request->getContent();
                $user = json_decode($user, true);
                $userkey = array_keys($user, true);
                $locale = isset($user['locale']) ? $user['locale'] : HelpersLang::getDefaultLang();

                /** Set every value to empty string with exceptions */
                foreach ($columns_flip as $key => $value) {
                    $label = HelpersData::setLabel($key);
                    if (HelpersData::getType($key) == 'number') {
                        $columns_flip[$key] = 0;
                    } else if (in_array($label, $userkey)) {
                        $columns_flip[$key] = $user[$label];
                    } else {
                        $columns_flip[$key] = '';
                    }            
                }

                unset($columns_flip['id']);              

                /** Insert the user in the DB */
                $columns_flip['create_read_information'] = Carbon::now();
                $columns_flip['createdby_read_information'] = 'user';
                $columns_flip['lastmod_read_information'] = Carbon::now();
                $columns_flip['currenttimezone_read_general'] = Carbon::now();
                $columns_flip['lastmodby_read_information'] = 'user';
                $columns_flip['customerlang_txt_general'] = $locale;
                $columns_flip['enabled_is_general'] = 'true';
                $columns_flip['name_txt_general'] = $user['email'];
                $columns_flip['tokenactive_read_general'] = '';
                $columns_flip['endsession_read_general'] = '';
                $columns_flip['currentlocale_read_general'] = $locale;

                if ($level_registration == 0) {
                    $status = 'active';
                    $columns_flip['customerstatus_select_general'] = $status;                      
                }                           

                if ($level_registration == 2) {
                    $status = 'notactive';
                    $columns_flip['customerstatus_select_general'] = $status;                      
                }                           

                $insertCustomer = Db::table($attribute['table'])->insertGetId($columns_flip);
                if($insertCustomer){
                    $customer = Db::table($attribute['table'])->where('id', $insertCustomer)->first();
                    if(!$customer){
                        return response(['Customer not found'], 400);
                    }
                } else {
                    return response([
                        'message' => 'Customer not created', 
                        'object' => $columns_flip, 
                        'attribute' => $attribute
                    ], 400);
                }

                if ($level_registration == 0) {
                    $token = HelpersCustomer::createCustomerToken($customer);
                }

                $customer_id = $customer->id;

                unset($customer->id);

                if (($level_registration == 0 or $level_registration == 1 or $level_registration == 2) && $customer) {
                    $params = [];
                    if ($locale == null) {
                        $defaultlang = HelpersLang::getDefaultLang();
                        $locale = $defaultlang;
                    }
                    if ($level_registration == 0) {
                        $params['a'] = $customer->email_email_general;
                    }
                    if ($level_registration == 2) {
                        $token = md5(Carbon::now());
                        $body = [
                            'token' => $token,
                            'model' => 'customer',
                            'modelid' => $insertCustomer,
                        ];
                        Db::table('olmo_tokens')->insertGetId($body);
                        $params['activationtoken'] = $token;
                    }
                    $params['locale'] = $locale;
                    $forms = Db::table('olmo_form')->get();

                    if ($level_registration == 0) {
                        foreach ($forms as $form) {
                            $ids = explode(',', $form->formtype_multid_general);
                            if (in_array('8', $ids)) {
                                $formid = $form->id;
                                $params['id'] = $formid;
                            }
                        }
                    }

                    if ($level_registration == 2) {
                        foreach ($forms as $form) {
                            $ids = explode(',', $form->formtype_multid_general);
                            if (in_array('9', $ids)) {
                                $formid = $form->id;
                                $params['id'] = $formid;
                            }
                        }
                    }

                    if (isset($params['id'])) {
                        $customer = HelpersSerializer::routeResponse($customer);
                        $data = array_merge($params, $customer);
                        $send = FormController::sendingFormData($data, 'internal');
                    }
                }
            }
        }

        $response = [];
        $response['res'] = $valid;
        $response['user'] = $customer ? HelpersSerializer::routeResponse($customer) : [];
        $response['user']['id'] = $customer ? $customer_id : [];
        $response['user']['token'] = $token;
        $response['user']['status'] = $status;
        $response['err'] = $err;
        $response['mail'] = $send;

        return response($response, $code);
    }



    public function login(Request $request)
    {

        $username = $request->email;
        $password = $request->password;
        $user = Customer::where('email_email_general', $username)->where('password_pwd_general', $password)->first();
        $userlog = [];
        $statusCode = 200;

        if ($user && $user->customerstatus_select_general !== 'active') {

            $userlog['user'] = null;
            $statusCode = 401;
            $userlog['error'] = [
                "err_code" => 2,
                "err_text" => "User not active"
            ];
        } else if (!$user) {

            $userlog['user'] = null;
            $statusCode = 404;
            $userlog['error'] = [
                "err_code" => 1,
                "err_text" => "User not exist"
            ];
        } else {

            $token = HelpersCustomer::createCustomerToken($user); 

            /**
             * Add the real name of a country insted of the code
             */
            if(isset($user->country_txt_profile)){
                if($user->country_txt_profile != ''){
                    $getCountry = Db::table('olmo_country')->where('name_txt_general', $user->country_txt_profile)->first();
                    if($getCountry){
                        $user->countrytitle_txt_profile = $getCountry->title_txt_general;
                    }
                }
            }

            $user['token'] = $token;
            $user['cart'] = [];
            // $user['discount'] = ApiCart::getDiscountGlobal($user);
            // $user             = ApiHelper::FrontResponseStrict($user);
            $userlog['user'] = HelpersSerializer::cleanKeyObj($user->toArray());
            $userlog['user_id'] = $user->id;
            $userlog['error'] = [];
        }

        return response($userlog, $statusCode);
    }

    public function logoutAuth()
    {        
        $response = HelpersCustomer::endCustomerSession();
        return $response;
    }

    public function profileCustomer(Request $request)
    {

        $attribute = Customer::theAttributes();
        $content = $request->getContent();
        $content = json_decode($content, true);
        $rules = $attribute['rules'];
        $required = $attribute['required'];
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing('olmo_customer');
        $tableMapCustomer = [];
        foreach ($columns as $column) {
            $field = HelpersData::setLabel($column);
            if ($field == isset($content[$field])) {
                $tableMapCustomer[$column] = $content[$field];
            }
        }
        if (isset($tableMapCustomer['name_txt_general'])) {
            unset($tableMapCustomer['name_txt_general']);
        }

        Db::table('olmo_customer')->where('id', $customer['id'])->update($tableMapCustomer);
        $customer = Db::table('olmo_customer')->where('id', $customer['id'])->first();

        $response = [];
        $code = 200;
        $response['user'] = [];
        // $response['buildRequest'] = $buildRequest->all();
        // $response['request'] = $request->all();

        if ($customer) {
            // $updatedUser = $this->update($buildRequest, $attribute);
            $response['user'] = HelpersSerializer::routeResponse($customer);
            $xtoken = $request->header('x-token');
            $response['user']['token'] = $xtoken;
        } else {
            $response['err'] = ['User not found'];
            $code = 400;
        }

        return response($response, $code);
    }

    public function profileResetPwd(Request $request)
    {

        $customer = HelpersCustomer::getActiveToken($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $code = 200;
        $newpassword = $request->input('password');

        $update = [
            'password_pwd_general' => $newpassword,
            'confirmpassword_pwd_general' => $newpassword
        ];

        $updated = Customer::where('id', $customer['id'])->update($update);
        if ($updated) {
            HelpersCustomer::resetCustomerToken($customer, $request);
        }

        $params = [];
        $locale = $customer['customerlang_txt_general'];
        if ($locale == null) {
            $defaultlang = HelpersLang::getDefaultLang();
            $locale = $defaultlang;
        }
        $params['a'] = $customer['email_email_general'];
        $params['locale'] = $locale;
        $forms = DB::table('olmo_form')->get();

        foreach ($forms as $form) {
            $ids = explode(',', $form->formtype_multid_general);
            if (in_array('13', $ids)) {
                $formid = $form->id;
                $params['id'] = $formid;
            }
        }
        $response['mail'] = FormController::sendingFormData($params, 'internal');
        $response['status'] = 'ok';
        $response['user'] = $updated ? true : false;


        return response($response, $code);
    }

    public function profileChangePwd(Request $request)
    {

        $user = HelpersCustomer::getCustomer($request);   
        if (!$user) {
            $xtoken = $request->header('x-token');
            return response(['User Session not found', $xtoken], 403);
        }
        $code = 200;
        $password = $request->input('password');
        $newpassword = $request->input('newpassword');
        $attribute = Customer::theAttributes();
        $customer = DB::table($attribute['table'])->where('id', $user['id'])->where('password_pwd_general', $password)->first();

        if (!$customer) {
            $code = 404;
            $response['user'] = ["The password doesn't match"];
        } else {
            $update = [
                'password_pwd_general' => $newpassword,
                'confirmpassword_pwd_general' => $newpassword
            ];

            $updated = Customer::where('id', $user['id'])->update($update);

            $params = [];
            $locale = $customer->customerlang_txt_general;
            if ($locale == null) {
                $defaultlang = HelpersLang::getDefaultLang();
                $locale = $defaultlang;
            }
            $params['a'] = $customer->email_email_general;
            $params['locale'] = $locale;
            $forms = DB::table('olmo_form')->get();

            foreach ($forms as $form) {
                $ids = explode(',', $form->formtype_multid_general);
                if (in_array('13', $ids)) {
                    $formid = $form->id;
                    $params['id'] = $formid;
                }
            }
            $response['mail'] = FormController::sendingFormData($params, 'internal');
            $response['status'] = 'ok';
            $response['user'] = $updated ? true : false;
        }

        return response($response, $code);
    }


    public function profileActivate(Request $request)
    {
        $token = $request->input('token');
        if($token){
            $userToken = Db::table('olmo_tokens')->where('token', $token)->first();
            if(!$userToken){
                return response(['This token is not valid, We are Sorry'], 404);
            }
            $response = HelpersCustomer::changeStatus($userToken->modelid, 'active');
            
            $customer = Db::table('olmo_customer')->where('id', $userToken->modelid)->first();

            $forms = DB::table('olmo_form')->get();

            foreach ($forms as $form) {
                $ids = explode(',', $form->formtype_multid_general);
                if (in_array('8', $ids)) {
                    $formid = $form->id;
                    $params['id'] = $formid;
                    $params['email'] = $customer->email_email_general;
                    $params['locale'] = $customer->currentlocale_read_general;
                }
            }

            if (isset($params['id'])) {
                $customer = HelpersSerializer::routeResponse($customer);
                $data = array_merge($params, $customer);                
                FormController::sendingFormData($data, 'internal');
            }                        

            $userToken = Db::table('olmo_tokens')->where('token', $token)->delete();            

            $userlog['user'] = HelpersSerializer::cleanKeyObj($customer);
            $userlog['error'] = [];            

            return response($userlog, 200);
        }



        $email = $request->input('email');
        if (!$email) {
            return response('Email not present in the body', 400);
        }

        $user = Db::table('olmo_customer')->where('email_email_general', $email)->first();
        if (!$user)
            $response = 400;
        else {
            $response = HelpersCustomer::changeStatus($user->id, 'active');
        }

        return response($response);
    }

    public function checkSession(Request $request)
    {
        $user = HelpersCustomer::getCustomer($request);

        $code = 200;
        if (!$user) {
            $txt = ['User Session not found'];
            $code = 404;
        } else {
            $txt = ['User Session active'];
            $code = 200;
            if ($user['customerstatus_select_general'] != 'active') {
                $txt = ['User Session disabled'];
                $code = 400;
            }
        }

        return response($txt, $code);
    }

    public function profilePasswordRecovery(Request $request)
    {

        $code = 200;
        $response = [];
        $email = $request->input('email');
        $reseturl = $request->input('reseturl');
        $customer = DB::table('olmo_customer')->where('email_email_general', $email)->first();
        $formid = null;

        if (!$customer) {
            $code = 400;
            $response['status'] = "User didn't find";
        } else {

            $params = [];
            $token = HelpersCustomer::createCustomerToken($customer);
            $locale = $customer->customerlang_txt_general;

            if ($locale == null) {
                $defaultlang = HelpersLang::getDefaultLang();
                $locale = $defaultlang;
            }

            $params['email'] = $email;
            $params['reseturl'] = $reseturl . $token;
            $params['locale'] = $locale;
            $params['a'] = $email;
            $forms = DB::table('olmo_form')->get();

            foreach ($forms as $form) {
                $ids = explode(',', $form->formtype_multid_general);
                if (in_array('12', $ids)) {
                    $formid = $form->id;
                    $params['id'] = $formid;
                }
            }
            if ($formid) {
                
                $res['mail'] = FormController::sendingFormData($params, 'internal');

            }
            $res['status'] = 'ok';
        }

        return response($res, $code);
    }

    public function getForms(Request $request)
    {

        $type = $request->type;
        $getType = Db::table('olmo_formtype')->where('name_txt_general', $type)->first();
        $response = null;

        if($getType){
            $sql = "SELECT * FROM olmo_form WHERE CONCAT(',',formtype_multid_general,',') REGEXP ',$getType->id,' LIMIT 1";
            $response = DB::select($sql);

            if($response){
                $id = $response[0]->id;
                $data = Db::table('olmo_form')->where('id', $id)->first();
        
                if ($data) {
                    foreach ($data as $key => $value) {
                        $type = HelpersData::getType($key);
                        if ($type == 'formfield') {
                            $fields = FormController::getFields($value);
                        }
                    }
        
                    $form = json_decode(json_encode($data), true);
                    $form = HelpersSerializer::cleanKeyObj($form);
                    $form['fields'] = HelpersSerializer::cleanKeyArray($fields);
        
                    // $form = HelpersSerializer::routeResponse();
        
                    return response($form, 200);
                }
            }            
        }    

        return abort(404);
    }

    public function addAdress(Request $request)
    {

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        if(is_array($customer)){
            $customerid = $customer['id'];
        }else{
            $customerid = $customer;
        }
        $attribute['table'] = 'olmo_customeritem';
        $type = $request->input('_type')[0];

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing($attribute['table']);

        /** Flip values in the keys */
        $columns_flip = array_flip($columns);

        $address = $request->getContent();
        $address = json_decode($address, true);
        $addresskey = array_keys($address, true);

        /** Set every value to empty string with exceptions */
        foreach ($columns_flip as $key => $value) {
            $label = HelpersData::setLabel($key);
            if (HelpersData::getType($key) == 'number') {
                $columns_flip[$key] = 0;
            } else if (in_array($label, $addresskey)) {
                $columns_flip[$key] = $request[$label];
            } else {
                $columns_flip[$key] = '';
            }            
        }

        unset($columns_flip['id']);

        /** Insert the user in the DB */
        $columns_flip['enabled_is_general'] = 'true';
        $columns_flip['default_is_address'] = 'false';
        $columns_flip['trash_hidden_general'] = '';
        $columns_flip['create_read_information'] = Carbon::now();
        $columns_flip['createdby_read_information'] = is_array($customer) ? $customer['email_email_general'] : 'guest';
        $columns_flip['lastmod_read_information'] = Carbon::now();
        $columns_flip['lastmodby_read_information'] = '';
        $columns_flip['typeaddress_select_address'] = $type;
        $columns_flip['customer_hidden_content'] = $customerid;
        $columns_flip['locale_hidden_content'] = HelpersLang::getDefaultLang();
        $columns_flip['parentid_hidden_content'] = '';

        $insertAddress = Db::table($attribute['table'])->insertGetId($columns_flip);
        
        if(is_array($customer)){
            $customerAddresse = $customer['customeritem_list_general'].','.$insertAddress;
        } else {
            $customeradd = Db::table('olmo_customer')->where('xguest_read_general', $customerid)->first();
            $customeradd = json_decode(json_encode($customeradd, true), true);
            $customerAddresse = $customeradd['customeritem_list_general'].','.$insertAddress;
        }
        if(is_array($customer)){
            Db::table('olmo_customer')->where('id', $customerid)->update(['customeritem_list_general' => $customerAddresse]);
        } else {
            Db::table('olmo_customer')->where('xguest_read_general', $customerid)->update(['customeritem_list_general' => $customerAddresse]);
        }

        $response = Db::table($attribute['table'])->where('id', $insertAddress)->first();

        return $response;
    }

    public function setDefaultAddress(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $customerid = $customer['id'];
        $id = $request->input('id');
        $default  = $request->input('_default');

        Db::table('olmo_customeritem')->where('customer_hidden_content', $customerid)->where('typeaddress_select_address', $default[0])->update([
            'default_is_address' => 'false'
        ]);

        Db::table('olmo_customeritem')->where('id', $id)->where('customer_hidden_content', $customerid)->update([
            'default_is_address' => 'true'
        ]);

        return response(200);
    }

    public function editAdress(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $customerid = $customer['id'];

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing('olmo_customeritem');

        /** Flip values in the keys */
        $columns_flip = array_flip($columns);

        $address = $request->getContent();
        $address = json_decode($address, true);
        $addresskey = array_keys($address, true);

        /** Set every value to empty string with exceptions */
        foreach ($columns_flip as $key => $value) {
            $label = HelpersData::setLabel($key);
            if (HelpersData::getType($key) == 'number') {
                $columns_flip[$key] = 0;
            } else if (in_array($label, $addresskey)) {
                $columns_flip[$key] = $request[$label];
            } else {
                $columns_flip[$key] = '';
            }            
        }

        unset($columns_flip['id']);
        unset($columns_flip['_type']);
        unset($columns_flip['_default']);
        unset($columns_flip['typeaddress_select_address']);
        unset($columns_flip['parentid_hidden_content']);
        unset($columns_flip['customer_hidden_content']);
        unset($columns_flip['trash_hidden_general']);
        unset($columns_flip['enabled_is_general']);
        unset($columns_flip['default_is_address']);

        $columns_flip['create_read_information'] = Carbon::now();
        $columns_flip['createdby_read_information'] = $customer['email_email_general'];
        $columns_flip['lastmod_read_information'] = Carbon::now();
        $columns_flip['lastmodby_read_information'] = $customer['email_email_general'];
        $columns_flip['locale_hidden_content'] = HelpersLang::getDefaultLang();    
        
        $address = Db::table('olmo_customeritem')->where('id', $request->input('id'))->where('customer_hidden_content', $customerid)->update($columns_flip);
        $address = Db::table('olmo_customeritem')->where('id', $request->input('id'))->first();
        
        return $address;
    }

    public function deleteAdress(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        if(is_array($customer)){
            $customerid = $customer['id'];
        } else {
            $customerid = $customer;
        }         
        $id = $request->input('id');

        $response = Db::table('olmo_customeritem')->where('id', $id)->where('customer_hidden_content', $customerid)->update(['enabled_is_general' => 'false']);
        
        return response($response);;
    }

    public function getListAddress(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        if(is_array($customer)){
            $customerid = $customer['id'];
        } else {
            $customerid = $customer;
            $customer = Db::table('olmo_customer')->where('xguest_read_general', $customerid)->first();
            $customer = json_decode(json_encode($customer, true), true);
        }        

        $addressesid = explode(',', $customer['customeritem_list_general']);
        $addresses = [];

        if ($customerid) {
            foreach ($addressesid as $addressid) {
                $address = DB::table('olmo_customeritem')->where('id', $addressid)->where('enabled_is_general', 'true')->first();

                /**
                 * Add the real name of a country insted of the code
                 */
                if(isset($address->country_txt_address)){
                    if($address->country_txt_address != ''){
                        $getCountry = Db::table('olmo_country')->where('name_txt_general', $address->country_txt_address)->first();
                        $address->countrytitle_txt_address = $getCountry->title_txt_general;
                    }
                }

                if($address){
                    array_push($addresses, (array)$address);
                }
            }

            $addresses = HelpersSerializer::cleanKeyArray($addresses);

            if(!$addresses){
                return response([], 200);
            }

            $response = array_map(function (array $arr) {
                $arr['_type'] = [$arr['typeaddress']];
                $arr['_default'] = $arr['default'] == "true" ? [$arr['typeaddress']] : [];
                return $arr;
            }, $addresses);

            return response($response, 200);
        } else {
            return  response(400);
        }
    }

    public function createGuestCustomer(Request $request) {

        $lang = $request->lang;
        $guestToken = $request->input('x-guest');
        $email = $request->input('email');

        if($email == ''){
            return response(400);
        }

        $columns = [
            'name_txt_general' => $email,
            'email_email_general' => $email,
            'customerstatus_select_general' => 'active',
            'xguest_read_general' => $guestToken,
            'enabled_is_general' => 'true',
            'position_ord_general' => 0,
            'createdby_read_information' => 'guest',
            'lastmodby_read_information' => '',
            'type_read_general' => 'guest',
            'token_read_general' => '',
            'tokenactive_read_general' => '',
            'endsession_read_general' => '',
            'currentlocale_read_general' => $lang,
            'currenttimezone_read_general' => '',
            'password_pwd_general' => '',
            'confirmpassword_pwd_general' => '',
            'customerlang_txt_general' => $lang,
            'privacy_is_general' => 'true',
            'terms_is_general' => 'true',
            'customeritem_list_general' => '',
            'name_txt_profile' => '',
            'surname_txt_profile' => '',
            'company_txt_profile' => '',
            'country_txt_profile' => '',
            'vat_txt_profile' => '',
        ];

        $response = Db::table('olmo_customer')->insertGetId($columns);

        if($response){
            return response(['email' => $email],200);
        }
        return response([], 400);


    }
}
