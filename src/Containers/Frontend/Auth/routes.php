<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Frontend\Auth\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('auth/login', [Controller::class, 'login']);

Route::post('auth/register', [Controller::class, 'register']);

Route::post('auth/logout', [Controller::class, 'logoutAuth']);


/** Profile access */

Route::post('auth/profile', [Controller::class, 'profileCustomer']);

Route::post('auth/activate', [Controller::class, 'profileActivate']);


/** Reset passowrd */

Route::post('auth/password-recovery', [Controller::class, 'profilePasswordRecovery']);

Route::post('auth/password-reset', [Controller::class, 'profileResetPwd']);

Route::post('auth/password-change', [Controller::class, 'profileChangePwd']);


/** Check Session */

Route::post('auth/checksession', [Controller::class, 'checkSession']);


/** Auth Forms */

Route::get('auth/forms/{lang}/{type}', [Controller::class, 'getForms']);
Route::get('forms/{lang}/address/{type}', [Controller::class, 'getForms']);

/** Address */

Route::get('/address', [Controller::class, 'getListAddress']);

Route::post('/address', [Controller::class, 'addAdress']);

Route::patch('/address', [Controller::class, 'editAdress']);

Route::delete('/address', [Controller::class, 'deleteAdress']);

Route::post('/address/setdefault', [Controller::class, 'setDefaultAddress']);

/** Address */

Route::post('{lang}/createguestcustomer', [Controller::class, 'createGuestCustomer']);