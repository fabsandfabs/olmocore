<?php

namespace Olmo\Core\Generator\Commands;

use Olmo\Core\Generator\GeneratorCommand;
use Olmo\Core\App\Helpers\HelpersForm;
use Olmo\Core\Generator\Interfaces\ComponentsGenerator;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\DB;

// namespace App\Console\Commands;

use Illuminate\Console\Command;

class FormGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'olmo:generate:form';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create default form instance';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $forms = HelpersForm::defaultForms();

        $name = $this->ask('Write the name of the form');
        $count_name_form = Db::table('olmo_form')->where('name_txt_general', 'LIKE', $name."%%")->get()->count();
        
        if($count_name_form != 0){
            $count_name_form += 1;
            $name = $name.'-'.$count_name_form;
            $this->info("Duplicated name, the new is: $name\n");
        }
        $wannaLoad = true;
        $seeding = [];
        do {
            $this->info("I'm trying to load the json seeder for this form...\npath -> app/Settings/Form/Seeding/$name.json");
            if( $seeding = HelpersForm::jsonSeeding($name)){
                $this->info("Found!");
                break;
            }else{
                $wannaLoad = boolval($this->confirm("I can't find the file...do you want to upload it?"));
            }
        } while ($wannaLoad);

        $type = $this->choice('What type of form do you need to generate?', array_keys($forms), 0);

        if(!array_key_exists($type, $forms)){
            $this->info("Type -> $type not found");
            return 0;
        }
        
        $fields = $forms[$type]['fields'];
        $fieldsId = [];
        $form_id = self::createEmptyForm($name);

        $this->withProgressBar($forms[$type]['rows'], function ($row) use ($fields, &$fieldsId, $form_id, $seeding) {
            $insert_elements = [];
            foreach($fields as $key => $field){
                $insert_elements[$field] = $row[$key];
            }
            $insert_elements['postid_hidden_general'] = $form_id;
            $insert_elements = array_merge( self::emptyFields(), $insert_elements );

            $field_id = Db::table('olmo_formfield')->insertGetId($insert_elements);
            if(!empty($seeding)){
                $field_name = $insert_elements['name_txt_general'];
                if(isset($seeding[$field_name])){
                    $options = $seeding[$field_name];
                    $option_ids = [];
                    foreach($options as $option){
                        $option['fieldid_hidden_general'] = $field_id;
                        $option_ids[] = Db::table('olmo_formfieldoptions')->insertGetId($option);
                    }
                    Db::table('olmo_formfield')->where('id', $field_id)->update(['option_optionfield_general' => implode(',', $option_ids)]);
                } 
            }
            $fieldsId[] = $field_id;
            // $this->info($row);
        });

        Db::table('olmo_form')->where('id', $form_id)->update(['formfield_formfield_fields' => implode(',', $fieldsId)]);
        return 0;
    }

    public static function createEmptyForm($name){
        return Db::table('olmo_form')->insertGetId([
            'enabled_is_general' => 'true',
            'name_txt_general' => $name,
            'formtype_multid_general' => '15,16',
            'createdby_read_information' => 'Olmo',
            'lastmodby_read_information' => 'Olmo',
            'mailchimp_multid_general' => '',
            'smtp_id_general' => '',
	        'a_txt_setting' => '',	
            'cc_txt_setting' => '',	
            'bcc_txt_setting' => '',
            'replyto_txt_setting' => '',
            'subject_txt_setting' => '',
            'subjecttemplate_filemanager_setting' => '',	
            'subjectautoresponse_txt_setting' => '',
            'subjectautoresponsetemplate_filemanager_setting' => '',
            'strings_filemanager_setting' => '',	
            'formfield_formfield_fields' => '',
            'csvfield_csvfield_csv' => '',
            'trash_hidden_general' => ''
        ]);
    }

    public static function emptyFields(){
        return [
            'label_txt_general' => '',
            'default_txt_general' => '',	
            'placeholder_txt_general' => '',
            'class_txt_general' => '',
            'attribute_txt_general' => '',	
            'dependentfield_select_general' => '',
            'dependentaction_select_general' => '',
            'dependenttrigger_txt_general' => '',
            'option_optionfield_general' => '',
            'filesize_txt_general' => ''
        ];
    }
}
