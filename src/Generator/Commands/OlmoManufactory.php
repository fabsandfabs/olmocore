<?php

namespace Olmo\Core\Generator\Commands;

use Olmo\Core\Generator\GeneratorCommand;
use Olmo\Core\App\Helpers\HelpersOlmo as Olmo;
use Olmo\Core\Generator\Interfaces\ComponentsGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Filesystem\Filesystem as IlluminateFilesystem;

class OlmoManufactory extends Command
{
    /**
     * The options which can be passed to the command. All options are optional. You do not need to pass the
     * "--container" and "--file" options, as they are globally handled. Just use the options which are specific to
     * this generator.
     *
     * @var  array
     */
    public $inputs = [
        ['ui', null, InputOption::VALUE_OPTIONAL, 'The user-interface to generate the Controller for.'],
        ['stub', null, InputOption::VALUE_OPTIONAL, 'The stub file to load for this generator.'],
        ['file', null, InputOption::VALUE_OPTIONAL, 'The name of the file'],
    ];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'olmo:generate:filemanager';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the olmo filemanager structure';


    public function __construct(IlluminateFilesystem $fileSystem)
    {
        parent::__construct();

        $this->fileSystem = $fileSystem;
    }
    /**
     * @void
     *
     * @throws GeneratorErrorException|FileNotFoundException
     */
    public function handle()
    {
        $this->info(json_encode(Olmo::createFolderStructure(),true));
    }
}
