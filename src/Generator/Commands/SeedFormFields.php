<?php

namespace Olmo\Core\Generator\Commands;

use Olmo\Core\Generator\GeneratorCommand;
use Olmo\Core\App\Helpers\HelpersForm;
use Olmo\Core\Generator\Interfaces\ComponentsGenerator;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\DB;

// namespace App\Console\Commands;

use Illuminate\Console\Command;

class SeedFormFields extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'olmo:seed:form';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeding data for the specified form';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask('Write the name of the form');
        $form = Db::table('olmo_form')->where('name_txt_general', '=', $name)->first();
        $seeding = [];
        $searching = true;
        if($form){
            do {
                $this->info("I'm trying to load the json seeder for this form...\npath -> app/Settings/Form/Seeding/$name.json");
                if( $seeding = HelpersForm::jsonSeeding($name)){
                    $this->info("Found!\n");
                    break;
                }
                sleep(5);
            } while ($searching);
        }else{
            $this->info("Form $name not found!");
            die;
        }

        $fields = Db::table('olmo_formfield')->where('postid_hidden_general', '=', $form->id)->get();
        if(!empty($seeding)){
            $this->withProgressBar($fields, function ($field) use ($seeding) { // TODO: é comoda per ciclare ma non da output concreto
                $field_name = $field->name_txt_general;
                $field_id = $field->id;
                if(isset($seeding[$field_name])){
                    $options = $seeding[$field_name];
                    $option_ids = [];
                    foreach($options as $option){
                        $option['fieldid_hidden_general'] = $field_id;
                        $option_ids[] = Db::table('olmo_formfieldoptions')->insertGetId($option);
                    }
                    Db::table('olmo_formfield')->where('id', $field_id)->update(['option_optionfield_general' => implode(',', $option_ids)]);
                    $this->info("\n$field_name seeded!");
                } 
            });
        }else{
            $this->info("\nThe file is empty!");
            die;
        }
    } 
    
}
