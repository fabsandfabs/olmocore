<?php

namespace Olmo\Core\Providers;

use Olmo\Core\Foundation\Olmo;
use Olmo\Core\Foundation\OlmoDefault;
use Illuminate\Support\ServiceProvider as LaravelAppServiceProvider;

class OlmoProvider extends LaravelAppServiceProvider
{

    public function boot(): void
    {

    }

    public function register(): void
    {
        parent::register();
        
        // Register Core Facade Classes, should not be registered in the $aliases property, since they are used
        // by the auto-loading scripts, before the $aliases property is executed.
        $this->app->alias(Olmo::class, 'Olmo');
        $this->app->alias(OlmoDefault::class, 'OlmoDefault');
    }

}
