<?php

namespace Olmo\Core\Loaders;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class OlmoLoadersContainers
{

    private const CONTAINERS_DIRECTORY_NAME = 'Containers';  

    public static function getAllContainerPaths(): array
    {        
        $sectionNames = self::getSectionNames();        
        $containerPaths = [];
        foreach ($sectionNames as $name) {
            $sectionContainerPaths = self::getSectionContainerPaths($name);
            foreach ($sectionContainerPaths as $containerPath) {
                $containerPaths[] = $containerPath;
            }
        }
        
        return $containerPaths;
    }    
    
    public static function getSectionNames(): array
    {
        $sectionNames = [];        
        foreach (self::getSectionPaths() as $sectionPath) {
            $sectionNames[] = basename($sectionPath);
        }
    
        return $sectionNames;
    }    
    
    public static function getSectionPaths(): array
    {                
        return File::directories(app_path(self::CONTAINERS_DIRECTORY_NAME));
    }
    
    public static function getSectionContainerPaths(string $sectionName): array
    {        
        return File::directories(app_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . $sectionName));
    }


    // Below is the default migration tacking action, 
    // user cannot takes control of this cause is the minimum set up to run the Backoffice

    public static function getAllDefaultContainerPaths(): array
    {        
        $sectionNames = self::getDefaultSectionNames();        
        $containerPaths = [];
        foreach ($sectionNames as $name) {
            $sectionContainerPaths = self::getDefaultSectionContainerPaths($name);
            foreach ($sectionContainerPaths as $containerPath) {
                $containerPaths[] = $containerPath;
            }
        }
        
        return $containerPaths;
    }    
    
    public static function getDefaultSectionNames(): array
    {
        $sectionNames = [];        
        foreach (self::getDefaultSectionPaths() as $sectionPath) {
            $sectionNames[] = basename($sectionPath);
        }
    
        return $sectionNames;
    }    
    
    public static function getDefaultSectionPaths(): array
    {                
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME);
    }
    
    public static function getDefaultSectionContainerPaths(string $sectionName): array
    {        
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . $sectionName);
    }    

}